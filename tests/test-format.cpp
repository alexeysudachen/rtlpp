
#include <nano++/format.hxx>
#include <nano++/string.hxx>
#include <nano++/datetime.hxx>
#include <nano++/sys/cmdline.hxx>

using namespace nano;

void af(Array<String> const& a)
{
	Cerr | _S*"%T" % a;
}

void bf(Array<int> const& a)
{
	Cerr | _S*"%T" % a;
}

template < class T > void print(T const& l)
{
	String Q;
	int len = 0;
	T const* q = l.Glue(len);
	for (int i = 0; q ; q = q->next, ++i)
	{
		Q += _S*"(%?=>%?)" % q->t % q->q;
	}
	Cerr | Q;
}

CXX_NO_INLINE void myff()
{
	char a[128] = {0};
	time_t t = time(0);
	Cerr | _S*"%#04x %$x" % 0 % 0;
	a | _S(127)*"hello world, %d, %s" % t % ctime(&t);
	Cerr || a;
}

int main(int argc, char** argv)
{
	Cmdline->Init(argc, argv);
	//myff();
	char a[128] = {0};
	char* aa = a;
	time_t t = time(0);
	Array<char> I = (_L, 'a', 'B', 'c', 'D', 'e', 'F');
	Array<int>  II = Array<int>((_L, 0x30, 0x31, 0x32, 0x33));

	//Cerr|_S*"%#04x %$x" %0 %0;
	a | _S*"hello world, %d, %s" % t % ctime(&t);
	//aa|_S*"hello world, %d, %s" %t%ctime(&t);
	String Q = _S * L"%15s" % a;
	Chars  c = "Chars value";
	Chars  uc = String(L"Chars value unicode");
	Cerr | c;
	Cerr | upper(uc);
	Cerr || a;
	//(char*)a|_S*"-hello world, %d, %s" %t%ctime(&t);
	//Cerr||a;
	Cerr | "^^hello " + _S*"%?" % "world" + "!";
	Cerr | "^^hello " + _S / 1 + "!";

	Cerr | Q;
	Cerr | _S*"? = %?" % Q;
	Cerr | _S*"T = %T" % Q;
	Cerr | _S& Q;
	Cerr | _S*"%B"  % I;
	Cerr | _S*"%3@" % I;
	Cerr | _S*"%$@" % I;
	Cerr | _S*"%+@" % I;
	Cerr | _S*"I: %?" % I;
	Cerr | _S*"II:%@" % II;
	Cerr | _S*"%+@" % II;

	Cerr | _S / (_L, "мама", "мыла", "раму");
	//bf((_L,3,15,126,17));

	//print(_L("1",1)("02",2)("03",3));
	//print(_L("1","_1")("02","_2")("03","_3xxx"));

	Cerr | _S / (_L, 3, 15, 126, 17);
	Cerr | "_L = " + _S / _L("1", 1)("02", 2)("03", 3);
	Cerr | "_L = " + _S / _L("1", "_1")("02", "_2")("03", "_3xxx");
	Cerr | "I = " + _S / I;
	Cerr | "a = " + _S / a;
	Cerr | "Q = " + Q;
	Cerr | "10 = " + _S / 10;

	Cerr | _S& date_Of * 24 - 12 - 2010;
	Cerr | _S& date_Of * 12 / 24 / 2010;

	DateTime dt = DateTime::now;
	Cerr | _S& dt;
	DateTime dt1 = date_Of * 13 - 11 - 1976;
	Cerr | _S& dt1;

	Cerr | _S*"%d,%d,%f,%f" % 1 % -1 % 1. % -1.;
	Cerr | _S*"%.3f,%.3f" % 1.005f % (1.005f * 1000);
	Cerr | _S*"%.3f,%.3f" % -1.005f % (-1.005f * 1000);
}

