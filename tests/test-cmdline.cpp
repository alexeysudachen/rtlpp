
#include <nano++/sys/cmdline.hxx>
#include <nano++/sys/cpu/info.hxx>

using namespace nano;

int main(int argc, char** argv)
{
	Cmdline->Init(argc, argv, "h|?|help,o:,bits:");
	Cerr | _S*"h: %?" % Cmdline->HasOpt("h");
	Cerr | _S*"o: %?" % Cmdline->First("o");
	Cerr | _S*"bits: %?" % Cmdline->First("bits");
	Cerr | FormatCPUInfo();
	Cerr | _S / Cmdline;
	return 0;
}
