
#include "../nano++/tuple.hxx"
#include "../nano++/array.hxx"
#include "../nano++/hashtable.hxx"
#include "../nano++/crypto/md.hxx"
#include <gtest/gtest.h>

using namespace nano;
enum { TABLE_COUNT = 10000, AUTO_REHASH = 1 };

template < class Key_P, class Value_P, bool auto_rehash_P = AUTO_REHASH > struct Params
{
	typedef Key_P Key;
	typedef Value_P Value;
	enum { auto_rehash = auto_rehash_P };
};

template < class T > T generate_unique(int i);

template<> int generate_unique<int>(int i)
{
	return i;
}

template<> void* generate_unique<void*>(int i)
{
	return (void*)(i^~longptr_t(0));
}

template<> String generate_unique<String>(int i)
{
	return hex_encode(md5_digest(&i,4));
}

template < class Params > struct Fixture : testing::Test
{
	typedef typename Params::Key Key;
	typedef typename Params::Value Value;

	typedef Array< Pair< Key, Value > > DataSet;
	DataSet dataset_;
	typedef Hashtable< Key, Value, Params::auto_rehash> Table;

	virtual void SetUp()
	{
		DataSet().Swap(dataset_);

		for (int i = 0; i < TABLE_COUNT; ++i)
		{
			Key key = generate_unique<Key>(i);
			Value val = generate_unique<Value>(i);
			dataset_.Push(pair(key, val));
		}
	}

	virtual void TearDown()
	{
		DataSet().Swap(dataset_);
	}
};

TYPED_TEST_CASE_P(Fixture);

TYPED_TEST_P(Fixture, Put)
{
	Fixture::Table htable;
	EXPECT_TRUE(htable.Count() == 0);
	auto dataset = shuffle(dataset_);
	for (auto i = dataset.Begin(); i != dataset.End(); ++i)
	{
		EXPECT_TRUE(htable.Find(i->_1) == 0);
		htable.Put(i->_1, i->_2);
		EXPECT_TRUE(htable.Find(i->_1) != 0);
		EXPECT_TRUE(htable.Get(i->_1) == i->_2);
	}
	EXPECT_TRUE(htable.Count() == dataset.Count());
}

TYPED_TEST_P(Fixture, Del)
{
	Fixture::Table htable;
	EXPECT_TRUE(htable.Count() == 0);
	auto dataset = shuffle(dataset_);
	for (auto i = dataset.Begin(); i != dataset.End(); ++i)
	{
		EXPECT_TRUE(htable.Find(i->_1) == 0);
		htable.Put(i->_1, i->_2);
		EXPECT_TRUE(htable.Find(i->_1) != 0);
		EXPECT_TRUE(htable.Get(i->_1) == i->_2);
	}
	EXPECT_TRUE(htable.Count() == dataset.Count());
	dataset.Shuffle();
	for (auto i = dataset.Begin(); i != dataset.End(); ++i)
	{
		EXPECT_TRUE(htable.Find(i->_1) != 0);
		htable.Del(i->_1);
		EXPECT_TRUE(htable.Find(i->_1) == 0);
	}
}

REGISTER_TYPED_TEST_CASE_P(Fixture, Put, Del);
typedef testing::Types < Params<String, int>,
        Params<String, void*>,
        Params<String, String>
        > StringKeyCases;
typedef testing::Types < Params<int, int>,
        Params<int, void*>,
        Params<int, String>
        > IntKeyCases;

INSTANTIATE_TYPED_TEST_CASE_P(WithIntKey, Fixture, IntKeyCases);
INSTANTIATE_TYPED_TEST_CASE_P(WithStringKey, Fixture, StringKeyCases);

int main(int argc, char** argv)
{
	printf("Running main() from gtest_main.cc\n");
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
