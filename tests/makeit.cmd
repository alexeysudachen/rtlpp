
@echo off
cd %~dp0

set BUILDER = 
set GOAL=ALL
set DEBUG=YES
set CPU=X86
set ANALYZE=NO
set VSVER=10
set BUILDER_ARCH=

:next
if .%1. == .10.      set VSVER=10
if .%1. == .13.      set VSVER=13
if .%1. == .X86.     set CPU=X86
if .%1. == .X86.     set BUILDER_ARCH= 
if .%1. == .X64.     set CPU=X64
if .%1. == .X64.     set BUILDER_ARCH=_amd64
if .%1. == .release. set DEBUG=NO
if .%1. == .debug.   set DEBUG=YES
if .%1. == .clean.   set GOAL=clean
if .%1. == .rebuild. set GOAL=clean ALL
if .%1. == .analyze. set ANALYZE=YES
if .%1. == .. goto :build
shift
goto :next

:build
python Make.rtlpp.files.py 
vs%VSVER%%BUILDER_ARCH% nmake CPU=%CPU% DEBUG=%DEBUG% ANALYZE=%ANALYZE% %GOAL%
goto :eof




