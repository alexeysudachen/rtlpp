
#include "../nano++/sys/winlpc.hxx"
#include "../nano++/sys/thread.hxx"
#include <gtest/gtest.h>

using namespace nano;

const String default_port_name = "123456";

struct LpcTest: testing::Test
{
	struct Packet : SelfPtr<Packet>
	{
		u64_t counter;
		u32_t crc32;
		Digest128 md5;
		byte_t data[1024];
	};

	struct ServerThread: ThreadObject
	{
		String portname;

		ServerThread(Strarg portname) : portname(portname)
		{
		}

		void Run()
		{
			u64_t counter = 0;
			LpcPort lpc_port = lpc_bind(portname);
			while ( !ThreadIsStopping() )
			{
				LpcWorker worker = lpc_port->Accept(100^milliseconds);
				while ( worker->Listen(1^seconds) )
				{
					Packet* packet = (Packet*)worker->Payload().Begin();
					EXPECT_TRUE( worker->Payload().Count() == sizeof(*packet) );
					EXPECT_TRUE( counter < packet->counter );
					counter = packet->counter;
					++packet->counter;

					EXPECT_TRUE( hashcode_crc32(0,packet->data,sizeof(packet->data)) == packet->crc32 );
					EXPECT_TRUE( md5_digest(packet->data) == packet->md5 );

					for ( int i = 0, j = sizeof(packet->data); i < j/2;  ++i )
						swap(packet->data[i],packet->data[j-i-1]);

					packet->crc32 = hashcode_crc32(0,packet->data,sizeof(packet->data));
					packet->md5 = md5_digest(packet->data);

					worker->Reply(sizeof(*packet));
				}
			}
		}
	};

	Thread server_thread;

	virtual void SetUp() 
	{
		server_thread = rcc_ptr(new ServerThread(default_port_name));
		server_thread->Start();
		relax(100^milliseconds);
	}

	virtual void TearDown()
	{
		if ( server_thread ) 
			server_thread->StopAndWait();
	}
};

TEST_F(LpcTest, PingPong)
{
	LpcClient lpcc = lpc_connect(default_port_name);
	int counter = 1;
	for ( int i = 0; i < 100; ++i )
	{
		LpcTest::Packet packet;
		packet->counter = ++counter;

		soft_random_bytes(packet->data,sizeof(packet->data));
		
		packet->crc32 = hashcode_crc32(0,packet->data,sizeof(packet->data));
		packet->md5   = md5_digest(packet->data);

		EXPECT_TRUE( lpcc->Request(byte_range((byte_t*)&packet,sizeof(packet))) );

		LpcTest::Packet *packet2 = (LpcTest::Packet*)lpcc->Payload().Begin();
		EXPECT_TRUE( lpcc->Payload().Count() == sizeof(packet) );
		EXPECT_TRUE( counter == packet->counter );
		EXPECT_TRUE( hashcode_crc32(0,packet->data,sizeof(packet->data)) == packet->crc32 );
		EXPECT_TRUE( md5_digest(packet->data) == packet->md5 );
	}
}

int main(int argc, char** argv)
{
	printf("Running main() from gtest_main.cc\n");
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
