
#include "../nano++/dicto.hxx"
using namespace nano;

int main()
{
	Dicto<char> d;
	d.Put("Q",'a');
	Cerr|_S*"'Q' => %?"%d.Get("Q");
	Cerr|_S*"'Z' => %?"%((const Dicto<char>&)d).Get("Z",0);
	
	Dicto<int> a = _L("Q",1)("Z",2);
	Cerr|_S*"'Q' => %?"%a.Get("Q");
	Cerr|_S*"'Z' => %?"%a.Get("Z",0);
	Cerr|_S*"'E' => %?"%a.Get("E",0);
}

