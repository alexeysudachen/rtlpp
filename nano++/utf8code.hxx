
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

namespace nano
{
	struct Utf8Encoder
	{
		static CXX_NO_INLINE int Length(char c)
		{
			static char length[256] =
			{
				/* Map UTF-8 encoded prefix byte to sequence length.  zero means
				   illegal prefix.  see RFC 2279 for details */
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
				2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
				3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 0, 0
			};
			return length[((byte_t)c)];
		}

		static CXX_NO_INLINE wchar_t Decode(void const* p, int* cnt)
		{
			unsigned char const* text = (unsigned char const*)p;
			int c = -1;
			int c0 = *text++;
			if (cnt) ++*cnt;
			if (c0 < 0x80)
				c = (wchar_t)c0;
			else
			{
				int c1 = 0;
				int c2 = 0;
				int c3 = 0;
				int l = Length(c0);
				switch (l)
				{
					case 2:
						if ((c1 = *text) > 0)
							c = ((c0 & 0x1f) << 6) + (c1 & 0x3f);
						if (cnt) ++*cnt;
						break;
					case 3:
						if ((c1 = *text) > 0 && (c2 = text[1]) > 0)
							c = ((c0 & 0x0f) << 12) + ((c1 & 0x3f) << 6) + (c2 & 0x3f);
						if (cnt) *cnt += 2;
						break;
					case 4: // hm, UCS4 ????
						if ((c1 = *text) > 0 && (c2 = text[1]) > 0 && (c3 = text[2]) > 0)
							c = ((c0 & 0x7) << 18) + ((c1 & 0x3f) << 12) + ((c2 & 0x3f) << 6) + (c3 & 0x3f);
						if (cnt) *cnt += 3;
						break;
					default:
						break;
				}
			}
			return c;
		}

		static CXX_NO_INLINE char* Encode(void* _bf, wchar_t c, int* cnt)
		{
			char* bf = (char*)_bf;
			int l = 0;
			if (c < 0x80)
			{
				*bf++ = (char)c;
				l = 1;
			}
			else if (c < 0x0800)
			{
				*bf++ = (char)(0xc0 | (c >> 6));
				*bf++ = (char)(0x80 | (c & 0x3f));
				l = 2;
			}
			else
			{
				*bf++ = (char)(0xe0 | (c >> 12));
				*bf++ = (char)(0x80 | ((c >> 6) & 0x3f));
				*bf++ = (char)(0x80 | (c & 0x3f));
				l = 3;
			}
			if (cnt) *cnt += l;
			return bf;
		}
	};

}

