
/*

 (C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization of the copyright holder.

 */

#pragma once
#include "require.hxx"
#include "array.hxx"
#include "xptnfo.hxx"
#include "hashcode.hxx"

namespace nano
{
	struct hashtable_base_t
	{
		typedef void (*kv_free_t)(longptr_t* key, longptr_t* value);
		typedef void (*kv_copy_t)(longptr_t* key_copy, longptr_t key, longptr_t* value_copy, longptr_t value);
		typedef bool (*is_equal_t)(longptr_t key1, longptr_t key2);
		typedef u32_t(*hash_of_t)(longptr_t key);

		struct record_t
		{
			record_t* next;
			longptr_t key;
			longptr_t value;
		};

		struct header_t
		{
			kv_free_t  kv_free;
			kv_copy_t  kv_copy;
			is_equal_t is_equal;
			hash_of_t  hash_of;
			int count;
			AlignedPool<record_t, 1024> record_pool;
		};

		mutable ExtendedArray<record_t*, header_t> table;
		hashtable_base_t(int mod, kv_free_t kvf, kv_copy_t kvc, is_equal_t ise, hash_of_t hof)
			: table(mod)
		{
			header_t* h = table.Extra();
			h->count = 0;
			h->hash_of = hof;
			h->is_equal = ise;
			h->kv_copy = kvc;
			h->kv_free = kvf;
		}

		~hashtable_base_t()
		{
			if (table.RefCount() == 1)
				Clear();
		}

		void Clear()
		{
			header_t* h = table.Extra();
			for (int i = 0, j = table.Count(); i < j; ++i)
			{
				record_t** nrec = &table[i];
				record_t* rec;
				while (!!(rec = *nrec))
				{
					*nrec = rec->next;
					h->kv_free(&rec->key, &rec->value);
					rec->next = 0;
					h->record_pool.Free(rec);
					--h->count;
				}
			}
			STRICT_REQUIRE(h->count == 0);
		}

		CXX_NO_INLINE record_t** Backet(longptr_t key) const
		{
			header_t* h = table.Extra();
			u32_t hashcode = h->hash_of(key);
			record_t** nrec = &table[hashcode % table.Count()];

			while (*nrec)
			{
				if (h->is_equal(key, (*nrec)->key))
					break;
				nrec = &(*nrec)->next;
			}

			return nrec;
		}

		CXX_NO_INLINE longptr_t* Put(longptr_t key, longptr_t value)
		{
			header_t* h = table.Extra();
			record_t** nrec = Backet(key);
			record_t* rec = *nrec;

			if (rec)
			{
				longptr_t oldval = rec->value;
				h->kv_copy(0, 0, &rec->value, value);
				h->kv_free(0, &oldval);
			}
			else
			{
				rec = (record_t*)h->record_pool.Malloc();
				memset(rec, 0, sizeof(*rec));
				h->kv_copy(&rec->key, key, &rec->value, value);
				*nrec = rec;
				++h->count;
			}

			return &rec->value;
		}

		CXX_NO_INLINE longptr_t* Get(longptr_t key) const
		{
			record_t** nrec = Backet(key);
			if (*nrec)
				return &(*nrec)->value;
			return 0;
		}

		CXX_NO_INLINE void Del(longptr_t key)
		{
			header_t* h = table.Extra();
			record_t** nrec = Backet(key);
			if (record_t* rec = *nrec)
			{
				*nrec = rec->next;
				rec->next = 0;
				h->kv_free(&rec->key, &rec->value);
				h->record_pool.Free(rec);
				--h->count;
			}
		}

		CXX_NO_INLINE void Rehash(int newmod)
		{
			if (newmod == table.Count()) return;

			record_t* acc = 0;
			for (int i = 0, iE = table.Count(); i < iE; ++i)
			{
				record_t** p = &table[i];
				while (*p)
				{
					record_t* foo = *p;
					*p = foo->next;
					foo->next = acc;
					acc = foo;
				}
			}
			table.Resize(newmod);
			while (acc)
			{
				record_t* p = acc;
				acc = p->next;
				record_t** nrec = Backet(p->key);
				p->next = *nrec;
				*nrec = p;
			}
		}
	};

	template < int cookie > CXX_NO_INLINE
	int new_hashmap_mod(int elements_count)
	{
		static int modules[] =
		{
			29, 59, 127, 257, 521, 1049, 2099, 4201, 8419, 16843, 33703, 67409,
			134837, 269683, 539389, 1078787, 2157587, 4315183, 8630387, 17260781,
			34521589, 69043189, 138086407, 276172823, 552345671, 1104691373
		};

		for (int i = 0; i < count(modules); ++i)
		{
			if (elements_count <= modules[i])
				return modules[i];
		}

		return modules[count(modules) - 1];
	}

	template < class Key, class Value, bool auto_rehash = false >
	struct Hashtable : private hashtable_base_t
	{
	private:
		struct key_element : LongptrElement<Key> {};
		struct val_element : LongptrElement<Value> {};

		static void kv_free(longptr_t* key, longptr_t* val)
		{
			if (key) key_element::Kill(*key);
			if (val) val_element::Kill(*val);
		}
		static void kv_copy(longptr_t* key, longptr_t key_value, longptr_t* val, longptr_t val_value)
		{
			if (key) key_element::Creat(*key, key_value);
			if (val) val_element::Creat(*val, val_value);
		}

	public:
		Hashtable(int avg_count = 29)
			: hashtable_base_t(new_hashmap_mod<0>(avg_count),
			                   &kv_free, &kv_copy,
			                   &key_element::Ise, &key_element::Hof)
		{}

		Value* Put(Key const& key, Value const& val = Value())
		{
			Value* r = val_element::Discover(
			               hashtable_base_t::Put(key_element::Cover(key),
			                                     val_element::Cover(val)));
			if (auto_rehash)
				Rehash();

			return r;
		}

		Value* Find(Key const& key)
		{
			return val_element::Discover(
			           hashtable_base_t::Get(key_element::Cover(key)));
		}

		Value const* Find(Key const& key) const
		{
			return val_element::Discover(
			           hashtable_base_t::Get(key_element::Cover(key)));
		}

		Value const& Get(Key const& key) const
		{
			const Value* found = Find(key);
			if (!found)
			{
				Chars m = _S * "doesn't have element with key %?" % key;
				OCCURED(NoElement, m);
				BUGTRAP(+m);
			}
			return *found;
		}

		Value const& Get(Key const& key, Value const& dflt) const
		{
			Value const* found = Find(key);
			if ( found )
				return *found;
			return dflt;
		}

		void Del(Key const& key)
		{
			return hashtable_base_t::Del(key_element::Cover(key));
		}

		int Count() const
		{
			return hashtable_base_t::table->count;
		}

		bool Empty() const
		{
			return !Count();
		}

		operator bool() const
		{
			return !!Count();
		}

		void Rehash()
		{
			int elements_count = hashtable_base_t::table->count;
			if (elements_count > 2 * hashtable_base_t::table.Count())
				hashtable_base_t::Rehash(new_hashmap_mod<0>(elements_count));
		}

		void Swap(Hashtable &ht)
		{
			hashtable_base_t::table.Swap(((hashtable_base_t&)ht).table);
		}

		using hashtable_base_t::Clear;

		typedef void (Hashtable<Key,Value,auto_rehash>::*BoolType) ();
		operator BoolType () const
		{
			return Empty() ? 0 : &Rehash;
		}

	};

}
