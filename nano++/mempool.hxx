
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "atomic.hxx"
#include "memory.hxx"

namespace nano
{

	enum { 
		SHARED_POOL_PAGE_SIZE = 8 * 1024, 
		SHARED_POOL_ELMSIZE_MAX = 256,
	};

	struct PoolPageHdr 
	{ 
		AtomicNext* volatile *chunk_list; 
		u32_t signature; 
	};

    inline void _atomic_shared_free(void* p, AtomicNext* volatile *chunk_list)
	{
		AtomicNext* q = ptr_cast(p);
		q->next = 0;
		atomic_push(chunk_list, q);
	}

	inline void shared_pool_free(void* p)
	{
		AtomicNext* q = ptr_cast(p);
		PoolPageHdr *hdr = (PoolPageHdr *)(longptr_t(p)&~(SHARED_POOL_PAGE_SIZE-1));
		REQUIRE( hdr->signature == CXX_FOUR_CHARS('P','O','O','L') );
		q->next = 0;
		atomic_push(hdr->chunk_list, q);
	}

	template < size_t elm_size, int cookie = 0 >
	struct AtomicRawPool
	{
		enum { CHUNK_PAGE_SIZE = SHARED_POOL_PAGE_SIZE };
		enum { CHUNK_PER_PAGE  = CHUNK_PAGE_SIZE / elm_size };

		NANO_STATIC_CHECK(elm_size >= sizeof(void*));

		static AtomicNext* volatile chunk_list;
		static GigaPageBlock page_block;

		static CXX_NO_INLINE void Extend()
		{
			union Q { AtomicNext a; byte_t d[elm_size]; };
			static int clk = 0;
			Q* q = 0;
			int first = 1;
			__xchg_lockon__(&clk)
			{
				if (chunk_list) return;
				q = (Q*)mem_chk_giga_malloc(&page_block, CHUNK_PAGE_SIZE, __FILE__, __LINE__);
				PoolPageHdr *hdr = (PoolPageHdr*)q; 
				hdr->chunk_list = &chunk_list;
				hdr->signature = CXX_FOUR_CHARS('P','O','O','L');
				if ( elm_size < sizeof(void*)*2 ) ++first;
				atomic_push(&chunk_list, &(q + first)->a);
			}
			for (int i = first+1; i < CHUNK_PER_PAGE; ++i)
			{
				atomic_push(&chunk_list, &(q + i)->a);
			}
		};

		static inline void* Malloc()
		{
			AtomicNext* q;
			do
			{
				q = atomic_pop(&chunk_list);
				if (!q)
					Extend();
			}
			while (!q);
			return q;
		}

		static inline void Free(void* p)
		{
			shared_pool_free(p);
		}
	};

	template < size_t N, int Q> AtomicNext* volatile AtomicRawPool<N, Q>::chunk_list = 0;
	template < size_t N, int Q> GigaPageBlock AtomicRawPool<N, Q>::page_block = {0,};

	template < class T, int cookie = 0 >
	struct SharedPool : AtomicRawPool< aligned_sizeof<T>::Value, cookie  >
	{};

	struct SimplePool : Uncopiable
	{
		int refcount;
		int left;
		const int page_size;
		const int elm_size; // aligned
		void** chunk_list;
		void** page_list;
		GigaPageBlock page_block;

		struct ptr_pair { void* ptrs[2]; };

		SimplePool(size_t elm_size, size_t page_size = 4096)
			: elm_size(elm_size), page_size(page_size), chunk_list(0), page_list(0), left(0)
		{
			REQUIRE(elm_size>=sizeof(void*));
			memset(&page_block,0,sizeof(page_block));
		}

		~SimplePool()
		{
			while (page_list)
			{
				void** p = page_list;
				page_list = (void**)*p;
				mem_giga_free(p[1], CXX_FILE_LINE);
				SharedPool<ptr_pair>::Free(p);
			}
		}

		CXX_NO_INLINE void Extend()
		{
			REQUIRE(left == 0);
			const size_t per_page = page_size / elm_size;
			void** q = 0, **p = 0;
			q = (void**)mem_chk_giga_malloc(&page_block,page_size, CXX_FILE_LINE);
			p = (void**)SharedPool<ptr_pair>::Malloc();
			p[0] = page_list;
			p[1] = q;
			page_list = p;
			left = per_page;
		};

		inline void* Malloc()
		{
			void* q;

			if (!chunk_list && !left)
			{
				Extend();
			}

			if (chunk_list)
			{
				q = chunk_list;
				chunk_list = (*(void***)q);
			}
			else
			{
				REQUIRE(left > 0);
				q = ((char*)page_list[1] + elm_size * (left - 1));
				--left;
			}

			return q;
		}

		inline void Free(void* p)
		{
			void** q = (void**)p;
			*q = chunk_list;
			chunk_list = q;
		}
	};

	template < class T, size_t page_size = 4096 >
	struct AlignedPool
	{
		SimplePool* pool;

		inline AlignedPool()
		{
			pool = new(SharedPool<SimplePool>::Malloc())
			SimplePool(aligned_sizeof<T>::Value, page_size);
			pool->refcount = 1;
		}

		inline AlignedPool(AlignedPool const& apool)
		{
			pool = apool.pool;
			ATOMIC_INCREMENT(&pool->refcount);
		}

		inline ~AlignedPool()
		{
			if (!ATOMIC_DECREMENT(&pool->refcount))
			{
				pool->~SimplePool();
				SharedPool<SimplePool>::Free(pool);
			}
		}

		inline void* Malloc() { return pool->Malloc(); }
		inline void Free(void* ptr) { return pool->Free(ptr); }
	};

	template < size_t pool_size > struct private_pool_allocator
	{
		static void *Malloc(size_t size) 
		{
			if ( pool_size/2 < size ) 
				return AtomicRawPool<pool_size,0>::Malloc();
			else
				return private_pool_allocator< pool_size/2 >::Malloc(size);
		}
	};

	template <> struct private_pool_allocator < sizeof(void*) >
	{
		static void *Malloc(size_t size) 
		{
			return AtomicRawPool< sizeof(void*),0>::Malloc();
		}
	};

	inline void *shared_pool_malloc(size_t size)
	{
		REQUIRE(size <= SHARED_POOL_ELMSIZE_MAX);
		return private_pool_allocator<SHARED_POOL_ELMSIZE_MAX>::Malloc(size);
	}
}

