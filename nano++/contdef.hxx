
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "require.hxx"
#include "memory.hxx"
#include "mempool.hxx"
#include "hashcode.hxx"

namespace nano
{

	struct CXX_NO_VTABLE AllocatorInterface
	{
		virtual void* ReallocateStorage(void* p, size_t newsize) = 0;
		virtual void FreeStorage(void*) = 0;
		virtual void* AllocateNode(size_t size) = 0;
		virtual void FreeNode(void*) = 0;
	protected:
		AllocatorInterface() {}
		virtual ~AllocatorInterface() {}
	};

	template < class S >
	struct InitContainerStrategy : S
	{
		typedef typename S::ContType ContType;
		typedef typename S::ItemType ItemType;

		static CXX_NO_INLINE void CopyConstruct(AllocatorInterface* a, ItemType* o, ContType const* p, int count)
		{
			int i = 0;
			CXX_TRY
			{
				for (; i < count; ++i) new(S::Malloc(a, o + i))  ContType(p[i]);
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element construction in container", CXX_FILE_LINE);
			}
		}

		static CXX_NO_INLINE void InitConstruct(AllocatorInterface* a, ItemType* o, ContType const& q, int count)
		{
			int i = 0;
			CXX_TRY
			{
				for (; i < count; ++i) new(S::Malloc(a, o + i))  ContType(q);
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element construction in container", CXX_FILE_LINE);
			}
		}

		static CXX_NO_INLINE void DfltConstruct(AllocatorInterface* a, ItemType* o, int count)
		{
			int i = 0;
			CXX_TRY
			{
				for (; i < count; ++i) new(S::Malloc(a, o + i)) ContType();
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element construction in container", CXX_FILE_LINE);
			}
		}
		static CXX_NO_INLINE void Destruct(AllocatorInterface* a, ItemType* o, int count)
		{
			int i = 0;
			CXX_TRY
			{
				for (; i < count; ++i) { S::PtrOf(o + i)->~ContType(); S::Free(a, o + i); }
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element destructin in container", CXX_FILE_LINE);
			}
		}
	};

	template < class T >
	struct PlaneContainerStrategy
	{
		typedef PlaneContainerStrategy<T> ContStrgy;
		typedef T ContType;
		typedef T ItemType;
		typedef T* Iterable;
		typedef T const* ConstIterable;
		static inline void* Malloc(AllocatorInterface*, ItemType* q) { return q; }
		static inline void Free(AllocatorInterface*, ItemType* q) { ; }
		static inline T* PtrOf(ItemType* q) { return q; }
		static inline T& RefOf(ItemType* q) { return *q; }
	};

	template < class T >
	struct HeapContainerStrategy
	{
		typedef HeapContainerStrategy<T> ContStrgy;
		typedef T ContType;
		typedef T* ItemType;
		typedef T* const* Iterable;
		typedef T const* const* ConstIterable;
		static inline void* Malloc(AllocatorInterface* a, ItemType* q)
		{
			STRICT_REQUIRE(*q == 0);
			return (*q = ptr_cast(a->AllocateNode(sizeof(T))));
		}
		static inline void Free(AllocatorInterface* a, ItemType* q)
		{
			STRICT_REQUIRE(*q != 0);
			a->FreeNode(*q); *q = 0;
		}
		static inline T* PtrOf(ItemType* q) { return *q; }
		static inline T& RefOf(ItemType* q) { return **q; }
	};

	template < class T >
	struct PoolContainerStrategy
	{
		typedef PoolContainerStrategy<T> ContStrgy;
		typedef T ContType;
		typedef T* ItemType;
		typedef T* const* Iterable;
		typedef T const* const* ConstIterable;
		static inline void* Malloc(AllocatorInterface*, ItemType* q)
		{
			STRICT_REQUIRE(*q == 0);
			return (*q = ptr_cast(SharedPool<T>::Malloc()));
		}
		static inline void Free(AllocatorInterface*, ItemType* q)
		{
			STRICT_REQUIRE(*q != 0);
			SharedPool<T>::Free(*q); *q = 0;
		}
		static inline T* PtrOf(ItemType* q) { return *q; }
		static inline T& RefOf(ItemType* q) { return **q; }
	};

	template < class T >
	struct LinearContainerStrategy :
			cxx_if_else <
	(sizeof(T) > sizeof(void*) * 4), // maximum 20% overhead
	InitContainerStrategy<HeapContainerStrategy<T> >,
	InitContainerStrategy<PlaneContainerStrategy<T> >
	>::Type
	{};

	template < class T >
	struct PlaneInitStrategy : PlaneContainerStrategy<T>
	{
		static inline void CopyConstruct(AllocatorInterface* a, T* o, T const* p, int count)
		{
			memcpy(o, p, count * sizeof(T));
		}

		static inline void InitConstruct(AllocatorInterface* a, T* o, T t, int count)
		{
			//if ( sizeof(T) == 1 )
			//  memset(o,t,count);
			//else
			for (int i = 0; i < count; ++i) o[i] = t;
		}

		static inline void DfltConstruct(AllocatorInterface* a, T* o, int count)
		{
			memset(o, 0, count * sizeof(T));
		}

		static inline void Destruct(AllocatorInterface* a, T* o, int count)
		{
			//memset(o,0,count*sizeof(T));
		}
	};

	template <> struct InitContainerStrategy< PlaneContainerStrategy<char> > : PlaneInitStrategy<char> {};
	template <> struct InitContainerStrategy< PlaneContainerStrategy<unsigned char> > :
			PlaneInitStrategy<unsigned char> {};
	template <> struct InitContainerStrategy< PlaneContainerStrategy<short> > : PlaneInitStrategy<short> {};
	template <> struct InitContainerStrategy< PlaneContainerStrategy<unsigned short> > :
			PlaneInitStrategy<unsigned short> {};
	template <> struct InitContainerStrategy< PlaneContainerStrategy<long> > : PlaneInitStrategy<long> {};
	template <> struct InitContainerStrategy< PlaneContainerStrategy<unsigned long> > :
			PlaneInitStrategy<unsigned long> {};
	template <class T> struct InitContainerStrategy< PlaneContainerStrategy<T*> > : PlaneInitStrategy<T*> {};

	template < class T > struct LongptrElement
	{
		static void Kill(longptr_t& val)
		{
			CXX_TRY
			{
				if (sizeof(T) <= sizeof(longptr_t))
					((T*)&val)->~T();
				else
				{
					((T*)val)->~T();
					mem_free((T*)val, CXX_FILE_LINE);
				}
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element destructin in container", CXX_FILE_LINE);
			}
		}
		static void Creat(longptr_t& val, longptr_t val_value)
		{
			CXX_TRY
			{
				if (sizeof(T) <= sizeof(longptr_t))
					new ((T*)&val) T(*(T const*)&val_value);
				else
				{
					val = (longptr_t)mem_chk_malloc(sizeof(T), CXX_FILE_LINE);
					new ((T*)val) T(*(T const*)val_value);
				}
			}
			CXX_CATCH_ALL
			{
				fatalerror("exception occured during element construction in container", CXX_FILE_LINE);
			}
		}
		static bool Ise(longptr_t a, longptr_t b)
		{
			if (sizeof(T) <= sizeof(longptr_t))
				return (*(T const*)&a) == (*(T const*)&b);
			else
				return *((T const*)a) == *((T const*)b);
		}
		static u32_t Hof(longptr_t val)
		{
			if (sizeof(T) <= sizeof(longptr_t))
				return hashcode(*(T const*)&val);
			else
				return hashcode(*(T const*)val);
		}
		static longptr_t Cover(T const& val)
		{
			if (sizeof(T) <= sizeof(longptr_t))
				return *(longptr_t*)&val;
			else
				return (longptr_t)&val;
		}
		static T* Discover(longptr_t* val)
		{
			if (sizeof(T) <= sizeof(longptr_t))
				return (T*)val;
			else if (val)
				return (T*)*val;
			else
				return 0;
		}
	};

	template < class T > struct LongptrChrElement
	{
		static void Kill(longptr_t& val)
		{
			mem_free((void*)val, CXX_FILE_LINE);
		}

		static void Creat(longptr_t& val, longptr_t val_value)
		{
			T* str = (T*)val_value;
			int len = chrs_len(str);
			T* nval = (T*)mem_chk_malloc((len + 1) * sizeof(T), CXX_FILE_LINE);
			if (str) memcpy(nval, str, len * sizeof(T));
			nval[len] = 0;
			val = (longptr_t)nval;
		}

		static bool Ise(longptr_t a, longptr_t b)
		{
			return chrs_eql((T*)a, (T*)b);
		}

		static u32_t Hof(longptr_t val)
		{
			return hashcode((T const*)val);
		}

		static longptr_t Cover(T const* val)
		{
			return (longptr_t)val;
		}

		static T const** Discover(longptr_t* val)
		{
			return (T**)val;
		}
	};

	template <> struct LongptrElement<char*> : LongptrChrElement<char> {};
	template <> struct LongptrElement<wchar_t*> : LongptrChrElement<wchar_t> {};
	template <> struct LongptrElement<char const*> : LongptrChrElement<char> {};
	template <> struct LongptrElement<wchar_t const*> : LongptrChrElement<wchar_t> {};
}


