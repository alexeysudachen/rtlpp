
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "atomic.hxx"
#include "cxxsup.hxx"
#include "clog.hxx"
#include "format.hxx"

namespace nano
{
	struct BugTrap
	{
		static CXX_NO_INLINE void Catch(pchar_t msg, pchar_t file, int lineno, pchar_t pfx = 0)
		{
			if (!pfx) pfx = "bugtrap: ";
			if (file)
			{
				char at[128] = {0};
				at <<= _S*" at '%s':%d" % file % lineno;
				Abort(msg, pfx, at);
			}
			else
				Abort(msg, pfx);
		}

		static CXX_NO_INLINE void Warning(pchar_t msg, pchar_t file, int lineno, pchar_t pfx = 0)
		{
			if (!pfx) pfx = "warning: ";
			if (file)
			{
				char at[128] = {0};
				at <<= _S*" at '%s':%d" % file % lineno;
				LogTraceBack(msg, pfx, at);
			}
			else
				LogTraceBack(msg, pfx, 0);
		}

		static int CXX_NO_INLINE take_backtrace(void** cbk, int count)
		{
#if CXX_COMPILER_IS_GXX && !SYSTEM_IS_WINDOWS
			return ::backtrace(cbk, count);
#else
			return 0;
#endif
		}

		static int CXX_NO_INLINE format_backtrace(char* format_buffer, int buffer_size, void** cbk, int frames)
		{
			int S = buffer_size;
#if CXX_COMPILER_IS_GXX && !SYSTEM_IS_WINDOWS
			for (int i = 0; i < frames; ++i)
			{
				Dl_info dlinfo = {0};
				int o;
				if (dladdr(cbk[i], &dlinfo))
				{
					int status;
					char* dgld = __cxxabiv1::__cxa_demangle(dlinfo.dli_sname, 0, 0, &status);
					int dif = (char*)cbk[i] - (char*)dlinfo.dli_saddr;
					char c = dif > 0 ? '+' : '-';
					o = format_buffer | _S(S)*"  => %s %c%x (%p at %s)\n"
					    % (dgld ? dgld : dlinfo.dli_sname)
					    % c
					    % cxx_abs(dif)
					    % cbk[i]
					    % basename((char*)dlinfo.dli_fname);
					if (dgld) free(dgld);
				}
				else
					o = format_buffer | _S(S)*"  => %p\n" % cbk[i];
				S -= o; format_buffer += o;
			}
#endif
			return S - buffer_size;
		}


		static CXX_NO_INLINE void LogTraceBack(pchar_t msg, pchar_t pfx = 0, pchar_t at = 0)
		{
			static char error_buffer[8 * 1024] = {0};
			static int buffer_lock = 0;

			void* cbk[128] = {0};
			int frames = take_backtrace(cbk, 127);

			__xchg_lockon__(&buffer_lock)
			{
				memset(error_buffer, 0, sizeof(error_buffer));
				int L = sizeof(error_buffer);
				int l = 0;
				l += (error_buffer + l) | _S(L - l)*"%s%s%s" % pfx % msg % at;
				if (1)
				{
					l += (error_buffer + l) | _S(L - l)*"\n--backtrace--\n";
					l += format_backtrace(error_buffer + l, L - l, cbk, frames);
				}
				Clog->Enable();
				Clog | error_buffer;
			}
		}

		static CXX_NO_INLINE void Abort(pchar_t msg, pchar_t pfx = 0, pchar_t at = 0)
		{
			LogTraceBack(msg, pfx, at);
#if SYSTEM_IS_WINDOWS
			if ( IsDebuggerPresent() )
				__debugbreak();
#endif
			abort();
		}
	};

	inline void require(bool expr, pchar_t msg, pchar_t file = 0, int lineno = 0)
	{
		if (!expr) BugTrap::Catch(msg, file, lineno, "assert: ");
	}

	inline void require(bool expr, Format const& msg, pchar_t file = 0, int lineno = 0)
	{
		char local[128] = {0};
		if (!expr) BugTrap::Catch(local <<= msg, file, lineno, "assert: ");
	}

	inline void warning(bool expr, pchar_t msg, pchar_t file = 0, int lineno = 0)
	{
		if (!expr) BugTrap::Warning(msg, file, lineno);
	}

	inline void bugtrap(pchar_t msg, pchar_t file = 0, int lineno = 0)
	{
		BugTrap::Catch(msg, file, lineno);
	}

	inline void bugtrap(Format const& msg, pchar_t file = 0, int lineno = 0)
	{
		char local[128] = {0};
		BugTrap::Catch(local <<= msg, file, lineno);
	}

	inline void traceback(pchar_t msg = 0, pchar_t pfx = 0, pchar_t at = 0)
	{
		BugTrap::LogTraceBack(msg, pfx, at);
	}

	inline void fatalerror(pchar_t msg, pchar_t file = 0, int lineno = 0)
	{
		BugTrap::Catch(msg, file, lineno, "fatal: ");
	}
}

#if defined _STRICT
#define STRICT_WARNING(expr) ::nano::warning( (expr), #expr, __FILE__, __LINE__)
#define STRICT_REQUIRE(expr) ::nano::require( (expr), #expr, __FILE__, __LINE__)
#define STRICT_CHECK(expr, msg) ::nano::require( (expr), msg, __FILE__, __LINE__)
#else
#define STRICT_WARNING(expr)
#define STRICT_REQUIRE(expr) ((void)0)
#define STRICT_CHECK(expr, msg) ((void)0)
#endif

#if defined _STRIPASSERT
# define STRIP_ASSERT_FILTER(x,y) y
#else
# define STRIP_ASSERT_FILTER(x,y) x
#endif

#define CXX_FILE_LINE STRIP_ASSERT_FILTER(__FILE__,0), __LINE__
#define REQUIRE(expr) ::nano::require( (expr), STRIP_ASSERT_FILTER(#expr,0), CXX_FILE_LINE )
#define BUGTRAP(msg)  ::nano::bugtrap( msg, CXX_FILE_LINE  )


