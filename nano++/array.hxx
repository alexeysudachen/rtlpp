
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "autolist.hxx"
#include "atomic.hxx"
#include "mempool.hxx"
#include "memory.hxx"
#include "format.hxx"
#include "require.hxx"
#include "writer.hxx"
#include "contdef.hxx"
#include "random.hxx"

namespace nano
{

	template < class T >
	struct ArrayStrategy : LinearContainerStrategy<T>
	{
		enum { GROW_STEP = (8 * 1024) / sizeof(T) };

		static int CXX_NO_INLINE ChooseCapacity(int required, int count, int capacity)
		{
			if (capacity < required)
				if (required < GROW_STEP / 2)
					return ::nano::align(cxx_max(required, count * 2), 8);
				else
					return ::nano::align(required, GROW_STEP);
			return capacity;
		}

	};

	template <class T> struct StrExtraChars { enum { Count = 0}; };
	template <> struct StrExtraChars<char> { enum { Count = 1 }; };
	template <> struct StrExtraChars<wchar_t> { enum { Count = 1 }; };

	// !!!ATTENTION!!!
	//
	// contained objects should not have pointers to self
	// memory storage is reallocatable!!!
	//
	// otherwise define specialization of ArrayStrategy<YourType>
	// as drived from HeapContainerStrategy<YourType>
	//
	// !!!ATTENTION!!!
	template < class T, class Strgy > struct Array;
	template < class T, class Strgy = ArrayStrategy<T> > struct ConstArray
	{
		// Strgy added to tamplate parameters for file mapping proposes only
		// usually you don't need to use it

		typedef T ContType;
		typedef typename Strgy::ItemType Q;
		typedef typename Strgy::ItemType ItemType;

		struct Data : AllocatorInterface
		{
			int refcount;
			int count;
			int capacity;
			Q* begin;

			Data() : refcount(1), count(0), capacity(0), begin(0) {}

			virtual ~Data() {}
			virtual void* ReallocateStorage(void* stg, size_t size) { return mem_chk_realloc(stg, size, CXX_FILE_LINE); }
			virtual void FreeStorage(void* stg) { mem_free(stg, CXX_FILE_LINE); }
			virtual void* AllocateNode(size_t size) { return mem_chk_malloc(size, CXX_FILE_LINE); }
			virtual void FreeNode(void* n) { mem_free(n, CXX_FILE_LINE); }

			static void* operator new(size_t size)
			{
				if (size == sizeof(Data))
					return SharedPool<Data>::Malloc();
				else
					return shared_pool_malloc(size);
			}

			static void operator delete(void* p)
			{
				shared_pool_free(p);
			}

			virtual Data* Copy() = 0; //{ return new Data(*this); };
		};

		mutable Data* data_;

		ConstArray(ConstArray<T> const& a) : data_(a.Nolocal_()) {}
		ConstArray(Array<T, Strgy> const& a, struct TakeDataTag);

		ConstArray& operator = (ConstArray<T> const& a)
		{
			ConstArray guard(data_);
			data_ = a.Nolocal_();
			return *this;
		}
		~ConstArray() { Drop(); }

		inline int RefCount() const { if (data_) return data_->refcount; return 0; }

		Data* AcquireBufferRef_() const
		{
			if (data_)
			{
				ATOMIC_INCREMENT(&data_->refcount);
				return data_;
			}
			return 0;
		}

		CXX_NO_INLINE Data* Copy_(int new_capacity = -1) const
		{
			if (data_)
			{
				if (new_capacity < data_->count) new_capacity = data_->count;
				size_t memsz = new_capacity * sizeof(Q);
				Data* d = data_->Copy();
				d->begin  = ptr_cast(data_->ReallocateStorage(0, memsz));
				d->capacity = new_capacity;
				Strgy::CopyConstruct(d, d->begin, data_->begin, data_->count);
				if (int fill_count = d->capacity - d->count)
					memset(d->begin + d->count, 0, sizeof(Q) * fill_count);
				d->refcount = 1;
				return d;
			}
			return 0;
		}

		CXX_NO_INLINE void NolocalCopy_(int new_capacity = -1) const
		{
			REQUIRE(data_ != 0);

			if (!data_->begin) return;

			if (new_capacity < data_->count + StrExtraChars<T>::Count)
				new_capacity = data_->count + StrExtraChars<T>::Count;
			size_t memsz = new_capacity * sizeof(Q);
			Q* p = ptr_cast(data_->ReallocateStorage(0, memsz));
			memcpy(p, data_->begin, sizeof(Q)*data_->count);
			for (int i = data_->count; i < new_capacity; ++i)
				memset(p + i, 0, sizeof(Q));
			data_->begin = p;
			data_->capacity = new_capacity;

			int q;
			do
				q = (volatile int&)data_->refcount;
			while (!ATOMIC_CMPXCHNGE(&data_->refcount, q&~0x40000000, q));
		}

		CXX_NO_INLINE Data* Nolocal_() const
		{
			if (data_)
			{
				if (data_->refcount & 0x40000000)
					NolocalCopy_();
				return AcquireBufferRef_();
			}
			else
				return 0;
		}

		CXX_NO_INLINE ConstArray<T> Copy() const
		{
			ConstArray<T> t;
			t.data_ = Copy_();
			return t;
		}

		inline T const& at(int i) const
		{
			STRICT_REQUIRE(IsAllocated() && i >= 0 && i < Count());
			return Strgy::RefOf(data_->begin + i);
		}

		typedef int (ConstArray<T, Strgy>::*BoolType)() const;
		inline operator BoolType() const { return Count() ? &ConstArray<T, Strgy>::Count : 0; }
		inline bool IsAllocated() const { return !!data_; }
		inline T const& operator[](int i) const { return at(i); }
		inline bool Empty() const { return !Count(); }

		int Capacity() const
		{
			STRICT_REQUIRE(!data_ || data_->capacity >= 0);
			return data_ ? data_->capacity : 0;
		}
		int Count() const
		{
			STRICT_REQUIRE(!data_ || (data_->capacity >= data_->count && data_->count >= 0));
			return data_ ? data_->count : 0;
		}

		typedef typename Strgy::ConstIterable const_J;
		inline const_J Begin() const { return data_ ? data_->begin : 0; }
		inline const_J End() const { return data_ ? (data_->begin + data_->count) : 0; }
		inline const_J operator+ () const { return Begin(); }

	protected:

		CXX_NO_INLINE void Drop()
		{
			if (data_)
			{
				int conter = ATOMIC_DECREMENT(&data_->refcount);
				if (!(conter&~0x40000000))     // there is no reference to
				{
					Strgy::Destruct(data_, data_->begin, data_->count);
					if (!conter)   // is dynamic storage
						data_->FreeStorage(data_->begin);
					delete data_;
				}
				data_ = 0;
			}
		}

		ConstArray(Data* d) : data_(d) {}
	private:
		ConstArray();
	};

	template < class T, class Strgy = ArrayStrategy<T> > struct ArrayMasquarade;

	template < class T, class Strgy = ArrayStrategy<T> > struct Array : ConstArray<T, Strgy>
	{
		typedef ConstArray<T, Strgy> __base;
		typedef typename __base::Data Data;
		typedef typename __base::Q Q;
		typedef typename __base::ItemType ItemType;
		typedef typename __base::ContType ContType;
		using __base::data_;
		using __base::Drop;
		using __base::RefCount;
		using __base::Count;
		using __base::Capacity;
		using __base::Nolocal_;
		using __base::NolocalCopy_;
		using __base::Copy_;
		using __base::AcquireBufferRef_;
		using __base::IsAllocated;
		using __base::at;
		using __base::Begin;
		using __base::End;
		using __base::operator+;
		using __base::operator[];

		struct _Data : Data {
			virtual Data* Copy() { return new _Data(*this); };
		};

		explicit Array(struct NilTag = Nil) : __base((Data*)0) {}

		Array(int count, T const& dflt) : __base((Data*)0)
		{
			if (count)
			{
				Reserve(count);
				Strgy::InitConstruct(data_, data_->begin, dflt, count);
				data_->count = count;
			}
		}

		ArrayMasquarade<T, Strgy>& Masquarade()
		{
			return *(ArrayMasquarade<T, Strgy>*)this;
		}

		Array(Array<T, Strgy>& a, struct TakeDataTag) : __base(a, __take_data__) {}
		Array(ArrayMasquarade<T, Strgy> const& a) : __base(a) {}
		Array(ConstArray<T, Strgy> const& a) : __base(a.Copy_()) {}
		Array(T const* p, int L) : __base((Data*)0) { Insert(0, p, L); }
		//template < int N > Array( T const (&p)[N] ) : __base((Data*)0) { Insert(0,p,N); }

		explicit Array(int count) : __base((Data*)0)
		{
			if (count)
				Resize(count);
		}

		Array(struct UseLocalBufferTag, void* buffer, size_t size) : __base((Data*)0)
		{
			AttachToLocal(buffer, size);
		}

		template < class Z, int N >
		Array(struct UseLocalBufferTag, Z(&buffer)[N]) : __base((Data*)0)
		{
			AttachToLocal(buffer, N * sizeof(Z));
		}

		template < class X > CXX_NO_INLINE Array(list_Of<X> const& l) : __base((Data*)0)
		{
			int len = 0;
			list_Of<X> const* q = l.Glue(len);
			Reserve(len);
			for (int i = 0; q ; q = q->next, ++i)
			{
				STRICT_REQUIRE(i < data_->capacity);
				Strgy::InitConstruct(data_, data_->begin + i, q->t, 1);
				++data_->count;
			}
		}

		Array<T, Strgy>& operator = (Array<T, Strgy> const& a)
		{
			Array<T, Strgy> q;
			q.Swap(*this);
			data_ = a.Nolocal_();
			return *this;
		}

		CXX_NO_INLINE void AttachToLocal(void* b, size_t memsize)
		{
			STRICT_REQUIRE(memsize >= sizeof(T) * 1);
			if (data_)
			{
				if (data_->begin || data_->refcount > 1)
				{
					Data* q = data_->Copy();
					Drop();
					data_ = q;
				}
			}
			else
				data_ = new _Data();
			data_->capacity = memsize / sizeof(T);
			data_->refcount = 0x40000001;
			data_->begin = ptr_cast(b);
		}

		void Clear() { Drop(); }   // compatibility alias

		void Detach() const
		{
			if (RefCount() > 1)
				Copy().Swap(*this);
		}

		void Take(Array<T, Strgy>& a)
		{
			Array<T, Strgy> q;
			q.Swap(a);
			Drop();
			this->Swap(q);
		}

		Array<T, Strgy> forget()
		{
			Array<T, Strgy> q;
			Swap(q);
			return q;
		}

		CXX_NO_INLINE Array<T, Strgy> Copy() const
		{
			Array<T> t;
			t.data_ = Copy_();
			return t;
		}

		CXX_NO_INLINE Array<T, Strgy> Nolocal() const
		{
			Array<T, Strgy> t;
			t.data_ = Nolocal_();
			return t;
		}

		inline void Swap(Array<T, Strgy>& a)
		{
			swap(a.data_, data_);
		}

		inline T& at(int i)
		{
			STRICT_REQUIRE(IsAllocated() && i >= 0 && i < Count());
			return Strgy::RefOf(data_->begin + i);
		}

		inline T& operator[](int i) { return at(i); }

		typedef typename Strgy::Iterable J;
		inline J Begin() { return data_ ? data_->begin : 0; }
		inline J End()   { return data_ ? (data_->begin + data_->count) : 0; }
		inline J operator+ () { return Begin(); }

		inline void Grow(int grow) { ResizeBuffer(Count() + grow, 0); }
		inline void Resize(int new_count) { ResizeBuffer(new_count, 0); }
		inline void ReserveMore(int reserve)
		{ int count = Count(); ResizeBuffer(count, Strgy::ChooseCapacity(count + reserve + StrExtraChars<T>::Count, count, Capacity())); }
		inline void Reserve(int new_capacity) { ResizeBuffer(Count(), ::nano::align(new_capacity + StrExtraChars<T>::Count, 8)); }
		inline void Compact(int extra = 0)
		{ int count = Count(); ResizeBuffer(count, ::nano::align(count + extra + StrExtraChars<T>::Count, 8)); }

		void SetCount(int count)
		{
			STRICT_REQUIRE(count >= 0);
			int capacity  = Capacity();
			if (count > capacity)
			{
				if (capacity)
					data_->count = capacity;
				ResizeBuffer(count, 0);
			}
			else if (data_)
				data_->count = count;
		}

		CXX_NO_INLINE void ResizeBuffer(int new_count, int new_capacity, T const* dflt = 0)
		{
			int count = Count();
			int capacity = Capacity();

			STRICT_REQUIRE(Capacity() >= Count());
			STRICT_REQUIRE(Count() >= 0);

			if (!new_capacity)
				new_capacity = Strgy::ChooseCapacity(new_count + StrExtraChars<T>::Count, count, capacity);
			else if (new_capacity < new_count + StrExtraChars<T>::Count)
				new_capacity = Strgy::ChooseCapacity(new_count + StrExtraChars<T>::Count, count, capacity);

			Q* begin = data_ ? data_->begin : 0;

			if (count && new_count < count)
			{
				Strgy::Destruct(data_, begin + new_count, count - new_count);
				data_->count = new_count;
			}

			if ((new_capacity && new_capacity != capacity) || (new_count > count && capacity < new_count))
			{
				//printf("-count: %d->%d, capacity: %d->%d\n",count,new_count,capacity,new_capacity);

				if (!data_)
				{
					// we don't really need to handle exception here
					data_ = new _Data();
				}

				if (data_->refcount & 0x40000000)
				{
					NolocalCopy_(new_capacity);
				}
				else
				{
					size_t memsz = new_capacity * sizeof(Q);
					data_->begin = ptr_cast(data_->ReallocateStorage(data_->begin, memsz));
					data_->capacity = new_capacity;

					if (count < new_capacity)
						memset(data_->begin + count, 0, (new_capacity - count)*sizeof(Q));
				}

				begin = data_->begin;
			}

			if (new_count > count)
			{
				if (dflt)
					Strgy::InitConstruct(data_, begin + count, *dflt, new_count - count);
				else
					Strgy::DfltConstruct(data_, begin + count, new_count - count);
				data_->count = new_count;
			}

			//printf("@count: %d, capacity: %d\n",Count(),Capacity());
			STRICT_REQUIRE(Capacity() >= Count());
			STRICT_REQUIRE(Count() >= 0);
		}

		CXX_NO_INLINE T* Insert(int idx, T const& val, int cnt = 1)
		{
			if (cnt <= 0) return 0;
			require(idx <= Count() && idx >= 0, _S*"insert postion %d out of range [0,%d]" % idx % Count());
			ReserveMore(cnt);
			Q* pos = data_->begin + idx;
			Q* end = data_->begin + data_->count;
			if (pos != end)
				memmove(pos + cnt, pos, (end - pos) * sizeof(Q));
			Strgy::InitConstruct(data_, pos, val, cnt);
			data_->count += cnt;
			return Strgy::PtrOf(pos);
		}

		T* Push(T const& val = T(), int cnt = 1) { return Insert(Count(), val, cnt); }

		CXX_NO_INLINE T* Insert(int idx, T const* S, int cnt)
		{
			if (!cnt) return 0;
			require(idx <= Count() && idx >= 0, "insert postion out of range", CXX_FILE_LINE);
			ReserveMore(cnt);
			Q* pos = data_->begin + idx;
			Q* end = data_->begin + data_->count;
			if (pos != end)
				memmove(pos + cnt, pos, (end - pos) * sizeof(Q));
			Strgy::CopyConstruct(data_, pos, S, cnt);
			data_->count += cnt;
			return Strgy::PtrOf(pos);
		}

		inline T* Append(T const* S, int cnt) { return Insert(Count(), S, cnt); }
		inline T* Insert(int idx, T const* S, T const* E) { return Insert(idx, S, E - S); }
		inline T* Append(T const* S, T const* E) { return Insert(Count(), S, E); }

		inline T const& First() const { return const_cast<Array<T>*>(this)->First(); }
		inline T& First()
		{
			STRICT_CHECK(Count() != 0, "access to first element of empty array");
			return Strgy::RefOf(data_->begin);
		}

		inline T const& Last() const { return const_cast<Array<T>*>(this)->Last(); }
		inline T& Last()
		{
			STRICT_CHECK(Count() != 0, "access to last element of empty array");
			return * (End() - 1);
		}

		inline Array<T, Strgy>& Cut(int n)
		{
			if (Count() >= n && n >= 0) Resize(Count() - n);
			return *this;
		}

		inline void Shuffle()
		{
			LinearPRG prg;
			for (int i = 1, iE = Count(); i < iE; ++i)
			{
				int idx = next_random(&prg, i + 1);
				if (i != idx)
					swap(data_->begin[i], data_->begin[idx]);
			}
		}

	protected:
		Array(Data* d) : __base(d) {}
	};

	template < class T, class Strgy >
	struct ArrayMasquarade : Array<T, Strgy>
	{};

	template < class T, class Strgy > inline
	ConstArray<T, Strgy>::ConstArray(Array<T, Strgy> const& a, struct TakeDataTag)
		: data_(0)
	{ swap(a.data_, data_); }

	template < class T, int R >
	struct LocalArray : Array<T>
	{
		typedef Array<T> __base;
		union { byte_t local[sizeof(T)*R]; void* alignment; };
		LocalArray() : __base(::nano::__use_local_buffer__, local) { memset(local, 0, sizeof(local));}
	};

	template < class T > struct LocalArray<T, 0> : Array<T> {};

	template < class T, class F >
	struct ExtendedArray : Array<T>
	{
		typedef Array<T> __base;

		struct Data : Array<T>::Data
		{
			F extra;
		};

		struct _Data : Data
		{
			virtual Array<T>::Data* Copy() { return new _Data(*this); };
		};

		ExtendedArray(Data* edata = 0) : __base(edata ? edata : new _Data()) {}

		explicit ExtendedArray(size_t count, Data* edata = 0) : __base(edata ? edata : new _Data())
		{
			__base::Resize(count);
		}

		F* Extra() { return & ((Data*)__base::data_)->extra; }
		F* operator-> () { return & ((Data*)__base::data_)->extra; }
	};

	template < class T > struct ArrayWriter : GenericWriter<T>
	{
		Array<T>& a_;
		int start_len;
		ArrayWriter(Array<T>& a) : a_(a) { start_len = a.Count(); }

		/*override*/ bool Push(T const* a, int& n)
		{
			a_.Append(a, n); n = 0;
			return true;
		}

		/*override*/ void Trim(int len)
		{
			if (len >= 0)
				a_.Resize(start_len + len);
		};

		/*override*/ int Count() const
		{
			return a_.Count() - start_len;
		}
	};

	typedef Array<byte_t> Octets;

	template < class T, class Strgy >
	void format(TypeQuote<Array<T, Strgy> > q, XprintAgent& j, char fmt, unsigned perc)
	{
		typedef typename Array<T, Strgy>::Q Q;
		Array<T, Strgy> const* L = &*q;
		int pitch = ZBHWQ_Width(fmt);
		switch (fmt)
		{
			case 'Z': case 'B': case 'H': case 'W': case 'Q':
			l:
				if (+*L)
					j.SprintMemory(fmt, +*L, cxx_min_no0(perc, (sizeof(Q)*L->Count()) / ZBHWQ_Width(fmt)), pitch);
				break;
			case '@':
			{
				static char t[] = " BHHWWWWQ";
				pitch = (byte_t*)((Q*)0 + 1) - (byte_t*)0;
				fmt = (pitch < 9) ? t[pitch] : 'Q';
				goto l;
			}
			case 'T':
			{
				j->Fill(1, '[');
				int count = L->Count();
				for (int i = 0, E = count; i < E; ++i)
				{
					if (i) j->Copy(2, ", ");
					(_S*"%T" % L->at(i)).FormatOut(j->Writer());
				}
				j->Fill(1, ']');
				break;
			}
			default:
			{
				const char Sfmt_[] = "{Array<%s>:%p[%d],%d*%d,%d:%#4@%s}";
				char Sfmt[sizeof(Sfmt_) + 1]; strcpy(Sfmt, Sfmt_);
				int n = cxx_min(16 / cxx_min(sizeof(T), 8), 9);
				Sfmt[31] = '0' + n;
				(_S * Sfmt
				 % Typename<T>::Cstr()
				 % L->Begin()
				 % L->Count()
				 % L->Capacity()
				 % sizeof(typename Array<T, Strgy>::Q)
				 % L->RefCount()
				 % *L
				 % (L->Count() > n ? "..." : "")).FormatOut(j->Writer());
			}
		}
	}

	template < class T, class Q >
	void format(TypeQuote<map_Of<T, Q> > q, XprintAgent& j, char fmt, unsigned perc)
	{
		bool first = true;
		int len = 0;
		map_Of<T, Q> const* l = q->Glue(len);
		j->Fill(1, '[');
		while (l)
		{
			if (!first) j->Copy(2, ", ");
			else first = false;
			(_S*"%T=>%T" % l->t % l->q).FormatOut(j->Writer());
			l = l->next;
		}
		j->Fill(1, ']');
	}

	template < class T >
	void format(TypeQuote<list_Of<T> > q, XprintAgent& j, char fmt, unsigned perc)
	{
		bool first = true;
		int len = 0;
		list_Of<T> const* l = q->Glue(len);
		j->Fill(1, '[');
		while (l)
		{
			if (!first) j->Copy(2, ", ");
			else first = false;
			(_S*"%T" % l->t).FormatOut(j->Writer());
			l = l->next;
		}
		j->Fill(1, ']');
	}

	template < class T, class Strgy >
	inline int len(Array<T, Strgy> const& a) { return a.Count(); }

	inline char const* as_Cstr(Array<char>& a) 
	{ 
		STRICT_REQUIRE(a.Count()< a.Capacity() && !(+a)[a.Count()]); 
		return +a; 
	}

	inline char const* as_Cstr(Array<byte_t>& a) 
	{ 
		a.ReserveMore(1); 
		return (char*) + a; 
	}

	inline wchar_t const* as_Cstr(Array<wchar_t>& a) 
	{ 
		STRICT_REQUIRE(a.Count()< a.Capacity() && !(+a)[a.Count()]); 
		return +a; 
	}

	template < class T > CXX_NO_INLINE
	Array<T>& _fix_Cstr(Array<T>& q)
	{
		int i = 0;
		for (int j = q.Count(); i < j ; ++i)
			if (!q[i])
			{
				q.Resize(i);
				break;
			}
		q.Compact(1 - StrExtraChars<T>::Count);
		return q;
	};

	inline Array<char>& fix_Cstr(Array<char>& q) { return _fix_Cstr(q); }
	inline Array<byte_t>& fix_Cstr(Array<byte_t>& q) { return _fix_Cstr(q); }
	inline Array<wchar_t>& fix_Cstr(Array<wchar_t>& q) { return _fix_Cstr(q); }

	inline ByteRange byte_range(ConstArray<char> const& a)
	{   return ByteRange((byte_t const*)a.Begin(), a.Count()); }
	inline ByteRange byte_range(ConstArray<byte_t> const& a)
	{   return ByteRange((byte_t const*)a.Begin(), a.Count()); }

	template < class T, class Strgy >
	inline Array<T, Strgy> shuffle(ConstArray<T, Strgy> const& carr)
	{
		Array<T, Strgy> arr = carr.Copy();
		arr.Shuffle();
		return arr;
	}
}


