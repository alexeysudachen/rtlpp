

/*

 (C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization of the copyright holder.

 */

#pragma once
#include "deftypes.hxx"
#include "require.hxx"
#include "crcx.hxx"

namespace nano {

	inline u32_t hashcode(cbyte_t *p, int len)
	{
		return hashcode_crc32(p,len);
	}

	inline u32_t hashcode(int const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(unsigned int const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(short const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(unsigned short const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(long const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(unsigned long const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(i64_t const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(u64_t const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }
	inline u32_t hashcode(void * const &a) { return hashcode((cbyte_t*)&a,sizeof(a)); }

	inline u32_t hashcode(const char *a) { return hashcode((cbyte_t*)a,a?strlen(a):0); }
	inline u32_t hashcode(const wchar_t *a) { return hashcode((cbyte_t*)a,a?wcslen(a):0); }
}
