
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "require.hxx"
#include "cxxsup.hxx"
#include "format.hxx"
#include "string.hxx"
#include "threadlocal.hxx"

namespace nano
{

	struct UnknownXpt
#if defined _USESTL
			: std::exception
#endif
	{
		String message;
		String source;
		unsigned lineno;

		CXX_NO_INLINE UnknownXpt(String const& message, pchar_t source, unsigned lineno)
		{
			this->lineno  = lineno;
			this->source  = source;
			this->message = message;
		}

		virtual pchar_t Message() const
		{
			return +message;
		}
		virtual pchar_t Source() const
		{
			return +source;
		}
		virtual unsigned LineNo() const
		{
			return lineno;
		}
		virtual String Format() const
		{
			return String(_S*"%s: %s at '%s':%d" % TypeName() % message % source % lineno);
		}
		virtual pchar_t TypeName() const
		{
			return "nano::UnknownXpt";
		}

#if defined _USESTL
		virtual pchar_t what() const
		{ 
			return +message; 
		}
#endif

		virtual void Throw() const = 0;
		virtual UnknownXpt* Clone() const = 0;
		virtual void* Final() const = 0;
		virtual ~UnknownXpt() {}
	};

	struct XptInfo;
	static XptInfo* const IgnoreXpt = (XptInfo*) - 1L;
	static XptInfo* const CplusplusXpt = (XptInfo*)0;

#if defined _IGNOREXPT
	static XptInfo* const DefinedXpt = IgnoreXpt;
#else
	static XptInfo* const DefinedXpt = CplusplusXpt;
#endif

#define TRACEnTHROW_EXCEPTION(e) ::nano::XptInfo::ThrowException(e)
#define TRACE_EXCEPTION(e) ::nano::XptInfo::TraceException(&e)
#define NANO_XPT_THROW(e) TRACEnTHROW_EXCEPTION(e)

	struct XptInfo
	{
		static CXX_NO_INLINE void NestedExceptionOccured(UnknownXpt const* e, XptInfo const* xpt)
		{
			UnknownXpt const* e2 = xpt->Occured();
			char info1[128] = {0};
			info1 <<= _S*"%s: %s\n\tat '%s':%d" % e->TypeName() % e->Message() % e->Source() % e->LineNo();
			traceback(info1, "xptnfo: ");
			info1 <<= _S*"nested in context:\nt%s: %s\n\t\tat '%s':%d" % e2->TypeName() % e2->Message() % e2->Source() % e2->LineNo();
			Clog | info1;
			abort();
		}

#if CXX_USES_EXCEPTIONS && defined _USESTL
		static inline void TraceException(std::exception const* e)
		{
			char info[128] = {0};
			const std::type_info& ti = typeid(e);
			info <<= _S*"%s: %s" % ti.name() % e->what();
			traceback(ti.name(), "(std)xptnfo: ");
		}
#endif

		static CXX_NO_INLINE void TraceException(UnknownXpt const* e)
		{
			char info[256] = {0};
			info <<= _S*"%s: %s\n\tat '%s':%d" % e->TypeName() % e->Message() % e->Source() % e->LineNo();
			traceback(info, "xptnfo: ");
		}

		template < class T > static void ThrowException(T const& e)
		{
#if !CXX_USES_EXCEPTIONS
			Clog->Enable();
#endif
#if defined _TRACEXPT || !CXX_USES_EXCEPTIONS
			TraceException(&e);
#endif
#if CXX_USES_EXCEPTIONS
			throw e;
#else
			Clog | "fatal: unhandled exception";
# if SYSTEM_IS_WINDOWS
			TerminateProcess((HANDLE) - 1, -1);
# else
			abort();
# endif
#endif
		}

		void CXX_NO_INLINE Raise(UnknownXpt* e, bool trace = true)
		{
			if (this == CplusplusXpt)
			{
				struct _guard { UnknownXpt* e; ~_guard() { delete e; } };
				_guard g = {e};
				g.e->Throw();
			}
			else if (this == IgnoreXpt)
				return;

			if ( Occured() ) NestedExceptionOccured(e, this);
			if (trace) TraceException(e);
			Clear();
			xpt = e;
		}

		void Set(UnknownXpt* e)
		{
			if (this != CplusplusXpt && this != IgnoreXpt)
			{
				Clear();
				xpt = e;
			}
		}

		void CXX_NO_INLINE Clear()
		{
			if (this != CplusplusXpt && this != IgnoreXpt)
			{
				if (xpt) delete xpt;
				xpt = 0;
			}
		}

		void Rethrow()
		{
			if (UnknownXpt* e = Occured())
				next->Raise(e->Clone(), false);
		}

		XptInfo* operator ~()
		{
			Clear();
			return this;
		}

		inline UnknownXpt* Occured() const
		{
			return (this != CplusplusXpt && this != IgnoreXpt) ? this->xpt : 0;
		}

		XptInfo() : xpt(0), next(0) {}
		~XptInfo() { Clear(); }
		inline bool Good() const { return !this->Occured(); }

		pchar_t Message()
		{
			if (UnknownXpt* e = Occured())
				return e->Message();
			else
				return "";
		}

		UnknownXpt* xpt;
		XptInfo* next;

		//private:
		XptInfo(XptInfo const&);
		XptInfo& operator=(XptInfo const&);
	};

	struct CurrentXptNfo
	{
		byte_t none;

		CXX_NO_INLINE static ThreadLocal<XptInfo> const& Place()
		{
			static const ThreadLocal<XptInfo> xpt;
			return xpt;
		};

		XptInfo* operator->() const
		{
			return Place();
		};

		XptInfo* Set(XptInfo* x) const
		{
			ThreadLocal<XptInfo> const& q = Place();
			XptInfo* old = q;
			q = x;
			return old;
		}

		void Push(XptInfo* x) const
		{
			x->next = Set(x);
		}

		void Pop() const
		{
			XptInfo* q = Place();
			if (q)
			{
				Set(q->next);
				q->next = 0;
			}
		}
	};

	static const CurrentXptNfo XptNfo = {0};

#define \
	OCCURED(e,Ctx) ::nano::XptNfo->Raise(new e(Ctx,__FILE__,__LINE__))

	struct XptInfoHolder
	{
		struct XptInfo xpt;
		XptInfoHolder(int) { XptNfo.Push(&xpt); }
		~XptInfoHolder() { XptNfo.Pop(); }
		operator bool () const { return true; }
		XptInfoHolder(XptInfoHolder const&);
	};

#define __defxptnfo__ \
	if ( ::nano::XptInfoHolder CXX_LOCAL_ID(xptnfo) = 0 ) \
	goto CXX_LABEL; else CXX_LABEL:

#define XPTDECLARE_C(E,bE,Q) \
	E(String const &message = String(), pchar_t source=0, unsigned lineno=0)\
		: bE(message,source,lineno) {}\
	virtual ~E() {}\
	 
#define XPTDECLARE_B(E,bE,Q) \
	virtual pchar_t TypeName() const { return Q; }\
	virtual void Throw() const { NANO_XPT_THROW(*this); }\
	virtual UnknownXpt *Clone() const { return new E(*this); }\
	virtual void *Final() const { return (void*)this; }\
	 
#define XPTDECLARE(E,bE,Q) \
	struct E : bE\
	{\
		XPTDECLARE_C(E,bE,Q)\
		XPTDECLARE_B(E,bE,Q)\
	}\
	 
#define XPTDECLARE_OPT(E,bE,Q,T,N) \
	struct E : bE\
	{\
		T param;\
		T N() const { return param; } \
		E(T param, Strarg message = (pwide_t)0, pchar_t source=0, unsigned lineno=0)\
			: param(param), bE(message,source,lineno) {}\
		XPTDECLARE_B(E,bE,Q)\
	}\
	 
	XPTDECLARE(RuntimeXpt, UnknownXpt, "nano::RuntimeXpt");
	XPTDECLARE(SelfCheckError, RuntimeXpt, "nano::SelfCheckError");

	XPTDECLARE(UnknownError, RuntimeXpt, "nano::UnknwonError");
	XPTDECLARE(TestError, RuntimeXpt, "nano::TestError");
	XPTDECLARE(StdCpluplusXpt, RuntimeXpt, "nano::StdCpluplusXpt");

	XPTDECLARE(IOError, RuntimeXpt, "nano::IOError");
	XPTDECLARE(EndOfFile, IOError, "nano::EndOfFile");
	XPTDECLARE(AccessDenied, IOError, "nano::AccessDenied");

	XPTDECLARE(IndexOutOfRange, RuntimeXpt, "nano::IndexOutOfRange");
	XPTDECLARE(InvalidArgument, RuntimeXpt, "nano::InvalidArgument");
	XPTDECLARE(Unimplemented, RuntimeXpt, "nano::Unimplemented");
	XPTDECLARE(Illformed, RuntimeXpt, "nano::Illformed");
	XPTDECLARE(Unaligned, RuntimeXpt, "nano::Unaligned");
	XPTDECLARE(NoEnoughSpace, RuntimeXpt, "nano::NoEnoughSpace");
	XPTDECLARE(DataCorrupted, RuntimeXpt, "nano::DataCorrupted");
	XPTDECLARE(EmptyContainer, RuntimeXpt, "nano::EmptyContainer");
	XPTDECLARE(NoElement, RuntimeXpt, "nano::NoElement");

	XPTDECLARE(ArithmeticXpt, RuntimeXpt, "nano::ArithmeticXpt");
	XPTDECLARE(ZerroDivide, ArithmeticXpt, "nano::ZerroDivide");
	XPTDECLARE(InvalidDecimalNumber, ArithmeticXpt, "nano::InvalidDecimalNumber");
	XPTDECLARE(InvalidHexadecimalNumber, ArithmeticXpt, "nano::InvalidHexadecimalNumber");
	XPTDECLARE(InvalidBinaryNumber, ArithmeticXpt, "nano::InvalidBinaryNumber");

}


