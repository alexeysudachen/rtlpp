
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#if !defined nano_cxx_bitdata_HXX
#define nano_cxx_bitdata_HXX

#include "deftypes.hxx"
#include "algo.hxx"
#include "string.hxx"
#include "xptnfo.hxx"

namespace nano 
  {
    inline CXX_NO_INLINE String EncodeByXbit(void const *data, int count /*of bits*/, int BC, char const *bit_table )
      {
        Array<char> r((count+(BC-1))/BC);
        char *Q = r.Begin(); 
        ulong_t q = 0;
        if ( count%BC ) 
          {
            pop_bits(&q,data,&count,count%BC);
            *Q++ = bit_table[q];
          }
        while ( count )
          {
            q = 0;
            pop_bits(&q,data,&count,BC);
            *Q++ = bit_table[q];
          }
        REQUIRE( Q == r.End() );
        return r;
      }
  
    inline CXX_NO_INLINE Octets DecodeByXbit(String const &encoded, int BC, byte_t const *back_bit_table)
      {
        int count = 0;
        Octets r((encoded.Length()*BC+7)/8);
        char *S = +encoded.End()-1, *E = encoded.Begin()-1;
        while ( S != E )
          {
            byte_t bits = back_bit_table[*S--];
            if ( bits == 255 )
              { OCCURED(DataCorrupted,_S*"bad symbol in '%s'"%String(+encoded,-1,DOT_STRING_ENCODER));
                return Octets(); }
            push_bits(bits,+r,&count,BC);
          }
        return r;
      }

    inline String EncodeBy5bit(void const *data, size_t count /*of bits*/ )
      {
        static const char five_bit_table[] = "0123456789abcdefgjkmnpqrstuvwxyz";
        return EncodeByXbit(data,count,5,five_bit_table);
      }
    
    inline Octets DecodeBy5bit(String const &encoded)
      {
        static const byte_t five_bit_back_table[] /* 32 */ = 
          {
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
             0,  1,  2,  3,  4,  5,  6,  7,  8,  9,255,255,255,255,255,255,
            255, 10, 11, 12, 13, 14, 15, 16,255,255, 17, 18,255, 19, 20,255,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,255,255,255,255,255,
            255, 10, 11, 12, 13, 14, 15, 16,255,255, 17, 18,255, 19, 20,255,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
          };
        return DecodeByXbit(encoded,5,five_bit_back_table);
      }
    
    inline String EncodeBy6bit(void const *data, size_t count /*of bits*/ )
      {
        static const char six_bit_table[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@";
        return EncodeByXbit(data,count,6,six_bit_table);
      }
    
    inline Octets DecodeBy6bit(String const &encoded)
      {
        static const byte_t six_bit_back_table[] /* 64 */ = 
          {
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            0  ,1  ,2  ,3  ,4  ,5  ,6  ,7  ,8  ,9  ,255,255,255,255,255,255,
            63 ,36 ,37 ,38 ,39 ,40 ,41 ,42 ,43 ,44 ,45 ,46 ,47 ,48 ,49 ,50 ,
            51 ,52 ,53 ,54 ,55 ,56 ,57 ,58 ,59 ,60 ,61 ,255,255,255,255,62 ,
            255,10 ,11 ,12 ,13 ,14 ,15 ,16 ,17 ,18 ,19 ,20 ,21 ,22 ,23 ,24 ,
            25 ,26 ,27 ,28 ,29 ,30 ,31 ,32 ,33 ,34 ,35 ,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
            255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
          };
        return DecodeByXbit(encoded,6,six_bit_back_table);
      }
  }

#endif // nano_cxx_bitdata_HXX
