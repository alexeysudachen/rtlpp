
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "require.hxx"

#if CXX_USES_THREADS && !SYSTEM_IS_WINDOWS
#include <pthread.h>
#endif

namespace nano
{

	template < class T > struct ThreadLocal
	{
#if !CXX_USES_THREADS

		mutable T* val;
		T* get_ptr() const { return val; }
		void set_ptr(T* p) const { val = p; }
		ThreadLocal() : val(0) {}

#elif SYSTEM_IS_WINDOWS

		u32_t thr_id;
		ThreadLocal() : thr_id(TlsAlloc())
		{
			if (thr_id == TLS_OUT_OF_INDEXES)
				fatalerror("failed to allocate thread-local storage", __FILE__, __LINE__);
		}
		~ThreadLocal() { TlsFree(thr_id); }
		T* get_ptr() const { return (T*)TlsGetValue(thr_id); }
		void set_ptr(T* p) const { TlsSetValue(thr_id, p); }

#else

		pthread_key_t thr_id;
		ThreadLocal() : thr_id()
		{
			pthread_key_create(&thr_id, 0);
			if (!thr_id)
				fatalerror("failed to allocate thread-local storage", __FILE__, __LINE__);
		}
		~ThreadLocal() { pthread_key_delete(thr_id); }
		T* get_ptr() const { return (T*)pthread_getspecific(thr_id); }
		void set_ptr(T* p) const { pthread_setspecific(thr_id, p); }

#endif

		T* operator+() const { return get_ptr(); }
		operator T* () const { return get_ptr(); }
		T* operator =(T* p) const { set_ptr(p); return p; }
	protected:
		ThreadLocal<T>(ThreadLocal<T> const&);
		void operator =(ThreadLocal<T> const&);
	};

}


