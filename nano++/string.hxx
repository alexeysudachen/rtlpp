
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "writer.hxx"
#include "array.hxx"
#include "format.hxx"
#include "hashcode.hxx"

#if defined _USESTL
#include <string>
#endif

namespace nano
{
	int utf8_length(pchar_t a)
	{
		int n = 0;
		int l = 0;
		int L = strlen(a);
		while (l < L)
		{
			int q = Utf8Encoder::Length(a[l]);
			++n;
			STRICT_WARNING(q != 0);
			if (!q) ++l;
		}
		STRICT_WARNING(l <= L);
		return n;
	}

	wchar_t utf8_getwide(char const*& p)
	{
		wchar_t q = 0;
		if (p && *p)
		{
			int cnt = 0;
			q = Utf8Encoder::Decode(p, &cnt);
			while (*p && cnt--) ++p;
		}
		return q;
	}

	pchar_t utf8_skip(pchar_t a, int l)
	{
		while (*a && l--)
		{
			int q = Utf8Encoder::Length(*a);
			if (q) while (q-- && *a) ++a;
			else ++a;
		}
		return a;
	}

	inline char    chr_lower(char a)    { return tolower(a); }
	inline char    chr_upper(char a)    { return toupper(a); }
	inline bool    chr_isalpha(char c)  { return 0 != isalpha(c); }
	inline bool    chr_isalnum(char c)  { return 0 != isalnum(c); }
	inline bool    chr_isdigit(char c)  { return 0 != isdigit(c); }
	inline bool    chr_isxdigit(char c) { return 0 != isxdigit(c); }
	inline bool    chr_isupper(char c)  { return 0 != isupper(c); }
	inline bool    chr_islower(char c)  { return 0 != islower(c); }

	inline int     chr_lower(int a)     { return tolower(a); }
	inline int     chr_upper(int a)     { return toupper(a); }
	inline bool    chr_isalpha(int c)   { return 0 != isalpha(c); }
	inline bool    chr_isalnum(int c)   { return 0 != isalnum(c); }
	inline bool    chr_isdigit(int c)   { return 0 != isdigit(c); }
	inline bool    chr_isxdigit(int c)  { return 0 != isxdigit(c); }
	inline bool    chr_isupper(int c)   { return 0 != isupper(c); }
	inline bool    chr_islower(int c)   { return 0 != islower(c); }

	inline wchar_t chr_lower(wchar_t a)    { return towlower(a); }
	inline wchar_t chr_upper(wchar_t a)    { return towupper(a); }
	inline bool    chr_isalpha(wchar_t c)  { return 0 != iswalpha(c); }
	inline bool    chr_isalnum(wchar_t c)  { return 0 != iswalnum(c); }
	inline bool    chr_isdigit(wchar_t c)  { return 0 != iswdigit(c); }
	inline bool    chr_isxdigit(wchar_t c) { return 0 != iswxdigit(c); }
	inline bool    chr_isupper(wchar_t c)  { return 0 != iswupper(c); }
	inline bool    chr_islower(wchar_t c)  { return 0 != iswlower(c); }

	//inline int len(ExactType<pchar_t> const &val) { if ( !*val ) return 0; return strlen(*val); }
	//inline int len(ExactType<pwide_t> const &val) { if ( !*val ) return 0; return wcslen(*val); }
	inline int len(pchar_t const& val) { if (!val) return 0; return strlen(val); }
	inline int len(pwide_t const& val) { if (!val) return 0; return wcslen(val); }

	bool chr_isspace(int c) { return 0 != isspace(c); }

	template < class Q > CXX_NO_INLINE
	static void uutranslate(Array<Q>& S, Q const* text, int L)
	{
		int count = S.Count();
		S.Resize(L + count);
		for (int i = 0; i < L; ++i)
			S[i + count] = text[i];
		S.ReserveMore(1);
	}

	inline CXX_NO_INLINE void uutranslate(Array<char>& S, wchar_t const* text, int L)
	{
		for (int i = 0; i < L; ++i)
		{
			char Q[4];
			int cnt = 0;
			Utf8Encoder::Encode(Q, text[i], &cnt);
			S.Append(Q, cnt);
		}
		S.ReserveMore(1);
	}

	inline CXX_NO_INLINE void uutranslate(Array<wchar_t>& S, char const* text, int L)
	{
		for (int i = 0; i < L;)
		{
			int cnt = 0;
			S.Push(Utf8Encoder::Decode(text + i, &cnt));
			i += cnt;
		}
		S.ReserveMore(1);
	}

	template < class T, class Q >
	Array<T>& append_chars(Array<T>& S, Q const* text, int L = -1)
	{
		if (text)
		{
			if (L < 0) L = len(text);
			if (L) uutranslate(S, text, L);
		}
		return S;
	}

	template < class T, class Q > CXX_NO_INLINE
	Array<T> copy_chars(Q const* text, int L, T*)
	{
		Array<T> a;
		return append_chars(a, text, L);
	}

	template < class T >
	Array<T>& append_format(Array<T>& S, Format const& f)
	{
		ArrayWriter<T> w(S);
		f.FormatOut(&w);
		return S;
	}

	template < class T > CXX_NO_INLINE
	Array<T> copy_format(Format const& f, T*)
	{
		Array<T> a;
		return append_format(a, f);
	}

	template < int R > struct Strarg_;
	template < class T > struct Strbase;
	typedef Strbase<wchar_t> Unicode;
	typedef Strbase<char>    String;
	typedef ConstArray<char> Utf8_;
	typedef ConstArray<wchar_t> Ucs2_;

	struct LazyStringBase;
	struct LazyString;
	struct StrargBase_;

	template < class T > struct Strbase : ConstArray<T>
	{
		typedef T Chr;
		typedef ConstArray<T> __base;
		typedef Strbase<T> String_;
		typedef typename cxx_if_else < sizeof(T) == sizeof(char),
		        ConstArray<wchar_t>,
		        ConstArray<char> >::Type xstrin_;

		static String_ Take(Array<T>& q) { return String_(q, __take_data__); }
		static String_ Format(Format const& S) { return String_(S); }
		T const* operator+ () const { return __base::Begin(); }
		T const* Cstr() const { return as_Cstr(*this); }
		int Length() const { return __base::Count(); }

		Strbase() : __base(0) {}
		Strbase(String_ const& a) : __base(a) {}

		// convert from another string type
		Strbase(__base const& a)  : __base(a) {}
		Strbase(Array<T> const& a)  : __base(copy_chars(+a, a.Count(), (T*)0), __take_data__) {}
		Strbase(Array<T>& q, struct TakeDataTag) : __base(fix_Cstr(q), __take_data__) {}
		Strbase(xstrin_ const& a) : __base(copy_chars(+a, a.Count(), (T*)0), __take_data__) {}
		Strbase(wchar_t const* text, int L = -1) : __base(copy_chars(text, L, (T*)0), __take_data__) {}
		Strbase(char const* text, int L = -1) : __base(copy_chars(text, L, (T*)0), __take_data__) {}
		Strbase(struct Format const& S) : __base(copy_format(S, (T*)0), __take_data__) {}
		Strbase(StrargBase_ const& a);
		Strbase(LazyStringBase const& a);

		// concatinate two strings
		template <class P, class Q>
		Strbase(P const* pText, int pL, Q const* qText, int qL)
			: __base(append_chars(copy_chars(pText, pL), qText, qL), __take_data__) {}
	};

	inline char const* as_Cstr(Strbase<char> const& a) 
	{ 
		STRICT_REQUIRE( a.Count() < a.Capacity() && *a.End() == 0 );
		//a.ReserveMore(1); 
		return +a; 
	}

	inline wchar_t const* as_Cstr(Strbase<wchar_t> const& a) 
	{ 
		STRICT_REQUIRE( a.Count() < a.Capacity() && *a.End() == 0 );
		//a.ReserveMore(1); 
		return +a; 
	}

	struct Chars : Array<char>
	{
		typedef Array<char> __base;
		Chars() {}
		Chars(Chars const& a) : __base(a.Copy_()) {}

		Chars(wchar_t const* text, int L = -1) { copy_chars(text, L, (char*)0).Swap(*this); }
		Chars(char const* text, int L = -1) : __base(text, L < 0 ? len(text) : L) {}
		Chars(Format const& S) { copy_format(S, (char*)0).Swap(*this); }
		Chars(StrargBase_ const& a);
		Chars(LazyStringBase const& a);

		template < class T >
		Chars(Strbase<T> const& a) { append_chars(*this, +a, a.Length()); }

		Chars& operator=(Chars const& a) { Chars(a).Swap(*this); return *this; }

		char const* operator+ () const { return __base::Begin(); }
		char const* Cstr() const { return as_Cstr((__base&) * this); }
		int Length() const { return __base::Count(); }
	};

	inline Chars& operator += (Chars& o, char c) { o.Push(c); as_Cstr(o); return o; }
	inline Chars& operator += (Chars& o, char const* a) { append_chars(o, a, -1); return o; }
	inline Chars& operator += (Chars& o, wchar_t const* a) { append_chars(o, a, -1); return o; }

	struct LazyStringBase
	{
		LazyStringBase(void* l, LazyStringBase const* r, void (*a)(UnknownWriter*, void*))
			: l(l), r(r), append_to(a) {}
		String Str() const { return String(*this); }

		template < class T > CXX_NO_INLINE
		ArrayMasquarade<T> write_array(T*) const
		{
			T local[128] = {0};
			Array<T> a(__use_local_buffer__, local);
			ArrayWriter<T> w(a);
			write_to(&w);
			a.ReserveMore(1);
			return a.Masquarade();
		}

		template < class T > CXX_NO_INLINE
		void write_to_local(T* a, int N) const
		{
			RawMemWriter<T> w(a, N);
			write_to(w);
		}

		void write_to(UnknownWriter* w) const
		{
			if (append_to)
				append_to(w, (void*)l);
			else
			{
				((LazyStringBase*)l)->write_to(w);
				if (r) r->write_to(w);
			}
		}

		operator ArrayMasquarade<char>() const
		{
			return write_array((char*)0);
		}

	protected:
		void const* l;
		LazyStringBase const* r;
		void (*append_to)(UnknownWriter*, void*);
	};

	struct StrargBase_
	{
		typedef bool (StrargBase_::*BoolType)() const;
		operator BoolType() const { return (t && *t) ? &StrargBase_::operator! : 0; }
		bool operator !() const { return !operator BoolType(); };
		pchar_t operator +() const { return t; }
		pchar_t Cstr() const { return t ? t : ""; }
		int Length() const { return l < 0 ? (l = (t ? strlen(t) : 0)) : l; }
	protected:
		pchar_t t;
		mutable int l;
		StrargBase_(pchar_t t = 0, int l = 0) : t(t), l(l) {}
	private:
		StrargBase_(StrargBase_ const&);
	};

	struct LazyString : LazyStringBase
	{
		typedef LazyStringBase __base;
		LazyString(void* l, LazyString* r, void (*a)(UnknownWriter*, void*))
			: __base(l, r, a) {}
		LazyString(Format const& f)
			: __base((void*)&f, 0, &append_format) {}
		LazyString(char const* a)
			: __base((void*)a, 0, &append_chars) {}
		LazyString(String const &a)
			: __base((void*)+a, 0, &append_chars) {}
		LazyString(StrargBase_ const &a)
			: __base((void*)a.Cstr(), 0, &append_chars) {}
		LazyString(wchar_t const* a)
			: __base((void*)a, 0, &append_wchars) {}
		LazyString(Utf8_ const& S)
			: __base((void*)&S, 0, &append_utf8) {}
		LazyString(Ucs2_ const& S)
			: __base((void*)&S, 0, &append_ucs2) {}
		LazyString(LazyString const& a, LazyString const& b)
			: __base((void*)&a, &b, 0) {}

		static void append_format(UnknownWriter* w, void* l)
		{ ((Format*)l)->FormatOut(w); }
		static void append_chars(UnknownWriter* w, void* l)
		{ if (l) w->Copy(strlen((char*)l), (char*)l); }
		static void append_wchars(UnknownWriter* w, void* l)
		{ if (l) w->Copy(wcslen((wchar_t*)l), (wchar_t*)l); }
		static void append_utf8(UnknownWriter* w, void* l)
		{ Utf8_ *q = ptr_cast(l); w->Copy(q->Count(), q->Begin()); }
		static void append_ucs2(UnknownWriter* w, void* l)
		{ Ucs2_ *q = ptr_cast(l); w->Copy(q->Count(), q->Begin()); }

		LazyString(LazyString const&);

	private:
		LazyString();
		void operator =(LazyString const&);
	};

	LazyString operator +(LazyString const& a, LazyString const& b) { return LazyString(a, b); }
	template < int N > void operator <<= (char(&o)[N], LazyString const& a) { a.write_to_local(o, N); }
	template < int N > void operator <<= (wchar_t (&o)[N], LazyString const& a) { a.write_to_local(o, N); }

	inline void operator <<= (Array<char>& o, LazyString const& a)
	{
		o.Resize(0);
		ArrayWriter<char> w(o);
		a.write_to(&w);
	}

	template < class T > inline
	Strbase<T>::Strbase(LazyStringBase const& a):
		__base(a.write_array((T*)0), __take_data__) {}

	inline Chars::Chars(LazyStringBase const& a)
	{ a.write_array((char*)0).Swap(*this);}

	Unicode operator ||(int, Format const& S) { return Unicode::Format(S); }
	String operator  |(int, Format const& S) { return String::Format(S); }

	template < class T > inline
	void format(TypeQuote<Strbase<T> > const& S, XprintAgent& j, char fmt, unsigned perc)
	{
		j.SprintAjust(+*S, S->Length(), fmt == 'T' ? '\'' : 0);
	}

	template < int R > inline
	void format(TypeQuote<Strarg_<R> > const& S, XprintAgent& j, char fmt, unsigned perc)
	{
		j.SprintAjust(+*S, S->Length(), fmt == 'T' ? '\'' : 0);
	}

	inline
	void format(TypeQuote<Chars> const& S, XprintAgent& j, char fmt, unsigned perc)
	{
		j.SprintAjust(+*S, S->Length(), fmt == 'T' ? '\'' : 0);
	}

	inline void format(TypeQuote<LazyString> const& S, XprintAgent& j, char fmt, unsigned perc)
	{
		S->write_to(j->Writer());
	}

	template < int R = 0 >
	struct Strarg_ : StrargBase_
	{
		Strarg_(struct NilTag) : StrargBase_(0, 0) {}
		Strarg_(pchar_t s) : StrargBase_(s, -1) {}
#if defined _USESTL
		Strarg_(std::string const& s) : StrargBase_(s.c_str(),s.length()) {}
#endif
		//Strarg_(Utf8_ const &s) : StrargBase_(+s,s.Count()) {}
		Strarg_(ConstArray<char> const& s) : StrargBase_(+s, s.Count()) {}
#define STRARG_LOCAL_BUFFER , Mutable<LocalArray<char,R> > const &buf = 0
		Strarg_(pwide_t q STRARG_LOCAL_BUFFER)
		{
			StrargBase_::t = +append_chars((Array<char>&) * buf, q);
			StrargBase_::l = buf->Count();
		}
#if defined _USESTL
		Strarg_(std::wstring const& s STRARG_LOCAL_BUFFER)
		{
			StrargBase_::t = +append_chars((Array<char>&) * buf, &s.begin()[0], s.length() );
			StrargBase_::l = buf->Count();
		}
#endif
		//Strarg_(Ucs2_ const &s STRARG_LOCAL_BUFFER)
		Strarg_(ConstArray<wchar_t> const& s STRARG_LOCAL_BUFFER)
		{
			StrargBase_::t = +append_chars((Array<char>&) * buf, +s, s.Count());
			StrargBase_::l = buf->Count();
		}
		Strarg_(Format const& s STRARG_LOCAL_BUFFER)
		{
			StrargBase_::t = +append_format((Array<char>&) * buf, s);
			StrargBase_::l = buf->Count();
		}
		Strarg_(LazyStringBase const& s STRARG_LOCAL_BUFFER)
		{
			ArrayWriter<char> w(*buf);
			s.write_to(&w);
			t = +*buf;
			l = buf->Count();
		}

		operator ByteRange() const { return byte_range(StrargBase_::t, StrargBase_::Length()); }

#undef STRARG_LOCAL_BUFFER
	private:
		Strarg_();
		Strarg_(Strarg_ const&);
		Strarg_& operator=(Strarg_ const&);
	};
	typedef Strarg_<0> const& Strarg;

	template < class T > inline
	Strbase<T>::Strbase(StrargBase_ const& a):
		__base(copy_chars(+a, a.Length(), (T*)0), __take_data__) {}

	inline
	Chars::Chars(StrargBase_ const& a)
	{ append_chars(*this, +a, a.Length()); }

	inline Chars& operator += (Chars& o, StrargBase_ const& a)
	{
		append_chars(o, +a, a.Length());
		return o;
	}

	inline int len(StrargBase_ const& q) { return q.Length(); }

	inline Chars& operator += (Chars& o, LazyString const& a)
	{
		ArrayWriter<char> w(o);
		a.write_to(&w);
		return o;
	}

	inline bool operator |(Clog_t o, Strarg_<128> const& q) { return o->Out(+q); }
	inline void operator||(Cstd_t o, Strarg_<128> const& q) { o->Print(+q); }
	inline void operator |(Cstd_t o, Strarg_<128> const& q) { o->Println(+q); }

	typedef ConstArray<char> cchars_t;

	inline bool operator == (cchars_t const& a, Strarg b) { return !strcmp(+a, +b); }
	inline bool operator != (cchars_t const& a, Strarg b) { return strcmp(+a, +b); }
	inline bool operator < (cchars_t const& a, Strarg b) { return strcmp(+a, +b) < 0; }
	inline bool operator <= (cchars_t const& a, Strarg b) { return strcmp(+a, +b) <= 0; }
	inline bool operator > (cchars_t const& a, Strarg b) { return strcmp(+a, +b) > 0; }
	inline bool operator >= (cchars_t const& a, Strarg b) { return strcmp(+a, +b) >= 0; }

	inline bool operator == (Strarg a, cchars_t const& b) { return !strcmp(+a, +b); }
	inline bool operator != (Strarg a, cchars_t const& b) { return strcmp(+a, +b); }
	inline bool operator < (Strarg a, cchars_t const& b) { return strcmp(+a, +b) < 0; }
	inline bool operator <= (Strarg a, cchars_t const& b) { return strcmp(+a, +b) <= 0; }
	inline bool operator > (Strarg a, cchars_t const& b) { return strcmp(+a, +b) > 0; }
	inline bool operator >= (Strarg a, cchars_t const& b) { return strcmp(+a, +b) >= 0; }

	inline bool operator == (cchars_t const& a, cchars_t const& b) { return !strcmp(+a, +b); }
	inline bool operator != (cchars_t const& a, cchars_t const& b) { return strcmp(+a, +b); }
	inline bool operator < (cchars_t const& a, cchars_t const& b) { return strcmp(+a, +b) < 0; }
	inline bool operator <= (cchars_t const& a, cchars_t const& b) { return strcmp(+a, +b) <= 0; }
	inline bool operator > (cchars_t const& a, cchars_t const& b) { return strcmp(+a, +b) > 0; }
	inline bool operator >= (cchars_t const& a, cchars_t const& b) { return strcmp(+a, +b) >= 0; }

	typedef Array<String> StringArray;

	template < class tIchr, class tOchr > CXX_NO_INLINE
	Array< Strbase<tOchr> > _split(tIchr const* S, pchar_t delims, tOchr*)
	{
		typedef Strbase<tOchr> Str;
		Array<Str> L;
		if (!S) return L;
		if (delims)
			for (int q = 0;;)
			{
				int j = q;
				STRICT_REQUIRE(j <= len(S));
				for (; S[j]; ++j)
					for (pchar_t d = delims; *d; ++d)
						if (S[j] == *d)
							goto l;
			l:
				L.Push(Str(S + q, j - q));
				if (S[j]) q = j + 1;
				else break;
			}
		else // split by spaces
			for (tIchr const* p = S; *p;)
			{
				if (chr_isspace(*p)) { ++p; continue; }
				tIchr const* q = p;
				while (*q && !chr_isspace(*q)) ++q;
				L.Push(Str(p, q - p));
				p = q;
			}

		return L;
	}

	inline StringArray split(Strarg S, pchar_t delims = 0)
	{ return _split(+S, delims, (char*)0); }

	template < class F > CXX_NO_INLINE
	String utf8_translate_Of(pchar_t S, int L, F f)
	{
		if (!S || !L) return String();
		if (L < 0) L = len(S);

		char local[128] = {0};
		Array<char> q(__use_local_buffer__, local);

		if (L)
		{
			ArrayWriter<char> w(q);
			while (*S)
			{
				wchar_t c = utf8_getwide(S);
				w.Putc((wchar_t)f(c));
			}
		}

		return q;
	}

	inline String lower(pchar_t s, int L) { return s ? utf8_translate_Of(s, L, towlower) : String(); }
	inline String upper(pchar_t s, int L) { return s ? utf8_translate_Of(s, L, towupper) : String(); }
	inline String lower(pchar_t s, pchar_t E) { return lower(s, E - s); }
	inline String upper(pchar_t s, pchar_t E) { return upper(s, E - s); }
	inline String lower(Strarg s) { return lower(+s, len(s)); }
	inline String upper(Strarg s) { return upper(+s, len(s)); }

	inline bool starts(Strarg a, Strarg with)
	{
		char const* p = +a, *q = +with;
		while (*q && *p)
			if (*q++ != *p++)
				return false;
		return !*q;
	}

	inline bool starts_nocase(Strarg a, Strarg with)
	{
		char const* p = +a, *q = +with;
		while (*q && *p)
			if (towlower(utf8_getwide(q)) != towlower(utf8_getwide(p)))
				return false;
		return !*q;
	}

	inline bool ends(Strarg a, Strarg with)
	{
		int aL = len(a);
		int wL = len(with);
		if (wL && aL >= wL)
			return !memcmp(+a + aL - wL, +with, wL);
		return false;
	}

	inline bool ends_nocase(Strarg a, Strarg with)
	{
		int aL = utf8_length(+a);
		int wL = utf8_length(+with);
		if (wL && aL >= wL)
			return starts_nocase(utf8_skip(+a, aL - wL), +with);
		return false;
	}

	inline bool is_equal(Strarg a, Strarg b)
	{
		char const* p = +a, *q = +b;
		while (*q && *p)
			if (*q++ != *p++)
				return false;
		return !*q && !*p;
	}

	inline bool is_equal_nocase(Strarg a, Strarg b)
	{
		char const* p = +a, *q = +b;
		while (*q && *p)
			if (tolower(*q++) != tolower(*p++))
				return false;
		return !*q && !*p;
	}

	inline String strip(Strarg a, int c = 0)
	{
		char const* S = +a;
		char const* E = +a + len(a);
		while (S != E && isspace(*S)) ++S;
		while (S != E && isspace(E[-1])) --E;
		if (c)
			while (S + 1 < E && *S == c && E[-1] == c)
			{ ++S; --E; }
		return String(S, E - S);
	}

	inline String lstrip(Strarg a, pchar_t xx = 0)
	{
		char const* S = +a;
		char const* E = +a + len(a);
		while (S != E && isspace(*S)) ++S;
		if (xx)
			while (S + 1 < E)
			{
				char const* q = xx;
				for (; *q; ++q)
					if (*q == *S)
					{ ++S; break; }
				if (!*q) break;
			}
		return String(S, E - S);
	}

	inline String rstrip(Strarg a, pchar_t xx = 0)
	{
		char const* S = +a;
		char const* E = +a + len(a);
		while (S != E && isspace(*S)) ++S;
		if (xx)
			while (S + 1 < E)
			{
				char const* q = xx;
				for (; *q; ++q)
					if (*q == E[-1])
					{ --E; break; }
				if (!*q) break;
			}
		return String(S, E - S);
	}

	inline String lstrip(Strarg S, int c = 0)
	{
		char q[2] = { (char)c, 0 };
		return lstrip(S, q);
	}

	inline String rstrip(Strarg S, int c = 0)
	{
		char q[2] = { (char)c, 0 };
		return rstrip(S, q);
	}

	inline long    as_long(pwide_t p, unsigned radix = 10)  { wchar_t* f; return wcstol(p, &f, radix); }
	inline long    as_long(pchar_t p, unsigned radix = 10)  { char* f; return strtol(p, &f, radix); }
	inline long    as_long(Strarg a, unsigned radix = 10) { char* f; return strtol(+a, &f, radix); }
	inline double  as_double(pwide_t p)  { wchar_t* f; return wcstod(p, &f); }
	inline double  as_double(pchar_t p)  { char* f; return strtod(p, &f); }
	inline double  as_double(Strarg a) { char* f; return strtod(+a, &f); }

	inline int find_char(Strarg q, int c, int from = 0, int L = -1)
	{
		if (!q) return -1;
		if (L < 0) L = len(q);
		pchar_t p = +q + from, pE = +q + L;
		for (; p < pE ; ++p)
			if (*p == c)
				return int(p - +q);
		return -1;
	}

	inline int find_rchar(Strarg q, int c, int from = -1, int L = -1)
	{
		if (!q) return -1;
		if (L < 0) L = len(q);
		if (from == -1 || from >= L)
			from = L - 1;
		pchar_t p = +q + from;
		do
			if (*p == c)
				return int(p - +q);
		while (--p >= +q);
		return -1;
	}

	inline String chop(Strarg a)
	{
		if (int l = len(a))
		{
			char const* S = +a;
			char const* E = S + l - 1;
			while (S != E && (*E & 0xc0) == 0x80) --E;
			return String(S, E - S);
		}
		return String();
	}

	inline String leftsub(Strarg a, int l)
	{
		if (l < 0)
			l = len(a) + l;
		STRICT_REQUIRE(l >= 0);
		return String(+a, l);
	}

	inline String rightsub(Strarg a, int l)
	{
		int L = len(a);
		if (l < 0)
			l = L + l;
		STRICT_REQUIRE(l >= 0);
		STRICT_REQUIRE(l <= L);
		return String(+a + l, L - l);
	}

	inline String substr(Strarg a, int left, int length)
	{
		int L = len(a);

		if (left < 0)
			left = L + left;
		STRICT_REQUIRE(left >= 0);
		STRICT_REQUIRE(left <= L);

		if (length < 0)
			length = L + length - left;
		if (length < 0)
			length = 0;

		return String(+a + left, length);
	}

	struct HexChar
	{
		operator char const* () const { return c; }
		char const* operator +() const { return c; }
		char c[5];
	};

	inline byte_t unhex_byte(void const* p, bool prefix = true, int* cnt = 0)
	{
		byte_t r = 0;
		byte_t const* c = (byte_t const*)p;
		if (prefix)
			if (*c == '0' && c[1] == 'x') c += 2;
			else if (*c == '\\' && c[1] == 'x') c += 2;
			else if (*c == '%') ++c;
		for (int i = 4; i >= 0; i -= 4, ++c)
		{
			if (*c >= '0' && *c <= '9')
				r |= (*c - '0') << i;
			else if (*c >= 'a' && *c <= 'f')
				r |= (*c - 'a' + 10) << i;
			else if (*c >= 'A' && *c <= 'F')
				r |= (*c - 'A' + 10) << i;
		}
		if (cnt) *cnt = c - (byte_t const*)p;
		return r;
	}

	// 01, 0x01, \x01, %01
	inline HexChar hex_byte(byte_t a, char prefix = 0 /*0,x,\,%*/)
	{
		static char const symbols[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		HexChar h;
		char* q = h.c;
		switch (prefix)
		{
			case 'x': *q++ = '0'; *q++ = 'x'; break;
			case '\\': *q++ = '\\'; *q++ = 'x'; break;
			case '%': *q++ = '%'; break;
			default: break;
		}
		*q++ = symbols[(a >> 4)];
		*q++ = symbols[a & 0x0f];
		*q = 0;
		return h;
	}

	inline String dot_encode(ByteRange const& b)
	{
		Array<char> q = Array<char>((char const*) + b, b.Count());
		for (int i = 0; i < q.Count(); ++i)
			if ((q[i] & 0x80)) q[i] = '.';
		return String(q, __take_data__);
	}

	inline String hex_encode(ByteRange const& b)
	{
		Array<char> q = Array<char>(b.Count()*2);		
		for (int i = 0; i < b.Count(); ++i)
		{
			HexChar hc = hex_byte(b[i]);
			q[i*2] = hc.c[0];
			q[i*2+1] = hc.c[1];
		}
		return String(q, __take_data__);
	}

	inline u32_t hashcode(Strbase<char> const& S)
	{
		return hashcode((cbyte_t*)S.Begin(),S.Count());
	}
}


