
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "require.hxx"

#if !defined _NANO_MALLOC
#define _NANO_MALLOC(x) malloc(x)
#endif
#if !defined _NANO_REALLOC
#define _NANO_REALLOC(x,s) realloc(x,s)
#endif
#if !defined _NANO_FREE
#define _NANO_FREE(x) free(x)
#endif

namespace nano
{
	enum MEMORY_OPERATION
	{
	    MEM_OP_MALLOC,
	    MEM_OP_REALLOC,
	    MEM_OP_FREE,
	    MEM_OP_STRDUP,
	    MEM_OP_GIGA_MALLOC,
	};

	void OutOfMemoryOccured(MEMORY_OPERATION memop, size_t memsz, char const* filename, int lineno = 0)
	{
		pchar_t O[] = { "malloc", "realloc", 0, "strdup", "giga_malloc" };
		char local_buffer[128] = {0};
		local_buffer | _S(128)*"out of memory occured on %s: was required %d bytes" % O[memop] % memsz;
		fatalerror(local_buffer, filename, lineno);
	}

	inline void* mem_chk_realloc(void* p, size_t memsz, char const* filename = 0, int lineno = 0)
	{
		void* Q = _NANO_REALLOC(p, memsz);
		if (!Q)
			OutOfMemoryOccured(MEM_OP_REALLOC, memsz, filename, lineno);
		return Q;
	}

	inline void* mem_chk_malloc(size_t memsz, char const* filename = 0, int lineno = 0)
	{
		void* Q = _NANO_MALLOC(memsz);
		if (!Q)
			OutOfMemoryOccured(MEM_OP_REALLOC, memsz, filename, lineno);
		return Q;
	}

	inline void* mem_chk_zmalloc(size_t memsz, char const* filename = 0, int lineno = 0)
	{
		void* Q = mem_chk_malloc(memsz, filename, lineno);
		memset(Q, 0, memsz);
		return Q;
	}

	struct GigaPageBlock
	{
		enum { BLOCK_SIZE = 64 * 1024 };
		byte_t* address;
		size_t  sbrk;
		size_t  commited;
	};

	inline void* mem_chk_giga_malloc(GigaPageBlock* pb, size_t memsz, char const* filename = 0, int lineno = 0)
	{
		void* Q;
#if SYSTEM_IS_WINDOWS
		if (pb->address && (pb->sbrk + memsz) <= GigaPageBlock::BLOCK_SIZE)
		{
			size_t to_commit = ((pb->sbrk + memsz + 0x0fff)&~0x0fff) - pb->commited;
			if (to_commit)
			{
				void *q = VirtualAlloc(
				    (void*)(((longptr_t)pb->address + pb->sbrk + 0x0fff)&~0x0fff),
				    (to_commit + 0x0fff)&~0x0fff,
				    MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				REQUIRE( q != 0 );
			}
			Q = pb->address + pb->sbrk;
			pb->commited += to_commit;
			pb->sbrk += memsz;
		}
		else
		{
			Q = VirtualAlloc(0,
			                 GigaPageBlock::BLOCK_SIZE,
			                 MEM_RESERVE, PAGE_EXECUTE_READWRITE);
			Q = VirtualAlloc(Q, (memsz + 0x0fff)&~0x0fff, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
			pb->address = (byte_t*)Q;
			pb->sbrk = memsz;
			pb->commited = (memsz + 0x0fff)&~0x0fff;
		}
		if (!Q)
			OutOfMemoryOccured(MEM_OP_GIGA_MALLOC, memsz, filename, lineno);
#else
		Q = mem_chk_malloc((memsz + 0x0fff)&~0x0fff, filename, lineno);
#endif
		return Q;
	}

	inline void* mem_chk_copy_malloc(void* p, size_t memsz, char const* filename = 0, int lineno = 0)
	{
		void* Q = mem_chk_malloc(memsz, filename, lineno);
		memcpy(Q, p, memsz);
		return Q;
	}

	inline void* mem_chk_copy(void* p, size_t count, size_t total, char const* filename = 0, int lineno = 0)
	{
		void* Q = mem_chk_malloc(total, filename, lineno);
		memcpy(Q, p, count);
		if (total > count)
			memset((char*)Q + count, 0, total - count);
		return Q;
	}

	inline char* mem_chk_strdup(char const* p, char const* filename = 0, int lineno = 0)
	{
		char* Q = strdup(p);
		if (!Q)
			OutOfMemoryOccured(MEM_OP_STRDUP, 0, filename, lineno);
		return Q;
	}

	inline void mem_free(void* p, char const* filename = 0, int lineno = 0)
	{
		if (p)
		{
			_NANO_FREE(p);
		}
	}

	inline void mem_giga_free(void* p, char const* filename = 0, int lineno = 0)
	{
		if (p)
		{
#if SYSTEM_IS_WINDOWS
			if ( (longptr_t)p & (GigaPageBlock::BLOCK_SIZE - 1) == 0)
				VirtualFree(p, 0, MEM_RELEASE);
#else
			_NANO_FREE(p);
#endif
		}
	}
}
