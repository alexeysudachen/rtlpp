/*

 (C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization of the copyright holder.

 */

#pragma once

#if ( defined (_MSC_VER) && _MSC_VER >= 1600 ) || defined(__GXX_EXPERIMENTAL_CXX0X) || __cplusplus >= 201103L
#define CXX_COMPILER_IS_cpp0x_COMPATIBLE 1
#endif

#define CXX_COMPILER_UNKNOWN    0
#define CXX_COMPILER_MSVC       1
#define CXX_COMPILER_GCC        2
#define CXX_COMPILER_ICL        5

#define CXX_COMPILER_NAME       L"unknown compiler"
#define CXX_COMPILER_VERSION    0
#define CXX_COMPILER            CXX_COMPILER_UNKNOWN

#if defined (__INTEL_COMPILER)
# undef   CXX_COMPILER_NAME
# undef   CXX_COMPILER
# undef   CXX_COMPILER_VERSION
# define  CXX_COMPILER CXX_COMPILER_ICL
# define  CXX_COMPILER_NAME "Intel C++"
# define  CXX_COMPILER_VERSION __INTEL_COMPILER
#elif defined (_MSC_VER)
# undef   CXX_COMPILER_NAME
# undef   CXX_COMPILER
# undef   CXX_COMPILER_VERSION
# define  CXX_COMPILER CXX_COMPILER_MSVC
# define  CXX_COMPILER_NAME "Microsoft C++"
# define  CXX_COMPILER_VERSION _MSC_VER
#elif defined (__GNUC__)
# undef   CXX_COMPILER_NAME
# undef   CXX_COMPILER
# undef   CXX_COMPILER_VERSION
# define  CXX_COMPILER CXX_COMPILER_GCC
# define  CXX_COMPILER_NAME "GNU C++"
# define  CXX_COMPILER_VERSION (__GNUC__*0x100+__GNUC_MINOR__)
#else
# undef   CXX_COMPILER_NAME
# define  CXX_COMPILER_NAME "Unknown C++"
#endif

#define CXX_COMPILER_IS_GXX     (CXX_COMPILER == CXX_COMPILER_GCC )
#define CXX_COMPILER_IS_MSVC    (CXX_COMPILER == CXX_COMPILER_MSVC )
#define CXX_COMPILER_IS_ICL     (CXX_COMPILER == CXX_COMPILER_ICL )

#define CXX_COMPILER_IS_MSVC_COMPATIBLE   \
	(CXX_COMPILER_IS_MSVC || CXX_COMPILER_IS_ICL )

#if CXX_COMPILER_IS_MSVC
# define CXX_COMPILER_IS_NEWMSVC (_MSC_VER >= 1400)
#else
# define CXX_COMPILER_IS_NEWMSVC 0
#endif

#if CXX_COMPILER_IS_GXX && defined (__MINGW32_VERSION)
# define CXX_COMPILER_IS_MINGW 1
#else
# define CXX_COMPILER_IS_MINGW 0
#endif

#if CXX_COMPILER_IS_MSVC && CXX_COMPILER_VERSION >= 1400
# define CXX_FAKE_INLINE inline __declspec(noinline)
# define CXX_NO_INLINE __declspec(noinline)
#else
# if CXX_COMPILER_IS_GXX
#  define CXX_FAKE_INLINE inline /*__attribute__((noinline))*/
#  define CXX_NO_INLINE __attribute__((noinline))
# else
#  define CXX_FAKE_INLINE inline
#  define CXX_NO_INLINE
# endif
#endif

#if CXX_COMPILER_IS_MSVC_COMPATIBLE
# define CXX_FASTCALL     __fastcall
# define CXX_STDCALL      __stdcall
# define CXX_FORCEINLINE  __forceinline
# define CXX_CDECL        __cdecl
#else
# if CXX_COMPILER_IS_GXX
#  define CXX_STDCALL     __attribute__((stdcall))
#  define CXX_FASTCALL    __attribute__((regparm,stdcall))
#  define CXX_CDECL       __attribute__((cdecl))
# else
#  define CXX_FASTCALL
#  define CXX_STDCALL
#  define CXX_CDECL
# endif
# define CXX_FORCEINLINE  inline
#endif

#if CXX_USES_EXCEPTIONS
# if CXX_COMPILER_IS_GXX
#   define CXX_NO_THROW __attribute__((nothrow))
# else
#   define CXX_NO_THROW throw()
# endif
#else
#  define CXX_NO_THROW
#endif

#if CXX_COMPILER_IS_MSVC_COMPATIBLE
# define CXX_THREADLOCAL_IF __thread
//# define CXX_THREADLOCAL  __declspec(thread)
#else
# if CXX_COMPILER_IS_GXX && !(__APPLE__ && __MACH__)
#  define CXX_THREADLOCAL_IF __thread
# else
#  define CXX_THREADLOCAL_IF
#  define _USETLSAPI
# endif
# define CXX_FORCEINLINE  inline
#endif

#if CXX_COMPILER_IS_MSVC_COMPATIBLE
# define CXX_NO_VTABLE __declspec(novtable)
#else
# define CXX_NO_VTABLE
#endif

#if !defined _NOEXCEPTIONS
# if CXX_COMPILER_IS_MSVC_COMPATIBLE
#  if !defined _CPPUNWIND && !__EXCEPTIONS
#   define CXX_USES_EXCEPTIONS 0
#  else
#   define CXX_USES_EXCEPTIONS 1
#  endif
# else
#  if __EXCEPTIONS
#   define CXX_USES_EXCEPTIONS 1
#  else
#   define CXX_USES_EXCEPTIONS 0
#  endif
# endif
#else /* _NOEXCEPTIONS defined */
# define CXX_USES_EXCEPTIONS 0
#endif

#if CXX_USES_EXCEPTIONS
# define CXX_TRY        try
# define CXX_CATCH_ALL  catch(...)
# define CXX_CATCH(x)   catch(x)
# define CXX_RETHROW    throw
#else
# define CXX_TRY        if (0) ; else
# define CXX_CATCH(x)   if (1) ; else
# define CXX_CATCH_ALL  if (1) ; else
# define CXX_RETHROW
#endif

#define _CXX_COMPOSE(a,b) a##b
#define _CXX_COMPOSE3(a,b,c) a##b##c

#if CXX_COMPILER_IS_MSVC
# define CXX_WIDEPART(x) _CXX_COMPOSE(L,x)
#else
# define CXX_WIDEPART(x) x
#endif

#define CXX_WIDE(x) _CXX_COMPOSE(L,x)

#if !defined _NOTHREADS
# define CXX_USES_THREADS 1
# undef  _REENTRANT
# define _REENTRANT 1
#endif

#if CXX_USES_THREADS
# define CXX_THREADLOCAL CXX_THREADLOCAL_IF
#else
# define CXX_THREADLOCAL
#endif

#if !SYSTEM_IS_WINDOWS && !SYSTEM_IS_MACOSX && !SYSTEM_IS_LINUX
# if __WIN32__ || CXX_COMPILER_IS_MSVC_COMPATIBLE || CXX_COMPILER_IS_MINGW
#   define SYSTEM_IS_WINDOWS 1
# elif __APPLE__ && __MACH__
#   define SYSTEM_IS_MACOSX 1
# elif __unix__ || __linux__ || __linux
#   define SYSTEM_IS_POSIX 1
# else
#   error "unknown platform"
# endif
#endif

#if defined _M_IX86
#   if !defined _X86_
#		define _X86_
#	endif
#   define __i386
#elif defined _M_X64
#   define __x86_64
#	if !defined _AMD64_
#		define _AMD64_
#	endif
#endif

#if SYSTEM_IS_WINDOWS
# if !defined WINVER
#   define WINVER 0x600
# endif
# include <WinDef.h>
# include <WinBase.h>
# include <excpt.h>
# include <intrin.h>
#else
# include <unistd.h>
#endif

#if SYSTEM_IS_WINDOWS
#include <malloc.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <math.h>
#include <stdarg.h>
#include <limits.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>

#if SYSTEM_IS_WINDOWS
# include <io.h>
# define snprintf _snprintf
#endif

#if CXX_COMPILER_IS_GXX
# include <libgen.h>
# if !SYSTEM_IS_WINDOWS
#   include <execinfo.h>
#   include <dlfcn.h>
# endif
# include <cxxabi.h>
#endif

#include <typeinfo>
#include <new>

#if SYSTEM_IS_WINDOWS
# include <objbase.h>
#endif

#define CXX_BIT(x) (1L<<x)
#define CXX_LABEL _CXX_COMPOSE(_teggox_label_L,__LINE__)
#define CXX_ID_(n,a) _CXX_COMPOSE3(n,L,a)
#define CXX_LOCAL_ID(n) CXX_ID_(n,__LINE__)

#if SYSTEM_IS_WINDOWS
inline void msleep(size_t ms)
{
	Sleep(ms);
}
inline void usleep(size_t us)
{
	Sleep((us + 999) / 1000);
}
#else
inline void msleep(size_t ms)
{   usleep(ms * 1000);}
#endif

size_t cxx_min(size_t a, size_t b)
{
	return a < b ? a : b;
}
size_t cxx_max(size_t a, size_t b)
{
	return a > b ? a : b;
}
size_t cxx_min_no0(size_t a, size_t b)
{
	return a && a < b ? a : b ? b : a;
}

#if SYSTEM_IS_WINDOWS
# if !defined _FILEi64
#   define _FILEi32
# endif
#endif

