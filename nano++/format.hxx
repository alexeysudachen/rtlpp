/*

 (C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization of the copyright holder.

 */

#pragma once
#include "deftypes.hxx"
#include "algo.hxx"
#include "writer.hxx"
#include "cxxsup.hxx"

namespace nano
{
	struct _SFormatter
	{
		int limit_;
		_SFormatter operator()(int limit) const
		{
			_SFormatter S = { limit };
			return S;
		}
	};

	static const _SFormatter _S = { 0 };

	struct Autoformat
	{
		void const* q;
		void (*fmt)(void const*, XprintAgent&, char fmt, unsigned perc);
	};
	template<class T> Autoformat autoformat(T const& t);

	struct Format
	{
		union
		{
			Format const* ft_;
			int limit_;
		};
		enum CONTENT_TYPE
		{
		    x1, x2, xS = 3, xW, xI, xU, xF, xP, xA, xB, xUL, xIL, xULL, xILL, xC, xLC
		} t_;
		union
		{
			char chr_;
			wchar_t wchr_;
			pchar_t sval_;
			pwide_t wval_;
			int ival_;
			unsigned uval_;
			int long ilval_;
			unsigned long ulval_;
			double fval_;
			void const* pval_;
			Autoformat auto_;
#if CXX_COMPILER_IS_MSVC_COMPATIBLE
			unsigned __int64 ullval_; __int64 illval_;
#else
			unsigned long long ullval_;
			long long illval_;
#endif
		};

		explicit inline Format(char const* fmt, int limit = 0)
			: t_(x1)
		{
			limit_ = limit;
			sval_ = fmt;
		}
		explicit inline Format(wchar_t const* fmt, int limit = 0)
			: t_(x2)
		{
			limit_ = limit;
			wval_ = fmt;
		}

		explicit inline Format(Format const* ft, bool v)
			: ft_(ft), t_(xB)
		{
			ival_ = v;
		}
		explicit inline Format(Format const* ft, pchar_t s)
			: ft_(ft), t_(xS)
		{
			sval_ = s;
		}
		explicit inline Format(Format const* ft, pwide_t w)
			: ft_(ft), t_(xW)
		{
			wval_ = w;
		}
		explicit inline Format(Format const* ft, char c)
			: ft_(ft), t_(xC)
		{
			chr_ = c;
		}
		explicit inline Format(Format const* ft, wchar_t c)
			: ft_(ft), t_(xLC)
		{
			wchr_ = c;
		}
		explicit inline Format(Format const* ft, int l)
			: ft_(ft), t_(xI)
		{
			ival_ = l;
		}
		explicit inline Format(Format const* ft, long u)
			: ft_(ft), t_(xIL)
		{
			ilval_ = u;
		}
		explicit inline Format(Format const* ft, unsigned u)
			: ft_(ft), t_(xU)
		{
			uval_ = u;
		}
		explicit inline Format(Format const* ft, float f)
			: ft_(ft), t_(xF)
		{
			fval_ = f;
		}
		explicit inline Format(Format const* ft, double f)
			: ft_(ft), t_(xF)
		{
			fval_ = f;
		}

		explicit inline Format(Format const* ft, unsigned long u)
			: ft_(ft), t_(xUL)
		{
			ulval_ = u;
		}
		explicit inline Format(Format const* ft, Autoformat const& l)
			: ft_(ft), t_(xA)
		{
			auto_ = l;
		}

		explicit inline Format(Format const* ft, u64_t u)
			: ft_(ft), t_(xULL)
		{
			ullval_ = u;
		}
		explicit inline Format(Format const* ft, i64_t u)
			: ft_(ft), t_(xILL)
		{
			illval_ = u;
		}
		inline Format operator %(u64_t const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(i64_t const& val) const
		{
			return Format(this, val);
		}

		explicit inline Format(Format const* ft, void const* p, bool _)
			: ft_(ft), t_(xP)
		{
			pval_ = p;
		}
		inline Format operator %(void const* val) const
		{
			return Format(this, val, false);
		}
		inline Format operator %(void* val) const
		{
			return Format(this, val, false);
		}
		//template < class T > inline Format operator % (T *val) const { return Format(this,(void*)val,false); }

		inline Format operator %(float const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(double const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(int const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(long const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(unsigned int const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(unsigned long const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(char val[]) const
		{
			return Format(this, (char const*) val);
		}
		inline Format operator %(wchar_t val[]) const
		{
			return Format(this, (wchar_t const*) val);
		}
		inline Format operator %(char const val[]) const
		{
			return Format(this, (char const*) val);
		}
		inline Format operator %(wchar_t const val[]) const
		{
			return Format(this, (wchar_t const*) val);
		}
		inline Format operator %(char const& val) const
		{
			return Format(this, val);
		}
		inline Format operator %(wchar_t const& val) const
		{
			return Format(this, val);
		}

		template<class tIv> CXX_NO_INLINE
		static bool _FmtNumber(XprintAgent& xa, tIv val, char fmt, unsigned perc)
		{
			switch (fmt)
			{
				case 'x':
					xa.SprintXlower(val);
					break;
				case 'X':
					xa.SprintXupper(val);
					break;
				case 'b':
					xa.SprintBinary(val);
					break;
				case 'o':
					xa.SprintOctet(val);
					break;
				case 'f':
				case 'g':
					xa.SprintFloat(val, perc);
					break;
				case 'u':
					xa.SprintUnsigned(val);
					break;
				case 'd':
					xa.SprintSigned(val);
					break;
				case 'c':
					xa.SprintChar((char) val);
					break;
				case 'C':
					xa.SprintChar((wchar_t) val);
					break;
				default:
					return false;
			}
			return true;
		}

		template<class tIv>
		static void _FmtChr(XprintAgent& xa, tIv val, char fmt, unsigned perc)
		{
			if (!_FmtNumber(xa, val, fmt, perc))
			{
				if (val < 30)
					val = '.';
				xa.SprintAjust(&val, 1, fmt == 'T' ? '\'' : 0);
			}
		}

		template<class tIv>
		static void _FmtInt(XprintAgent& xa, tIv val, char fmt, unsigned perc)
		{
			if (!_FmtNumber(xa, val, fmt, perc))
				xa.SprintSigned(val);
		}

		template<class tIv>
		static void _FmtUint(XprintAgent& xa, tIv val, char fmt, unsigned perc)
		{
			if (!_FmtNumber(xa, val, fmt, perc))
				xa.SprintUnsigned(val);
		}

		static void _FmtBool(XprintAgent& xa, unsigned val, char fmt, unsigned perc)
		{
			if (!_FmtNumber(xa, val, fmt, perc))
				xa.SprintStringC((val ? "true" : "false"), perc);
		}

		static void _FmtFloat(XprintAgent& xa, double val, char fmt, unsigned perc)
		{
			if (fmt == 'g' || fmt == 'f' || !_FmtNumber(xa, (int) val, fmt, perc))
				xa.SprintFloat(val, perc ? perc : 3);
		}

		static CXX_NO_INLINE void _FmtPtr(XprintAgent& xa, void const* val, char fmt, int limit = 0)
		{
			if (!_FmtNumber(xa, (longptr_t) val, fmt, 0))
				switch (fmt)
				{
					case '@':
					case 'Z':
					case 'B':
					case 'H':
					case 'W':
					case 'Q':
						xa.SprintMemory(fmt, val, limit);
						break;
					default:
						xa.SprintPointer(val);
				}
		}

		static CXX_NO_INLINE void _FmtUtf8(XprintAgent& xa, char const* val, char fmt, int limit)
		{
			if (fmt == 's' || fmt == 'S' || fmt == '?' || fmt == 'T')
				xa.SprintStringC(val, limit, fmt == 'T' ? '\'' : 0);
			else if (fmt == 'a')
				xa.SprintStringMaskC(val, limit);
			else
				_FmtPtr(xa, val, fmt, limit);
		}

		static CXX_NO_INLINE void _FmtUcs2(XprintAgent& xa, wchar_t const* val, char fmt, int limit)
		{
			if (fmt == 's' || fmt == 'S' || fmt == '?' || fmt == 'T')
				xa.SprintStringW(val, limit, fmt == 'T' ? '\'' : 0);
			else if (fmt == 'a')
				xa.SprintStringMaskW(val, limit);
			else
				_FmtPtr(xa, val, fmt, limit);
		}

		CXX_NO_INLINE bool OutValue(XprintAgent& xa, unsigned perc, char fmt) const
		{
			switch (t_)
			{
				case Format::x1:
					break;
				case Format::x2:
					break;
				case Format::xS:
					_FmtUtf8(xa, sval_, fmt, perc);
					break;
				case Format::xW:
					_FmtUcs2(xa, wval_, fmt, perc);
					break;
				case Format::xI:
					_FmtInt(xa, ival_, fmt, perc);
					break;
				case Format::xU:
					_FmtUint(xa, uval_, fmt, perc);
					break;
				case Format::xIL:
					_FmtInt(xa, ilval_, fmt, perc);
					break;
				case Format::xUL:
					_FmtUint(xa, ulval_, fmt, perc);
					break;
				case Format::xILL:
					_FmtInt(xa, illval_, fmt, perc);
					break;
				case Format::xULL:
					_FmtUint(xa, ullval_, fmt, perc);
					break;
				case Format::xF:
					_FmtFloat(xa, fval_, fmt, perc);
					break;
				case Format::xP:
					_FmtPtr(xa, pval_, fmt, perc);
					break;
				case Format::xB:
					_FmtBool(xa, ival_, fmt, perc);
					break;
				case Format::xC:
					_FmtChr(xa, chr_, fmt, perc);
					break;
				case Format::xLC:
					_FmtChr(xa, wchr_, fmt, perc);
					break;
				case Format::xA:
					auto_.fmt(auto_.q, xa, fmt, perc);
					break;
			}
			return true;
		}

		CXX_NO_INLINE int FormatOut(UnknownWriter* wr) const
		{
			struct XarglistTno: XsprintArglist
			{
				mutable Format const** a_;
				virtual bool OutValue(XprintAgent& xa, unsigned perc, char fmt, int long_fmt = 0) const
				{
					if (*a_)
						(*a_++)->OutValue(xa, perc, fmt);
					return true;
				}
				XarglistTno(Format const** a)
					: a_(a)
				{
				}
			};

			unsigned no = 0;
			Format const* f = this;
			for (; f->t_ >= xS; f = f->ft_)
				++no;
			Format const** a = (Format const**) _alloca((no + 1) * sizeof(Format*));
			f = this;
			for (unsigned i = 1; i <= no; ++i, f = f->ft_)
				a[no - i] = f;
			a[no] = 0;
			if (f->t_ == Format::x1)
				return abs(VsprintF(wr, f->sval_, XarglistTno(a)));
			if (f->t_ == Format::x2)
				return abs(VsprintF(wr, f->wval_, XarglistTno(a)));
			return 0;
		}

		int Limit() const
		{
			Format const* f = this;
			while (f->t_ != Format::x1 && f->t_ != Format::x2)
				f = f->ft_;
			return f->limit_;
		}
	};

	//inline Format operator %(Format const &S, TypeName const &t) { return S % t.Cstr(); }

	inline Format operator *(_SFormatter const& S, char const* fmt)
	{
		return Format(fmt, S.limit_);
	}
	inline Format operator *(_SFormatter const& S, wchar_t const* fmt)
	{
		return Format(fmt, S.limit_);
	}

	inline int operator |(ExactType<char*> b, Format const& S)
	{
		RawMemWriter<char> wr(*b, S.Limit());
		return S.FormatOut(&wr);
	}
	inline char* operator <<= (ExactType<char*> b, Format const& S)
	{   return operator |(b, S), *b; }

	inline int operator |(ExactType<wchar_t*> b, Format const& S)
	{
		RawMemWriter<wchar_t> wr(*b, S.Limit());
		return S.FormatOut(&wr);
	}
	inline wchar_t* operator <<= (ExactType<wchar_t*> b, Format const& S)
	{   return operator |(b, S), *b; }

	template<class T, int N> CXX_NO_INLINE
	inline int operator |(T(&b)[N], Format const& S)
	{
		int l = S.Limit();
		if (!l || l >= N)
			l = N-1;
		if ( !l )
		{
			return 0;
		}
		else
		{
			RawMemWriter<T> wr(b, l);
			b[l] = 0;
			return S.FormatOut(&wr);
		}
	}

	template<class T, int N>
	inline T* operator <<= (T(&b)[N], Format const& S)
	{   return operator |(b, S), b; }

	inline void format(TypeName t, XprintAgent& j, char, unsigned)
	{
		pchar_t tname = t.Cstr();
		j->Copy(strlen(tname), tname);
	}

	template<class T> inline void format(TypeQuote<T*> p, XprintAgent& j, char fmt, unsigned perc)
	{
		Format::_FmtPtr(j, *p, fmt, perc);
	}

	template<class T> struct _autoformat_F
	{
		static void f(void const* q, XprintAgent& j, char fmt, unsigned perc)
		{
			format(TypeQuote<T>((T*) q), j, fmt, perc);
		}
	};

	template<class T> inline Autoformat autoformat(T const& q)
	{
		Autoformat a = { &q, &_autoformat_F<T>::f };
		return a;
	}

	template<class T> inline Format operator %(Format const& f, T const& b)
	{
		return Format(&f, autoformat(b));
	}

	inline Format const& S_format_any()
	{
		static Format S = _S * "%?";
		return S;
	}
	inline Format const& S_format_type()
	{
		static Format S = _S * "%T";
		return S;
	}

	template<class T> Format repr(T const& t)
	{
		return S_format_any() % t;
	}
	template<class T> Format operator /(_SFormatter const& S, T const& t)
	{
		return repr(t);
	}
	template<class T> Format operator &(_SFormatter const& S, T const& t)
	{
		return repr(t);
	}
}

