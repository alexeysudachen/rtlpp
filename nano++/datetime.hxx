
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "string.hxx"

namespace nano
{

	struct LongDate
	{
#if !defined _BINENDIAN
		u32_t segundes: 16;
		u32_t day:   5;
		u32_t month: 4;
		u32_t year:  7;
#else
		u32_t year:  7;
		u32_t month: 4;
		u32_t day:   5;
		u32_t segundes: 16;
#endif
		u32_t Hour() const { return segundes * 2 / 3600; }
		u32_t Minute() const { return (segundes * 2 % 3600) / 60; }
		u32_t Segundo() const { return segundes * 2 % 60; }
	};

	NANO_STATIC_CHECK(sizeof(LongDate) == 4);

	struct DateTime
	{

		enum DT_NOW { now };
		enum DT_NOTIME { notime };
		enum DT_INFINITY { infinity };
		enum DT_RAW { raw };

		enum { CC_BARIER = 0 };

		i32_t  millis;

		struct
		{
#if !defined _BIGENDIAN
			u32_t day:   5;
			u32_t month: 4;
			u32_t year:  22;
			u32_t cc:    1;
#else
			u32_t cc:    1;
			u32_t year:  22;
			u32_t month: 4;
			u32_t day:   5;
#endif
		} dmyc;

		u32_t _first_word() const { return (u32_t)millis; }
		u32_t _second_word() const
		{
			u32_t q = dmyc.year;
			q = q << 5 | dmyc.day;
			q = q << 4 | dmyc.month;
			q = q << 1 | dmyc.cc;
			return q;
		}

		void _from_two_words(u32_t first, u32_t second)
		{
			millis = (i32_t)first;
			dmyc.cc = second & 1; second >>= 1;
			dmyc.month = second & 15; second >>= 4;
			dmyc.day = second & 31; second >>= 5;
			dmyc.year = second;
		}

		u16_t ToL16date()
		{
			return u16_t(((CcYear() - 1970) % 128) << 9 | dmyc.month << 5 | dmyc.day);
		}

		static DateTime FromL16date(u16_t q)
		{
			DateTime d = raw;
			d.millis = 0;
			d.dmyc.cc = 1;
			d.dmyc.day   = q & 31; q >>= 5;
			d.dmyc.month = q & 15; q >>= 4;
			d.dmyc.year  = q + 1970;
			return d;
		}

		u32_t ToL32date()
		{
			u32_t q = ((CcYear() - 1970) % 128) << 25 | dmyc.month << 21 | dmyc.day << 16 | (millis / 2000 % 3600 * 12);
			return q;
		}

		static inline u32_t MakeL32date(unsigned day, unsigned month, unsigned year, unsigned millis = 0)
		{
			u32_t q = ((year - 1970) % 128) << 25 | (month & 15) << 21 | (day & 31) << 16 | (millis / 2000 % 3600 * 12);
			return q;
		}

		LongDate ToLongDate()
		{
			u32_t q = ToL32date();
			return *(LongDate*)&q;
		}

		static inline LongDate MakeLongDate(unsigned day, unsigned month, unsigned year, unsigned millis = 0)
		{
			u32_t q = MakeL32date(day, month, year, millis);
			return *(LongDate*)&q;
		}

		static DateTime FromL32date(u32_t q)
		{
			DateTime d = raw;
			d.dmyc.cc = 1;
			d.millis     = (q & 0x0ffff) * 2000; q >>= 16;
			d.dmyc.day   = q & 31; q >>= 5;
			d.dmyc.month = q & 15; q >>= 4;
			d.dmyc.year  = q + 1970;
			return d;
		}

		static DateTime FromLongDate(LongDate ld)
		{
			return FromL32date(*(u32_t*)&ld);
		}

		unsigned Day()   const { return dmyc.day; }
		unsigned Month() const { return dmyc.month; }
		unsigned Year()  const { return dmyc.cc ? CcYear() : -CcYear();}
		signed   CcYear()const { return dmyc.year; }
		bool     AD()    const { return !!dmyc.cc; }
		bool     BC()    const { return !dmyc.cc; }

		pwide_t Smon(bool shortform = true) const
		{
			static pwide_t m[12][2] =
			{
				{L"Jan", L"January"},
				{L"Feb", L"February"},
				{L"Mar", L"March"},
				{L"Apr", L"April"},
				{L"May", L"May"},
				{L"Jun", L"June"},
				{L"Jul", L"July"},
				{L"Aug", L"August"},
				{L"Sep", L"September"},
				{L"Oct", L"October"},
				{L"Nov", L"November"},
				{L"Dec", L"December"},
			};

			if (millis == -1)   // is not valid date
				return shortform ? L"Inv" : L"Invalid";

			unsigned month = Month();
			REQUIRE(month > 0 && month <= 12);
			return m[month - 1][shortform ? 0 : 1];
		}

		pwide_t Lmon() const
		{
			return Smon(false);
		}

		DateTime& Day(unsigned d)
		{
			REQUIRE(d > 0 && d <= 31);
			dmyc.day = d; return *this;
		}

		DateTime& Month(unsigned m)
		{
			REQUIRE(m > 0 && m <= 12);
			dmyc.month = m; return *this;
		}

		DateTime& Year(signed y)
		{
			y < 0 ? dmyc.year = -y, dmyc.cc = 0 : dmyc.year = y, dmyc.cc = 1;
			return *this;
		}

		DateTime& Time(unsigned h, unsigned m, unsigned s, unsigned u)
		{
			millis = h * 60 * 60 * 1000 + m * 60 * 1000 + s * 1000 + u;
			return *this;
		}

		enum { USECONDS_COUNT = 1 };

		unsigned Hour() const   { return (millis / (60 * 60 * 1000 * USECONDS_COUNT)) % 24; }
		unsigned Minute() const { return (millis / (60 * 1000 * USECONDS_COUNT)) % 60; }
		unsigned Segundo() const { return (millis / (1000 * USECONDS_COUNT)) % 60; }
		unsigned Millis() const { return (millis / USECONDS_COUNT) % 1000; }

		DateTime(DT_RAW) {}
		DateTime(DT_NOTIME) { millis = -1; memset(&dmyc, 0xff, sizeof(dmyc)); dmyc.cc = 0; }
		DateTime(DT_INFINITY) { millis = -1; memset(&dmyc, 0xff, sizeof(dmyc)); }
		DateTime(LongDate d) { *this = FromLongDate(d); }

		bool Isnotime() { return millis == -1 && !dmyc.cc; }
		bool Isinfinity() { return millis == -1 && dmyc.cc; }

		DateTime()
		{
			memset(&dmyc, 0, sizeof(dmyc));
			millis = 0;
		}

		DateTime(DT_NOW)
		{
			InitWholeFromPOSIXtime(time(0));
		}

		DateTime(time_t const& t)
		{
			InitWholeFromPOSIXtime(t);
		}

		DateTime ShiftYear(int y)
		{
			if (millis == -1)
				return *this;
			DateTime d = *this;
			d.Year(d.Year() + y);
			return d;
		}

		void InitWholeFromPOSIXtime(time_t t)
		{
			if (tm* xtm = localtime(&t))
			{
				tm tm = *xtm;
				Year(tm.tm_year + 1900);
				Month(tm.tm_mon + 1);
				Day(tm.tm_mday);
				Time(tm.tm_hour, tm.tm_min, tm.tm_sec, 0);
			}
			else
				*this = notime;
		}

		static DateTime FromPOSIXtime(time_t t)
		{
			DateTime dt(raw);
			dt.InitWholeFromPOSIXtime(t);
			return dt;
		}

		static DateTime FromCstring(Strarg cStr)
		{
			DateTime dt(raw);
			dt.InitWholeFromCstr(cStr);
			return dt;
		}

		void InitWholeFromCstr(Strarg cStr)
		{
			unsigned day, mon, h, m, s, u;
			signed year;
			char b, c;

			// M$VC swscanf isn't able to use constant strings
			sscanf(+cStr, "%d-%d-%d %c%c %d:%d:%d %d",
			       &day, &mon, &year, &b, &c, &h, &m, &s, &u);

			if (b == 'b' || b == 'B') year = -year;
			Day(day).Month(mon).Year(year).Time(h, m, s, u);
		}

		enum { PRINT_BUFFER_REQUIRED = 64 };

		char* SprintTo(char* local_buffer, int& l, bool show_time = true, bool ad_bc = false) const
		{
			int L = PRINT_BUFFER_REQUIRED;
			l = local_buffer | _S(L)*"%02d-%02d-%04d" % Day() % Month() % CcYear();
			if (ad_bc)
				l += local_buffer + l | _S(L - l)*" %s" % (BC() ? "BC" : "AD");
			if (show_time)
				l += local_buffer + l | _S(L - l)*" %02d:%02d:%02d" % Hour() % Minute() % Segundo();
			return local_buffer;
		}

		String Str(bool show_time = true, bool ad_bc = false) const
		{
			int l = 0;
			char local_buffer[PRINT_BUFFER_REQUIRED] = {0};
			return String(SprintTo(local_buffer, l, show_time, ad_bc), l);
		}

		void FormatTo(XprintAgent& j, char fmt, unsigned perc) const
		{
			int l = 0;
			char local_buffer[PRINT_BUFFER_REQUIRED] = {0};
			SprintTo(local_buffer, l);
			j->Copy(l, local_buffer);
		}

		time_t PosixTime()
		{
			tm tm;
			tm.tm_year = Year() - 1900;
			tm.tm_mon = Month() - 1;
			tm.tm_mday = Day();
			tm.tm_hour = Hour();
			tm.tm_min = Minute();
			tm.tm_sec = Segundo();
			return mktime(&tm);
		}
	};

	template <> struct ConstRef<DateTime> { typedef DateTime const Type; enum { ByRef = 0 }; };
	template <> struct ConstRef<LongDate> { typedef LongDate const Type; enum { ByRef = 0 }; };

	inline void format(TypeQuote<DateTime> q, XprintAgent& j, char fmt, unsigned perc)
	{ q->FormatTo(j, fmt, perc); }
	inline void format(TypeQuote<LongDate> q, XprintAgent& j, char fmt, unsigned perc)
	{ DateTime(*q).FormatTo(j, fmt, perc); }

	//date_Of*<m>/<d>/<y>
	//date_Of*11/24/2009
	//date_Of*<d>-<m>-<y>
	//date_Of*24-11-2009

	struct date_Of { byte_t none; };
	struct date_Of_0 { int d; date_Of_0(int d) : d(d) {} };
	struct date_Of_Us { int day; int month; date_Of_Us(int d, int m) : day(d), month(m) {} };
	struct date_Of_Eu : date_Of_Us { date_Of_Eu(int d, int m) : date_Of_Us(m, d) {} };
	inline LongDate operator /(date_Of_Us const& n, int y) { return DateTime::MakeLongDate(n.day, n.month, y); }
	inline LongDate operator -(date_Of_Eu const& n, int y) { return DateTime::MakeLongDate(n.day, n.month, y); }
	inline date_Of_Us operator /(date_Of_0 const& n, int u) { return date_Of_Us(u, n.d); }
	inline date_Of_Eu operator -(date_Of_0 const& n, int u) { return date_Of_Eu(u, n.d); }
	inline date_Of_0  operator *(date_Of const&, int d) { return date_Of_0(d); }

	struct date_Of date_Of = {0};

	struct SecondsTag { char _; };
	const struct SecondsTag seconds = {0};
	struct MilliSecondsTag { char _; };
	const struct MilliSecondsTag milliseconds = {0};
	struct MicroSecondsTag { char _; };
	const struct MicroSecondsTag microseconds = {0};

	struct Timeout {
		u64_t timeout;
		Timeout(u64_t val) : timeout(val) {};
		u64_t MicroSeconds() const { return timeout; }
		u64_t MilliSeconds() const { return timeout/1000; }
		u64_t Seconds() const { return timeout/1000000; }
	};

	// 0.5^seconds
	Timeout  operator ^(double value,SecondsTag)   { return Timeout((u64_t)(value * 1000000)); }
	// 3^seconds
	Timeout  operator ^(int value,SecondsTag)      { return Timeout((u64_t)value * 1000000); }
	// 3000^milliseconds
	Timeout  operator ^(int value,MilliSecondsTag) { return Timeout((u64_t)value * 1000); }
	// 3000000^microseconds
	Timeout  operator ^(int value,MicroSecondsTag) { return Timeout((u64_t)value); }

} // namespace

