
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "compiler.hxx"

namespace nano
{

	inline unsigned bitcount_8(unsigned q)
	{
		static byte_t Q[256] = {0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
		                        5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
		                        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
		                        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
		                        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		                        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		                        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		                        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
		                       };
		return Q[q & 0x0ff];
	}

	template < class T >
	unsigned bitcount(T u)
	{
		if (u)
			for (int i = sizeof(u) * 8 - 8; i >= 0; i -= 8)
				if (unsigned l = bitcount_8(u >> i))
					return l + i;
		return 0;
	}

	inline size_t min_pow2(size_t a)
	{
		if (a) --a;
		return 1 << bitcount(a);
	}

	inline size_t CXX_NO_INLINE align(size_t a, size_t mod)
	{
		if (!mod) mod = 1;
		size_t Q = min_pow2(mod) - 1;
		return (a + Q)&~Q;
	}

	template < class T >
	inline void swap(T& a, T& b)
	{
		T c = b;
		b = a;
		a = c;
	}

	template < class T > inline T abs(T a) { return a >= 0 ? a : -a; }
	template < class T > inline T neg2zero(T a) { return a > 0 ? a : 0; }

	inline CXX_NO_INLINE bool full_write(int fd, void const* data, size_t count)
	{
		byte_t const* d = (byte_t const*)data;
		while (count)
		{
			int q = write(fd, d, count);
			if (q < 0) return false;
			count -= q;
			d += q;
		}
		return true;
	}

	inline CXX_NO_INLINE bool full_write(FILE* fd, void const* data, size_t count)
	{
		byte_t const* d = (byte_t const*)data;
		while (count)
		{
			int q = fwrite(d, 1, count, fd);
			if (!q && ferror(fd)) return false;
			count -= q;
			d += q;
		}
		return true;
	}

	inline int pop_bits(ulong_t* r, void const* b, int* bits_count, int count)
	{
#if 1
		byte_t const* bits = (byte_t const*)b;
		int bC = *bits_count - 1;
		if (bC < 0) return 0;
		int Q = cxx_min(count, bC + 1);
		int r_count = Q;

		while (((bC + 1) & 7) && Q)
		{
			*r = (*r << 1) | ((bits[bC / 8] >> (bC % 8)) & 1);
			--bC; --Q;
		}

		while (bC >= 0 && Q)
			if (Q > 7)
			{
				*r = (*r << 8) | bits[bC / 8];
				Q -= 8; bC -= 8;
			}
			else
			{
				*r = (*r << Q) | bits[bC / 8] >> (8 - Q);
				bC -= Q; Q = 0;
			}

		*bits_count = bC + 1;
		return r_count;
#else
		int r_count = 0;
		byte_t* bits = (byte_t*)b;
		while (count && *bits_count)
		{
			int q = *bits_count - 1;
			*r = (*r << 1) | ((bits[q / 8] >> (q % 8)) & 1);
			--*bits_count;
			--count;
			++r_count;
		}
		return r_count;
#endif
	}

	inline void push_bits(ulong_t bits, void* b, int* bits_count, int count)
	{
		while (count--)
		{
			int q = *bits_count;
			byte_t* d = ((byte_t*)b + q / 8);
			*d = ((bits & 1) << (q % 8)) | *d&~(1 << (q % 8));
			++*bits_count;
			bits >>= 1;
		}
	}

}

