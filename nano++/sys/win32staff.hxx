
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#if SYSTEM_IS_WINDOWS

#include "../deftypes.hxx"
#include "../string.hxx"
#include <winbase.h>

namespace nano
{
	inline CXX_NO_INLINE String winerror_format(u32_t err)
	{
		wchar_t* b = 0;
		int len = FormatMessageW(
		              FORMAT_MESSAGE_ALLOCATE_BUFFER |
		              FORMAT_MESSAGE_FROM_SYSTEM |
		              FORMAT_MESSAGE_IGNORE_INSERTS,
		              NULL,
		              err,
		              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		              (LPWSTR)&b,
		              0,
		              NULL);
		if (len)
		{
			String R = String(b);
			LocalFree(b);
			return strip(R);
		}
		else
			return String();
	}

	inline CXX_NO_INLINE bool is_WOW64()
	{
#if defined __x86_64 
		return 0;
#else
		static int system_is = 0;
		if (!system_is)
		{
			int (__stdcall * fIsWow64Process)(HANDLE, int*) = 0;
			int is_wow64 = 0;
			(void*&)fIsWow64Process = GetProcAddress(GetModuleHandleA("kernel32.dll"),"IsWow64Process");
			if (fIsWow64Process && fIsWow64Process(GetCurrentProcess(), &is_wow64) && is_wow64)
				system_is = 64;
			else
				system_is = 32;
		}
		return system_is == 64;
#endif
	}

	void relax(Timeout tm)
	{
		Sleep(tm.MilliSeconds());
	}
}

#ifdef __x86_64
#define IS_WINKERNEL_64 1
#else
#define IS_WINKERNEL_64 (::nano::is_WOW64())
#endif


#endif // SYSTEM_IS_WINDOWS



