
/*

(C)2013, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#if SYSTEM_IS_WINDOWS

#include "../deftypes.hxx"
#include <winternl.h>

namespace nano
{
	typedef struct _LPC_MESSAGE_HEADER32
	{
		USHORT                  DataLength;
		USHORT                  TotalLength;
		USHORT                  MessageType;
		USHORT                  DataInfoOffset;
		ULONG                   ProcessId;
		ULONG                   ThreadId;
		ULONG                   MessageId;
		ULONG                   CallbackId;
	} LPC_MESSAGE_HEADER32, *PLPC_MESSAGE_HEADER32;

	typedef struct _LPC_MESSAGE_HEADER64
	{
		USHORT                  DataLength;
		USHORT                  TotalLength;
		USHORT                  MessageType;
		USHORT                  DataInfoOffset;
		ULONGLONG               ProcessId;
		ULONGLONG               ThreadId;
		ULONGLONG               MessageId;
		ULONGLONG               CallbackId;
	} LPC_MESSAGE_HEADER64, *PLPC_MESSAGE_HEADER64;

	typedef enum _LPC_TYPE
	{
	    LPC_NEW_MESSAGE,	// A new message
	    LPC_REQUEST,		// A request message
	    LPC_REPLY,			// A reply to a request message
	    LPC_DATAGRAM,		//
	    LPC_LOST_REPLY,		//
	    LPC_PORT_CLOSED,	// Sent when port is deleted
	    LPC_CLIENT_DIED,	// Messages to thread termination ports
	    LPC_EXCEPTION,		// Messages to thread exception port
	    LPC_DEBUG_EVENT,	// Messages to thread debug port
	    LPC_ERROR_EVENT,	// Used by ZwRaiseHardError
	    LPC_CONNECTION_REQUEST // Used by ZwConnectPort
	} LPC_TYPE;

	typedef enum _PORT_INFORMATION_CLASS
	{
	    PortNoInformation
	} PORT_INFORMATION_CLASS, *PPORT_INFORMATION_CLASS;

#ifdef __x86_64
	typedef LPC_MESSAGE_HEADER64 LPC_MESSAGE_HEADER;
	typedef LPC_MESSAGE_HEADER64* PLPC_MESSAGE_HEADER;
#else
	typedef LPC_MESSAGE_HEADER32 LPC_MESSAGE_HEADER;
	typedef LPC_MESSAGE_HEADER32* PLPC_MESSAGE_HEADER;
#endif

	typedef struct _LPC_TERMINATION_MESSAGE
	{
		LPC_MESSAGE_HEADER      Header;
		LARGE_INTEGER           CreationTime;
	} LPC_TERMINATION_MESSAGE, *PLPC_TERMINATION_MESSAGE;

	typedef struct _LPC_SECTION_MEMORY
	{
		ULONG                   Length;
		ULONG                   ViewSize;
		PVOID                   ViewBase;
	} LPC_SECTION_MEMORY, *PLPC_SECTION_MEMORY;

	typedef struct _LPC_SECTION_MEMORY32
	{
		ULONG                   Length;
		ULONG                   ViewSize;
		ULONG                   ViewBase;
	} LPC_SECTION_MEMORY32, *PLPC_SECTION_MEMORY32;

	typedef struct _LPC_SECTION_MEMORY64
	{
		ULONGLONG               Length;
		ULONGLONG               ViewSize;
		ULONGLONG               ViewBase;
	} LPC_SECTION_MEMORY64, *PLPC_SECTION_MEMORY64;

	struct LPC_SECTION_OWNER_MEMORY
	{
		SIZE_T                  Length;
		HANDLE                  SectionHandle;
		SIZE_T                  OffsetInSection;
		SIZE_T                  ViewSize;
		PVOID                   ViewBase;
		PVOID                   OtherSideViewBase;
	};

	template <int cookie> struct NativeAPI_
	{
		static NativeAPI_<cookie>* instance;
		static NativeAPI_<cookie> instance_data;

		long(__stdcall* fnCreateWaitablePort)(
		    HANDLE*              PortHandle,
		    OBJECT_ATTRIBUTES*   ObjectAttributes,
		    ULONG                MaxConnectInfoLength,
		    ULONG                MaxDataLength,
		    ULONG                MaxPoolUsage
		);

		long(__stdcall* fnConnectPort)(
		    HANDLE*                      ClientPortHandle,
		    UNICODE_STRING*              ServerPortName,
		    SECURITY_QUALITY_OF_SERVICE* SecurityQos,
		    LPC_SECTION_OWNER_MEMORY*    ClientSharedMemory,
		    LPC_SECTION_MEMORY*          ServerSharedMemory,
		    ULONG*                       MaximumMessageLength,
		    VOID*                        ConnectionInfo,
		    ULONG*                       ConnectionInfoLength
		);

		long(__stdcall* fnListenPort)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  ConnectionRequest
		);

		long(__stdcall* fnAcceptConnectPort)(
		    HANDLE*                   ServerPortHandle,
		    VOID*                     PortContext,
		    LPC_MESSAGE_HEADER*       ConnectionMsg,
		    BOOLEAN                   AcceptConnection,
		    LPC_SECTION_OWNER_MEMORY* ServerSharedMemory,
		    LPC_SECTION_MEMORY*       ClientSharedMemory
		);

		long(__stdcall* fnCompleteConnectPort)(HANDLE PortHandle);

		long(__stdcall* fnReplyWaitReceivePort)(
		    HANDLE               PortHandle,
		    VOID**               PortContext,
		    LPC_MESSAGE_HEADER*  Reply,
		    LPC_MESSAGE_HEADER*  IncomingRequest
		);

		long(__stdcall* fnReplyWaitReplyPort)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Reply
		);

		long(__stdcall* fnRequestPort)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Request
		);

		long(__stdcall* fnRequestWaitReplyPort)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Request,
		    LPC_MESSAGE_HEADER*  IncomingReply
		);

		long(__stdcall* fnWriteRequestData)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Request,
		    ULONG                DataIndex,
		    VOID*                Buffer,
		    ULONG                Length,
		    ULONG*               ResultLength
		);

		long(__stdcall* fnReadRequestData)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Request,
		    ULONG                DataIndex,
		    VOID*                Buffer,
		    ULONG                Length,
		    ULONG*               ResultLength
		);

		long(__stdcall* fnReplyPort)(
		    HANDLE               PortHandle,
		    LPC_MESSAGE_HEADER*  Reply
		);

		long(__stdcall* fnMapViewOfSection)(
		    HANDLE               SectionHandle,
		    HANDLE               ProcessHandle,
		    PVOID*               BaseAddress,
		    ULONG_PTR            ZeroBits,
		    SIZE_T               CommitSize,
		    PLARGE_INTEGER       SectionOffset,
		    PSIZE_T              ViewSize,
		    unsigned             InheritDisposition,
		    ULONG                AllocationType,
		    ULONG                Win32Protect);

		long(__stdcall* fnUnmapViewOfSection)(
		    HANDLE               ProcessHandle,
		    PVOID                BaseAddress);

		long(__stdcall* fnCreateSection)(
		    HANDLE*              Handle,
		    ULONG                Access,
		    OBJECT_ATTRIBUTES*   ObjectAttributes,
		    LARGE_INTEGER*       MaxSize,
		    ULONG                PageAttributes,
		    ULONG                SectionAttributes,
		    HANDLE               FileHandle);

		long(__stdcall* fnClose)(HANDLE Handle);
		void(__stdcall* fnInitUnicodeString)(
		    PUNICODE_STRING     DestinationString,
		    PCWSTR              SourceString
		);

		static CXX_NO_INLINE NativeAPI_<cookie>* Initialize()
		{
			if (!instance)
			{
				HMODULE ntDLL = GetModuleHandleA("ntdll");
				(void*&)instance_data.fnAcceptConnectPort = GetProcAddress(ntDLL, "NtAcceptConnectPort");
				(void*&)instance_data.fnCompleteConnectPort = GetProcAddress(ntDLL, "NtCompleteConnectPort");
				(void*&)instance_data.fnCreateWaitablePort = GetProcAddress(ntDLL, "NtCreateWaitablePort");
				(void*&)instance_data.fnListenPort = GetProcAddress(ntDLL, "NtListenPort");
				(void*&)instance_data.fnReadRequestData = GetProcAddress(ntDLL, "NtReadRequestData");
				(void*&)instance_data.fnReplyPort = GetProcAddress(ntDLL, "NtReplyPort");
				(void*&)instance_data.fnReplyWaitReceivePort = GetProcAddress(ntDLL, "NtReplyWaitReceivePort");
				(void*&)instance_data.fnReplyWaitReplyPort = GetProcAddress(ntDLL, "NtReplyWaitReplyPort");
				(void*&)instance_data.fnRequestPort = GetProcAddress(ntDLL, "NtRequestPort");
				(void*&)instance_data.fnRequestWaitReplyPort = GetProcAddress(ntDLL, "NtRequestWaitReplyPort");
				(void*&)instance_data.fnWriteRequestData = GetProcAddress(ntDLL, "NtWriteRequestData");
				(void*&)instance_data.fnConnectPort = GetProcAddress(ntDLL, "NtConnectPort");
				(void*&)instance_data.fnUnmapViewOfSection = GetProcAddress(ntDLL, "NtUnmapViewOfSection");
				(void*&)instance_data.fnMapViewOfSection = GetProcAddress(ntDLL, "NtMapViewOfSection");
				(void*&)instance_data.fnCreateSection = GetProcAddress(ntDLL, "NtCreateSection");
				(void*&)instance_data.fnClose = GetProcAddress(ntDLL, "NtClose");
				(void*&)instance_data.fnInitUnicodeString = GetProcAddress(ntDLL, "RtlInitUnicodeString");
				instance = &instance_data;
			}
			return instance;
		}
	};

	template <int cookie> NativeAPI_<cookie>* NativeAPI_<cookie>::instance = NativeAPI_<0>::Initialize();
	template <int cookie> NativeAPI_<cookie> NativeAPI_<cookie>::instance_data = {0,};

	struct NativeAPI_Gate
	{
		NativeAPI_<0>* operator->() const
		{
			return NativeAPI_<0>::instance;
			//? NativeAPI_<0>::instance
			//: NativeAPI_<0>::Initialize();
		}
	};

	static const NativeAPI_Gate NativeAPI = NativeAPI_Gate();
}

#endif // SYSTEM_IS_WINDOWS

