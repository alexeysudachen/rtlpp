
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../deftypes.hxx"
#include "../require.hxx"
#include "../string.hxx"

namespace nano
{

	XPTDECLARE(IllformedUrl, Illformed, "nano::IllformedUrl");

	struct UrlInfo
	{
		int port;
		String proto;
		String user;
		String password;
		String host;
		String uri;
		String args;
	} UrlInfo;

	/* proto://[user[:password]@]host[:port]/uri[?cgi_args] */
	bool url_parse(Strarg url, UrlInfo* uinfo)
	{
		char const* u = url;
		char const* uu1 = 0;
		char const* uu2 = 0;

		while (*u && isspace(*u)) ++u;
		uu1 = u;
		while (*u && *u != ':') { ++u; }
		uu2 = u++; // end proto
		if (*u++ != '/' || *u++ != '/')
		{
			OCCURED(IllformedUrl, "illformed url or bad proto");
			return false;
		}

		uinfo->proto = lower(uu1, uu2);
		uu1 = u;
		uu2 = 0;
		while (*u && *u != '/')
		{
			if (*u == '@')
			{
				uinfo->user = String(uu1, uu2 ? uu2 : u);
				if (uu2)
					uinfo->password = String(uu2 + 1, u);
				uu1 = ++u;
				uu2 = 0;
			}
			else if (*u == ':')
			{
				uu2 = u++;
			}
			else ++u;
		}

		if (uu2)
			uinfo->port = strtol(uu2 + 1, 0, 10);
		uinfo->host = lower(uu1, uu2);

		if (*u)   // parse uri && args
		{
			uu1 = u; // u->'/'
			uu2 = 0;

			while (*u && u != '?') ++u;
			if (*u == '?') uu2 = u++;
			while (*u) ++u;

			uinfo->uri = String(uu1, uu2 ? uu2 : u);
			if (uu2)
				uinfo->args = String(uu2, u);
		}

		if (!uinfo->port)
			if (uinfo->proto == "http") uinfo->port = 80;
			else if (uinfo->proto == "https") uinfo->port = 443;
			else if (uinfo->proto == "ftp") uinfo->port = 21;

		return true;
	}

	inline String url_quote(byte_range const& ba)
	{
		Array<char> q;
		for (int i = 0, j = b.Count(); i < j; ++i)
		{
			char b = ba[i];
			if (isalnum(b) || b == '_' || b == '.' || b == '-')
				q.Push(b);
			else if (b == ' ')
				q.Push('+');
			else
				q.Append(+hex_byte(b, '%'), 3);
		}
		return String(q, __take_data__);
	}

	inline Octets url_unquote(byte_range const& ba)
	{
		Octets q;
		int count = ba.Count();
		byte_t const* d = +ba;

		for (int i = 0; i < count;)
		{
			int j = i;
			for (; j < count && d[j] != '%' && d[j] != '+'; ++j) {}
			if (j != i)
				q.Append(d + i, j - i);
			if (j < count && d[j] == '+')
			{
				++j;
				q.Push(' ');
			}
			else if (j < count && d[j] == '%')
			{
				if (j + 2 < count)
				{
					int cc = 0;
					q.Push(unhex_byte(&d[j + 1], false, &cc));
					j += cc + 1;
				}
				else // error
					break;
			}
			i = j;
		}

		return q;
	}

}

