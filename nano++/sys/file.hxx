
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../deftypes.hxx"
#include "../string.hxx"
#include "../ios.hxx"
#include "../require.hxx"
#include "../datetime.hxx"

#if !SYSTEM_IS_WINDOWS
#include <dirent.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

namespace nano
{

	XPTDECLARE(FileError, IOError, "nano::FileError");

	inline int get_index_of_path_split_R(Strarg path)
	{
		int i0 = find_rchar(path, '/');
#if SYSTEM_IS_WINDOWS
		int i1 = find_rchar(path, '\\');
		if (i0 < i1) i0 = i1;
#endif
		return i0;
	}

	inline int get_index_of_path_split_L(Strarg path)
	{
		int i0 = find_char(path, '/');
#if SYSTEM_IS_WINDOWS
		int i1 = find_char(path, '\\');
		if (i0 < 0 || (i0 > i1 && i1 >= 0)) i0 = i1;
#endif
		return i0;
	}

	inline CXX_NO_INLINE String file_basename(Strarg name, pchar_t sfxes = 0)
	{
		pchar_t p = +name;

		int i = get_index_of_path_split_R(p);
		if (i >= 0)
			p += (i + 1);

		if (sfxes)
			if (StringArray x = split(sfxes, "; "))
				for (int i = 0; i < x.Count(); ++i)
					if (ends_nocase(p, x[i]))
					{
						int L = strlen(p);
						int l = x[i].Length();
						return String(p, L - l);
					}

		return String(p);
	}

	inline CXX_NO_INLINE String file_dirname(Strarg name)
	{
		int i = get_index_of_path_split_R(name);
		if (i >= 0)
			return String(+name + i + 1);
		return String();
	}

	inline CXX_NO_INLINE String file_absname(Strarg name)
	{
#if SYSTEM_IS_WINDOWS
#else
#endif
		return String();
	}

	//inline CXX_NO_INLINE String file_canonicname(Strarg name)
	//  {
	//  }

	inline CXX_NO_INLINE String file_suffix(Strarg name)
	{
		int i = find_rchar(name, '.');
		if (i > 0)
			return String(+name + i);
		else
			return String();
	}

	inline CXX_NO_INLINE String file_join(Strarg dirname, Strarg filename, pchar_t sfx = 0)
	{
#if SYSTEM_IS_WINDOWS
		String d = rstrip(dirname, "\\/");
		String f = lstrip(filename, "\\/");
		return d + "\\" + f;
#else
		String d = rstrip(dirname, '/');
		String f = lstrip(filename, '/');
		return d + "/" + f;
#endif
	}

	inline String current_directory()
	{
#if SYSTEM_IS_WINDOWS
		Array<wchar_t> b(1024);
		return String(_wgetcwd(+b, b.Count()));
#else
		Array<char> b(1024);
		return String(getcwd(+b, b.Count()));
#endif
	}

	inline CXX_NO_INLINE bool check_file_error(pchar_t op, FILE* f, Strarg fname, bool look_to_errno = false)
	{
		int err = 0;
		String errS;

		if (look_to_errno)
		{
			if ((err = errno))
				errS = strerror(err);
		}
		else if (f && (err = ferror(f)) && !feof(f))
		{
			errS = strerror(err);
			clearerr(f);
		}
		if (err)
			OCCURED(FileError, _S * L"%s failed on file '%s': %s" % op % fname % errS);
		return !err;
	}

	struct FileStats
	{
		time_t ctime;
		time_t mtime;
		foffset_t length;

		bool exists: 1;
		bool is_regular: 1;
		bool is_tty: 1;
		bool is_symlink: 1;
		bool is_unisok: 1;
		bool is_directory: 1;
		bool is_writable: 1;
		bool is_readable: 1;
		bool is_executable: 1;
	};

#if SYSTEM_IS_WINDOWS
#if !defined _FILEi32
	typedef struct __stat64 _filestats_t;
#else
	typedef struct _stat _filestats_t;
#endif
#if !defined S_IWUSR
	enum
	{
	    S_IWUSR = _S_IWRITE,
	    S_IRUSR = _S_IREAD,
	    S_IXUSR = _S_IEXEC,
	};
#endif
#else
	typedef struct stat _filestats_t;
#endif

	inline CXX_NO_INLINE FileStats translate_filestats(_filestats_t& fst, FileStats& st)
	{
		st.exists = 1;
		st.is_regular = (fst.st_mode & S_IFMT) == S_IFREG;
		st.is_directory = (fst.st_mode & S_IFMT) == S_IFDIR;
#if !SYSTEM_IS_WINDOWS
		st.is_symlink = (fst.st_mode & S_IFMT) == S_IFLNK;
		st.is_unisok  = (fst.st_mode & S_IFMT) == S_IFSOCK;
#endif
		st.is_writable = (fst.st_mode & S_IWUSR) != 0;
		st.is_readable = (fst.st_mode & S_IRUSR) != 0;
		st.is_executable = (fst.st_mode & S_IXUSR) != 0;
		st.length = fst.st_size;
		st.ctime  = fst.st_ctime;
		st.mtime  = fst.st_mtime;
		return st;
	}

	inline CXX_NO_INLINE FileStats file_stats(Strarg name, bool ignorerr = false)
	{
		FileStats st; memset(&st, 0, sizeof(st));
		_filestats_t fst = {0};
		int err;
#if SYSTEM_IS_WINDOWS
#if !defined _FILEi32
		err = _wstat64(+Unicode(name), &fst);
#else
		err = _wstat(+Unicode(name), &fst);
#endif
#else
		err = stat(+name, &fst);
#endif
		if (!err)
			translate_filestats(fst, st);
		else if (!ignorerr)
			if (errno != ENOENT && errno != ENOTDIR)
				check_file_error("getting stats", 0, name, true);
		return st;
	}

	inline DateTime file_ctime(Strarg name)   { return file_stats(name).ctime; }
	inline DateTime file_mtime(Strarg name)   { return file_stats(name).mtime; }
	inline size_t file_length(Strarg name)      { return file_stats(name).length; }
	inline bool file_exists(Strarg name)        { return file_stats(name).exists; }
	inline bool file_is_regular(Strarg name)    { return file_stats(name).is_regular; }
	inline bool file_is_direcory(Strarg name)   { return file_stats(name).is_directory; }
	inline bool file_is_writable(Strarg name)   { return file_stats(name).is_writable; }
	inline bool file_is_readable(Strarg name)   { return file_stats(name).is_readable; }
	inline bool file_is_executable(Strarg name) { return file_stats(name).is_executable; }

	inline CXX_NO_INLINE bool create_directory(Strarg name)
	{
		if (!file_stats(name, true).is_directory)
		{
#if SYSTEM_IS_WINDOWS
			if (_wmkdir(+Unicode(name)) < 0)
#else
			if (mkdir(+name, 0755) < 0)
#endif
				return check_file_error("mkdir", 0, name, true);
		}
		return true;
	}

	inline CXX_NO_INLINE bool create_required_dirs(Strarg name, bool is_dirname = false)
	{
		String dirpath = is_dirname ? String(name) : file_dirname(name);
		if (dirpath
		    && !(dirpath.Length() == 2 && dirpath[1] == ':')
		    && !(dirpath.Length() == 1 &&
		         (dirpath[0] == '/' || dirpath[0] == '\\')))
		{
			if (create_required_dirs(dirpath))
				if (!create_directory(dirpath))
					return check_file_error("createdir", 0, dirpath, true), false;
		}
		return true;
	}

	inline bool copy_file(Strarg from, Strarg to, bool overwrite_if_exists = true)
	{
		OCCURED(Unimplemented, "copy_file"); return false;
	}

	inline bool rename_file(Strarg from, Strarg to, bool overwrite_if_exists = true)
	{
		OCCURED(Unimplemented, "rename_file"); return false;
	}

	enum
	{
	    LIST_DIRECTORIES = 1,
	    LIST_FILES = 2,
	};

	inline CXX_NO_INLINE StringArray file_list(
	    Strarg dirname, int opts = LIST_DIRECTORIES | LIST_FILES)
	{
		StringArray L;
#if SYSTEM_IS_WINDOWS
		WIN32_FIND_DATAW fdtw;
		HANDLE hfnd = FindFirstFileW(+Unicode(file_join(dirname, "*.*")), &fdtw);
		if (hfnd && hfnd != INVALID_HANDLE_VALUE)
		{
			do
				if (wcscmp(fdtw.cFileName, L".") && wcscmp(fdtw.cFileName, L".."))
					if (
					    ((fdtw.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					     && (opts & LIST_DIRECTORIES))
					    || (opts & LIST_FILES))
						L.Push(String(fdtw.cFileName));
			while (FindNextFileW(hfnd, &fdtw));
			FindClose(hfnd);
		}
#else
		if (DIR* dir = opendir(+dirname))
		{
			struct dirent* dp = 0;
			while (0 != (dp = readdir(dir)))
				if (strcmp(dp->d_name, ".") && strcmp(dp->d_name, ".."))
				{
					if ((dp->d_type == DT_DIR && (opts & LIST_DIRECTORIES))
					    || (opts & LIST_FILES))
						L.Push(dp->d_name);
				}
			closedir(dir);
		}
#endif
		return L;
	}

	inline bool unlink_file(Strarg name, bool force = false)
	{
		FileStats st = file_stats(name, true);
		int err = 0;
		if (st.exists)
			if (st.is_directory && force)
			{
				if (StringArray dirlist = file_list(name))
					for (int i = 0, j = dirlist.Count(); i < j; ++i)
						if (!unlink_file(file_join(name, dirlist[i]), force))
							return false;
			}
			else
				err =
#if SYSTEM_IS_WINDOWS
				    _wunlink(+Unicode(name));
#else
				    unlink(+name);
#endif
		if (err < 0)
			return check_file_error("unlink", 0, name, true);
		return true;
	}

	struct FileStream : IOs
	{
		virtual FileStats Stats() = 0;
		virtual foffset_t Length() = 0;
		foffset_t Available() { return Length() - Tell(); }
	};

	typedef RccPtr<FileStream> File;

	struct CfileStream: FileStream
	{
		FILE* f;
		bool shared;
		String name;

		void Flush()
		{
			if (f)
			{
				fflush(f);
				check_file_error("fflush", f, name, false);
			}
		}

		void Close()
		{
			if (f)
			{
				if (!shared)
					fclose(f);
				f = 0;
				shared = false;
			}
		}

		bool already_closed()
		{
			if (!f)
				OCCURED(FileError, "file '%s' already closed");
			return !f;
		}

		virtual int WriteData(void const* p, int count, int min_count)
		{
			if (already_closed()) return -1;

			int cc = 0;
			byte_t const* d = (byte_t const*)p;
			for (; cc < count ;)
			{
				int q = fwrite(d, 1, count - cc, f);
				if (q <= 0 && !check_file_error("CfileStream/IOs::WriteData", f, name, false))
					return -1;
				if (!q && cc >= min_count)
					return cc;
				cc += q;
				d += q;
			}
			return cc;
		}

		virtual int ReadData(void* p, int count, int min_count)
		{
			if (already_closed()) return -1;

			int cc = 0;
			byte_t* d = (byte_t*)p;
			for (; cc < count ;)
			{
				int q = fread(d, 1, count - cc, f);
				if (q <= 0)
				{
					if (feof(f))
						if (cc >= min_count) return cc;
						else
						{ OCCURED(EndOfFile, _S*"end of file '%s'" % name); return -1; }
					else if (!check_file_error("CfileStream/IOs::ReadData", f, name, false))
						return -1;
				}
				if (!q && cc >= min_count) return cc;
				cc += q;
				d += q;
			}
			return cc;
		}

		virtual foffset_t Seek(foffset_t pos, int whence)
		{
			if (already_closed()) return -1;

			foffset_t old = Tell();
			if (old < 0) return -1;

			int q =
#if SYSTEM_IS_WINDOWS
#if !defined _FILEi32
			    _fseeki64(f, pos, whence);
#else
			    fseek(f, (long)pos, whence);
#endif
#else
			    fseeko(f, pos, whence);
#endif
			if (q < 0 && !check_file_error("CfileStream/IOs::Seek", f, name, false))
				return -1;
			return old;
		}

		virtual foffset_t Tell()
		{
			if (already_closed()) return -1;

			foffset_t q =
#if SYSTEM_IS_WINDOWS
#if !defined _FILEi32
			    _ftelli64(f);
#else
			    ftell(f);
#endif
#else
			    ftello(f);
#endif
			if (q < 0 && !check_file_error("CfileStream/IOs::Tell", f, name, false))
				return -1;
			return q;
		}

		virtual String Gets()
		{
			enum { FGETS_BUFFER_SIZE = 5 /*256*/ };
			if (already_closed()) return String();

			char* S;
			int cc = 0;
			Array<char> q(FGETS_BUFFER_SIZE);
		l:
			S = fgets(+q, q.Count() - cc, f);
			if (!S)
				if (feof(f))
					return String(+q);
				else
				{
					check_file_error("CfileStream/IOs::Gets", f, name, false);
					return String();
				}
			cc = strlen(S);
			if (S[cc - 1] != '\n')
			{
				q.Resize(cc + FGETS_BUFFER_SIZE);
				goto l;
			}
			return String(q, __take_data__);
		}

		virtual FileStats Stats()
		{
			FileStats st; memset(&st, 0, sizeof(st));
			if (already_closed()) return st;

			_filestats_t fst = {0};
			int err;
#if SYSTEM_IS_WINDOWS
#if !defined _FILEi32
			err = _fstat64(fileno(f), &fst);
#else
			err = _fstat(fileno(f), &fst);
#endif
#else
			err = fstat(fileno(f), &fst);
#endif
			if (!err)
				translate_filestats(fst, st);
			else
				check_file_error("Stats", f, name, false);
			return st;
		};

		virtual foffset_t Length()
		{
			return Stats().length;
		}


		CfileStream(FILE* f, Strarg name, bool shared)
			: f(f), shared(shared), name(name) {}
		~CfileStream() { Close(); }

	private:
		friend File file_share(FILE* f, Strarg name);
		friend File file_acquire(FILE* f, Strarg name);
		static void* operator new(size_t size)
		{
			STRICT_REQUIRE(size == sizeof(CfileStream));
			return SharedPool<CfileStream>::Malloc();
		}
		static void operator delete(void* p)
		{
			SharedPool<CfileStream>::Free(p);
		}
	};

	inline File file_share(FILE* f, Strarg name = (char*)0)
	{
		return File(new CfileStream(f, (name ? +name : 0 | _S*"{FILE:%p}" % f), true));
	}

	inline File file_acquire(FILE* f, Strarg name = (char*)0)
	{
		return File(new CfileStream(f, (name ? +name : 0 | _S*"{FILE:%p}" % f), false));
	}

	inline bool file_access_is_satisfied(Strarg path, StreamAccess access)
	{
		FileStats st = file_stats(path);
		if ((*access & STREAM_CREATE_PATH) && !st.exists)
			if (!create_required_dirs(path))
				return false;
		if (st.exists)
			if (st.is_directory)
			{ OCCURED(FileError, _S*"file '%s' is directory" % path); return false; }
			else if ((*access & STREAM_CREATE_MASK) == STREAM_CREATENEW)
			{ OCCURED(FileError, _S*"file '%s' already exists" % path); return false; }
			else if ((*access & STREAM_CREATE_MASK) == STREAM_CREATEALWAYS)
			{ if (!unlink_file(path)) return false; }
			else;
		else if ((*access & STREAM_CREATE_MASK) == STREAM_OPENEXISTS)
		{ OCCURED(FileError, _S*"file '%s' does not exist" % path); return false; }

		return true;
	}

	inline File file_open(Strarg path, StreamAccess access = STREAM_CONSTANT)
	{
		if (file_access_is_satisfied(path, access))
			if (FILE* f = fopen(+path, access.Cstr()))
				return file_acquire(f, file_basename(path));
		return File(0);
	}

	inline void operator | (File const& f, String S) { f->Write(+S, S.Length()); }

}

