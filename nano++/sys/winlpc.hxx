
/*

(C)2013, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "../compiler.hxx"

#if SYSTEM_IS_WINDOWS

#include "../deftypes.hxx"
#include "../datetime.hxx"
#include "../rccptr.hxx"
#include "../xptnfo.hxx"
#include "winative.hxx"
#include "win32staff.hxx"

namespace nano
{

	XPTDECLARE(LpcError, IOError, "nano::LpcError");
	XPTDECLARE(LpcDsntAliveError, LpcError, "nano::LpcDsntAliveError");

	struct LpcPortData
	{
		HANDLE port;
		union LpcMsg
		{
			LPC_MESSAGE_HEADER32 _32;
			LPC_MESSAGE_HEADER64 _64;

			CXX_NO_INLINE void Fill()
			{
				if (IS_WINKERNEL_64)
				{
					_64.TotalLength = sizeof(_64);
					_64.DataLength  = 0;
				}
				else 
				{
					_32.TotalLength = sizeof(_32);
					_32.DataLength  = 0;
				}
			}

			CXX_NO_INLINE u32_t GetLpcProcessId()
			{
				if (IS_WINKERNEL_64)
				{
					// if kernel is 64-bit
					return _64.ProcessId;
				}
				else
				{
					// kernel is 32-bit
					return _32.ProcessId;
				}
			}	
		};

		LpcMsg msg;

		LpcPortData() : port(0) {}
		virtual ~LpcPortData() { Close(); }

		void Close()
		{
			NativeAPI->fnClose(port);
			port = 0;
		}

		bool IsAlive()
		{
			return port != 0;
		}
	};

	static const int WINLPC_MAX_PAYLOAD_LENGTH = 64 * 1024;

	static const int WINLPC_MAX_DATA_SIZE =
	    IS_WINKERNEL_64
	    ? sizeof(LpcPortData::LpcMsg) - sizeof(LpcPortData::LpcMsg()._64)
	    : sizeof(LpcPortData::LpcMsg) - sizeof(LpcPortData::LpcMsg()._32);

	static const int WINLPC_MAX_MESG_SIZE = sizeof(LpcPortData::LpcMsg);

	struct LpcClient_ : Refcounted<LpcPortData>
	{
		HANDLE   section;
		byte_t*  view;
		int      repl_len;

		LpcClient_()
			: section(0), view(0)
		{}

		virtual ~LpcClient_()
		{
			if (view)
				NativeAPI->fnUnmapViewOfSection(INVALID_HANDLE_VALUE, view);
			if (section)
				NativeAPI->fnClose(section);
		}

		ByteRange Payload()
		{
			return byte_range((byte_t*)view, repl_len);
		}

		CXX_NO_INLINE size_t RequestInternal(size_t payload_len)
		{
			long ntst;
			repl_len = 0;

			LpcMsg msg2;
			memset(&msg,0,sizeof(msg));
			memset(&msg2,0,sizeof(msg2));
			msg2.Fill();

			*(u16_t*)(view+(WINLPC_MAX_PAYLOAD_LENGTH-2)) = (u16_t)payload_len;

			ntst = NativeAPI->fnRequestWaitReplyPort(port, (LPC_MESSAGE_HEADER*)&msg2,(LPC_MESSAGE_HEADER*)&msg);

			if (ntst == 0)
			{
				repl_len = *(u16_t*)(view+(WINLPC_MAX_PAYLOAD_LENGTH-2));
			}

			return ntst;
		}

		CXX_NO_INLINE bool Request(Bytesarg bytes)
		{
			if (!IsAlive())
			{
				OCCURED(LpcDsntAliveError, "LPC client port does not connected");
				return false;
			}

			if (bytes.Count() > (WINLPC_MAX_PAYLOAD_LENGTH-2))
			{
				OCCURED(LpcError, "LPC message to long");
				return false;
			}

			//memcpy(view, bytes.Begin(), bytes.Count());
			for ( int i = 0; i < bytes.Count(); ++i ) 
				view[i] = bytes[i];

			long ntst = RequestInternal(bytes.Count());

			if (ntst != 0)
			{
				OCCURED(LpcError, _S*"failed to request on LPC client port, NTSTATUS(%08x)" % ntst);
				return false;
			}

			return true;
		}

	};

	typedef RccPtr<LpcClient_> LpcClient;

	inline CXX_NO_INLINE LpcClient lpc_connect(Strarg port_name)
	{
		LpcClient client = __new__;
		long ntst;
		SECURITY_QUALITY_OF_SERVICE sqos =
		{
			sizeof(SECURITY_QUALITY_OF_SERVICE),
			SecurityImpersonation,
			SECURITY_DYNAMIC_TRACKING,
			TRUE
		};
		OBJECT_ATTRIBUTES oa;
		UNICODE_STRING us_name;
		Unicode wname = "\\BaseNamedObjects\\" + port_name;

		NativeAPI->fnInitUnicodeString(&us_name, wname.Cstr());
		InitializeObjectAttributes(&oa, &us_name, 0, 0, 0);

		LARGE_INTEGER max_size; max_size.QuadPart = WINLPC_MAX_PAYLOAD_LENGTH;
		ntst = NativeAPI->fnCreateSection(&client->section,
		                                  SECTION_MAP_READ | SECTION_MAP_WRITE,
		                                  0, &max_size, PAGE_READWRITE,
		                                  SEC_COMMIT, 0);
		if (ntst != 0)
		{
			OCCURED(LpcError, _S*"failed to create LPC shared buffer, NTSTATUS(%08x)" % ntst);
			return __nil__;
		}

		LPC_SECTION_OWNER_MEMORY memown = {sizeof(LPC_SECTION_OWNER_MEMORY), 0,};
		memown.SectionHandle = client->section;
		memown.ViewBase = 0; //client->view;
		memown.ViewSize = WINLPC_MAX_PAYLOAD_LENGTH;

		ULONG max_msglen = 0;

		ntst = NativeAPI->fnConnectPort(&client->port, &us_name, &sqos, &memown, 0, &max_msglen, 0, 0);
		if (ntst != 0)
		{
			OCCURED(LpcError, _S*"failed to connect LPC port, NTSTATUS(%08x)" % ntst);
			return __nil__;
		}

		client->view = ptr_cast(memown.ViewBase);
		return client;
	}

	struct LpcWorker_ : Refcounted<LpcPortData>
	{
		byte_t* payload;
		size_t rqst_len;

		LpcWorker_()
			: rqst_len(0), payload(0)
		{}

		~LpcWorker_()
		{
		}

		ByteRange Payload()
		{
			return byte_range((byte_t*)payload, rqst_len);
		}

		CXX_NO_INLINE bool Listen(Timeout timeout = 0)
		{
			if (!IsAlive())
			{
				OCCURED(LpcDsntAliveError, "LPC server port does not connected");
				return false;
			}

			long ntst;

			ntst = NativeAPI->fnReplyWaitReceivePort(port, 0, 0, (LPC_MESSAGE_HEADER*)&msg);
			if (ntst != 0)
			{
				OCCURED(LpcError, _S*"LPC failed to receive mesg, NTSTATUS(%08x)" % ntst);
				return false;
			}
		
			if (msg._32.MessageType == LPC_REQUEST)
			{
				rqst_len = *(u16_t*)(payload+(WINLPC_MAX_PAYLOAD_LENGTH-2));
				if ( rqst_len > WINLPC_MAX_PAYLOAD_LENGTH )
				{
					OCCURED(NoEnoughSpace, "rquest is to long");
					return false;
				}

				return true;
			}

			if ( msg._32.MessageType == LPC_PORT_CLOSED )
			{
				Close();
				return false;
			}

			return false;
		}

		CXX_NO_INLINE bool Reply(size_t payload_len)
		{
			if (!IsAlive() || !payload)
			{
				OCCURED(LpcDsntAliveError, "LPC server port does not connected");
				return false;
			}

			if (payload_len > (WINLPC_MAX_PAYLOAD_LENGTH-2) )
			{
				OCCURED(NoEnoughSpace, "replay is to long");
				return false;
			}

			long ntst;
			rqst_len = *(u16_t*)(payload+(WINLPC_MAX_PAYLOAD_LENGTH-2));

			ntst = NativeAPI->fnReplyPort(port, (LPC_MESSAGE_HEADER*)&msg);
			if ( ntst != 0 )
			{
				OCCURED(LpcError, _S*"LPC failed to reply, NTSTATUS(%08x)" % ntst);
				return false;
			}

			return true;
		}

		CXX_NO_INLINE bool Reply(Bytesarg reply)
		{
			if (!IsAlive() || !payload)
			{
				OCCURED(LpcDsntAliveError, "LPC server port does not connected");
				return false;
			}

			if (reply.Count() > (WINLPC_MAX_PAYLOAD_LENGTH-2))
			{
				OCCURED(NoEnoughSpace, "replay is to long");
				return false;
			}

			memcpy(payload, reply.Begin(), reply.Count());
			return Reply(reply.Count());
		}
	};

	typedef RccPtr<LpcWorker_> LpcWorker;

	struct LpcPort_ : Refcounted<LpcPortData>
	{
		CXX_NO_INLINE LpcWorker Accept(Timeout timeout = 0)
		{
			LpcWorker worker = __new__;
			long ntst;

			if (!IsAlive())
			{
				OCCURED(LpcDsntAliveError, "LPC connection port does not connected");
				return __nil__;
			}

			ntst = NativeAPI->fnListenPort(port, (LPC_MESSAGE_HEADER*)&msg);
			if (ntst != 0)
			{
				OCCURED(LpcError, _S*"failed to listen LPC connection port, NTSTATUS(%08x)" % ntst);
				return __nil__;
			}

			LPC_SECTION_MEMORY64 secmem64 =
			{
				(IS_WINKERNEL_64 ? sizeof(LPC_SECTION_MEMORY64) : sizeof(LPC_SECTION_MEMORY)),
				0,
			};

			ntst = NativeAPI->fnAcceptConnectPort(&worker->port, 0,
			                                      (LPC_MESSAGE_HEADER*)&msg,
			                                      TRUE, 0, (LPC_SECTION_MEMORY*)&secmem64);

			worker->payload = (byte_t*)(IS_WINKERNEL_64 ? (void*)secmem64.ViewBase : (void*)((LPC_SECTION_MEMORY&)secmem64).ViewBase);

			if (WINLPC_MAX_PAYLOAD_LENGTH != (IS_WINKERNEL_64 ? (u64_t)secmem64.ViewSize : (u64_t)((
			                                      LPC_SECTION_MEMORY&)secmem64).ViewSize))
			{
				OCCURED(LpcError, _S*"failed to accept on LPC connection port, invalid palyload size" % ntst);
				return __nil__;
			}

			if (ntst != 0)
			{
				OCCURED(LpcError, _S*"failed to accept on LPC connection port, NTSTATUS(%08x)" % ntst);
				return __nil__;
			}

			ntst = NativeAPI->fnCompleteConnectPort(worker->port);

			if (ntst != 0)
			{
				OCCURED(LpcError, _S*"failed to complete on LPC connection port, NTSTATUS(%08x)" % ntst);
				return __nil__;
			}

			return worker;
		}

		LpcPort_()
		{}

		virtual ~LpcPort_()
		{}
	};

	typedef RccPtr<LpcPort_> LpcPort;

	inline CXX_NO_INLINE LpcPort lpc_bind(Strarg port_name)
	{
		LpcPort connector = __new__;
		long ntst;
		OBJECT_ATTRIBUTES oa;
		UNICODE_STRING us_name;
		Unicode wname = "\\BaseNamedObjects\\" + port_name;
		SECURITY_DESCRIPTOR sd;
		NativeAPI->fnInitUnicodeString(&us_name, wname.Cstr());
		InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
		SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);
		InitializeObjectAttributes(&oa, &us_name, 0, 0, &sd);
		ntst = NativeAPI->fnCreateWaitablePort(&connector->port, &oa, 0, WINLPC_MAX_MESG_SIZE, 0);
		if (ntst != 0)
		{
			OCCURED(LpcError, _S*"failed to complete on LPC connection port, NTSTATUS(%08x)" % ntst);
			return __nil__;
		}
		return connector;
	}

}

#endif // SYSTEM_IS_WINDOWS
