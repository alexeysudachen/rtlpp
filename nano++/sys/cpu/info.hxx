
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#if !defined cxx_nano_sys_cpu_info_HXX
#define cxx_nano_sys_cpu_info_HXX

#include "../../string.hxx"

namespace nano 
  {

    struct CPUINFO
      {
        char id[16];
        char tag[64];
        unsigned family;
        unsigned model;
        unsigned stepping;
        unsigned revision;
        unsigned number;
        unsigned MMX:1;
        unsigned MMX2:1;
        unsigned SSE:1;
        unsigned SSE2:1;
        unsigned SSE3:1;
        unsigned SSSE3:1;
        unsigned HTT:1;
        unsigned EST:1;
        unsigned PAE:1;
      };

    inline void QueryCPUID(void *rgs,int id)
      {
      #if CXX_COMPILER_IS_MSVC_COMPATIBLE
        __asm mov eax, id
        __asm push esi
        __asm push ebx
        __asm push ecx
        __asm push edx
        __asm _emit 0x0f
        __asm _emit 0xa2
        __asm mov esi,rgs
        __asm mov [esi],eax
        __asm mov [esi+4],ebx
        __asm mov [esi+8],ecx
        __asm mov [esi+12],edx
        __asm pop edx
        __asm pop ecx
        __asm pop ebx
        __asm pop esi
      #else
        __asm__ volatile ( \
        " \
          mov %0,%%eax\n\
          .byte 0x0f\n\
          .byte 0xa2\n\
          mov %%eax,(%1)\n\
          mov %%ebx,4(%1)\n\
          mov %%ecx,8(%1)\n\
          mov %%edx,12(%1)\n"
          :: "r" (id), "r" (rgs) : "eax", "ebx","ecx","edx", "memory");
      #endif
      }

    inline CXX_NO_INLINE void GetCPUInfo(CPUINFO *cpui)
      {
        unsigned r[4];
        memset(cpui,0,sizeof(*cpui));

        QueryCPUID(r,0);
        *(unsigned*)(cpui->id+0) = r[1];
        *(unsigned*)(cpui->id+4) = r[3];
        *(unsigned*)(cpui->id+8) = r[2];

        QueryCPUID(r,1);
        cpui->family = (r[0]>>8)&0x0f;
        cpui->model  = (r[0]>>4)&0x0f;
        cpui->stepping  = r[0]&0x0f;
        //if ( 2 == (r[0] & 0x3000) >> 12 ) cpui->dual = 1;
        cpui->number = (r[1] >> 16) &0xff;

        if ( r[3] & CXX_BIT(23) ) cpui->MMX = 1;
        if ( r[3] & CXX_BIT(24) ) cpui->MMX2 = 1;
        if ( r[3] & CXX_BIT(25) ) cpui->SSE = 1;
        if ( r[3] & CXX_BIT(26) ) cpui->SSE2 = 1;
        if ( r[3] & CXX_BIT(28) ) cpui->HTT = 1;
        if ( r[2] & CXX_BIT(0) )  cpui->SSE3 = 1;
        if ( r[2] & CXX_BIT(9) )  cpui->SSSE3 = 1;
        if ( r[2] & CXX_BIT(7) )  cpui->EST = 1;
        if ( r[2] & CXX_BIT(6) )  cpui->PAE = 1;


        QueryCPUID(cpui->tag,0x80000002);
        QueryCPUID(cpui->tag+16,0x80000003);
        QueryCPUID(cpui->tag+32,0x80000004);
        for ( int i = 1; i < 64 && cpui->tag[i] ; ) {
          if ( cpui->tag[i] == cpui->tag[i-1] && cpui->tag[i] == ' ')
            memmove(cpui->tag+i-1,cpui->tag+i,64-i);
          else ++i;
        }
      }

    inline CXX_NO_INLINE string_t FormatCPUInfo(CPUINFO const *cpui=0)
      {
        struct join {
          chars_t &S; pchar_t q;
          join(chars_t &s,pchar_t q) : S(s), q(q) {} 
          inline void _if(bool expr)
            { if ( expr ) { 
                if (S) 
                  S += " "; 
                  S += q; } } };

        CPUINFO c = {{0}};
        if ( !cpui )
          {
            GetCPUInfo(&c);
            cpui = &c;
          }
          
        chars_t fts;
        join(fts,"mmx"). _if( cpui->MMX );
        join(fts,"mmx2")._if( cpui->MMX2 );
        join(fts,"sse"). _if( cpui->SSE );
        join(fts,"sse2")._if( cpui->SSE2 );
        join(fts,"sse3")._if( cpui->SSE3 );
        join(fts,"sse4")._if( cpui->SSSE3 );
        join(fts,"htt"). _if( cpui->HTT );
        join(fts,"est"). _if( cpui->EST );
        join(fts,"pae"). _if( cpui->PAE );

        return string_t(_S*"%s {%s} %s"
          %cpui->id
          %+fts
          %cpui->tag);
      }

} // namespace

#endif // cxx_nano_sys_cpu_info_HXX
