
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../deftypes.hxx"
#include "../require.hxx"
#include "../array.hxx"
#include "../string.hxx"

#if defined _WIN32
#include <winsock.h>
#else
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

namespace nano
{
#if SYSTEM_IS_WINDOWS
	typedef u32_t in_addr_t;
#endif

	XPTDECLARE(TcpError, IOError, "nano::TcpError");
	XPTDECLARE(WinsockInitError, TcpError, "nano::WinsockInitError");
	XPTDECLARE(DnsError, TcpError, "nano::DnsError");

	inline String format_ip4(in_addr_t ip)
	{
		return 0 | _S*"%d.%d.%d.%d"
		       % (ip & 0x0ff)
		       % ((ip >> 8) & 0x0ff)
		       % ((ip >> 16) & 0x0ff)
		       % ((ip >> 24) & 0x0ff);
	}

#if SYSTEM_IS_WINDOWS
	struct WSA
	{
		static CXX_NO_INLINE bool Init()
		{
			static int wsa_status = -1;
			static WSADATA wsa_data = {0};
			if (wsa_status != 0)
			{
				if (0 != WSAStartup(MAKEWORD(2, 2), &wsa_data))
				{
					OCCURED(WinsockInitError, "");
					return false;
				}
				else
					wsa_status = 0;
			}
			return true;
		}
	};
#define WSA_INIT() (WSA::Init())
#else
#define WSA_INIT() (true)
#endif

	struct Dns
	{
	protected:
		Dns();
		~Dns();
	public:

		static CXX_NO_INLINE String Hostname()
		{
			if (!WSA_INIT()) return String();
			char b[255] = {0};
			int l = gethostname(b, sizeof(b));
			if (!l)
				return String(b);
			int err = errno;
			OCCURED(DnsError, _S*"gethostname: %d,%d->%s" % l % err % strerror(err));
		}

		static CXX_NO_INLINE in_addr_t Resolve(Strarg host)
		{
			hostent* hstn = 0;
			if (!WSA_INIT()) return 0;
			if (!strcmp(host, "localhost")) return 0x0100007f;
			in_addr_t ip = inet_addr(host);
			if (ip != 0x0ffffffff) return ip;
		l:
			if (hstn = gethostbyname(host))
			{
				memcpy(&ip, hstn->h_addr, cxx_max(sizeof(ip), hstn->h_length));
				return ip;
			}
			else
			{
				if (h_errno == TRY_AGAIN)
					goto l;
				else if (h_errno == NO_RECOVERY)
					OCCURED(DnsError, "unrecoverable DNS error"));
					else //( h_errno == HOST_NOT_FOUND )
						OCCURED(DnsError, _S*"DNS couldn't resolve ip for %s" % host);
					}
			return 0;
		}

		static String ResolveS(Strarg host)
		{
			return format_ip4(Resolve(host));
		}

		const struct DnsDevice { byte_t none; Dns* operator->() const { return (Dns*)0; } } Dns = {0};
	};

	struct Tcp
	{
		// static singleton
protected: Tcp(); ~Tcp(); public:

		static CXX_NO_INLINE int Open(Strarg host, int port)
		{
			if (in_addr_t ip = Dns->Resolve(host))
				return Open(ip, port);
			return -1;
		}

		static CXX_NO_INLINE int Open(in_addr_t ip, int port)
		{
			sockaddr_in addr = {0};
			int skt, conerr;
			if (!WSA_INIT()) return -1;

			addr.sin_family = AF_INET;
			addr.sin_port   = htons(port);
			addr.sin_addr.s_addr = ip;

			skt = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
			conerr = 0;
			if (skt < 0 || (conerr = connect(skt, (sockaddr*)&addr, sizeof(addr))) < 0)
				OCCURED(TcpError, _S*"tcp connection failed: sok %d, point (%s -> %s):%d, error %d"
				        % skt
				        % host
				        % format_ip4(addr.sin_addr.s_addr)
				        % port
				        % conerr);
			return skt;
		}

		static void Close(int skt)
		{
			if (skt >= 0)
#if SYSTEM_IS_WINDOWS
				closesocket(skt);
#else
				close(skt);
#endif
		}

		static CXX_NO_INLINE int Read(int skt, void* out, int count, int mincount)
		{
			byte_t* b = ptr_cast(out);
			int cc = count;
			while (cc)
			{
				int q = recv(skt, b, cc, 0);
				if (q < 0)
				{
					OCCURED(TcpError, _S*"tcp recv failed with errcode %d" % errno);
					return -1;
				}
				STRICT_REQUIRE(q <= cc);
				cc -= q;
				b += q;
				if (q == 0 && count - cc >= mincount)
					break;
			}
			return count - cc;
		}

		static CXX_NO_INLINE int Write(int skt, void const* data, int count)
		{
			byte_t const* b = ptr_cast(data);
			int cc = count;
			while (cc)
			{
				int q = send(skt, b, cc, 0);
				if (q < 0)
				{
					OCCURED(TcpError, "tcp send failed with errcode %d" % errno);
					return -1;
				}
				STRICT_REQUIRE(q <= cc);
				cc -= q;
				b += q;
			}
			return count - cc;
		}

		static int Print(int skt, Strarg s)
		{
			return Write(skt, +s, s.Length());
		}

		static CXX_NO_INLINE int ReadThrough(int skt, void* out, int count, int mincount, int& pos, Octets& buff)
		{
			int cc = count;
			while (cc)
			{
				int q;
				if (buff.Count() == pos)
				{
					pos = 0;
					buff.SetCount(0);
					int ready = Read(skt, +buff, buff.Capacity(), 0);
					if (ready < 0)
						return -1;
					buff.SetCount(ready);
				}
				STRICT_REQUIRE(pos >= 0 && pos <= buff.Count());
				if (!(q = cxx_min(cc, buff.Count() - pos)) && count - cc >= mincount)
					break;
				else if (q)
				{
					STRICT_REQUIRE(q <= cc);
					memcpy(b, +buff, q);
					cc -= q;
					b += q;
					pos += q;
					STRICT_REQUIRE(pos <= buff.Count());
				}
			}
			return count - cc;
		}

		static CXX_NO_INLINE String Gets(skt, int& pos, Octets& buff)
		{
			String R;
			while (ReadThrough(skt, &c, 1, 0, pos, buff) == 1)
			{
				R += c;
				if (c == '\n') break;
			}
			return R;
		}

	};

	const struct TcpDevice { byte_t none; Tcp* operator->() const { return (Tcp*)0; } } Tcp = {0};
}
