
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "win32staff.hxx"

namespace nano
{

	XPTDECLARE(ProcessCreateError, RuntimeXpt, "nano::ProcessCreateError");

	enum
	{
	    CRPROC_EXTEND_ENVIRON = 1,
	    CRPROC_SUSPEND        = 2,
	};

	struct ProcessControlObject : __refcounted__
	{
#if SYSTEM_IS_WINDOWS
		HANDLE process;
		HANDLE thread;
#else
		int pid;
#endif

		~ProcessControlObject()
		{
#if SYSTEM_IS_WINDOWS
			CloseHandle(process);
			CloseHandle(thread);
#else
#endif
		}
	};

	typedef RccPtr<ProcessControlObject> Process;

	inline CXX_NO_INLINE Process create_process(
	    Strarg executable, Strarg cmdline, unsigned flags, pchar_t* newenv)
	{
#if SYSTEM_IS_WINDOWS
		Array<wchar_t> env;
		if (newenv)
		{
			if (flags & CRPROC_EXTEND_ENVIRON)
			{
				pwide_t e = GetEnvironmentStringsW();
				pwide_t E = e;
				while (*E || E[1]) ++E;
				env.Append(e, E);
			}
			while (*newenv)
			{
				Unicode u = *newenv;
				env.Push(0);
				env.Append(+u, u.Length());
			}
			env.Push(0);
			env.Push(0);
		}

		unsigned crflags = 0;
		if (env) crflags |= CREATE_UNICODE_ENVIRONMENT;
		if (flags & CRPROC_SUSPEND) crflags |= CREATE_SUSPENDED;
		PROCESS_INFORMATION pinfo = {0};
		STARTUPINFOW sinfo = {sizeof(STARTUPINFOW), 0};
		sinfo.wShowWindow = SW_SHOWDEFAULT;

		if (CreateProcessW(+Unicode(executable), (LPWSTR) + Unicode(cmdline),
		                   0, 0, 0,
		                   crflags,
		                   env ? +env : 0,
		                   0, &sinfo, &pinfo))
		{
			Process proc = rcc_ptr(new ProcessControlObject());
			proc->process = pinfo.hProcess;
			proc->thread  = pinfo.hThread;
			return proc;
		}
		else
		{
			long e = GetLastError();
			OCCURED(ProcessCreateError, winerror_format(e));
			return Process(0);
		}
#else
#endif
	}

	inline int wait_process_exit(Process p)
	{
#if SYSTEM_IS_WINDOWS
		unsigned long code;
		while (GetExitCodeProcess(p->thread, &code) && code == STILL_ACTIVE)
			Sleep(100);
		return (int)code;
#else
#endif
	}
}


