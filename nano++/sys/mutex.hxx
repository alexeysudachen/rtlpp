
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../deftypes.hxx"
#include "../memory.hxx"
#include "../atomic.hxx"
#include "../require.hxx"
#include "../mempool.hxx"

#if !SYSTEM_IS_WINDOWS
# include <pthread.h>
#endif

namespace nano
{


#if SYSTEM_IS_WINDOWS && CXX_USES_THREADS

	struct PlatfSpecificMutex
	{
		CRITICAL_SECTION cs;
		HANDLE event;

		PlatfSpecificMutex() : event(0)
		{
			InitializeCriticalSection(&cs);
		}

		~PlatfSpecificMutex()
		{
			DeleteCriticalSection(&cs)
			if (event) CloseHandle(event);
		}

		static void* operator new(size_t size)
		{
			STRICT_REQUIRE(size == sizeof(PlatfSpecificMutex));
			return SharedPool<PlatfSpecificMutex>::Malloc();
		}
		static void operator delete(void* p)
		{
			SharedPool<PlatfSpecificMutex>::Free(p);
		}
	};

	inline CXX_NO_INLINE void init_and_lock_mutex(PlatfSpecificMutex* volatile* m)
	{
		PlatfSpecificMutex* p = new PlatfSpecificMutex();
		EnterCriticalSection(&p->cs);
		if (!ATOMIC_CMPXCHNGE_PTR((void*)m, p, 0))
		{
			LeaveCriticalSection(&p->cs);
			delete p;
			EnterCriticalSection(&(*m)->cs);
		}
	}

	inline CXX_NO_INLINE void delete_mutex(PlatfSpecificMutex* volatile* m)
	{
		if (*m)
		{
			PlatfSpecificMutex* q = *m;
			if (ATOMIC_CMPXCHNGE_PTR((void*)m, 0, q))
			{
				delete q;
			}
		}
	}

	inline CXX_NO_INLINE void lock_mutex(PlatfSpecificMutex* volatile* m)
	{
		if (!*m)
			init_and_lock_mutex(m);
		else
			EnterCriticalSection(&(*m)->cs);
	}

	inline CXX_NO_INLINE void unlock_mutex(PlatfSpecificMutex* volatile* m)
	{
		LeaveCriticalSection(&(*m)->cs);
	}

	inline CXX_NO_INLINE bool wait_until_on_mutex(PlatfSpecificMutex* volatile* m, long millis)
	{
		return true;
	}

	inline CXX_NO_INLINE void wait_infinity_on_mutex(PlatfSpecificMutex* volatile* m) {}
	inline CXX_NO_INLINE void notify_on_mutex(PlatfSpecificMutex* volatile* m) {}
	inline CXX_NO_INLINE void notify_all_on_mutex(PlatfSpecificMutex* volatile* m) {}

#elif CXX_USES_THREADS

	struct PlatfSpecificMutex
	{
		pthread_mutex_t cs;
		pthread_cond_t cond;
		struct
		{
			int cond_initialized : 1;
			int lock_count: 16;
		} f;
#if _STRICT
		pthread_t owner;
#endif

		static void* operator new(size_t size)
		{
			STRICT_REQUIRE(size == sizeof(PlatfSpecificMutex));
			return SharedPool<PlatfSpecificMutex>::Malloc();
		}
		static void operator delete(void* p)
		{
			SharedPool<PlatfSpecificMutex>::Free(p);
		}
	};

	inline CXX_NO_INLINE void init_and_lock_mutex(PlatfSpecificMutex* volatile* m)
	{
		PlatfSpecificMutex* p = new PlatfSpecificMutex();
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&p->cs, &attr);
		pthread_mutexattr_destroy(&attr);
		pthread_mutex_lock(&p->cs);
		if (!ATOMIC_CMPXCHNGE_PTR((void*)m, p, 0))
		{
			pthread_mutex_unlock(&p->cs);
			pthread_mutex_destroy(&p->cs);
			delete p;
			pthread_mutex_lock(&(*m)->cs);
		}
#if _STRICT
		p->owner = pthread_self();
		++p->lock_count;
#endif
	}

	inline CXX_NO_INLINE void dlete_mutex(PlatfSpecificMutex* volatile* m)
	{
		if (*m)
		{
			PlatfSpecificMutex* q = *m;
			if (ATOMIC_CMPXCHNGE_PTR((void*)m, 0, q))
			{
				pthread_mutex_destroy(&q->cs);
				if (q->f.cond_initialized)
					pthread_cond_destroy(&q->cond);
				delete q;
			}
		}
	}

	inline CXX_NO_INLINE void lock_mutex(PlatfSpecificMutex* volatile* m)
	{
		if (!*m)
			init_and_lock_mutex(m);
		else
			pthread_mutex_lock(&(*m)->cs);

#if _STRICT
		if (!((*m)->lock_count || !(*m)->owner)) abort();
		if (!(*m)->owner)(*m)->owner = pthread_self();
		if ((*m)->owner != pthread_self()) abort();
		++(*m)->lock_count;
#endif
	}

	inline CXX_NO_INLINE void unlock_mutex(PlatfSpecificMutex* volatile* m)
	{
#if _STRICT
		if ((*m)->owner != pthread_self()) abort();
		if (!--(*m)->lock_count)(*m)->owner = 0;
#endif
		pthread_mutex_unlock(&(*m)->cs);
	}

	inline CXX_NO_INLINE bool wait_until_on_mutex(PlatfSpecificMutex* volatile* m, long millis)
	{
#if _STRICT
		if ((*m)->owner != pthread_self()) abort();
#endif
		if (!(*m)->f.cond_initialized)
		{
			pthread_cond_init(&(*m)->cond, 0);
			(*m)->f.cond_initialized = 1;
		}

		if (millis > 0)
		{
			struct timespec ts;
			ts.tv_nsec = millis * 1000 * 1000;
			ts.tv_sec = millis / 1000;
			if (int st = pthread_cond_timedwait(&(*m)->cond, &(*m)->cs, &ts))
				return false;
		}
		else
			pthread_cond_wait(&(*m)->cond, &(*m)->cs);

		return true;
	}

	inline CXX_NO_INLINE void wait_infinity_on_mutex(PlatfSpecificMutex* volatile* m)
	{
		wait_until_on_mutex(m, -1);
	}

	inline CXX_NO_INLINE void notify_on_mutex(PlatfSpecificMutex* volatile* m)
	{
#if _STRICT
		if (!(*m) || !(*m)->f.cond_initialized) abort();
#endif
		pthread_cond_signal(&(*m)->cond);
	}

	inline CXX_NO_INLINE void notify_all_on_mutex(PlatfSpecificMutex* volatile* m)
	{
#if _STRICT
		if (!(*m) || !(*m)->f.cond_initialized) abort();
#endif
		pthread_cond_broadcast(&(*m)->cond);
	}

#endif

#if CXX_USES_THREADS

	struct MutexData
	{
		mutable PlatfSpecificMutex* volatile mutex;
		void Lock() { lock_mutex(&mutex); }
		void Unlock() { unlock_mutex(&mutex); }
	};

	struct Mutex : MutexData
	{
		Mutex()
		{
			mutex = 0;
		}
		~Mutex()
		{
			delete_mutex(&mutex);
		}
		void Notify() const { notify_on_mutex(&mutex); }
		void NotifyAll() const { notify_all_on_mutex(&mutex); }
		void Wait() const { wait_infinity_on_mutex(&mutex); }
		bool Wait(long millis) const { return wait_until_on_mutex(&mutex, millis); }
	};

	template <class T> struct Synchronizer
	{
		T* dta;
		Synchronizer(T* dta) : dta(dta)
		{
			dta->Lock();
		}
		~Synchronizer()
		{
			dta->Unlock();
		}
		operator bool () const { return true; }
	};

	typedef Synchronizer<MutexData> MutexSynchronizer;

#define __lockon__(x) \
	if ( ::nano::MutexSynchronizer CXX_LOCAL_ID(lck) = x ) \
	goto CXX_LABEL; else CXX_LABEL:

#else

	struct MutexData { void* none; };
	struct Mutex : MutexData {};
#define __lockon__(x) if(0);else

#endif

} //namespace

