
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#include "../../deftypes.hxx"

namespace nano
  {
   
    // The Machine field has one of the following values that specifies its CPU type. 
    // An image file can be run only on the specified machine or on a system that emulates the specified machine.
    enum pe32_machine
      {
        PE_FILE_MACHINE_UNKNOWN   = 0x0,    // The contents of this field are assumed to be applicable to any machine type
        PE_FILE_MACHINE_AM33      = 0x1d3,  // Matsushita AM33
        PE_FILE_MACHINE_AMD64     = 0x8664, // x64
        PE_FILE_MACHINE_ARM       = 0x1c0,  // ARM little endian
        PE_FILE_MACHINE_EBC       = 0xebc,  // EFI byte code
        PE_FILE_MACHINE_I386      = 0x14c,  // Intel 386 or later processors and compatible processors
        PE_FILE_MACHINE_IA64      = 0x200,  // Intel Itanium processor family
        PE_FILE_MACHINE_M32R      = 0x9041, // Mitsubishi M32R little endian
        PE_FILE_MACHINE_MIPS16    = 0x266,  // MIPS16
        PE_FILE_MACHINE_MIPSFPU   = 0x366,  // MIPS with FPU
        PE_FILE_MACHINE_MIPSFPU16 = 0x466,  // MIPS16 with FPU
        PE_FILE_MACHINE_POWERPC   = 0x1f0,  // Power PC little endian
        PE_FILE_MACHINE_POWERPCFP = 0x1f1,  // Power PC with floating point support
        PE_FILE_MACHINE_R4000     = 0x166,  // MIPS little endian
        PE_FILE_MACHINE_SH3       = 0x1a2,  // Hitachi SH3
        PE_FILE_MACHINE_SH3DSP    = 0x1a3,  // Hitachi SH3 DSP
        PE_FILE_MACHINE_SH4       = 0x1a6,  // Hitachi SH4
        PE_FILE_MACHINE_SH5       = 0x1a8,  // Hitachi SH5
        PE_FILE_MACHINE_THUMB     = 0x1c2,  // Thumb
        PE_FILE_MACHINE_WCEMIPSV2 = 0x169,  // MIPS little-endian WCE v2      

      };
      
      
    // The Characteristics field contains flags that indicate attributes of the object or image file. 
    // The following flags are currently defined.    
    enum pe32_characteristics 
      {

        PE_FILE_RELOCS_STRIPPED   = 0x0001,
          // Image only, Windows CE, and Windows NT® and later. 
          // This indicates that the file does not contain base 
          // relocations and must therefore be loaded at its preferred 
          // base address. If the base address is not available, 
          // the loader reports an error. The default behavior of the 
          // linker is to strip base relocations from executable (EXE) files.
        PE_FILE_EXECUTABLE_IMAGE  = 0x0002,
          // Image only. This indicates that the image file is valid 
          // and can be run. If this flag is not set, it indicates a linker error.
        PE_FILE_LINE_NUMS_STRIPPED = 0x0004,
          // COFF line numbers have been removed. 
          // This flag is deprecated and should be zero.
        PE_FILE_LOCAL_SYMS_STRIPPED = 0x0008,
          // COFF symbol table entries for local symbols have been removed. 
          // This flag is deprecated and should be zero.
        PE_FILE_AGGRESSIVE_WS_TRIM = 0x0010,
          // Obsolete. Aggressively trim working set. This flag is deprecated 
          // for Windows 2000 and later and must be zero.
        PE_FILE_LARGE_ADDRESS_ AWARE = 0x0020,
          // Application can handle > 2-GB addresses.
        // = 0x0040 - This flag is reserved for future use.
        PE_FILE_BYTES_REVERSED_LO = 0x0080,
          // Little endian: the least significant bit (LSB) precedes 
          // the most significant bit (MSB) in memory. 
          // This flag is deprecated and should be zero.
        PE_FILE_32BIT_MACHINE = 0x0100,
          // Machine is based on a 32-bit-word architecture.
        PE_FILE_DEBUG_STRIPPED = 0x0200,
          // Debugging information is removed from the image file.
        PE_FILE_REMOVABLE_RUN_FROM_SWAP = 0x0400,
          // If the image is on removable media, fully load it and copy it to the swap file.
        PE_FILE_NET_RUN_FROM_SWAP = 0x0800,
          // If the image is on network media, fully load it and copy it to the swap file.
        PE_FILE_SYSTEM = 0x1000,
          // The image file is a system file, not a user program.
        PE_FILE_DLL = 0x2000,
          // The image file is a dynamic-link library (DLL). 
          // Such files are considered executable files for almost all purposes, 
          // although they cannot be directly run.
        PE_FILE_UP_SYSTEM_ONLY = 0x4000,
          // The file should be run only on a uniprocessor machine.
        PE_FILE_BYTES_REVERSED_HI = 0x8000,
          // Big endian: the MSB precedes the LSB in memory. 
          //This flag is deprecated and should be zero.      
      };
    
    enum 
      {
        PE_OPT32_MAGIC        = 0x10b,
        PE_OPT64_MAGIC        = 0x20b,
        PE_DOS_SIGNATURE      = 0x5A4D,
        PE_NT_SIGNATURE       = 0x00004550,
      }
    
    // The following values defined for the Subsystem field of the optional header determine 
    // which Windows subsystem (if any) is required to run the image.
    enum pe32_subsystem
      {
        PE_SUBSYSTEM_UNKNOWN        = 0,    // An unknown subsystem
        PE_SUBSYSTEM_NATIVE         = 1,    // Device drivers and native Windows processes
        PE_SUBSYSTEM_WINDOWS_GUI    = 2,    // The Windows graphical user interface (GUI) subsystem
        PE_SUBSYSTEM_WINDOWS_CUI    = 3,    // The Windows character subsystem
        PE_SUBSYSTEM_POSIX_CUI      = 7,    // The Posix character subsystem
        PE_SUBSYSTEM_WINDOWS_CE_GUI = 9,    // Windows CE
        PE_SUBSYSTEM_EFI_APPLICATION  = 10, // An Extensible Firmware Interface (EFI) application
        PE_SUBSYSTEM_EFI_BOOT_SERVICE_DRIVER = 11, //An EFI driver with boot services
        PE_SUBSYSTEM_EFI_RUNTIME_DRIVER  = 12, // An EFI driver with run-time services
        PE_SUBSYSTEM_EFI_ROM        = 13, // An EFI ROM image
        PE_SUBSYSTEM_XBOX           = 14, // XBOX    
      };
      
    // The following values are defined for the DllCharacteristics field of the optional header.
    enum pe32_dll_characteristics
      {
        // = 0x0001  - Reserved, must be zero.
        // = 0x0002  - Reserved, must be zero.
        // = 0x0004  - Reserved, must be zero.
        // = 0x0008  - Reserved, must be zero.
        PE_DLL_CHARACTERISTICS_DYNAMIC_BASE     = 0x0040, // DLL can be relocated at load time.
        PE_DLL_CHARACTERISTICS_FORCE_INTEGRITY  = 0x0080, // Code Integrity checks are enforced.
        PE_DLL_CHARACTERISTICS_NX_COMPAT        = 0x0100, // Image is NX compatible.
        PE_DLLCHARACTERISTICS_NO_ISOLATION      = 0x0200, // Isolation aware, but do not isolate the image.
        PE_DLLCHARACTERISTICS_NO_SEH            = 0x0400, 
          // Does not use structured exception (SE) handling. No SE handler may be called in this image.
        PE_IMAGE_DLLCHARACTERISTICS_NO_BIND     = 0x0800, // Do not bind the image.
        // = 0x1000 - Reserved, must be zero.
        PE_DLLCHARACTERISTICS_WDM_DRIVER        = 0x2000, // A WDM driver.
        PE_DLLCHARACTERISTICS_TERMINAL_SERVER_AWARE = 0x8000 // Terminal Server aware.      
      };
      
    struct dos_header
      {
        u16_t   e_magic;       // Magic number
        u16_t   e_cblp;        // Bytes on last page of file
        u16_t   e_cp;          // Pages in file
        u16_t   e_crlc;        // Relocations
        u16_t   e_cparhdr;     // Size of header in paragraphs
        u16_t   e_minalloc;    // Minimum extra paragraphs needed
        u16_t   e_maxalloc;    // Maximum extra paragraphs needed
        u16_t   e_ss;          // Initial (relative) SS value
        u16_t   e_sp;          // Initial SP value
        u16_t   e_csum;        // Checksum
        u16_t   e_ip;          // Initial IP value
        u16_t   e_cs;          // Initial (relative) CS value
        u16_t   e_lfarlc;      // File address of relocation table
        u16_t   e_ovno;        // Overlay number
        u16_t   e_res[4];      // Reserved words
        u16_t   e_oemid;       // OEM identifier (for e_oeminfo)
        u16_t   e_oeminfo;     // OEM information; e_oemid specific
        u16_t   e_res2[10];    // Reserved words
        u32_t   e_lfanew;      // File address of new exe header

        bool GoodSig() { return e_magic == PE_DOS_SIGNATURE; }
      };

    struct coff_file_header
      {
        u16_t Machine;
        u16_t NumberOfSections;
        u32_t TimeDateStamp;
        u32_t PointerToSymbolTable;
        u32_t NumberOfSymbols;
        u16_t SizeOfOptionalHeader;
        u16_t Characteristics;
      };

    struct pe32_data_directory 
      {
        u32_t VirtualAddress;
        u32_t Size;
      };

    struct pe32_section_header 
      {
        byte_t Name[8];
        union {
          u32_t PhysicalAddress;
          u32_t VirtualSize;
        } Misc;
        u32_t VirtualAddress;
        u32_t SizeOfRawData;
        u32_t PointerToRawData;
        u32_t PointerToRelocations;
        u32_t PointerToLinenumbers;
        u16_t NumberOfRelocations;
        u16_t NumberOfLinenumbers;
        u32_t Characteristics;
      };

    template < class x86_64 > struct p32_optional_baseof { u32_t Code; u32_t Data; };
    template <> struct p32_optional_baseof<pc_amd64> { u32_t Code; };  

    template < class x86_64 > struct pe32_optional_header
      {
        typedef typename x86_64::dwordptr_t dpr_t;
        u16_t   Magic;
        byte_t  MajorLinkerVersion;
        byte_t  MinorLinkerVersion;
        u32_t   SizeOfCode;
        u32_t   SizeOfInitializedData;
        u32_t   SizeOfUninitializedData;
        u32_t   AddressOfEntryPoint;
        p32_optional_baseof<x86_64> BaseOf;
        dpr_t   ImageBase;
        u32_t   SectionAlignment;
        u32_t   FileAlignment;
        u16_t   MajorOperatingSystemVersion;
        u16_t   MinorOperatingSystemVersion;
        u16_t   MajorImageVersion;
        u16_t   MinorImageVersion;
        u16_t   MajorSubsystemVersion;
        u16_t   MinorSubsystemVersion;
        u32_t   Win32VersionValue;
        u32_t   SizeOfImage;
        u32_t   SizeOfHeaders;
        u32_t   CheckSum;
        u16_t   Subsystem;
        u16_t   DllCharacteristics;
        dpr_t   SizeOfStackReserve;
        dpr_t   SizeOfStackCommit;
        dpr_t   SizeOfHeapReserve;
        dpr_t   SizeOfHeapCommit;
        u32_t   LoaderFlags;
        u32_t   NumberOfRvaAndSizes;
        pe32_data_directory DataDirectory[0x10];
      };
    
    template < class x86_64 > struct pe32_headers
      {
        u32_t Signature;
        coff_file_header FileHeader;
        pe32_optional_header<x86_64> OptionalHeader;

        pe32_section_header *QueryFirstSection() 
          {
            return (pe32_section_header*)
              ((byte_t*)this
                +((longptr_t)&((nt_headers_t*)0)->OptionalHeader)
                +FileHeader.SizeOfOptionalHeader);
          }
    
        int SizeOfRequiredBuffer()
          {
            return sizeof(pe32_headers<x86_64>) 
              + FileHeader.SizeOfOptionalHeader - ((longptr_t)&((pe32_headers<x86_64>*)0)->OptionalHeader)
              + sizeof(section_header) * FileHeader.NumberOfSections;
          }
      
        bool GoodSig() { return Signature == PE_NT_SIGNATURE; }
      };
    
    struct pe32_import_descriptor
      {
        union 
          {
            u32_t Characteristics;    // 0 for terminating null import descriptor
            u32_t OriginalFirstThunk; // RVA to original unbound IAT (PIMAGE_THUNK_DATA)
          };
        u32_t   TimeDateStamp;        // 0 if not bound,
                                      // -1 if bound, and real date\time stamp
                                      //     in IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT (new BIND)
                                      // O.W. date/time stamp of DLL bound to (Old BIND)
        u32_t ForwarderChain;         // -1 if no forwarders
        u32_t Name;
        u32_t FirstThunk;             // RVA to IAT (if bound this IAT has actual addresses)
      };
  }
