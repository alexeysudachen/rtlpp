
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#if !defined nano_cxx_sys_winreg_HXX && SYSTEM_IS_WINDOWS
#define nano_cxx_sys_winreg_HXX

#include "../format.hxx"
#include "../rccptr.hxx"

namespace nano 
  {

    XPTDECLARE( WinRegError, RuntimeXpt, "nano::WinRegError");

    struct WinReg: __refcounted__
      {
        HKEY hkey;
    
        CXX_NO_INLINE RccPtr<WinReg> Create(Unicode const &subkey)
          {
            HKEY rkey = 0;
            long err = 0;
            if ( ERROR_SUCCESS == (err = RegCreateKeyExW(hkey,+subkey,0,0,0,KEY_ALL_ACCESS,0,&rkey,0)) )
              return RccPtr<WinReg>(new WinReg(rkey));
            OCCURED(WinRegError,_S*"subkey '%08x\\%s' creation failed: errcode %08x"%hkey%subkey%err);
            return RccPtr<WinReg>(0);
          }
      
        CXX_NO_INLINE RccPtr<WinReg> Open(Unicode const &subkey)
          {
            HKEY rkey = 0;
            long err = 0;
            if ( ERROR_SUCCESS == (err = RegOpenKeyExW(hkey,+subkey,0,KEY_ALL_ACCESS,&rkey)) )
              return RccPtr<WinReg>(new WinReg(rkey));
            OCCURED(WinRegError,_S*"subkey '%08x\\%s' opening failed: errcode %08x"%hkey%subkey%err);
            return RccPtr<WinReg>(0);
          }
    
        CXX_NO_INLINE RccPtr<WinReg> OpenReadonly(Unicode const &subkey)
          {
            HKEY rkey = 0;
            long err = 0;
            if ( ERROR_SUCCESS == (err = RegOpenKeyExW(hkey,+subkey,0,KEY_READ,&rkey)) )
              return RccPtr<WinReg>(new WinReg(rkey));
            OCCURED(WinRegError,_S*"subkey '%08x\\%s' opening failed: errcode %08x"%hkey%subkey%err);
            return RccPtr<WinReg>(0);
          }

        CXX_NO_INLINE String QueryString(Unicode const &name, Strarg dflt=L"")
          {
            DWORD ltype = REG_SZ;
            long err = 0;
            DWORD L = 0;
            RegQueryValueExW(hkey,+name,0,&ltype,0,&L);
            Unicode q(L);
            if ( ERROR_SUCCESS == (err=RegQueryValueExW(hkey,+name,0,&ltype,(LPBYTE)+q,&L)) )
              return q;
            if ( err != ERROR_FILE_NOT_FOUND )
              OCCURED(WinRegError,_S*"Query value of '%s' failed: errcode %08x" %name%err);
            return String(+dflt);
          }
      
        CXX_NO_INLINE bool SetString(Unicode const &name, Unicode const &value)
          {
            DWORD ltype = REG_SZ;
            long err = 0;
            if ( ERROR_SUCCESS == (err=RegSetValueExW(hkey,+name,0,ltype,(LPBYTE)value,L)) )
              return true;
            OCCURED(WinRegError,_S*"Set value of '%s' failed: errcode %08x" %name%err);
            return false;
          }
      
        CXX_NO_INLINE u32_t QueryDword(Unicode const &name, u32_t dflt=0)
          {
            DWORD ltype = REG_DWORD;
            DWORD L = 4;
            long err = 0;
            u32_t q = 0;
            if ( ERROR_SUCCESS == (err=RegQueryValueExW(hkey,+name,0,&ltype,(LPBYTE)&q,&L)) )
              return q;
            if ( err != ERROR_FILE_NOT_FOUND )
              OCCURED(WinRegError,_S*"Query value of '%s' failed: errcode %08x" %name%err);
            return dflt;
          }
      
        CXX_NO_INLINE bool SetDword(Unicode const &name, u32_t data)
          {
            DWORD ltype = REG_DWORD;
            long err = 0;
            if ( ERROR_SUCCESS == (err=RegSetValueExW(hkey,+name,0,ltype,(LPBYTE)&data,4)) )
              return true;
            OCCURED(WinRegError,_S*"Set value of '%s' failed: errcode %08x" %name%err);
            return false;
          }
      
        CXX_NO_INLINE Octets QueryBinary(Unicode const &name)
          {
            DWORD ltype = REG_BINARY;
            long err = 0;
            DWORD L = 0;
            Octets q;
            RegQueryValueExW(hkey,+name,0,&ltype,0,&L);
            q.Resize(L);
            if ( ERROR_SUCCESS == (err=RegQueryValueExW(hkey,+name,0,&ltype,(LPBYTE)+q,&L)) )
              return Octets(q,SWAP_CONTENT);
            if ( err != ERROR_FILE_NOT_FOUND )
              OCCURED(WinRegError,_S*"Query value of '%s' failed: errcode %08x" %name%err);
            return Octets();
          }
      
        CXX_NO_INLINE bool SetBinary(Unicode const &name, void const *data, size_t count)
          {
            DWORD ltype = REG_BINARY;
            long err = 0;
            if ( ERROR_SUCCESS == (err=RegSetValueExW(hkey,+name,0,ltype,(LPBYTE)data,count)) )
              return true;
            OCCURED(WinRegError,_S*"Set value of '%s' failed: errcode %08x" %name%err);
            return false;
          }
      
        bool SetBinary(Unicode const &name, Octets const &q)
          { return SetBinary(name,+q,q.Count());}
      

        ~WinReg()
          {
            if ( hkey != 0 )
              RegCloseKey(hkey);
          }
      
        WinReg(HKEY h) : hkey(h) {}

      private:
        
        WinReg(WinReg const&);
        void operator=(WinReg const&);
        friend RccPtr<WinReg> open_key( HKEY base );

        static void *operator new(size_t size) 
          {
            STRICT_REQUIRE( size == sizeof(WinReg) );
            return SharedPool<WinReg>::Malloc();
          } 
          
        static void operator delete( void *p ) { SharedPool<WinReg>::Free(p); }
      };
  
    typedef RccPtr<WinReg> WinRegKey; 

    inline WinRegKey open_key( HKEY base ) { return WinRegKey(new WinReg(base)); }
    inline WinRegKey open_key( HKEY base, Unicode subkey ) { return WinReg(base).OpenSubkey(subkey); }
    inline WinRegKey create_key( HKEY base, Unicode subkey ) { return WinReg(base).CreateSubkey(subkey); }

  } // namespace

#endif // nano_cxx_sys_winreg_HXX && SYSTEM_IS_WINDOWS
