
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../rccptr.hxx"
#include "../xptnfo.hxx"
#include "../format.hxx"
#include "../string.hxx"

#include "win32staff.hxx"

namespace nano
{

	XPTDECLARE(ThreadError, RuntimeXpt, "nano::ThreadError");

#if defined CXX_USES_THREADS

#if SYSTEM_IS_WINDOS
	typedef HANDLE pthread_t;
#endif

	struct ThreadObject : __refcounted__
	{
		enum STATUS
		{
		    ST_PREPARED = 0,
		    ST_STARTING = 1,
		    ST_WORKING  = 2,
		    ST_STOPPING = 3,
		    ST_FINISHED = 4,
		    ST_FAILED   = -1
		};

		ThreadObject() : status(ST_PREPARED) {}

		void Stop()
		{
			if (ThreadIsWorking())
				status = ST_STOPPING;
		}

		bool Start()
		{
			RccPtr<ThreadObject> th_lock = rcc_refe(this);
			th_lock->AddRef(); // extra lock, will relased by thread

			status = ST_STARTING;

#if SYSTEM_IS_WINDOWS
			HANDLE th = CreateThread(0,/*thssize*/0, &start_routine_, this, 0, 0);
			int err = th ? 0 : GetLastError();
			CloseHandle(th);
#else
			pthread_t th;
			pthread_attr_t attr;
			pthread_attr_init(&attr);
			pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
			int err = pthread_create(&th, &attr, &start_routine_, this);
			pthread_attr_destroy(&attr);
#endif

			if (err)
			{
				th_lock->Release(); // opps, release extralock;

				String msg;
#if SYSTEM_IS_WINDOWS
				msg = winerror_format(err);
#else
				switch (err)
				{
					case EINVAL: msg = "invalid thread options"; break;
					case EAGAIN: msg = "to many threads"; break;
					default:     msg = _S*"unknwon error %d" % err; break;
				}
#endif
				status = ST_FAILED;
				OCCURED(ThreadError, msg);
				return false;
			}

			return true;
		}

		XptInfo* Xpt() { return &xpt; }

		bool ThreadIsStarting() const { return status == ST_STARTING; }
		bool ThreadIsWorking()  const { return status == ST_WORKING; }
		bool ThreadIsStopping() const { return status == ST_STOPPING; }
		bool ThreadIsFinished() const { return status == ST_FINISHED; }

		STATUS ThreadStatus() const { return status; }

		void StopAndWait() 
		{
			Stop();
			while ( !ThreadIsFinished() )
				Sleep(100);
		}

	protected:
		volatile STATUS status;
		XptInfo  xpt;

		virtual void Run() {}
		virtual void RunNoXpt() CXX_NO_THROW
		{
#if CXX_USES_EXCEPTIONS
			try {
#endif
				return Run();
#if CXX_USES_EXCEPTIONS
			}
			catch (UnknownXpt const& e)
			{
				xpt.Set(e.Clone());
#if _USESTL
			}
			catch (std::exception& e)
			{
				xpt.Set(new StdCpluplusXpt(e.what()));
#endif
			}
			catch (...)
			{
				xpt.Set(new UnknownError());
			}
#endif
		}

#if SYSTEM_IS_WINDOWS
		static DWORD __stdcall start_routine_(void* p)
#else
		static void* start_routine_(void* p)
#endif
		{
			ThreadObject* thx = (ThreadObject*)p;
			thx->status = ST_WORKING;
			RccPtr<ThreadObject> th_lock = rcc_ptr(thx); // aqcuire extra lock
			thx->RunNoXpt();
			thx->status = ST_FINISHED;
#if SYSTEM_IS_WINDOWS
			ExitThread(0);
#else
			pthread_exit(0);
#endif
			return 0; // fake
		}
	};

	typedef RccPtr<ThreadObject> Thread;

#endif

} // namespace

