
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "../array.hxx"
#include "../string.hxx"
#include "../dicto.hxx"
#include "../xptnfo.hxx"
#include "../atomic.hxx"
#include "../tuple.hxx"
#include "../autoptr.hxx"
#include "../format.hxx"

#include "file.hxx"

namespace nano
{

	XPTDECLARE(IllformedCmdine, Illformed, "nano::IllformedCmdline");

	enum
	{
	    CMDLINE_OPTS_FIRST = 1,
	};

	struct Cmdline
	{
		enum ARGTYPE { NO_ARG = 0, POSSIBLE_ARG = 1, MANDATORY_ARG = 2 };
		typedef Dicto< Pair<int, ARGTYPE> > OptionsDicto;
		typedef Array< Pair<StringArray, bool> > ParamsList;

		ParamsList odats;
		OptionsDicto opts;
		StringArray args;
		String startdir;
		String appdir;
		String fullpath;
		String basename;

		pchar_t Fullpath() const { return +fullpath; }
		pchar_t Basename() const { return +basename; }
		pchar_t Appdir()   const { return +appdir; }
		pchar_t Startdir() const { return +startdir; }

		size_t OptCount(pchar_t opt) const { int Q = HasOpt(opt); return Q <= 0 ? 0 : Q; }

		int HasOpt(Strarg opt) const
		{
			if (!opts.Empty())
				if (Pair<int, ARGTYPE> const* a = opts.Find(opt))
					if (odats.Count() && odats.at(a->_1)._2)
						if (a->_2 == NO_ARG || !odats.at(a->_1)._1.Count())
							return -1;
						else
							return odats.at(a->_1)._1.Count();
			return 0;
		}

		String First(Strarg opt, Strarg dflt = Nil) const
		{
			return Opt(opt, 0, dflt);
		}

		String Opt(Strarg opt, int no, Strarg dflt = Nil) const
		{
			if (!opts.Empty())
				if (Pair<int, ARGTYPE> const* a = opts.Find(opt))
					if (odats.Count() && odats.at(a->_1)._2)
						if (a->_2 != NO_ARG && odats.at(a->_1)._1.Count() > no && no >= 0)
							return odats.at(a->_1)._1[no];
			return dflt;
		}

		long IntFirst(Strarg opt, long dflt = 0) const
		{
			if (String Q = First(opt))
				return as_long(Q);
			return dflt;
		}

		long IntOpt(Strarg opt, int no, long dflt = 0) const
		{
			if (String Q = Opt(opt, no))
				return as_long(Q);
			return dflt;
		}

		bool Init(int argc, char** argv, pchar_t pattern = "", int optional = 0)
		{
#if SYSTEM_IS_WINDOWS

			Array<MallocPtr<char> > tmp_argv(argc);
			for (int i = 0; i < argc; ++i)
			{
				int L = strlen(argv[i]);
				// convert argv: CP_ACP -> UTF-16 -> UTF-8
				Array<wchar_t> tmp(L * 2 + 2);
				MultiByteToWideChar(CP_ACP, 0, argv[i], L, +tmp, L);
				tmp_argv[i] = malloc_ptr(strdup(+Strarg_<128>(tmp)));
			}
			argv = (char**)tmp_argv.Begin();

			SetConsoleCP(CP_UTF8);
			SetConsoleOutputCP(CP_UTF8);

			if (1)
			{
				Array<wchar_t> b(1024);
				int L = GetModuleFileNameW(0, &b[0], 1023);
				fullpath = String(+b, L);
			}

#elif SYSTEM_IS_MACOSX

#else

#endif

			basename = file_basename(fullpath);
			appdir   = file_dirname(fullpath);
			startdir = current_directory();

			OptionsDicto oO;
			ParamsList oD;
			StringArray oA;

			oA.Reserve(argc);
			StringArray L = split(pattern, ",; \t\n\r");
			oD.Reserve(L.Count());

			for (int i = 0; i < L.Count(); ++i)
			{
				StringArray Q = split(L[i], "|");
				if (Q)
				{
					int N = oD.Count();
					oD.Push();//->_2 = false;
					for (int j = 0; j < Q.Count(); ++j)
					{
						ARGTYPE at = NO_ARG;
						if (ends(Q[j], ":"))
							at = MANDATORY_ARG, Q[j] = chop(Q[j]);
						else if (ends(Q[j], "="))
							at = POSSIBLE_ARG, Q[j] = chop(Q[j]);
						oO.Put(+Q[j], pair(N, at));
					}
				}
			}

			bool argument_passed = false;
			for (int i = 1; i < argc; ++i)
			{
				if ((*argv[i] == '-' && argv[i][1] && (argv[i][1] != '-' || argv[i][2]))
				    && (!(optional & CMDLINE_OPTS_FIRST) || !argument_passed))
				{
					char* Q = argv[i] + 1;
					if (*Q == '-') ++Q;
					STRICT_REQUIRE(*Q && *Q != '-');
					StringArray L = split(Q, "=");
					if (Pair<int, ARGTYPE>* a = oO.Find(+L[0]))
					{
						if (a->_2 == MANDATORY_ARG && L.Count() == 1)
						{
							if (argc > i + 1)
							{
								L.Push(argv[i + 1]); ++i;
							}
							else
							{
								OCCURED(IllformedCmdine,
								        _S*"comandline option -%s requires parameter" % L[0]);
								return false;
							}
						}
						if (a->_2 == NO_ARG && L.Count() > 1)
						{
							OCCURED(IllformedCmdine,
							        _S*"comandline option -%s does not have parameter" % L[0]);
							return false;
						}
						oD.at(a->_1)._2 = true;
						if (L.Count() > 1)
							oD.at(a->_1)._1.Push(L[1]);
					}
					else
					{
						OCCURED(IllformedCmdine,
						        _S*"unknown comandline option -%s" % L[0]);
						return false;
					}
				}
				else
				{
					argument_passed = true;
					oA.Push(argv[i]);
				}
			}

			opts  = oO;
			odats = oD;
			args  = oA;

			return true;
		}

		int ArgCount() const
		{
			if (args)
				return args.Count();
			return 0;
		}

		pchar_t Arg(int no, pchar_t dflt = 0) const
		{
			if (args && args.Count() > no && no >= 0)
				return +args.at(no);
			return dflt;
		}

		long IntArg(int no, int dflt = 0) const
		{
			if (args && args.Count() > no && no >= 0)
				return as_long(args.at(no));
			return dflt;
		}
	};

	struct CmdlineDevice
	{
		byte_t none;

		static CXX_NO_INLINE Cmdline* Instance()
		{
			static int clk = 0;
			static Cmdline* proc = 0;
			if (!proc)
				__xchg_lockon__(&clk)
				if (!(void * volatile)proc)
				{
					proc = new Cmdline();
				}
			return proc;
		}

		Cmdline* operator->() const
		{
			return Instance();
		}
	};

	static const CmdlineDevice Cmdline = {0};

	inline void format(TypeQuote<CmdlineDevice> const&, XprintAgent& j, char fmt, unsigned perc)
	{
		(_S*"{comandline: %T %T }"
		 % Cmdline->args
		 % Cmdline->opts
		).FormatOut(j->Writer());
	}
}

