
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "atomic.hxx"

namespace nano
{

	template < class T > struct Typename
	{
		static pchar_t Cstr()
		{
			static const char* name = 0;
			if (!name)
			{
				const std::type_info& ti = typeid(T);
#if CXX_COMPILER_IS_GXX
				int status;
				char* q = __cxxabiv1::__cxa_demangle(ti.name(), 0, 0, &status);
				if (q)
				{
					if (!ATOMIC_CMPXCHNGE_PTR((void**)&name, q, 0)) free((void*)q);
				}
				else
#endif
					name = ti.name();
			}
			return name;
		}
	};

	struct TypeName
	{
		pchar_t name;
		template < class T > TypeName(T const&) : name(Typename<T>::Cstr()) {}
		pchar_t Cstr() const { return name; }
	};

	template < class T >
	struct TypeQuote
	{
		T* q;
		T& operator*() const { return *q; }
		T* operator->() const { return q; }
		T* operator+() const { return q; }
		TypeQuote(T* q) : q(q) {}
		operator TypeName() { return *q; }
	};

} // namespace
