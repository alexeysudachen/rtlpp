/*

 (C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization of the copyright holder.

 */

#pragma once
#include "hashtable.hxx"

namespace nano
{
	template < class T > struct Dicto : Hashtable< pchar_t, T, false >
	{
		typedef Hashtable< pchar_t, T, false > __base;

		Dicto()
			: __base(127)
		{}

		template < class U >
		Dicto(map_Of<pchar_t,U> const &m)
			: __base(127)
		{
			int len = 0;
			map_Of<pchar_t,U> const* q = m.Glue(len);
			for (int i = 0; q ; q = q->next, ++i)
			{
				__base::Put(q->t, q->q);
			}
		}

		T const& Get(Strarg key) const
		{
			return __base::Get(key.Cstr());
		}

		T const& Get(Strarg key, T const& dflt ) const
		{
			return __base::Get(key.Cstr(),dflt);
		}

		T const* Find(Strarg key) const
		{
			return __base::Find(key.Cstr());
		}

		T* Find(Strarg key)
		{
			return __base::Find(key.Cstr());
		}

		T* Put(Strarg key, T const& val = T())
		{
			return __base::Put(key.Cstr(), val);
		}

		void Del(Strarg key)
		{
			return __base::Del(key.Cstr());
		}
			
	};

}

