
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "rccptr.hxx"
#include "require.hxx"

namespace nano
{

	struct IOs;

	template < class T >
	int marshal_unsigned(void* p, int limit, T t)
	{
		STRICT_REQUIRE(p && limit >= (signed)sizeof(T));
		byte_t* q = (byte_t*)p;
		for (int i = 0; i < (signed)sizeof(T); ++i)
		{
			q[i] = (byte_t)(t >> i * 8);
		}
		return sizeof(T);
	}

	template < class T >
	T unmarshal_unsigned(void* p, int limit, int* shift = 0, T* = 0)
	{
		STRICT_REQUIRE(p && limit >= (signed)sizeof(T));
		byte_t const* q = (byte_t const*)p;
		T t;
		for (int i = 0; i < (signed)sizeof(T); ++i)
		{
			t |= ((T)q[i] << i * 8);
		}
		if (shift) *shift = (signed)sizeof(T);
		return t;
	}

	template < class T >
	int marshal_ieee(void* p, int limit, T t)
	{
		STRICT_REQUIRE(p && limit >= (signed)sizeof(T));
		memcpy(p, &t, sizeof(T));
		return sizeof(T);
	}

	template < class T >
	T unmarshal_ieee(void* p, int limit, T* = 0)
	{
		STRICT_REQUIRE(p && limit >= (signed)sizeof(T));
		T t;
		memcpy(&t, p, sizeof(T));
		return t;
	}

#if SYSTEM_IS_WINDOWS
	typedef i64_t foffset_t;
#else
	typedef off_t foffset_t;
#endif

	struct IOs: __refcounted__
	{
		virtual int WriteData(void const* p, int count, int min_count)
		{ OCCURED(Unimplemented, "WriteData is unimplemented in IOs"); return -1; }
		virtual int ReadData(void* p, int count, int min_count)
		{ OCCURED(Unimplemented, "ReadData is unimplemented in IOs"); return -1; }
		virtual foffset_t Seek(foffset_t, int whence)
		{ OCCURED(Unimplemented, "Seek is unimplemented in IOs"); return 0; }
		virtual foffset_t Tell()
		{ OCCURED(Unimplemented, "Tell is unimplemented in IOs"); return 0; }
		virtual foffset_t Available()
		{ OCCURED(Unimplemented, "Available is unimplemented in IOs"); return 0; }
		virtual String Gets()
		{ OCCURED(Unimplemented, "Gets is unimplemented in IOs"); return String(); }

		virtual void Flush() {}
		virtual void Close() {}
		bool Eof() { return !Available(); }

		CXX_NO_INLINE Octets Read(int count = -1, int min_count = -1)
		{
			enum { READ_BLOCK = 512 };
			Octets R;

			if (count > 0 || min_count > 0)
			{
				int sz = cxx_max(neg2zero(count), neg2zero(min_count));
				R.ReserveMore(sz);
				this->ReadData(R.End(), sz, sz);
				if (!XptNfo->Good()) return Octets();
				R.SetCount(R.Count() + sz);
				if (count > 0) count = neg2zero(count - sz);
			}

			while (count != 0 || XptNfo->Good())
			{
				int sz = count > 0 ? count : READ_BLOCK;
				R.ReserveMore(sz);
				int c = this->ReadData(R.End(), sz, 0);
				if (!XptNfo->Good()) return Octets();
				if (!c) break;
				STRICT_REQUIRE(c > 0 && c <= sz);
				STRICT_REQUIRE(count >= c || count < 0);
				R.SetCount(R.Count() + c);
				if (count) count -= c;
			}

			return R;
		}

		enum { COPY_BUFFER_SIZE = 512 };

		CXX_NO_INLINE bool CopyFrom(IOs* ios, int count = -1)
		{
			byte_t copy_buffer[COPY_BUFFER_SIZE];
			while (count != 0)
			{
				int q = count;
				if (count < 0 || count > COPY_BUFFER_SIZE)
					q = COPY_BUFFER_SIZE;
				if (int d = ios->ReadData(copy_buffer, q, 0))
					if (d > 0)
					{
						WriteData(copy_buffer, d, d);
						if (!XptNfo->Good()) return false;
						if (count > 0)
						{
							STRICT_REQUIRE(count <= d);
							count -= d;
						}
					}
					else
						return false;
				else
					break;
			}
			return true;
		}

		bool Read(void* p, int count)
		{ return this->ReadData(p, count, count) == count; }
		bool Write(void const* p, int count)
		{ return this->WriteData(p, count, count) == count; }
		bool Write(Octets const& q)
		{ return Write(+q, q.Count()); }
		bool Puts(Strarg S)
		{ int l = S.Length(); if (l) return Write(+S, l); return true; }

		bool Write8(byte_t b) { return this->Write(&b, 1); }
		bool Write16(u16_t u) { byte_t _[8]; marshal_unsigned<u16_t>(_, 2, u); return this->Write(_, 2); }
		bool Write32(u32_t u) { byte_t _[8]; marshal_unsigned<u32_t>(_, 4, u); return this->Write(_, 4);}
		bool Write64(u64_t u) { byte_t _[8]; marshal_unsigned<u64_t>(_, 8, u); return this->Write(_, 8); }


		byte_t Read8()  { byte_t q; this->Read(&q, 1); return q; }
		u16_t  Read16() { byte_t q[8]; this->Read(&q, 2); return unmarshal_unsigned<u16_t>(q, 2); }
		u32_t  Read32() { byte_t q[8]; this->Read(&q, 4); return unmarshal_unsigned<u32_t>(q, 4); }
		u64_t  Read64() { byte_t q[8]; this->Read(&q, 8); return unmarshal_unsigned<u64_t>(q, 8); }

		bool WriteFloat(float u) { byte_t _[sizeof(u)]; marshal_ieee(_, sizeof(u), u); return this->Write(_, sizeof(u)); }
		float ReadFloat() { byte_t q[sizeof(float)]; this->Read(&q, sizeof(q)); return unmarshal_ieee<float>(q, sizeof(q)); }

		bool WriteDouble(double u) { byte_t _[sizeof(u)]; marshal_ieee(_, sizeof(u), u); return this->Write(_, sizeof(u)); }
		double ReadDouble() { byte_t q[sizeof(double)]; this->Read(&q, sizeof(q)); return unmarshal_ieee<double>(q, sizeof(q)); }

		virtual Octets ReadAll()
		{
			byte_t readbuf[COPY_BUFFER_SIZE];
			Octets o;
			for (;;)
			{
				if (int q = this->ReadData(readbuf, COPY_BUFFER_SIZE, 0))
					if (q > 0)
						o.Append(readbuf, q);
					else
						return Octets();
				else
					break;
			}
			return o;
		}

	protected:
		virtual ~IOs() {}
	};

	typedef RccPtr<IOs> Stream;

	enum
	{
	    STREAM_READ        = 1,
	    STREAM_WRITE       = 2,
	    STREAM_READWRITE   = STREAM_READ | STREAM_WRITE,
	    STREAM_FORWARD     = 32,
	    STREAM_TEXT        = 64, // by default is binary mode
	    STREAM_CREATE_PATH = 128, // create expected path if doesn't exists
	    STREAM_APPEND      = 256,

	    //_STREAM_NEED_EXITENT_FILE = 0x080000,
	    STREAM_OPENEXISTS  = 0x000000, // open if exists
	    STREAM_CREATENEW   = 0x010000, // error if exists
	    STREAM_OPENALWAYS  = 0x020000, // if not exists create new
	    STREAM_CREATEALWAYS = 0x030000, // if exists unlink and create new
	    STREAM_OVERWRITE    = 0x040000, // if exists truncate
	    STREAM_CREATE_MASK = 0x0f0000,

	    STREAM_CREATE      = STREAM_READWRITE | STREAM_CREATE_PATH | STREAM_CREATEALWAYS,
	    STREAM_REUSE       = STREAM_READWRITE | STREAM_OPENALWAYS,
	    STREAM_CONSTANT    = STREAM_READ | STREAM_OPENEXISTS,
	    STREAM_MODIFY      = STREAM_READWRITE | STREAM_OPENEXISTS,
	};

	struct StreamAccess
	{
		int flags;
		StreamAccess() : flags(STREAM_READ) {}
		StreamAccess(int flags) : flags(flags) {}

		StreamAccess(pchar_t s) : flags(0)
		{
			for (; *s; ++s)
				switch (*s)
				{
					case '+': flags |= STREAM_WRITE; /*falldown*/
					case 'r': flags |= STREAM_READ; break;
					case 'a': flags |= STREAM_WRITE | STREAM_APPEND | STREAM_FORWARD | STREAM_OPENALWAYS;
					case 'w': flags |= STREAM_WRITE | STREAM_OVERWRITE; break;
					case 'c': flags |= STREAM_WRITE | STREAM_CREATEALWAYS;; break;
					case 't': flags |= STREAM_TEXT; break;
					case 'P': flags |= STREAM_CREATE_PATH; break;
					default: break;
				}
		}

		int operator*() const { return flags; }

		pchar_t Cstr() const
		{
			if (flags & STREAM_APPEND)
				if (!(flags & STREAM_FORWARD))
					if (!(flags & STREAM_TEXT))
						return "a+b";
					else
						return "a+";
				else if (!(flags & STREAM_TEXT))
					return "ab";
				else
					return "a";

			if (flags & STREAM_WRITE)
				if (flags & STREAM_CREATE_MASK)
					if (flags & STREAM_READ)
						if (!(flags & STREAM_TEXT))
							return "w+b";
						else
							return "w+";
					else if (!(flags & STREAM_TEXT))
						return "wb";
					else
						return "w";
				else if (!(flags & STREAM_TEXT))
					return "r+b";
				else
					return "r+";
			else if (!(flags & STREAM_TEXT))
				return "rb";
			else
				return "r";
		}

	};

	struct MemStream : IOs
	{
		byte_t* S_;
		int i_, L_;
		StreamAccess access_;

		static RccPtr<MemStream> Open(void* S, int L, StreamAccess const& access)
		{
			STRICT_REQUIRE(L >= 0);
			RccPtr<MemStream> m = RccPtr<MemStream>(new MemStream);
			m->access_ = access;
			m->S_ = (byte_t*)S;
			m->i_ = 0;
			m->L_ = L;
			return m;
		}

		foffset_t Available()
		{
			STRICT_REQUIRE(L_ >= i_);
			return L_ - i_;
		}

		int WriteData(void const* p, int count, int min_count)
		{
			STRICT_REQUIRE(count >= 0);
			STRICT_REQUIRE(min_count <= count);
			STRICT_REQUIRE(L_ >= i_);

			if (!(*access_ & STREAM_WRITE))
			{
				OCCURED(AccessDenied, "stream object is immutable");
				return 0;
			}

			int available = L_ - i_;

			if (neg2zero(min_count) > available)
			{
				OCCURED(NoEnoughSpace, "write operation requires more space");
				return 0;
			}

			count = cxx_min(count, available);
			memcpy(S_ + i_, p, count);
			i_ += count;

			return count;
		}

		int ReadData(void* p, int count, int min_count)
		{
			STRICT_REQUIRE(count >= 0);
			STRICT_REQUIRE(min_count <= count);
			STRICT_REQUIRE(L_ >= i_);

			int available = L_ - i_;

			count = cxx_min(count, available);
			memcpy(p, S_ + i_, count);
			i_ += count;

			if (neg2zero(min_count) > available)
				OCCURED(EndOfFile, "no more data");

			return count;
		}

		foffset_t Seek(foffset_t off, int whence)
		{
			STRICT_REQUIRE(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END);
			STRICT_REQUIRE(L_ >= i_);

			switch (whence)
			{
				case SEEK_SET: break;
				case SEEK_CUR: off += i_; break;
				case SEEK_END: off = L_ + off; break;
			}

			if (off > L_ || off < 0)
			{
				OCCURED(NoEnoughSpace, "seek out of stream");
				return 0;
			}

			foffset_t old = i_;
			i_ = off;
			return old;
		}

		foffset_t Tell()
		{
			STRICT_REQUIRE(L_ >= i_);
			return i_;
		}

	private:
		static void* operator new(size_t size)
		{
			STRICT_REQUIRE(size == sizeof(MemStream));
			return SharedPool<MemStream>::Malloc();
		}
		static void operator delete(void* p)
		{ SharedPool<MemStream>::Free(p); }
		MemStream() {}
	};

	Stream mem_open(void* S, void* E, StreamAccess const& access = STREAM_READWRITE)
	{ return MemStream::Open(S, (char*)E - (char*)S, access); }
	Stream mem_open(void* S, int L, StreamAccess const& access = STREAM_READWRITE)
	{ return MemStream::Open(S, L, access); }
	Stream mem_open(void const* S, void const* E, StreamAccess const& access = STREAM_READ)
	{ return MemStream::Open((void*)S, (char*)E - (char*)S, access); }
	Stream mem_open(void const* S, int L, StreamAccess const& access = STREAM_READ)
	{ return MemStream::Open((void*)S, L, access); }
	Stream mem_open(Octets& q, StreamAccess const& access = STREAM_READWRITE)
	{ return MemStream::Open(+q, q.Count(), access); }

	struct OutBuffer : IOs
	{
		Octets data;
		int p;

		Octets reset()
		{
			return data.forget();
		}

		void SwapBuffer(Octets& o)
		{
			data.Swap(o);
			p = 0;
		}

		foffset_t Available()
		{
			int a = data.Count() - p;
			return a < 0 ? 0 : a;
		}

		foffset_t Seek(foffset_t off, int whence)
		{
			STRICT_REQUIRE(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END);

			switch (whence)
			{
				case SEEK_SET: break;
				case SEEK_CUR: off += p; break;
				case SEEK_END: off = data.Count() + off; break;
			}

			if (off < 0)
			{
				OCCURED(NoEnoughSpace, "seek out of stream");
				return 0;
			}

			foffset_t old = p;
			p = (int)off;
			return old;
		}

		foffset_t Tell()
		{
			return p;
		}

		int WriteData(void const* d, int count, int min_count)
		{
			STRICT_REQUIRE(count >= 0);
			STRICT_REQUIRE(min_count <= count);

			if (p > data.Count()) data.Resize(p);
			memcpy(+data + p, d, count);
			p += count;

			return count;
		}

		int ReadData(void* d, int count, int min_count)
		{
			STRICT_REQUIRE(count >= 0);
			STRICT_REQUIRE(min_count <= count);

			int available = neg2zero(data.Count() - p);

			count = cxx_min(count, available);
			memcpy(d, +data + p, count);
			p += count;

			if (neg2zero(min_count) > available)
				OCCURED(EndOfFile, "no more data");

			return count;
		}

	private:
		static void* operator new(size_t size)
		{
			STRICT_REQUIRE(size == sizeof(OutBuffer));
			return SharedPool<OutBuffer>::Malloc();
		}
		static void operator delete(void* p)
		{ SharedPool<OutBuffer>::Free(p); }
		OutBuffer() : p(0) {}
		friend RccPtr<OutBuffer> outbuf_create();
	};

	typedef RccPtr<OutBuffer> Outbuf;

	Outbuf outbuf_create()
	{
		return Outbuf(__new__);
	};

	inline void operator | (Stream const& f, String S) { f->Write(+S, S.Length()); }
	inline void operator | (Outbuf const& f, String S) { f->Write(+S, S.Length()); }

}

