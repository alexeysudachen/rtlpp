
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "../deftypes.hxx"
#include "digest.hxx"

namespace nano
{
	struct MdAlgoBase 
	{
		enum { DIGEST_BYTES_COUNT = 16 };
		enum { DIGEST_BITS_COUNT = 128 };
		u32_t  state[4];
		u32_t  count[2];
		byte_t buffer[64];

		MdAlgoBase()
		{
			count[0] = count[1] = 0;
			state[0] = 0x67452301; state[1] = 0xefcdab89;
			state[2] = 0x98badcfe; state[3] = 0x10325476;
		}

		Digest128 Digest();
		void Update(void const* p, size_t l);
		void Encode(byte_t*, u32_t*, u32_t);
		void Decode(u32_t*, byte_t*, u32_t);

		void Update(ByteRange const &r)
		{
			Update(r.Begin(),r.Count());
		}

		void LongUpdate(ulong_t val)
		{
			byte_t S[sizeof(val)];
			for (int i = 0; i < sizeof(val); ++i)
				S[i] = byte_t(val >> (i * 8));
			Update(S, sizeof(S));
		}

		virtual void Transform(byte_t *) = 0;
	};

	struct MD5 : MdAlgoBase
	{
	private:
		virtual void Transform(byte_t *);
	};

	struct MD4 : MdAlgoBase
	{
	private:
		virtual void Transform(byte_t *);
	};

	template <class Algo>
	Digest128 md_digest(void const* data, size_t len)
	{
		Algo alg;
		alg.Update(data, len);
		return alg.Digest();
	}

	inline Digest128 md5_digest(void const* data, size_t len)
	{
		return md_digest<MD5>(data,len);
	}

	inline Digest128 md4_digest(void const* data, size_t len)
	{
		return md_digest<MD4>(data,len);
	}

	inline Digest128 md5_digest(ByteRange const &r)
	{
		return md_digest<MD5>(r.Begin(),r.Count());
	}

	inline Digest128 md4_digest(ByteRange const &r)
	{
		return md_digest<MD4>(r.Begin(),r.Count());
	}

	inline CXX_NO_INLINE void MdAlgoBase::Update(void const* input, size_t input_length)
	{
		u32_t i, index, partLen;
		index = (u32_t)((count[0] >> 3) & 0x3F);
		if ((count[0] += ((u32_t)input_length << 3)) < ((u32_t)input_length << 3))
			count[1]++;
		count[1] += ((u32_t)input_length >> 29);
		partLen = 64 - index;

		if (input_length >= partLen)
		{
			memcpy(&buffer[index], input, partLen);
			Transform(buffer);
			for (i = partLen; i + 63 < input_length; i += 64)
				Transform(&((byte_t*)input)[i]);
			index = 0;
		}
		else
			i = 0;
		memcpy(&buffer[index], &((byte_t*)input)[i], input_length - i);
	}

	inline CXX_NO_INLINE Digest128 MdAlgoBase::Digest()
	{
		static byte_t PADDING[64] =
		{
			0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		};
		
		byte_t bits[8];
		u32_t index, padLen;
		Encode(bits, count, 8);
		index = (u32_t)((count[0] >> 3) & 0x3f);
		padLen = (index < 56) ? (56 - index) : (120 - index);
		Update(PADDING, padLen);
		Update(bits, 8);

		Digest128 dgst;
		Encode((byte_t*)dgst.bits, state, 16);
		return dgst;
	}

	inline void MdAlgoBase::Encode(byte_t* output, u32_t* input, u32_t len)
	{
		u32_t i, j;

		for (i = 0, j = 0; j < len; i++, j += 4)
		{
			output[j]   = (byte_t)(input[i] & 0xff);
			output[j + 1] = (byte_t)((input[i] >> 8) & 0xff);
			output[j + 2] = (byte_t)((input[i] >> 16) & 0xff);
			output[j + 3] = (byte_t)((input[i] >> 24) & 0xff);
		}
	}

	inline void MdAlgoBase::Decode(u32_t* output, byte_t* input, u32_t len)
	{
		u32_t i, j;

		for (i = 0, j = 0; j < len; i++, j += 4)
			output[i] = ((u32_t)input[j]) | (((u32_t)input[j + 1]) << 8) |
			            (((u32_t)input[j + 2]) << 16) | (((u32_t)input[j + 3]) << 24);
	}


#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (y)) | ((x) & (z)) | ((y) & (z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))
#define FF(a, b, c, d, x, s) { \
		(a) += F ((b), (c), (d)) + (x); \
		(a) = ROTATE_LEFT ((a), (s)); \
	}
#define GG(a, b, c, d, x, s) { \
		(a) += G ((b), (c), (d)) + (x) + (u32_t)0x5a827999; \
		(a) = ROTATE_LEFT ((a), (s)); \
	}
#define HH(a, b, c, d, x, s) { \
		(a) += H ((b), (c), (d)) + (x) + (u32_t)0x6ed9eba1; \
		(a) = ROTATE_LEFT ((a), (s)); \
	}

	inline CXX_NO_INLINE void MD4::Transform(byte_t* block)
	{
		enum
		{
		    S11 = 3,
		    S12 = 7,
		    S13 = 11,
		    S14 = 19,
		    S21 = 3,
		    S22 = 5,
		    S23 = 9,
		    S24 = 13,
		    S31 = 3,
		    S32 = 9,
		    S33 = 11,
		    S34 = 15,
		};

		u32_t a = state[0], b = state[1], c = state[2], d = state[3], x[16];

		Decode(x, block, 64);

		/* Round 1 */
		FF(a, b, c, d, x[ 0], S11);  /* 1 */
		FF(d, a, b, c, x[ 1], S12);  /* 2 */
		FF(c, d, a, b, x[ 2], S13);  /* 3 */
		FF(b, c, d, a, x[ 3], S14);  /* 4 */
		FF(a, b, c, d, x[ 4], S11);  /* 5 */
		FF(d, a, b, c, x[ 5], S12);  /* 6 */
		FF(c, d, a, b, x[ 6], S13);  /* 7 */
		FF(b, c, d, a, x[ 7], S14);  /* 8 */
		FF(a, b, c, d, x[ 8], S11);  /* 9 */
		FF(d, a, b, c, x[ 9], S12);  /* 10 */
		FF(c, d, a, b, x[10], S13);  /* 11 */
		FF(b, c, d, a, x[11], S14);  /* 12 */
		FF(a, b, c, d, x[12], S11);  /* 13 */
		FF(d, a, b, c, x[13], S12);  /* 14 */
		FF(c, d, a, b, x[14], S13);  /* 15 */
		FF(b, c, d, a, x[15], S14);  /* 16 */

		/* Round 2 */
		GG(a, b, c, d, x[ 0], S21);  /* 17 */
		GG(d, a, b, c, x[ 4], S22);  /* 18 */
		GG(c, d, a, b, x[ 8], S23);  /* 19 */
		GG(b, c, d, a, x[12], S24);  /* 20 */
		GG(a, b, c, d, x[ 1], S21);  /* 21 */
		GG(d, a, b, c, x[ 5], S22);  /* 22 */
		GG(c, d, a, b, x[ 9], S23);  /* 23 */
		GG(b, c, d, a, x[13], S24);  /* 24 */
		GG(a, b, c, d, x[ 2], S21);  /* 25 */
		GG(d, a, b, c, x[ 6], S22);  /* 26 */
		GG(c, d, a, b, x[10], S23);  /* 27 */
		GG(b, c, d, a, x[14], S24);  /* 28 */
		GG(a, b, c, d, x[ 3], S21);  /* 29 */
		GG(d, a, b, c, x[ 7], S22);  /* 30 */
		GG(c, d, a, b, x[11], S23);  /* 31 */
		GG(b, c, d, a, x[15], S24);  /* 32 */

		/* Round 3 */
		HH(a, b, c, d, x[ 0], S31);  /* 33 */
		HH(d, a, b, c, x[ 8], S32);  /* 34 */
		HH(c, d, a, b, x[ 4], S33);  /* 35 */
		HH(b, c, d, a, x[12], S34);  /* 36 */
		HH(a, b, c, d, x[ 2], S31);  /* 37 */
		HH(d, a, b, c, x[10], S32);  /* 38 */
		HH(c, d, a, b, x[ 6], S33);  /* 39 */
		HH(b, c, d, a, x[14], S34);  /* 40 */
		HH(a, b, c, d, x[ 1], S31);  /* 41 */
		HH(d, a, b, c, x[ 9], S32);  /* 42 */
		HH(c, d, a, b, x[ 5], S33);  /* 43 */
		HH(b, c, d, a, x[13], S34);  /* 44 */
		HH(a, b, c, d, x[ 3], S31);  /* 45 */
		HH(d, a, b, c, x[11], S32);  /* 46 */
		HH(c, d, a, b, x[ 7], S33);  /* 47 */
		HH(b, c, d, a, x[15], S34);  /* 48 */

		state[0] += a;
		state[1] += b;
		state[2] += c;
		state[3] += d;

		memset(x, 0, sizeof(x));
	}

#undef F
#undef G
#undef H
#undef ROTATE_LEFT
#undef FF
#undef GG
#undef HH


#define ROTATE_LEFT(x,n) (((x) << (n)) | ((x) >> (32-(n))))
#define F(x, y, z) (((x) & (y)) | (~(x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & ~(z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | ~(z)))
#define FF(a, b, c, d, x, s, ac) (a) += F((b), (c), (d)) + (x) + (ac); (a) = ROTATE_LEFT((a), (s)) + (b)
#define GG(a, b, c, d, x, s, ac) (a) += G((b), (c), (d)) + (x) + (ac); (a) = ROTATE_LEFT((a), (s)) + (b)
#define HH(a, b, c, d, x, s, ac) (a) += H((b), (c), (d)) + (x) + (ac); (a) = ROTATE_LEFT((a), (s)) + (b)
#define II(a, b, c, d, x, s, ac) (a) += I((b), (c), (d)) + (x) + (ac); (a) = ROTATE_LEFT((a), (s)) + (b)

	inline CXX_NO_INLINE  void MD5::Transform(byte_t* block)
	{
		enum
		{
		    S11 = 7,
		    S12 = 12,
		    S13 = 17,
		    S14 = 22,
		    S21 = 5,
		    S22 = 9,
		    S23 = 14,
		    S24 = 20,
		    S31 = 4,
		    S32 = 11,
		    S33 = 16,
		    S34 = 23,
		    S41 = 6,
		    S42 = 10,
		    S43 = 15,
		    S44 = 21
		};

		u32_t a = state[0], b = state[1], c = state[2], d = state[3], x[16];

		Decode(x, block, 64);

		/* Round 1 */
		FF(a, b, c, d, x[ 0], S11, 0xd76aa478);  /* 1 */
		FF(d, a, b, c, x[ 1], S12, 0xe8c7b756);  /* 2 */
		FF(c, d, a, b, x[ 2], S13, 0x242070db);  /* 3 */
		FF(b, c, d, a, x[ 3], S14, 0xc1bdceee);  /* 4 */
		FF(a, b, c, d, x[ 4], S11, 0xf57c0faf);  /* 5 */
		FF(d, a, b, c, x[ 5], S12, 0x4787c62a);  /* 6 */
		FF(c, d, a, b, x[ 6], S13, 0xa8304613);  /* 7 */
		FF(b, c, d, a, x[ 7], S14, 0xfd469501);  /* 8 */
		FF(a, b, c, d, x[ 8], S11, 0x698098d8);  /* 9 */
		FF(d, a, b, c, x[ 9], S12, 0x8b44f7af);  /* 10 */
		FF(c, d, a, b, x[10], S13, 0xffff5bb1);  /* 11 */
		FF(b, c, d, a, x[11], S14, 0x895cd7be);  /* 12 */
		FF(a, b, c, d, x[12], S11, 0x6b901122);  /* 13 */
		FF(d, a, b, c, x[13], S12, 0xfd987193);  /* 14 */
		FF(c, d, a, b, x[14], S13, 0xa679438e);  /* 15 */
		FF(b, c, d, a, x[15], S14, 0x49b40821);  /* 16 */

		/* Round 2 */
		GG(a, b, c, d, x[ 1], S21, 0xf61e2562);  /* 17 */
		GG(d, a, b, c, x[ 6], S22, 0xc040b340);  /* 18 */
		GG(c, d, a, b, x[11], S23, 0x265e5a51);  /* 19 */
		GG(b, c, d, a, x[ 0], S24, 0xe9b6c7aa);  /* 20 */
		GG(a, b, c, d, x[ 5], S21, 0xd62f105d);  /* 21 */
		GG(d, a, b, c, x[10], S22,  0x2441453);  /* 22 */
		GG(c, d, a, b, x[15], S23, 0xd8a1e681);  /* 23 */
		GG(b, c, d, a, x[ 4], S24, 0xe7d3fbc8);  /* 24 */
		GG(a, b, c, d, x[ 9], S21, 0x21e1cde6);  /* 25 */
		GG(d, a, b, c, x[14], S22, 0xc33707d6);  /* 26 */
		GG(c, d, a, b, x[ 3], S23, 0xf4d50d87);  /* 27 */
		GG(b, c, d, a, x[ 8], S24, 0x455a14ed);  /* 28 */
		GG(a, b, c, d, x[13], S21, 0xa9e3e905);  /* 29 */
		GG(d, a, b, c, x[ 2], S22, 0xfcefa3f8);  /* 30 */
		GG(c, d, a, b, x[ 7], S23, 0x676f02d9);  /* 31 */
		GG(b, c, d, a, x[12], S24, 0x8d2a4c8a);  /* 32 */

		/* Round 3 */
		HH(a, b, c, d, x[ 5], S31, 0xfffa3942);  /* 33 */
		HH(d, a, b, c, x[ 8], S32, 0x8771f681);  /* 34 */
		HH(c, d, a, b, x[11], S33, 0x6d9d6122);  /* 35 */
		HH(b, c, d, a, x[14], S34, 0xfde5380c);  /* 36 */
		HH(a, b, c, d, x[ 1], S31, 0xa4beea44);  /* 37 */
		HH(d, a, b, c, x[ 4], S32, 0x4bdecfa9);  /* 38 */
		HH(c, d, a, b, x[ 7], S33, 0xf6bb4b60);  /* 39 */
		HH(b, c, d, a, x[10], S34, 0xbebfbc70);  /* 40 */
		HH(a, b, c, d, x[13], S31, 0x289b7ec6);  /* 41 */
		HH(d, a, b, c, x[ 0], S32, 0xeaa127fa);  /* 42 */
		HH(c, d, a, b, x[ 3], S33, 0xd4ef3085);  /* 43 */
		HH(b, c, d, a, x[ 6], S34,  0x4881d05);  /* 44 */
		HH(a, b, c, d, x[ 9], S31, 0xd9d4d039);  /* 45 */
		HH(d, a, b, c, x[12], S32, 0xe6db99e5);  /* 46 */
		HH(c, d, a, b, x[15], S33, 0x1fa27cf8);  /* 47 */
		HH(b, c, d, a, x[ 2], S34, 0xc4ac5665);  /* 48 */

		/* Round 4 */
		II(a, b, c, d, x[ 0], S41, 0xf4292244);  /* 49 */
		II(d, a, b, c, x[ 7], S42, 0x432aff97);  /* 50 */
		II(c, d, a, b, x[14], S43, 0xab9423a7);  /* 51 */
		II(b, c, d, a, x[ 5], S44, 0xfc93a039);  /* 52 */
		II(a, b, c, d, x[12], S41, 0x655b59c3);  /* 53 */
		II(d, a, b, c, x[ 3], S42, 0x8f0ccc92);  /* 54 */
		II(c, d, a, b, x[10], S43, 0xffeff47d);  /* 55 */
		II(b, c, d, a, x[ 1], S44, 0x85845dd1);  /* 56 */
		II(a, b, c, d, x[ 8], S41, 0x6fa87e4f);  /* 57 */
		II(d, a, b, c, x[15], S42, 0xfe2ce6e0);  /* 58 */
		II(c, d, a, b, x[ 6], S43, 0xa3014314);  /* 59 */
		II(b, c, d, a, x[13], S44, 0x4e0811a1);  /* 60 */
		II(a, b, c, d, x[ 4], S41, 0xf7537e82);  /* 61 */
		II(d, a, b, c, x[11], S42, 0xbd3af235);  /* 62 */
		II(c, d, a, b, x[ 2], S43, 0x2ad7d2bb);  /* 63 */
		II(b, c, d, a, x[ 9], S44, 0xeb86d391);  /* 64 */

		state[0] += a;
		state[1] += b;
		state[2] += c;
		state[3] += d;

		// clear sensitive information.
		memset((byte_t*) x, 0, sizeof(x));
	}

#undef F
#undef G
#undef H
#undef I
#undef ROTATE_LEFT
#undef FF
#undef GG
#undef HH
#undef II

}

