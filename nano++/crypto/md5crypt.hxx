
#if !defined nano_cxx_crypto_md5crypt
#define nano_cxx_crypto_md5crypt

namespace nano
  {

    struct Md5_Counter
      {
        
        int left;
        byte_t  bits[16];
        byte_t  counter[sizeof(unit_t)];
        Md5hash left_md5;
        
        Md5_Counter() 
          { 
            memset(bits,0,sizeof(bits)); 
            memset(counter,0,sizeof(counter)); 
            left = 0;
          }
        
        Md5_Counter(char const *key, uint_t starter = 1)
          {
            int l = strlen(key);
            left_md5.Update(key,l);
            left_md5.LongUpdate(l);
            if ( 1 )
              {
                Md5Hash md5 = left_md5;
                byte_t S[16];
                md5.Digest(S);
                left_md5.Update(S);
              }
            for ( int i = 0; i < sizeof(uint_t); ++i )
              counter[i] = byte_t(starter>>(i*8));
            Update();
          }
        
        CXX_NO_INLINE void Update()
          {
            unsigned C = 1;
            for ( int i = 0; i < sizeof(counter); ++i ) 
              { C = counter[i] + C; counter[i] = C; C >>= 8; }
            //left_md5.Update(bits+4,sizeof(bits)-4); // ?!
            Md5Hash md5 = left_md5;
            md5.Update(counter,sizeof(counter));
            md5.Digest(bits);
            left = (sizeof(bits)-4)*8;
          }
        
        ulong_t GetBits(int no)
          {
            ulong_t r = 0;
            while ( no )
              {
                if ( !left )
                  {
                    Update();
                  }
                int q = pop_bits(&r,bits,&left,no);
                no -= q;
              }
            return r;
          }
      };
      
    struct Md5_Decipher_XOR : private Md5_Counter
      {
        Md5_Decipher_XOR() {}
        
        Md5_Decipher_XOR(void const *key,int starter = 1) 
          : Md5_Counter(key,starter);
          {}
          
        inline Decrypt(void *b, uint_t data_len )
          {
            byte_t *p = ptr_cast(b);
            for ( uint_t i = 0; i < dala_len; ++i )
              p[i] ^= Md5_Counter::GetBits(8);
          }
      };
    
    struct Md5_Encipher_XOR : private Md5_Counter
      {
        Md5_Encipher_XOR() {}
        
        Md5_Encipher_XOR(void const *key,int starter = 1) 
          : Md5_Counter(key,starter);
          {}
          
        inline Encrypt(void *b, uint_t data_len )
          {
            byte_t *p = ptr_cast(b);
            for ( uint_t i = 0; i < dala_len; ++i )
              p[i] ^= Md5_Counter::GetBits(8);
          }
      };
      
  }
  
#endif nano_cxx_crypto_md5crypt

