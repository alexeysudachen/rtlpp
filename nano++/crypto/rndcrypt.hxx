
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#if !defined nano_cxx_crypto_rndcrypt_HXX
#define nano_cxx_crypto_rndcrypt_HXX

#include "../lint.hxx"
#include "../random.hxx"
#include "../array.hxx"
#include "../xptnfo.hxx"
#include "../crcx.hxx"

#include "newdes96.hxx"
#include "md5.hxx"

namespace teggox {

  struct RndCryptoKey
    {
      lint_t rsa_E;
      lint_t rsa_N;
      u32_t  seed;
    };

  inline void format(type_quote<RndCryptoKey> q,XprintAgent &j,char fmt,unsigned perc)
    {
      chars_t Q = format_t("{RCK|E:%?,N:%?,S:%d}") %q->rsa_E %q->rsa_N %q->seed;
      j->SprintAjust(+Q,Q.Length());
    }

  inline bool operator == ( RndCryptoKey const &a, RndCryptoKey const &b ) 
    { 
      return a.rsa_E == b.rsa_E &&
             a.rsa_N == b.rsa_N &&
             a.seed  == b.seed;
    }
  inline bool operator != ( RndCryptoKey const &a, RndCryptoKey const &b ) { return !operator==(a,b);}

  inline CXX_NO_INLINE octets_t RndEncrypt(RndCryptoKey ck, byte_range r, u32_t seed = 0)
    {
      int count = range.Count();
      REQUIRE ( !(count & (1<<31)) );
      
      int Q_lbyte = (rsa_N.BitCount()+7)/8;
      int Q_size = (Q_lbyte + 3)&~3;
      if ( Q_size < 16 )
        { OCCURED(Illformed,noxpt,"key is to short, minimum 128-bit is required"); return octets_t(); }
      
      outbuf_t out;
      octets_t Q_buffer(Q_size);
      byte_t *Q = Q_buffer.Begin();
      system_random(Q,Q_lbyte-1);
      
      NewDES96_Counter counter;
      u16_t data_rnd = random_bits(16);;
      byte_t digest[16];
      
      if (1)
        {
          Md5hash md5;
          byte_t q[2] = { byte_t(data_rnd), byte_t(data_rnd>>8) };
          md5.Update(q,2);
          md5.Update(+r.r.Count());
          md5.Digest(digest);
          NewDES96_Encipher e96(digest+8);
          e96.Encrypt(digest);
          counter = NewDES96_Counter(digest,seed);
        }
      
      byte_t crc8 = 0;
      crc8 = Crc8(crc8,digest,8);
      crc8 = Crc8_16(crc8,data_rnd);
      crc8 = Crc8_32(crc8,count);
      
      if ( stream_t p = mem_open(Q,Q_size) )
        {
          p->Write(digest,8);
          p->Write16(data_rnd);
          p->Write32(count);
          p->Write8(crc8);
        }
        
      lint_t::Load(Q,Q_size).expmod(rsa_E,rsa_N).Store(Q,Q_size);
      
      if ( 1 )
        {
          byte_t q[4];
          for ( int i = 0; i < 8; ++i ) q[i] = byte_t(seed >> i*8);
          Md5hash md5;
          md5.Update(q,4);
          md5.Digest(digest);
          NewDES96_Encipher e96(digest+8);
          e96.Encrypt_CBC(Q,Q_size/8);
        }
        
      out->Write(Q,Q_size);
      
      byte_t const *d = +r;
      u32_t C = ((u32_t)data_rnd+1)*((u32_t)(data_rnd<<8|data_rnd>>8)+1)+((seed&0x0f0f0f0f)+1);
      u32_t P = 0;
      
      while ( count )
        {
          unsigned c = cxx_min(count,16);
          for ( int i = 0; i < c; ++i )
            {
              int j = ( C & 3 ) + 1;
              C = C >> 3 | C << 29;
              u32_t q = *d;
              P = ( P << 1 ) ^ q;
              q ^= counter.GetBits(8);
              q = q >> j | q << 8-j;
              Q[i] = q;
              ++d;
            }
          out->Write(Q,c,noxpt);
          count -= c;
          C ^= P&~7;
        }
        
      return out->Reset();
    }

  template < class Gamma > TEGGOX_NOINLINE bool RndDecrypt(
    lint_t const &rsa_E, lint_t const &rsa_N, u32_t seed, 
    IoStream *in, octets_t &out, 
    NoXptNfo *noxpt, Gamma * /*selector*/ )
    {
      typename Gamma::Signature rndS = {0};
      //TEGGOX_STATIC_CHECK( sizeof(rndS) == 8 );
      STRICT_REQUIRE( sizeof(rndS) == 8 );
      
      int Q_size = ((rsa_N.BitCount()+7)/8 + 3)&~3;
      if ( Q_size < 16 )
        { OCCURED__(IllformedException,noxpt,"key is to short, minimum 128-bit is required"); return false; }

      octets_t Q_buffer(Q_size);
      byte_t *Q = Q_buffer.Begin();

      in->ReadFull(Q,Q_size,noxpt);
      if ( noxpt->Occured() ) return false;
      lint_t::Load(Q,Q_size).expmod(rsa_E,rsa_N).Store(Q,Q_size);
      IoStreamPtr p = MemStream::Open(Q,Q+Q_size);
      p->Read(&rndS,sizeof(rndS));
      u16_t  data_rnd = p->Read16le();
      u32_t  count = p->Read32le();
      byte_t orig_crc8 = p->ReadByte();
      
      byte_t crc8 = 0;
      crc8 = Crc8(crc8,&rndS,sizeof(rndS));
      crc8 = Crc8_16(crc8,data_rnd);
      crc8 = Crc8_32(crc8,count);
      
      if ( orig_crc8 != crc8 )
        { OCCURED__(DataCorruptedException,noxpt,"crc check failed"); return false; }

      out.Clear();
      out.Resize(count);
      Gamma rnd = Gamma(rndS,seed);
      
      byte_t *o = +out;
      u32_t C = ((u32_t)data_rnd+1)*((u32_t)(data_rnd<<8|data_rnd>>8)+1)+((seed&0x0f0f0f0f)+1);
      u32_t P = 0;
      
      while ( noxpt->Good() && count )
        {
          unsigned c = cxx_min<unsigned>(count,16);
          in->ReadFull(Q,c,noxpt);
          for ( int i = 0; i < c; ++i )
            {
              int j = (C & 3)+1;
              C = C >> 3 | C << 29;
              u32_t q = Q[i];
              q = (q << j | q >> 8-j) & 0x0ff;
              q ^= rnd.GetBits(8); //rnd.NextC();
              P = ( P << 1 ) ^ q;
              *o = q;
              ++o;
            }
          count -= c;
          C ^= P&~7;
        }
      
      if ( noxpt->Good() )
        if ( /*o != out.End() ||*/ Crc16(0,+out,out.Count()) != data_rnd )
          { OCCURED__(DataCorruptedException,noxpt,"data stream corrupted"); return false; }
      
      if ( noxpt->Occured() ) return false;
      return true;
    }

  inline TEGGOX_NOINLINE bool _RndLoadKeyFile_Fetch(pchar_t pat, chars_t &S, u32_t *q1, lint_t *q2)
    {
      int i = S.Search(pat);
      if ( i > 0 )
        {
          int j = S.Length();
          while ( i < j && !ChrIsDigit(S[i]) && S[i] != '-' ) ++i;
          if ( i < j )
            {
              int k = i; ++k;
              while ( k < j && ChrIsDigit(S[k]) ) ++k;
              chars_t subst = chars_t(&S[i],k-i);
              if ( q2 ) 
                *q2 = +subst;
              else if ( q1 ) 
                *q1 = subst.ToLong(); 
              return true;
            }
        }
        return false;
    }
    
  inline TEGGOX_NOINLINE RndCryptoKey RndLoadKeyFile(IoStream *ios,NoXptNfo *noxpt = CplusplusXpt)
    {
      RndCryptoKey R = { 0, 0, 0 };
      
        while ( noxpt->Good() && !ios->Eof() && ( !R.rsa_E || !R.rsa_N || !R.seed ) )
        {
          chars_t S = ios->ReadLine(noxpt);
          if ( !R.rsa_E && _RndLoadKeyFile_Fetch(" rsa_E = ",S,0,&R.rsa_E) ) continue;
          if ( !R.rsa_N && _RndLoadKeyFile_Fetch(" rsa_N = ",S,0,&R.rsa_N) ) continue;
          if ( !R.seed  && _RndLoadKeyFile_Fetch(" seed = " ,S,&R.seed,0) ) continue;
        }    
      
      if ( noxpt->Good() )
        {
          chars_t not_found;
          if ( !R.rsa_E ) not_found += " rsa_E";
          if ( !R.rsa_N ) not_found += " rsa_E";
          if ( !R.seed )  not_found += " seed";
          if ( not_found )
            OCCURED__(IllformedException,noxpt,_S*"file loaded but not found next key parts:%s"%not_found);
        }
      
      return R;
    }

  inline TEGGOX_NOINLINE RndCryptoKey RndLoadKeyFile(strarg_t filename,NoXptNfo *noxpt = CplusplusXpt)
    {
      if ( _TESTTRACE )
        Clog|_S*"Loading RCK form file %s" %filename;

      if ( CfilePtr f = Cfile::Open(filename,"r",noxpt) )
        return RndLoadKeyFile(+f,noxpt);
      
      return RndCryptoKey();
    }
    
  inline TEGGOX_NOINLINE bool RndWriteKeyFiles(strarg_t prefix, int bits, NoXptNfo *noxpt = CplusplusXpt)
    {
      chars_t name = +prefix;
      
      u32_t seed = 0;
      while ( !seed ) seed = random_bits(32);
      
      lint_t rsa_pub;
      lint_t rsa_priv;
      lint_t rsa_mod;
      lint_t::GenerateRsaKeyPair(&rsa_pub,&rsa_priv,&rsa_mod,bits);
      
      if ( _TESTTRACE )
        Clog|_S*"RSA PUBLIC: %?\nRSA PRIVATE:%?\nRSA_MODULO %?\nSEED: %d" 
          %rsa_pub %rsa_priv %rsa_mod %seed;
      
      if ( _TESTTRACE )
        Clog|_S*"Writing private RCK into file %s.pri.hxx" %name.Lower();
        
      if ( CfilePtr f = Cfile::Open(_S*"%s.pri.hxx"%name.Lower(),"w+",noxpt) )
        {
          f->Print(_S*   
                  "inline ::teggox::RndCryptoKey %sPrivateKey()\n  {\n"
                  "    static ::teggox::lint_t rsa_E = \"%?\";\n"
                  "    static ::teggox::lint_t rsa_N = \"%?\";\n"
                  "    static u32_t seed = %d;\n"
                  "    ::teggox::RndCryptoKey R = { rsa_E, rsa_N, seed };\n"
                  "    return R;\n  }\n"
                  % name
                  % rsa_priv
                  % rsa_mod
                  % seed
                  ,noxpt);
          f->Close();
          
          if ( noxpt->Occured() ) return false;
          
          if ( _SELFCHECK )
            {
              RndCryptoKey orig = { rsa_priv, rsa_mod, seed };
              RndCryptoKey ck = RndLoadKeyFile(_S*"%s.pri.hxx"%name.Lower());
              if ( orig != ck )
                {OCCURED__(SelfCheckError,noxpt,
                  _S*"failed on self check: loaded key pir (%s.pri.hxx) is corrupted %? != %?" 
                    %name %ck %orig);
                  return false; } 
            }
        }
      else return false;
        
      if ( _TESTTRACE )
        Clog|_S*"Writing public RCK into file %s.pub.hxx" %name.Lower();

      if ( CfilePtr f = Cfile::Open(_S*"%s.pub.hxx"%name.Lower(),"w+",noxpt) )
        {
          f->Print(_S*   
                  "inline ::teggox::RndCryptoKey %sPublicKey()\n  {\n"
                  "    static ::teggox::lint_t rsa_E = \"%?\";\n"
                  "    static ::teggox::lint_t rsa_N = \"%?\";\n"
                  "    static u32_t seed = %d;\n"
                  "    ::teggox::RndCryptoKey R = { rsa_E, rsa_N, seed };\n"
                  "    return R;\n  }\n"
                  % name
                  % rsa_pub
                  % rsa_mod
                  % seed,
                  noxpt);
          f->Close();

          if ( noxpt->Occured() ) return false;

          if ( _SELFCHECK )
            {
              RndCryptoKey orig = { rsa_pub, rsa_mod, seed };
              RndCryptoKey ck = RndLoadKeyFile(_S*"%s.pub.hxx"%name.Lower());
              if ( orig != ck )
                {OCCURED__(SelfCheckError,noxpt,
                  _S*"failed on self check: loaded key pir (%s.pub.hxx) is corrupted %? != %?" 
                  %name %ck %orig); 
                  return false; }
            }
        }
      else return false;
      
      return true;
    }
    
} // teggox

#endif // nano_cxx_crypto_rndcrypt_HXX

