
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "../deftypes.hxx"

namespace nano
{
	template <size_t count, size_t bitcount> 
	struct DigestBase
	{
		u64_t bits[count]; enum {BITS_COUNT = bitcount, BYTES_COUNT = count};

		inline bool operator ==(DigestBase const& rigth) const 
		{
			return memcmp(this->bits,rigth.bits,count) == 0;
		}

		inline bool operator <(DigestBase const& rigth) const 
		{
			return memcmp(this->bits,rigth.bits,count) < 0;
		}

		inline operator ByteRange() const
		{   
			return ByteRange((cbyte_t*)bits, BYTES_COUNT); 
		}

		inline byte_t* CopyTo(byte_t* bytes, int outlen)
		{
			if (BYTES_COUNT <= outlen)
				memcpy(bytes, bits, BYTES_COUNT);
			else
				BUGTRAP("oubuffer to small");
			return bytes;
		}

		template <int N>
		inline byte_t* CopyTo(byte_t (&bytes)[N])
		{
			NANO_STATIC_CHECK(N >= BYTES_COUNT);
			memcpy(bytes, bits, N);
			return bytes;
		}
	};

	template <size_t bitcount> struct Digest;

	template <> struct Digest<128> : DigestBase<16, 128> {};
	typedef Digest<128> Digest128;
	template <> struct Digest<160> : DigestBase<20, 160> {};
	typedef Digest<160> Digest160;
	template <> struct Digest<256> : DigestBase<32, 256> {};
	typedef Digest<256> Digest256;
	template <> struct Digest<512> : DigestBase<64, 512> {};
	typedef Digest<512> Digest512;

};


