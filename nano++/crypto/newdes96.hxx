
//
// Released to the public domain by Robert Scott
// -  Originally published in Cryptologia, Jan. 1985
//

#if !defined nano_cxx_crypto_newdes96_HXX
#define nano_cxx_crypto_newdes96_HXX

#include "../deftypes.hxx"
#include "../random.hxx"
#include "../bitdata.hxx"

namespace nano 
  {

    struct NewDES96_CipherBase
      {
        byte_t key_[15];

        void Setup(void const *k)
          {
            memcpy(key_,k,15);
          }

        static byte_t const *f_()
          {
            static byte_t f[256] = {
                32, 137, 239, 188, 102, 125, 221, 72, 212, 68, 81, 37, 86, 237, 147, 149,
                70, 229, 17, 124, 115, 207, 33, 20, 122, 143, 25, 215, 51, 183, 138, 142,
                146, 211, 110, 173, 1, 228, 189, 14, 103, 78, 162, 36, 253, 167, 116, 255,
                158, 45, 185, 50, 98, 168, 250, 235, 54, 141, 195, 247, 240, 63, 148, 2,
                224, 169, 214, 180, 62, 22, 117, 108, 19, 172, 161, 159, 160, 47, 43, 171,
                194, 175, 178, 56, 196, 112, 23, 220, 89, 21, 164, 130, 157, 8, 85, 251,
                216, 44, 94, 179, 226, 38, 90, 119, 40, 202, 34, 206, 35, 69, 231, 246,
                29, 109, 74, 71, 176, 6, 60, 145, 65, 13, 77, 151, 12, 127, 95, 199,
                57, 101, 5, 232, 150, 210, 129, 24, 181, 10, 121, 187, 48, 193, 139, 252,
                219, 64, 88, 233, 96, 128, 80, 53, 191, 144, 218, 11, 106, 132, 155, 104,
                91, 136, 31, 42, 243, 66, 126, 135, 30, 26, 87, 186, 182, 154, 242, 123,
                82, 166, 208, 39, 152, 190, 113, 205, 114, 105, 225, 84, 73, 163, 99, 111,
                204, 61, 200, 217, 170, 15, 198, 28, 192, 254, 134, 234, 222, 7, 236, 248,
                201, 41, 177, 156, 92, 131, 67, 249, 245, 184, 203, 9, 241, 0, 27, 46,
                133, 174, 75, 18, 93, 209, 100, 120, 76, 213, 16, 83, 4, 107, 140, 52,
                58, 55, 3, 244, 97, 197, 238, 227, 118, 49, 79, 230, 223, 165, 153, 59
              };
            return f;
          }

        CXX_NO_INLINE void Encrypt8(void *d) const
          {
            byte_t *data = (byte_t*)d;
            byte_t const *key = key_;
            byte_t const *f = f_();
         
            byte_t B0 = data[0], B1 = data[1], B2 = data[2], B3 = data[3], 
                   B4 = data[4], B5 = data[5], B6 = data[6], B7 = data[7];

            int i = 0;
            byte_t ex = 0;
            for (;;)
              {
                B4 = B4 ^ f[B0 ^ key[i] ^ ex];
                if ( ++i == 15 )
                  {
                    i = 0;
                    ex = key[7];
                  }
                B5 = B5 ^ f[B1 ^ key[i] ^ ex];
                if ( ++i == 15 )
                  {
                    i = 0;
                    ex = key[8];
                  }
                B6 = B6 ^ f[B2 ^ key[i] ^ ex];
                if ( ++i == 15 )
                  {
                    i = 0;
                    ex = key[9];
                  }
                B7 = B7 ^ f[B3 ^ key[i] ^ ex];
                if (++i == 15)
                  break;

                B1 = B1 ^ f[B4 ^ key[i++] ^ ex];
                B2 = B2 ^ f[B4 ^ B5];
                B3 = B3 ^ f[B6 ^ key[i++] ^ ex];
                B0 = B0 ^ f[B7 ^ key[i++] ^ ex];
              }

            data[0] = B0;
            data[1] = B1;
            data[2] = B2;
            data[3] = B3;
            data[4] = B4;
            data[5] = B5;
            data[6] = B6;
            data[7] = B7;
          }
        
        CXX_NO_INLINE void Decrypt8(void *d) const
          {
            byte_t *data = (byte_t*)d;
            byte_t const *key = key_;
            byte_t const *f = f_();

            byte_t B0 = data[0], B1 = data[1], B2 = data[2], B3 = data[3], 
                   B4 = data[4], B5 = data[5], B6 = data[6], B7 = data[7];
  
            int i = 14;
            byte_t ex = key[9];
            for (;;)
              {
                B7 = B7 ^ f[B3 ^ key[i] ^ ex];
                if (--i < 0)
                  {
                    i = 14;
                    ex = key[8];
                  }
                B6 = B6 ^ f[B2 ^ key[i] ^ ex];
                if (--i < 0)
                  {
                    i = 14;
                    ex = key[7];
                  }
                B5 = B5 ^ f[B1 ^ key[i] ^ ex];
                if (--i < 0)
                  {
                    i = 14;
                    ex = 0;
                  }
                B4 = B4 ^ f[B0 ^ key[i] ^ ex];
                if (--i < 0)
                  break;

                B0 = B0 ^ f[B7 ^ key[i--] ^ ex];
                B3 = B3 ^ f[B6 ^ key[i--] ^ ex];
                B2 = B2 ^ f[B4 ^ B5];
                B1 = B1 ^ f[B4 ^ key[i--] ^ ex];
              }
          
            data[0] = B0;
            data[1] = B1;
            data[2] = B2;
            data[3] = B3;
            data[4] = B4;
            data[5] = B5;
            data[6] = B6;
            data[7] = B7;
          }
      };
    
    struct NewDES96_Decipher : private NewDES96_CipherBase
      {
        using NewDES96_CipherBase::Setup;
        NewDES96_Decipher() {}
        
        NewDES96_Decipher(void const *key) 
          { 
            Setup(key); 
          }
        
        inline Decrypt(void *b, uint_t data_len )
          {
            int block_count = data_len/8;
            byte_t *data = (byte_t*)d;
            for ( int i = 0; i < block_count; ++i )
              Decrypt8(data+i*8);
          }
      };
    
    struct NewDES96_Encipher : private NewDES96_CipherBase
      {
        using NewDES96_CipherBase::Setup;
        NewDES96_Encipher() {}

        NewDES96_Encipher(void const *key) 
          { 
            Setup(key); 
          }
        
        inline Encrypt(void *b, uint_t data_len )
          {
            int block_count = data_len/8;
            byte_t *data = (byte_t*)d;
            for ( int i = 0; i < block_count; ++i )
              Encrypt8(data+i*8);
          }
      };

    struct NewDES96_Counter : private NewDES96_CipherBase
      {
        int left;
        byte_t bits[8];
        byte_t counter[8];
      
        NewDES96_Counter() 
          { 
            memset(bits,0,sizeof(bits)); 
            memset(counter,0,sizeof(counter)); 
            left = 0;
          }
        
        NewDES96_Counter(void const *key, uint_t starter = 1)
          {
            Setup(key);
            for ( int i = 0; i < 4; ++i )
              counter[i] = byte_t(starter >> i*8);
            Update();
          }

        CXX_NO_INLINE void Update()
          {
            unsigned C = 1;
            for ( int i = 0; i < 4; ++i ) 
              { 
                bits[i*2] ^= ( C = counter[i] + C ); 
                counter[i] = C; 
                C >>= 8; 
              }
            Encrypt8(bits);
            left = sizeof(bits)*8;
          }

        ulong_t GetBits(int no)
          {
            ulong_t r = 0;
            while ( no )
              {
                if ( !left )
                  {
                    Update();
                  }
                int q = pop_bits(&r,bits,&left,no);
                no -= q;
              }
            return r;
          }
      };
     
    struct NewDES96_Decipher_XOR : private NewDES96_Counter
      {
        NewDES96_Decipher_XOR() {}
        
        NewDES96_Decipher_XOR(void const *key,int starter = 1) 
          : NewDES96_Counter(key,starter);
          {}
          
        inline Decrypt(void *b, uint_t data_len )
          {
            byte_t *p = ptr_cast(b);
            for ( uint_t i = 0; i < dala_len; ++i )
              p[i] ^= NewDES96_Counter::GetBits(8);
          }
      };
    
    struct NewDES96_Encipher_XOR : private NewDES96_Counter
      {
        NewDES96_Encipher_XOR() {}
        
        NewDES96_Encipher_XOR(void const *key,int starter = 1) 
          : NewDES96_Counter(key,starter);
          {}
          
        inline Encrypt(void *b, uint_t data_len )
          {
            byte_t *p = ptr_cast(b);
            for ( uint_t i = 0; i < dala_len; ++i )
              p[i] ^= NewDES96_Counter::GetBits(8);
          }
      };

  } // namespace

#endif // nano_cxx_crypto_newdes96_HXX

