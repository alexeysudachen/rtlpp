
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "autoptr.hxx"
#include "guid.hxx"

namespace nano
{

	template < class T > struct RefeStrategy
	{
		static void AddRef (T* p) { if (p) p->AddRef (); }
		static void Release (T* p) { if (p) p->Release (); }
	};

	template < class T > inline T* refe (T* ref)
	{
		RefeStrategy<T>::AddRef (ref);
		return ref;
	}

	template < class T > inline void unrefe (T*& ref)
	{
		RefeStrategy<T>::Release (ref);
		ref = 0;
	}

	struct CXX_NO_VTABLE Ireferred
	{
		virtual u32_t Release () = 0;
		virtual u32_t AddRef () = 0;
		virtual void* QueryInterface (Guid const& guid) { return 0; }
		virtual ~Ireferred () {}
	};

	template < class T = Ireferred >
	struct Refcounted : T
	{
		u32_t Release ()
		{
			if (long refcount = ATOMIC_DECREMENT (&refcount_))
				return refcount;
			else
				return (delete this), 0;
		}

		u32_t AddRef ()
		{
			return ATOMIC_INCREMENT (&refcount_);
		}

		Refcounted () : refcount_ (1) {}
		u32_t _Refcount () { return refcount_; }

	protected:
		~Refcounted () {}

	private:
		Refcounted (const Refcounted&);
		Refcounted& operator = (const Refcounted&);
		int refcount_;
	};

	typedef Refcounted<> __refcounted__;

	template < class T = Ireferred >
	struct RccPtr
	{
		mutable T* ref_;

		typedef RccPtr<T> const& Ref;

		explicit RccPtr (T* t = 0) 
			: ref_ (t) 
		{}

		explicit RccPtr (T* t, bool addref) 
			: ref_ (t)
		{
			if (addref)
				refe(ref_);
		}

		RccPtr (struct NilTag) 
			: ref_ (0) 
		{}
		RccPtr (struct NewTag)
			: ref_ (new T ())
		{}

		RccPtr (const RccPtr<T>& a)
		{
			ref_ = refe(a.ref_);
		}

		~RccPtr ()
		{
			unrefe(ref_);
		}

		template < class Q > operator RccPtr<Q> () const
		{
			return RccPtr<Q> (refe (ref_));
		}

		bool operator ! () const
		{
			return !ref_;
		}

		typedef T* (RccPtr<T>::*BoolType) ();
		operator BoolType () const
		{
			return ref_ != 0 ? &RccPtr<T>::forget_ : 0;
		}

		T& operator * () const
		{
			return *ref_;
		}

		T* operator -> () const
		{
			return ref_;
		}

		const RccPtr& operator= (const RccPtr& a)
		{
			reset_ (refe (a.ref_));
			return *this;
		}

		bool operator == (const RccPtr& a) const
		{
			return ref_ == a.ref_;
		}

		bool operator != (const RccPtr& a) const
		{
			return ref_ != a.ref_;
		}

		bool operator < (const RccPtr& a) const
		{
			return ref_ < a.ref_;
		}

		bool operator > (const RccPtr& a) const
		{
			return ref_ > a.ref_;
		}

		T*& operator + () const
		{
			return  ref_;
		}

		void reset_ (T* t)
		{
			unrefe (ref_);
			ref_ = t;
		}

		T* forget_ ()
		{
			T* t = ref_;
			ref_ = 0;
			return t;
		}

		void Swap (RccPtr& p)
		{
			swap (p.ref_, ref_);
		}

	private:
		void operator= (const T*);
	};

	template < class T > inline
	T* refe (const RccPtr<T>& ref)
	{
		RefeStrategy<T>::AddRef (ref.ref_);
		return ref.ref_;
	}

	template < class T > inline
	void unrefe (RccPtr<T>& ref) { ref.reset_ ((T*)0); }
	template < class T > inline
	T* forget (RccPtr<T>& ref) { return ref.forget_ (); }
	template < class T > inline
	void reset (RccPtr<T>& ref, T* p) { ref.reset_ (p); }
	template < class T > inline
	RccPtr<T> rcc_ptr (T* ref) { return RccPtr<T> (ref); }
	template < class T > inline
	RccPtr<T> rcc_refe (T* ref) { return RccPtr<T> (refe (ref)); }

	template < class T > inline
	void swap (RccPtr<T>& to, RccPtr<T>& from)
	{
		to.Swap (from);
	}

	template < class T >
	RccPtr<T> rcc_cast (RccPtr<> const& p)
	{
		return p
		       ? rcc_refe ((T*) p->QueryInterface (*guid_Of ((T*)0)))
		       : RccPtr<T> (0);
	}
}

