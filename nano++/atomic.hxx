
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"

namespace nano {
#if !CXX_USES_THREADS

#define ATOMIC_INCREMENT(a) (++(*(a)))
#define ATOMIC_DECREMENT(a) (--(*(a)))
#define ATOMIC_BARRIER()

	inline bool ATOMIC_CMPXCHNGE(long* p, long val, long comp)
	{ long q = *p; if (q == comp) { *p = val; return true; } return false; }
	inline bool ATOMIC_CMPXCHNGE(u32_t* p, u32_t val, u32_t comp)
	{ u32_t q = *p; if (q == comp) { *p = val; return true; } return false;  }
	inline bool ATOMIC_CMPXCHNGE(u64_t* p, u64_t val, u64_t comp)
	{ u64_t q = *p; if (q == comp) { *p = val; return true; } return false; }
	inline bool ATOMIC_CMPXCHNGE(i32_t* p, i32_t val, i32_t comp)
	{ u32_t q = *p; if (q == comp) { *p = val; return true; } return false;  }
	inline bool ATOMIC_CMPXCHNGE(i64_t* p, i64_t val, i64_t comp)
	{ u64_t q = *p; if (q == comp) { *p = val; return true; } return false; }

	inline bool ATOMIC_CMPXCHNGE_PTR(void* p, void* val, void* comp)
	{ void* q = *(void**)p; if (q == comp) { *(void**)p = val; return true; } return false; }
	inline bool ATOMIC_CMPXCHNGE_LP(longptr_t* p, longptr_t val, longptr_t comp)
	{ longptr_t q = *p; if (q == comp) { *p = val; return true; } return false; }

	inline void PERFORM_THREAD_SWITCH() { usleep(1); }

#elif SYSTEM_IS_POSIX || ENABLE_GCC_ATOMICS

#include <sched.h>

#if __linux__ && __i386__

	NANO_STATIC_CHECK(sizeof(long) == sizeof(int32_t));

	inline i32_t ATOMIC_XADD(i32_t* p, i32_t l)
	{
		i32_t ret;
		__asm__ __volatile__("lock; xaddl %0,(%1)"
		                     : "=r"(ret) : "r"(p), "0"(l) : "memory");
		return ret;
	}

	inline i32_t ATOMIC_DECREMENT(i32_t* p) { return ATOMIC_XADD(p, -1) - 1; }
	inline u32_t ATOMIC_DECREMENT(u32_t* p) { return ATOMIC_DECREMENT((i32_t*)p); }
	inline i32_t ATOMIC_INCREMENT(i32_t* p) { return ATOMIC_XADD(p, 1) + 1; }
	inline u32_t ATOMIC_INCREMENT(u32_t* p) { return ATOMIC_INCREMENT((i32_t*)p); }

	inline long ATOMIC_DECREMENT(long* p) { return ATOMIC_DECREMENT((i32_t*)p); }
	inline long ATOMIC_INCREMENT(long* p) { return ATOMIC_INCREMENT((i32_t*)p); }

	inline bool ATOMIC_CMPXCHNGE(i32_t* p, i32_t val, i32_t comp)
	{
		i32_t ret;
		__asm__ __volatile__("lock; cmpxchgl %2,(%1)"
		                     : "=a"(ret) : "r"(p), "r"(val), "0"(comp) : "memory");
		return ret == comp;
	}

	inline bool ATOMIC_CMPXCHNGE(u32_t* p, u32_t val, u32_t comp) { ATOMIC_CMPXCHNGE((i32_t*)p, (i32_t)val, (i32_t)comp); }
	inline bool ATOMIC_CMPXCHNGE(long* p, long val, long comp) { ATOMIC_CMPXCHNGE((i32_t*)p, (i32_t)val, (i32_t)comp); }
	inline bool ATOMIC_CMPXCHNGE_PTR(void* p, void* val, void* comp) { ATOMIC_CMPXCHNGE((i32_t*)p, (i32_t)val, (i32_t)comp); };

#else // !__linux__ || !__i386__

	inline i64_t ATOMIC_DECREMENT(i64_t* p) { return __sync_sub_and_fetch((int64_t volatile*)p, 1); }
	inline i64_t ATOMIC_INCREMENT(i64_t* p) { return __sync_add_and_fetch((int64_t volatile*)p, 1); }
	inline i32_t ATOMIC_DECREMENT(i32_t* p) { return __sync_sub_and_fetch((int32_t volatile*)p, 1); }
	inline i32_t ATOMIC_INCREMENT(i32_t* p) { return __sync_add_and_fetch((int32_t volatile*)p, 1); }

	inline bool ATOMIC_CMPXCHNGE(u32_t* p, u32_t val, u32_t comp)
	{ return __sync_bool_compare_and_swap((u32_t * volatile)p, comp, val); }
	inline bool ATOMIC_CMPXCHNGE(i32_t* p, i32_t val, i32_t comp)
	{ return __sync_bool_compare_and_swap((u32_t * volatile)p, comp, val); }
	inline bool ATOMIC_CMPXCHNGE(u64_t* p, u64_t val, u64_t comp)
	{ return __sync_bool_compare_and_swap((u32_t * volatile)p, comp, val); }
	inline bool ATOMIC_CMPXCHNGE(i64_t* p, i64_t val, i64_t comp)
	{ return __sync_bool_compare_and_swap((i64_t * volatile)p, comp, val); }
	inline bool ATOMIC_CMPXCHNGE_PTR(void* p, void* val, void* comp)
	{ return __sync_bool_compare_and_swap((void * volatile*)p, comp, val); }

#if __i386__
	NANO_STATIC_CHECK(sizeof(long) == sizeof(int32_t));
	inline long ATOMIC_DECREMENT(long* p) { return __sync_sub_and_fetch((int32_t volatile*)p, 1); }
	inline long ATOMIC_INCREMENT(long* p) { return __sync_add_and_fetch((int32_t volatile*)p, 1); }
#elif __x86_64
	NANO_STATIC_CHECK(sizeof(long) == sizeof(int64_t));
	inline long ATOMIC_DECREMENT(long* p) { return __sync_sub_and_fetch((int64_t volatile*)p, 1); }
	inline long ATOMIC_INCREMENT(long* p) { return __sync_add_and_fetch((int64_t volatile*)p, 1); }
#endif

#endif // !__linux__ || !__i386__

	inline void PERFORM_THREAD_SWITCH()
	{
		sched_yield();
	}

#elif SYSTEM_IS_WINDOWS

	inline bool ATOMIC_CMPXCHNGE_PTR(void* p, void* val, void* comp)
	{ return InterlockedCompareExchangePointer((void * volatile*)p, val, comp) == comp; }

	#define ATOMIC_BARRIER() _ReadWriteBarrier()

	inline i32_t ATOMIC_DECREMENT(i32_t* p)
	{ return _InterlockedDecrement((long volatile*)p); }
	inline i32_t ATOMIC_INCREMENT(i32_t* p)
	{ return _InterlockedIncrement((long volatile*)p); }
	inline bool ATOMIC_CMPXCHNGE(u32_t* p, u32_t val, u32_t comp)
	{ return _InterlockedCompareExchange((long * volatile)p, val, comp) == (long)comp; }
	inline bool ATOMIC_CMPXCHNGE(i32_t* p, i32_t val, i32_t comp)
	{ return _InterlockedCompareExchange((long * volatile)p, val, comp) == comp; }
	inline long ATOMIC_DECREMENT(long* p)
	{ return _InterlockedDecrement((long volatile*)p); }
	inline long ATOMIC_INCREMENT(long* p)
	{ return _InterlockedIncrement((long volatile*)p); }
	inline bool ATOMIC_CMPXCHNGE(long* p, long val, long comp)
	{ return _InterlockedCompareExchange((long * volatile)p, val, comp) == comp; }
	inline bool ATOMIC_CMPXCHNGE_LP(longptr_t* p, longptr_t val, longptr_t comp)
	{ return _InterlockedCompareExchange((long * volatile)p, val, comp) == (long)comp; }

#if defined  __x86_64

#endif

	extern "C" BOOL __stdcall SwitchToThread();

	inline void PERFORM_THREAD_SWITCH()
	{
		SwitchToThread();
	}

#elif SYSTEM_IS_MACOSX

#include <libkern/OSAtomic.h>
#include <sched.h>

	inline i64_t ATOMIC_DECREMENT(i64_t* p) { return OSAtomicDecrement64((int64_t volatile*)p); }
	inline i64_t ATOMIC_INCREMENT(i64_t* p) { return OSAtomicIncrement64((int64_t volatile*)p); }
	inline i32_t ATOMIC_DECREMENT(i32_t* p) { return OSAtomicDecrement32((int32_t volatile*)p); }
	inline i32_t ATOMIC_INCREMENT(i32_t* p) { return OSAtomicIncrement32((int32_t volatile*)p); }

	inline bool ATOMIC_CMPXCHNGE(u32_t* p, u32_t val, u32_t comp)
	{ return OSAtomicCompareAndSwap32((int32_t)comp, (int32_t)val, (int32_t * volatile)p); }
	inline bool ATOMIC_CMPXCHNGE(i32_t* p, i32_t val, i32_t comp)
	{ return OSAtomicCompareAndSwap32((int32_t)comp, (int32_t)val, (int32_t * volatile)p); }

	inline bool ATOMIC_CMPXCHNGE(u64_t* p, u64_t val, u64_t comp)
	{ return OSAtomicCompareAndSwap64((int64_t)comp, (int64_t)val, (int64_t * volatile)p); }
	inline bool ATOMIC_CMPXCHNGE(i64_t* p, i64_t val, i64_t comp)
	{ return OSAtomicCompareAndSwap64((int64_t)comp, (int64_t)val, (int64_t * volatile)p); }

	inline bool ATOMIC_CMPXCHNGE_PTR(void* p, void* val, void* comp)
	{ return OSAtomicCompareAndSwapPtr(comp, val, (void * volatile*)p); }

#if __i386__
	NANO_STATIC_CHECK(sizeof(long) == sizeof(int32_t));
	inline long ATOMIC_DECREMENT(long* p) { return OSAtomicDecrement32((int32_t volatile*)p); }
	inline long ATOMIC_INCREMENT(long* p) { return OSAtomicIncrement32((int32_t volatile*)p); }
	inline bool ATOMIC_CMPXCHNGE(long* p, long val, long comp)
	{ return OSAtomicCompareAndSwap32((int32_t)comp, (int32_t)val, (int32_t * volatile)p); }
	inline bool ATOMIC_CMPXCHNGE_LP(longptr_t* p, longptr_t val, longptr_t comp)
	{ return OSAtomicCompareAndSwap32((int32_t)comp, (int32_t)val, (int32_t * volatile)p); }
#elif __x86_64
	NANO_STATIC_CHECK(sizeof(long) == sizeof(int64_t));
	inline long ATOMIC_DECREMENT(long* p) { return OSAtomicDecrement64((int64_t volatile*)p); }
	inline long ATOMIC_INCREMENT(long* p) { return OSAtomicIncrement64((int64_t volatile*)p); }
	inline bool ATOMIC_CMPXCHNGE(long* p, long val, long comp)
	{ return OSAtomicCompareAndSwap64((int64_t)comp, (int64_t)val, (int64_t * volatile)p); }
	inline bool ATOMIC_CMPXCHNGE_LP(longptr_t* p, longptr_t val, longptr_t comp)
	{ return OSAtomicCompareAndSwap64((int64_t)comp, (int64_t)val, (int64_t * volatile)p); }
#endif

	inline void PERFORM_THREAD_SWITCH()
	{
		sched_yield();
	}

#endif // SYSTEMS specific

	struct XchgLocker
	{
		int* dta;
		XchgLocker(int* dta) : dta(dta)
		{
			while (!ATOMIC_CMPXCHNGE(dta, 1, 0))
			{
				PERFORM_THREAD_SWITCH();
			}
		}
		~XchgLocker()
		{
			ATOMIC_DECREMENT(dta);
		}
		operator bool () const { return true; }
	};

#if CXX_USES_THREADS
#define __xchg_lockon__(x) \
	if ( ::nano::XchgLocker CXX_LOCAL_ID(xchg_lck) = x ) \
	goto CXX_LABEL; else CXX_LABEL:
#else
#define __xchg_lockon__(x) \
	if (0); else
#endif

	struct AtomicNext
	{
		AtomicNext* volatile next;
	};

	inline void atomic_push(AtomicNext* volatile* list, AtomicNext* item)
	{
		AtomicNext* q;
		do
		{
			ATOMIC_BARRIER();
			q = *list;
			if (q == (AtomicNext*) - 1) continue;
			item->next = q;
		}
		while (!ATOMIC_CMPXCHNGE_PTR((void*)list, (void*)item, (void*)q));
	}

	inline AtomicNext* atomic_pop(AtomicNext* volatile* list)
	{
		AtomicNext* q;
	retry:
		ATOMIC_BARRIER();
		q = *list;
		if (q == 0) return 0;

		if (q == (AtomicNext*) - 1
		    || !ATOMIC_CMPXCHNGE_PTR((void*)list, (void*)~longptr_t(0), (void*)q))
			goto retry;

		ATOMIC_CMPXCHNGE_PTR((void*)list, (void*)q->next, (void*)~longptr_t(0));
		q->next = 0;
		return q;
	}

} // namespace


