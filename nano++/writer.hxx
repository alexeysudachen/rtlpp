
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "utf8code.hxx"

namespace nano
{
	struct UnknownWriter
	{
		UnknownWriter* Writer() { return this; }
		virtual bool Copy(int n, wchar_t const* a) = 0;
		virtual bool Copy(int n, char const* a) = 0;
		virtual bool Fill(int n, char filler) = 0;
		virtual bool Overflow() const = 0;
		virtual int  Count() const = 0;
		virtual void Trim(int len) = 0;
		inline  bool Putc(wchar_t c) { return Copy(1, &c); }
		inline  bool Putc(char c) { return Copy(1, &c); }
		virtual ~UnknownWriter() {}
	};

	inline CXX_NO_INLINE int ZBHWQ_Width(char fmt)
	{
		switch (fmt)
		{
			case 'Z': case 'B': case '@': return 1;
			case 'H': return 2;
			case 'W': return 4;
			case 'Q': return 8;
		}
		return 0;
	}

	struct XprintAgent
	{
		int width;
		char filler;
		char present;
		UnknownWriter& w_;
		XprintAgent(UnknownWriter& w) : w_(w) {};
		~XprintAgent() {}
		UnknownWriter* operator->() { return &w_; }

		template < class T > CXX_NO_INLINE
		void SprintAjust(T const* p, int ln, int quoted = 0)
		{
			if (width < 0)
			{
				if (present == '#') w_.Fill(1, '#');
				w_.Copy(ln, p);
				if (-width > ln)
					w_.Fill(-width - ln, ' ');
			}
			else
			{
				if (filler == '0' && present == '#')
					w_.Fill(1, '#');
				if (width > ln)
					w_.Fill(width - ln, filler);
				if (filler == ' ' && present == '#')
					w_.Fill(1, '#');
				if (quoted)
					w_.Fill(1, quoted);
				w_.Copy(ln, p);
				if (quoted)
					w_.Fill(1, quoted);
			}
		}

		CXX_NO_INLINE void SprintXnumber(unsigned int val, char const symbols[], unsigned bno)
		{
			char foo[(sizeof(val) * 8) + 2];
			unsigned mask = 0x0ff >> (8 - bno);
			int i = sizeof(foo);

			if (val)
			{
				for (unsigned int v = val; v > 0;)
				{
					foo[--i] = symbols[ v & mask ];
					v >>= bno;
				}
			}
			else
				foo[--i] = '0';

			if (present == '$')
			{
				filler = '0';
				width = 2;
				if (val < 30 || val > 127)
					w_.Fill(1, '.');
				else
					w_.Fill(1, (char)val);
				w_.Fill(1, '$');
			}
			SprintAjust(foo + i, sizeof(foo) - i);
		}

		CXX_NO_INLINE void SprintPointer(void const* p)
		{
			char foo[sizeof(longptr_t) * 2 + 1 + 1];
			static char const symbols[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			memset(foo, 'x', sizeof(foo));
			foo[0] = '#';
			foo[sizeof(longptr_t) * 2 + 1] = 0;
			longptr_t v = (longptr_t) p;

			for (int i = sizeof(longptr_t) * 2; v > 0 && i > 0; --i)
			{
				foo[i] = symbols[ v & 0x0f ];
				v >>= 4;
			}

			SprintAjust(foo, sizeof(void*) * 2 + 1);
		}

		template < class tTc, class tTn > CXX_NO_INLINE
		static void XsprintPrepareDigits(tTc* p, unsigned& i, tTn v)
		{
			if (v)
			{
				while (v > 0)
				{
					p[--i] = '0' + (v % 10);
					v /= 10;
				}
			}
			else
				p[--i] = '0';
		}

		CXX_NO_INLINE void SprintFloat(double val, int perc)
		{
			static int perc_[] = { 0, 10, 100, 1000, 10000, 100000, 1000000 };
			if (perc > 6) perc = 6;
			char foo[64];
			unsigned i = sizeof(foo);
			val = val > 0 ? val + 1e-7 : val - 1e-7;
			if (perc)
			{
				unsigned val0 = (int(cxx_abs(val) * perc_[perc])) % perc_[perc] + perc_[perc];
				XsprintPrepareDigits(foo, i, val0);
				foo[i] = '.';
			}
			XsprintPrepareDigits(foo, i, int(cxx_abs(val)));
			if (cxx_is_negative(val)) foo[--i] = '-';
			SprintAjust(foo + i, sizeof(foo) - i);
		}

		template < class X > CXX_NO_INLINE void SprintBinary(X val)
		{
			static char const symbols[] = { '0', '1' };
			SprintXnumber(val, symbols, 1);
		}

		template < class X > CXX_NO_INLINE void SprintXlower(X val)
		{
			static char const symbols[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			SprintXnumber(val, symbols, 4);
		}

		template < class X > CXX_NO_INLINE void SprintXupper(X val)
		{
			static char const symbols[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
			SprintXnumber(val, symbols, 4);
		}

		template < class X > CXX_NO_INLINE void SprintOctet(X val)
		{
			static char const symbols[] = { '0', '1', '2', '3', '4', '5', '6', '7' };
			SprintXnumber(val, symbols, 3);
		}

		template < class X > CXX_NO_INLINE void SprintSigned(X val)
		{
			char foo[16];
			unsigned i = sizeof(foo);
			XsprintPrepareDigits(foo, i, cxx_abs(val));
			if (cxx_is_negative(val)) foo[--i] = '-';
			SprintAjust(foo + i, sizeof(foo) - i);
		}

		template < class X > CXX_NO_INLINE void SprintUnsigned(X val)
		{
			char foo[16];
			unsigned i = sizeof(foo);
			XsprintPrepareDigits(foo, i, val);
			SprintAjust(foo + i, sizeof(foo) - i);
		}

		template < class T > CXX_NO_INLINE void SprintTypedMemory(T val)
		{
			switch (present)
			{
				case '+': SprintUnsigned(val); break;
				case '-': SprintSigned(val); break;
				case '!': width = sizeof(val) * 8; SprintBinary(val); break;
				case '$':
					SprintXlower((byte_t)val);
					break;
				case '#':
				default:
					filler = '0';
					width = sizeof(val) * 2;
					SprintXlower((byte_t)val);
			}
		}

		CXX_NO_INLINE void SprintMemory(char fmt, void const* data, int limit, int pitch = 0)
		{
			int k = ZBHWQ_Width(fmt);
			if (!pitch) pitch = k;
			if (limit < 1) limit = 1;
			char ws = 0;
			byte_t const* S = (byte_t const*)data, *E = S + limit * pitch;
			if (k && S)
				for (; E - S >= k ; S += pitch)
				{
					width = 0;
					if (ws) w_.Fill(1, ws);
					else ws = ' ';
					switch (fmt)
					{
						case 'Z':
							if (!*S) return;
							else
							case 'B': case '@':
							SprintTypedMemory(*(byte_t const*)S); break;
						case 'H':
							SprintTypedMemory(*(u16_t const*)S); break;
						case 'W':
							SprintTypedMemory(*(u32_t const*)S); break;
						case 'Q':
							SprintTypedMemory(*(u64_t const*)S); break;
					}
				}
		}

		CXX_NO_INLINE void SprintChar(char val) { SprintAjust(&val, 1); }
		CXX_NO_INLINE void SprintChar(wchar_t val) { SprintAjust(&val, 1); }

		CXX_NO_INLINE void SprintStringC(char const* val, int limit = 0, int quoted = 0)
		{
			if (!val) val = "";
			int l = strlen(val);
			if (limit && limit < l) l = limit;
			SprintAjust(val, l, quoted);
		}

		enum { MASK_BUFFER_SIZE = 64 };

		CXX_NO_INLINE void SprintStringMaskC(char const* val, int limit = 0, int quoted = 0)
		{
			if (!val) val = "";
			int l = strlen(val);
			if (limit && limit < l) l = limit;
			char Q[MASK_BUFFER_SIZE] = {0};
			if (l > MASK_BUFFER_SIZE) l = MASK_BUFFER_SIZE;
			for (int i = 0; i < l; ++i)
				if (val[i] < 30) Q[i] = '.';
				else Q[i] = val[i];
			SprintAjust(Q, l, quoted);
		}

		CXX_NO_INLINE void SprintStringW(wchar_t const* val, int limit = 0, int quoted = 0)
		{
			if (!val) val = L"";
			int l = wcslen(val);
			if (limit && limit < l) l = limit;
			SprintAjust(val, l, quoted);
		}

		CXX_NO_INLINE void SprintStringMaskW(wchar_t const* val, int limit = 0, int quoted = 0)
		{
			if (!val) val = L"";
			int l = wcslen(val);
			if (limit && limit < l) l = limit;
			wchar_t Q[MASK_BUFFER_SIZE] = {0};
			if (l > MASK_BUFFER_SIZE) l = MASK_BUFFER_SIZE;
			for (int i = 0; i < l; ++i)
				if (val[i] < 30) Q[i] = '.';
				else Q[i] = val[i];
			SprintAjust(Q, l, quoted);
		}
	};

	struct XsprintArglist
	{
		virtual bool OutValue(XprintAgent& xa, unsigned perc, char fmt, int long_fmt = 0) const = 0;
		virtual ~XsprintArglist() {}
	};

#define LongFormOtval(X,T) \
	if ( long_fmt > 1 ) o.X(va_arg(*(va_list*)va_,T long long));\
	else if ( long_fmt > 0 ) o.X(va_arg(*(va_list*)va_,T long));\
	else o.X(va_arg(*(va_list*)va_,T int))

	struct XsprintArglistVa : XsprintArglist
	{
		void* va_;
		virtual bool OutValue(XprintAgent& o, unsigned perc, char fmt, int long_fmt = 0) const
		{
			switch (fmt)
			{
				case 'x': LongFormOtval(SprintXlower, unsigned); break;
				case 'X': LongFormOtval(SprintXupper, unsigned); break;
				case 'b': LongFormOtval(SprintBinary, unsigned); break;
				case 'i': case 'd': LongFormOtval(SprintSigned, signed); break;
				case 'u': LongFormOtval(SprintUnsigned, unsigned); break;
				case 'o': LongFormOtval(SprintOctet, unsigned); break;
				case 'c':
					if (!long_fmt) {o.SprintChar((char)va_arg(*(va_list*)va_, unsigned)); break;}
				case 'C': // else
					o.SprintChar((wchar_t)va_arg(*(va_list*)va_, unsigned)); break;
				case 's':
					if (!long_fmt) {o.SprintStringC(va_arg(*(va_list*)va_, pchar_t), perc); break;}
				case 'S': // else
					o.SprintStringW(va_arg(*(va_list*)va_, pwide_t), perc); break;
				case 'a':
					if (!long_fmt)
						o.SprintStringMaskC(va_arg(*(va_list*)va_, pchar_t), perc);
					else
					case 'A':
					o.SprintStringMaskW(va_arg(*(va_list*)va_, pwide_t), perc);
					break;
				case 'f': case 'g': o.SprintFloat(va_arg(*(va_list*)va_, double), perc); break;
				case 'p': o.SprintPointer(va_arg(*(va_list*)va_, void*)); break;
				case 'Q': // array of qwords    aka long long
				case 'W': // array of words     aka int
				case 'H': // array of halfwords aka short
				case 'B': // array of bytes     aka char
				case 'Z': // array of zero terminated bytes
					o.SprintMemory(fmt, va_arg(*(va_list*)va_, void*), perc);
					break;
					//case 'D': // posix datetime (time_t as time function result)

				default:
					o.SprintStringC("(badformat)", 0);
					return false;
			}
			return true;
		}
		XsprintArglistVa(void* va) : va_(va) {}
		~XsprintArglistVa() {}
	};
#undef LongFormOtval

	template < class T > CXX_NO_INLINE
	int VsprintF(UnknownWriter* w, T const* fmt, XsprintArglist const& xa)
	{
		XprintAgent o(*w);

		unsigned perc;
		int sign;
		T const* fmtI = fmt;

		int start_len = o->Count();

		do
		{
			T const* S = fmtI;
			while (*fmtI && *fmtI != '%') ++fmtI;
			if (S != fmtI) o->Copy(fmtI - S, S);
			if (o->Overflow()) goto lb_end;

			while (*fmtI == '%')
			{
				++fmtI;
				if (*fmtI == '%')
				{
					if (o->Overflow()) goto lb_end;
					o->Fill(1, '%');
					++fmtI;
					continue;
				}

				int long_fmt = 0;

				o.filler = ' ';
				o.width = 0;
				o.present = 0;
				perc = 0;
				sign = 0;

				while (*fmtI)
				{
					if (*fmtI == '-') { sign = -1; ++fmtI; }
					else if (*fmtI == '+') { sign = +1; ++fmtI; }
					else if (*fmtI == '$') { o.present = '$'; ++fmtI; }
					else if (*fmtI == '#') { o.present = '#'; ++fmtI; }
					else if (*fmtI == '!') { o.present = '!'; ++fmtI; }
					else if (*fmtI == '0') { o.filler = '0'; ++fmtI; }
					else if (isdigit(*fmtI))
						for (; isdigit(*fmtI) ; fmtI++)
							o.width = o.width * 10 + (*fmtI - '0');
					else if (*fmtI == '.')
					{
						++fmtI;
						for (; isdigit(*fmtI) ; fmtI++)
							perc = perc * 10 + (*fmtI - '0');
					}
					else
						break;
				}

				if (sign) o.width *= sign;
				while (*fmtI == 'l')
				{ ++long_fmt; ++fmtI; }

				switch (*fmtI)
				{
					case 's': case 'S': case 'a': case 'A':
						perc = abs(o.width);
						break;
					case '@': case 'Z': case 'B': case'H': case 'W': case 'Q': case 'T':
						perc = abs(o.width); o.width = 0;
						if (!o.present && sign) o.present = sign < 0 ? '-' : '+';
						break;
				}

				if (!xa.OutValue(o, perc, *fmtI++, long_fmt)) break;
			}
		}
		while (*fmtI);

	lb_end:
		if (!o->Overflow())
		{
			int count = o->Count();
			o->Fill(1, '\0');
			o->Trim(count);
			return count - start_len;
		}
		else
			return -(o->Count() - start_len);
	}

	template < class T >
	struct GenericWriterHelper : UnknownWriter
	{
		virtual bool Push(T const* a, int& n) = 0;
	};

	template < class T >
	static bool GenericWriterHelper_Write(int& l, T const*& a, GenericWriterHelper<T>* w)
	{
		if (!w->Push(a, l));
		return !l;
	}

	inline bool GenericWriterHelper_Write(int& l, wchar_t const*& a, GenericWriterHelper<char>* w)
	{
		Utf8Encoder e;
		while (l)
		{
			int cnt = 0;
			byte_t b[4];
			e.Encode(b, *a, &cnt);
			if (w->Push((char*)b, cnt))
			{
				++a;
				--l;
			}
			else return false;
		}
		return true;
	}

	inline bool GenericWriterHelper_Write(int& l, char const*& a, GenericWriterHelper<wchar_t>* w)
	{
		Utf8Encoder e;
		while (l > 0)
		{
			int cnt = 0;
			int n = 1;
			wchar_t c = e.Decode(a, &cnt);
			if (w->Push(&c, n))
			{
				a += cnt;
				l -= cnt;
			}
			else return false;
		}
		return true;
	}

	template < class T >
	struct GenericWriter : GenericWriterHelper<T>
	{
		bool overflow;

		GenericWriter() : overflow(false) {}

		/*override*/ bool CXX_NO_INLINE Fill(int n, char filler)
		{
			while (n)
			{
				T c = filler;
				int k = 1;
				if (this->Push(&c, k))
				{ --n; }
				else
				{
					overflow = true;
					return false;
				}
			}
			return true;
		}

		/*override*/ bool CXX_NO_INLINE Copy(int l, wchar_t const* a)
		{
			return GenericWriterHelper_Write(l, a, this);
		}

		/*override*/ bool CXX_NO_INLINE Copy(int l, char const* a)
		{
			return GenericWriterHelper_Write(l, a, this);
		}

		/*override*/ bool Overflow() const { return overflow; }
	};


	template < class T >
	struct RawMemWriter: GenericWriter<T>
	{
		T* p, *S;
		int limit;
		RawMemWriter(T* mem, int limit) : p(mem), S(mem), limit(limit) {}
		/*override*/ bool Push(T const* a, int& n)
		{
			if (n > limit)
				return false;

			for (int l = limit - n; limit > l; --limit, --n)
				*S++ = *a++;

			return true;
		}

		/*override*/ void Trim(int len)
		{
			if (len >= 0 && len < S - p)
				S = p + len;
		};
		/*override*/ int  Count() const { return S - p; }
	};

	template < class Q, class P > CXX_NO_INLINE
	int VsprintF(Q* out, unsigned sz, P const* fmt, void* va)
	{
		RawMemWriter<Q> w(out, sz);
		return VsprintF(&w, fmt, XsprintArglistVa(va));
	}

	template < class Q, class P > CXX_NO_INLINE
	int SprintF(Q* out, size_t sz, P const* fmt, ...)
	{
		va_list va;
		va_start(va, fmt);
		int retval = VsprintF(out, sz, fmt, &va);
		va_end(va);
		return retval;
	}
	/*
	    inline CXX_NO_INLINE int SprintF(void *out, size_t sz, char const *fmt, ...)
	        {
	          va_list va;
	          va_start(va,fmt);
	          int retval = VsprintF((char*)out,sz,fmt,&va);
	          va_end(va);
	          return retval;
	        }
	    inline CXX_NO_INLINE int SprintF(void *out, size_t sz, wchar_t const *fmt, ...)
	        {
	          va_list va;
	          va_start(va,fmt);
	          int retval = VsprintF((wchar_t*)out,sz/sizeof(wchar_t),fmt,&va);
	          va_end(va);
	          return retval;
	        }
	*/
}

