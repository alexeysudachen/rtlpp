
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"

namespace nano
{

	typedef GUID Guid;

	template < class T >
	struct guid_Of_Type
	{
		typedef typename T::Guid Guid;
	};

	template < class T > inline
		Guid const *guid_Of( T const * /*fake*/ = 0)
	{
		typedef typename guid_Of_Type<T>::Guid Guid;
		return &Guid::value;
	}


	template <
		unsigned tLx,
		unsigned tWx1, unsigned tWx2,
		unsigned tBx1, unsigned tBx2, unsigned tBx3, unsigned tBx4,
		unsigned tBx5, unsigned tBx6, unsigned tBx7, unsigned tBx8 >
	struct GuidDef
	{
		static Guid const value;
	};

	template <
		unsigned tLx,
		unsigned tWx1, unsigned tWx2,
		unsigned tBx1, unsigned tBx2, unsigned tBx3, unsigned tBx4,
		unsigned tBx5, unsigned tBx6, unsigned tBx7, unsigned tBx8 >
		Guid const GuidDef <tLx,tWx1,tWx2,tBx1,tBx2,tBx3,tBx4,tBx5,tBx6,tBx7,tBx8>
		::value = { tLx, tWx1, tWx2, { tBx1,tBx2,tBx3,tBx4,tBx5,tBx6,tBx7,tBx8 } };

	struct GuidLesser
	{
		bool operator() ( Guid const &a, Guid const &b ) const
		{
			return memcmp(&a,&b,sizeof(Guid)) < 0;
		}
	};

	inline int guid_cmpf( void const *a, void const *b )
	{
		return memcmp(a,b,sizeof(Guid));
	}
}

#define NANO_DECLARE_GUID(x,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
	typedef ::nano::GuidDef<l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8> x;

#define NANO_DECLARE_GUIDOF(x,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
	template <> \
		struct ::nano::guid_Of_Type<x> { typedef nano::GuidDef<l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8> Guid; };

#define NANO_DECLARE_GUIDOF_(x,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
	struct x; \
	NANO_DECLARE_GUIDOF(x,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#define NANO_GUIDOF_(x) ::nano::guid_Of_type<x>::Guid::value
#define NANO_GUIDOF(x) (*guid_Of((x*)0))


