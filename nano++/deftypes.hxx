
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "compiler.hxx"

#if CXX_COMPILER_IS_cpp0x_COMPATIBLE
#define NANO_STATIC_CHECK(expr) \
	static_assert( expr , #expr )
#else
#define NANO_STATIC_CHECK(expr) \
	void __static_check__( typename ::nano::static_check<expr>::isTrue _ );
#endif

namespace nano
{
	template < class T, int N > int count(const T(&a)[N]) { return N; }

	template < class T > struct Mutable
	{
		mutable T t;
		T* operator +() const { return &t; }
		T& operator *() const { return t; }
		T* operator->() const { return &t; }
		operator T& () { return t; }
		Mutable() {}
		Mutable(int) {}
		template < class X > T& operator=(X const& x) const { t = x; return t; }
	private:
		Mutable(Mutable const&);
		Mutable& operator=(Mutable const&);
	};

	template < bool tTrue = false > struct static_check { enum isFalse { value = 0 }; };
	template <> struct static_check<true> { enum isTrue { value = 1 }; };

	typedef unsigned int      u32_t;
	typedef unsigned short    u16_t;
	typedef unsigned char     byte_t;
	typedef unsigned char     uc8_t;
	typedef int               i32_t;
	typedef short             i16_t;
	typedef char              ic8_t;
	typedef char const*       pchar_t;
	typedef wchar_t const*    pwide_t;
	typedef void const*       pvoid_t;
	typedef u32_t  const      cu32_t;
	typedef u16_t  const      cu16_t;
	typedef byte_t const      cbyte_t;
	typedef unsigned long     ulong_t;
	typedef unsigned int      uint_t;
	typedef unsigned short    ushort_t;

#define CXX_FOUR_CHARS(C1,C2,C3,C4) \
	(((u32_t)(C4)<<24)|((u32_t)(C3)<<16)|((u32_t)(C2)<<8)|((u32_t)(C1)))

	template < bool tTe, class tTt, class tTf > struct cxx_if_else;
	template < class tTt, class tTf > struct cxx_if_else< true, tTt, tTf > { typedef tTt Type; };
	template < class tTt, class tTf > struct cxx_if_else< false, tTt, tTf > { typedef tTf Type; };

#if CXX_COMPILER_IS_MSVC_COMPATIBLE
	typedef unsigned __int64 u64_t;
	typedef __int64 i64_t;
#else
	typedef unsigned long long u64_t;
	typedef long long i64_t;
#endif

	inline u64_t cxx_u64_hilo(u32_t h, u32_t l)
	{
		return ((u64_t)h << 32) | (u64_t)l;
	}

	typedef cxx_if_else<sizeof(u64_t) == sizeof(void*) , u64_t, u32_t>::Type longptr_t;
	typedef cxx_if_else < (sizeof(wchar_t) > sizeof(u16_t)) , u32_t, u16_t >::Type uwchar_t;
	typedef cxx_if_else<sizeof(unsigned long) == 8, u32_t, u16_t>::Type halflong_t;

	NANO_STATIC_CHECK(sizeof(longptr_t) == sizeof(void*));
	NANO_STATIC_CHECK(sizeof(longptr_t) == sizeof(const void*));

	struct pc_i386 { typedef u32_t dwordptr_t; };
	struct pc_amd64 { typedef u64_t dwordptr_t; };

	struct Uncopiable
	{
		Uncopiable() {}
	private:
		Uncopiable(Uncopiable const&);
		Uncopiable& operator=(Uncopiable const&);
	};

	struct Empty {};
	struct VirtualDestructor { virtual ~VirtualDestructor() = 0; };

	template < class T >
	struct ExactType
	{
		T const& _;
		T const& operator *() const { return _; }
		T const* operator ->() const { return &_; }
		ExactType(T const& t) : _(t) {}
	};

#if !SYSTEM_IS_WINDOWS
	typedef struct GUID
	{
		u32_t   Data1;
		u16_t   Data2;
		u16_t   Data3;
		byte_t  Data4[8];
	} GUID;
#endif

	template<class T> struct SelfPtr
	{
		T* operator->() { return (T*)this; }
		T const* operator->() const { return (T const*)this; }	
	};

	template < class X > struct T_signed
	{
		enum { IsSigned = 1 };
		static inline bool is_negative(X x) { return x < 0; }
		static inline X absolute(X x) { return x < 0 ? -x : x; }
	};

	template < class X > struct T_signed_Unsigned
	{
		enum { IsSigned = 0 };
		static inline bool is_negative(X x) { return false; }
		static inline X absolute(X x) { return x; }
	};

	template <> struct T_signed<unsigned char>  : T_signed_Unsigned<unsigned char> {};
	template <> struct T_signed<unsigned short> : T_signed_Unsigned<unsigned short> {};
	template <> struct T_signed<unsigned int>   : T_signed_Unsigned<unsigned int> {};
	template <> struct T_signed<unsigned long>  : T_signed_Unsigned<unsigned long> {};
	template <> struct T_signed<u64_t>          : T_signed_Unsigned<u64_t> {};

#if _MSC_VER > 1400 || (__GNUC__ && __GNUC__ <= 3)
	template <> struct T_signed<wchar_t>  : T_signed_Unsigned<unsigned char> {};
#endif

	template < class X > inline bool cxx_is_negative(X x) { return T_signed<X>::is_negative(x); }
	template < class X > inline X cxx_abs(X x) { return T_signed<X>::absolute(x); }

	template < class X > struct cxx_sizeof { enum { Value = sizeof(X) }; };
	template<> struct cxx_sizeof<void> { enum { Value = 0 }; };

	template < class X > struct aligned_sizeof
	{ enum { Value = (cxx_sizeof<X>::Value + (sizeof(void*) - 1))& ~(sizeof(void*) - 1) }; };

	template < class T > struct ConstRef { typedef T      const& Type; enum { ByRef = 1 }; };
	template <> struct ConstRef<char>    { typedef char   const Type; enum { ByRef = 0 }; };
	template <> struct ConstRef<int>     { typedef int    const Type; enum { ByRef = 0 }; };
	template <> struct ConstRef<long>    { typedef long   const Type; enum { ByRef = 0 }; };
	template <> struct ConstRef<float>   { typedef float  const Type; enum { ByRef = 0 }; };
	template <> struct ConstRef<double>  { typedef double const Type; enum { ByRef = 0 }; };
	template < class T > struct ConstRef<T const*> { typedef T const* const Type; enum { ByRef = 0 }; };

	template < class T, int byref > struct T_refstore { T _; T& assign(T const& t) { _ = t; return _; } };
	template < class T > struct T_refstore<T, 0> { T assign(T t) { return t; } };
	template < class T > void default_construct(void* p) { new(p) T(); }
	template < class T > void default_destruct(void* p) { ((T*)p)->~T(); }
	template <> void default_construct<void>(void* p) {}
	template <> void default_destruct<void>(void* p) {}

	struct PtrCastProxy
	{
		void* p;
		template < class T > inline operator T* () const { return (T*)p; }
		inline PtrCastProxy(void* p) : p(p) {}
	};
	inline PtrCastProxy ptr_cast(void* p) { return PtrCastProxy(p); }

	struct PtrCastConstProxy
	{
		void const* p;
		template < class T > inline operator const T* () const { return (const T*)p; }
		inline PtrCastConstProxy(void const* p) : p(p) {}
	};
	inline PtrCastConstProxy ptr_cast(void const* p) { return PtrCastConstProxy(p); }

	struct NilTag {void* _; operator char* () const { return 0; } };
	static struct NilTag const Nil = {0};
	static struct NilTag const __nil__ = {0};
	struct TakeDataTag { char _; };
	static struct TakeDataTag const __take_data__ = {0};
	struct UseLocalBufferTag { char _; };
	static struct UseLocalBufferTag const __use_local_buffer__ = {0};
	struct EmptyTag { char _; };
	static struct EmptyTag const __empty__ = {0};

	inline int chrs_len(char const* str)
	{   return str ? strlen(str) : 0; }
	inline int chrs_len(wchar_t const* str)
	{   return str ? wcslen(str) : 0; }
	inline int chrs_eql(char const* a, char const* b)
	{   return a == b || (a && !strcmp(a, b)); }
	inline int chrs_eql(wchar_t const* a, wchar_t const* b)
	{   return a == b || (a && !wcscmp(a, b)); }

	struct ByteRange : SelfPtr<ByteRange>
	{
		byte_t const* S;
		int count;

		int Count() const { return count; }
		byte_t const* Begin() const { return S; }
		byte_t const* End() const { return S; }
		byte_t const* operator + () const { return S; }
		byte_t operator[](int i) const { return S[i]; }

		ByteRange(byte_t const* s, int l) : S(s), count(l) {}
		
		template <int N> 
		ByteRange(byte_t const (&b)[N]) 
			: S(b), count(N) {}
		template <int N> 
		ByteRange(char const (&b)[N]) 
			: S(b), count(N) {}

		ByteRange() : S(0), count(0) {}
		ByteRange(struct EmptyTag) : S(0), count(0) {}
	};

	inline ByteRange byte_range(char const* s, char const* e) 
	{	return ByteRange((byte_t const *)s,e-s); }
	inline ByteRange byte_range(char const* s, int l) 
	{	return ByteRange( (byte_t const *)s,l); }
	inline ByteRange byte_range(char const* s) 
	{	return ByteRange((byte_t const*)s, (s ? strlen(s) : 0)); }
	inline ByteRange byte_range(byte_t const* s, byte_t const* e) 
	{	return ByteRange(s,e-s); }
	inline ByteRange byte_range(byte_t const* s, int l) 
	{	return ByteRange(s,l); }
	
	template <int N> 
	inline ByteRange byte_range(byte_t const (&b)[N]) { return ByteRange( b, N ); }

	typedef ByteRange const& Bytesarg;	

	template < class t1, class t2 >
	struct Pair
	{
		t1 _1;
		t2 _2;
		Pair(t1 const& a, t2 const& b) : _1(a), _2(b) {}
		Pair() : _1(t1()), _2(t2()) {}

	};

	template < class t1, class t2 >
	Pair<t1, t2> pair(t1 const& a, t2 const& b) { return Pair<t1, t2>(a, b); }

}
