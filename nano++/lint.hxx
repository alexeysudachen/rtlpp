
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#if !defined nano_cxx_lint_HXX
#define nano_cxx_lint_HXX

#include "deftypes.hxx"
#include "string.hxx"
#include "format.hxx"
#include "prime.hxx"
#include "random.hxx"
#include "xptnfo.hxx"

namespace nano {
  
    struct lint_t
      {
        enum { DIGIT_MASK = ((halflong_t)-1L), DIGIT_SHIFT = sizeof(halflong_t)*8 };
        enum { BITSTEP = sizeof(halflong_t)*8 };
        enum { POSITIVE = 1, NEGATIVE = -1 };
          
        struct data : refcounted_t<Empty>
          {
            enum { SIZE_OF_SPECIFIC = 3, CAPACITY = -3, COUNT = -1, SIGN = -2, CLEAR_ADDICT = 2 };
            halflong_t *value;
            halflong_t *Base() { return value-SIZE_OF_SPECIFIC; }
            size_t Count() const { return value[COUNT]; }
            size_t Capacity() const { return value[CAPACITY]; }
            signed char &sign() { return *(signed char*)(value+SIGN); }
          
            void require_buffer_size(size_t q)
              {
                if ( Capacity() < q )
                  {
                    halflong_t *n = (new halflong_t[q+SIZE_OF_SPECIFIC])+SIZE_OF_SPECIFIC;
                    cxx_swap(n,value);
                    memcpy(Base(),n-SIZE_OF_SPECIFIC,(n[COUNT]+SIZE_OF_SPECIFIC)*sizeof(halflong_t));
                    value[CAPACITY] = q;
                    STRICT_REQUIRE( Count() < q ); 
                    memset(value+Count(),0,(q-Count())*sizeof(halflong_t));
                    delete[] (n-SIZE_OF_SPECIFIC);
                  }
              }
         
            void Reserve(size_t q)
              {
                STRICT_REQUIRE( q > 0 );
                require_buffer_size(q);
              }
          
            void Resize(size_t q)
              {
                //STRICT_REQUIRE( q > 0 );
                if ( q > 0 )
                  require_buffer_size(q);
                value[COUNT] = q;
              }
          
            data(size_t q=4) : value((new halflong_t[q+SIZE_OF_SPECIFIC])+SIZE_OF_SPECIFIC) 
              { 
                STRICT_REQUIRE( q > 0 );
                memset(value,0,q*sizeof(halflong_t)); 
                value[CAPACITY] = q;
                value[COUNT] = 1;
                value[SIGN] = POSITIVE;
              }
            
            ~data() { delete[] Base(); }
  
            static CXX_NO_INLINE RccPtr<data> New(ulong_t u=0,int sign=1)
              {
                RccPtr<data> d = rcc_ptr( new data(4) );
              
                d->sign() = sign;
                halflong_t *p = d->value;
                STRICT_ONLY(halflong_t *E = p+d->Capacity());
                while ( u )
                  {
                    STRICT_REQUIRE( p < E );
                    *p++ = (halflong_t)u;
                    u >>= DIGIT_SHIFT;
                  }
                d->value[COUNT] = cxx_max<halflong_t>(1,p - d->value);
                return d;
              }

            RccPtr<data> Clone()
              {
                RccPtr<data> d = rcc_ptr( new data(cxx_max<size_t>(4,Count())) );
                memcpy(d->value,value,Count()*sizeof(halflong_t));
                d->value[CAPACITY] = cxx_max<halflong_t>(4,value[COUNT]);
                d->value[COUNT] = value[COUNT];
                d->value[SIGN] = value[SIGN];
                return d;
              }

            void Clear() 
              { 
                memset(value-CLEAR_ADDICT,0,(Capacity()+CLEAR_ADDICT)*sizeof(halflong_t)); 
                value[COUNT] = 1;
                value[SIGN] = POSITIVE;
              }
          
            void Push(halflong_t q) 
              { 
                require_buffer_size(Count()+1); 
                value[Count()] = q; 
                ++value[COUNT]; 
              }
          
            inline halflong_t CXX_NO_INLINE Digit(int i) const
              {
                return (unsigned)i >= Count() ? 0 : value[(unsigned)i];
              }
            
            inline halflong_t &at(int i)
              {
                STRICT_REQUIRE( i >= 0 && i < Count() );
                return value[i];
              }

           inline halflong_t at(int i) const
            {
              STRICT_REQUIRE( i >= 0 && i < Count() );
              return value[i];
            }

           void CXX_NO_INLINE set_bit(int n, bool val) 
              { 
                STRICT_REQUIRE( (n>=0) && ((n+BITSTEP-1)/BITSTEP) <= Count() );
                int idx = n/BITSTEP; 
                if ( val )
                  value[idx] |= (1L << (n%BITSTEP)); 
                else
                  value[idx] &= ~(1L << (n%BITSTEP)); 
              }
        
            halflong_t CXX_NO_INLINE bit(int n) const
              { 
                STRICT_REQUIRE( (n>=0) && ((n+BITSTEP-1)/BITSTEP <= Count()) );
                int idx = n/BITSTEP; 
                return (value[idx]>>(n%BITSTEP))&1;
              }
        
            size_t CXX_NO_INLINE BitCount() const 
              {
                int i = Count() - 1;
                while ( i >= 0 && !value[i] ) --i;
                if ( i < 0 ) return 0;
                size_t bc = cxx_bitcount(value[i]);
                size_t bcc = bc + i*BITSTEP;   
                return bcc;
              }
            
            halflong_t Last() const
              {
                STRICT_REQUIRE(Count()>0);
                return value[Count()-1];
              }
            
            void Strip()
              {
                while ( value[COUNT] > 1 && !value[value[COUNT]-1] ) --value[COUNT];
              }
          };
            
        size_t BitCount() const { return d->BitCount(); }
        halflong_t Digit(int i) const { return d->Digit(i); }
        int Sign() const { return d->sign(); } 
        bool Bit(int i) const { return d->bit(i); }
      
        lint_t  add(lint_t const &q) const;
        lint_t  add1(lint_t const &q,int) const;
        lint_t  subtract(lint_t const &q) const;
        lint_t  subtract1(lint_t const &q) const;
        lint_t  multiply(lint_t const &q) const;
        lint_t  divide(lint_t const &q) const;
        lint_t  divrem(lint_t const &q, lint_t *rem) const;
        lint_t  modulo(lint_t const &q) const;
        lint_t  expmod(lint_t const &e, lint_t const &mod) const;
        lint_t  invmod(lint_t const &mod) const;
        lint_t  pow(long e) const;
        lint_t  lshift(size_t q) const { lint_t R = Clone(); R._lshift(q); return R; }
        lint_t  rshift(size_t q) const { lint_t R = Clone(); R._rshift(q); return R; }
        lint_t  band(lint_t const &q) const;
        lint_t  bor(lint_t const &q) const;
        lint_t  bxor(lint_t const &q) const;
        int     band_int(int q) const { return d->at(0)&q; }

        inline size_t Count() const               { return d->Count(); }
        inline halflong_t operator[](int i) const { return d->at(i); }
        inline halflong_t &operator[](int i)      { return d->at(i); }
        inline halflong_t &at(int i)              { return d->at(i); }
        inline halflong_t at(int i) const         { return d->at(i); }
        inline halflong_t Last() const            { return d->Last();}

        bool equal(lint_t const &q) const;
        bool below(lint_t const &q) const;
        bool above(lint_t const &q) const;
        bool xequal(lint_t const &q) const;
        bool less(lint_t const &q) const;
        bool greater(lint_t const &q) const;
      
        lint_t &_hishiftQ(lint_t const &lo, size_t count, size_t *bits);
        lint_t &_hishift1(lint_t const &lo, size_t *bits);
        lint_t &_subtract(lint_t const &q);
        lint_t &_lshift(size_t q);
        lint_t &_lshift1();
        lint_t &_rshift(size_t q);
        long _cmp(lint_t const &q) const;
        lint_t &_bor(halflong_t q)  { d->at(0)|=q; return *this; }
        lint_t &_band(halflong_t q) { d->at(0)&=q; return *this; }

        lint_t &_inc(halflong_t delta = 1);
        lint_t &_multiply(halflong_t);

        lint_t(lint_t const &q) : d( q.d ) {}
        lint_t(long q = 0) : d (data::New(cxx_abs(q),cxx_sign(q))) {}
        lint_t(int  q)     : d (data::New(cxx_abs(q),cxx_sign(q))) {}
        lint_t(ulong_t q)  : d (data::New(cxx_abs(q),cxx_sign(q))) {}
        lint_t(uint_t  q)  : d (data::New(cxx_abs(q),cxx_sign(q))) {}
        lint_t(float  q)   : d (data::New((ulong_t)cxx_abs(q),cxx_sign(q))) {}
        lint_t(double  q)  : d (data::New((ulong_t)cxx_abs(q),cxx_sign(q))) {}
      
        lint_t(Strarg p,int radix=10) { setup(p,radix); if (!d) d = data::New(); }
      
        void setup(Strarg,int radix);

        lint_t const& operator =(lint_t const& q) { d = q.d; return *this; }
      
        bool Zero() const { return BitCount() == 0; }
        bool operator!() const { return Zero(); }
        lint_t Clone() const { lint_t R; R.d = d->Clone(); return R; }
      
        String Digits(int radix=10) const;
        String Digits_10() const;
        String Digits_16() const;
        String Digits_2() const;
      
        lint_t GCD(lint_t b);
      
        bool IsPrime(int q=PRIME_TEST_Q) const;
        bool FermaPrimeTest(int q=PRIME_TEST_Q) const;
        lint_t FirstMutalPrime(int skip_primes = 3) const;
        static lint_t Prime(int bits, int q=PRIME_TEST_Q, int maxcount=100);
        static void GenerateRsaKeyPair(
                    lint_t /*out*/ *rsa_pub, lint_t /*out*/ *rsa_priv, lint_t /*out*/ *rsa_mod,
                    unsigned bits=128);
        static void GenerateRsaPQ(
                    lint_t /*out*/ *p, unsigned pBits,
                    lint_t /*out*/ *q, unsigned qBits,
                    lint_t /*out*/ *n);
                  
        static lint_t Load(void const *d, size_t count,bool = false);
        static lint_t Sload(void const *d, size_t count);
        bool Store(void *d, size_t count, bool = false) const;
        bool Sstore(void *d, size_t count) const;
        int RequiredStorageSize(bool S=false) const;
      
        lint_t operator -() const { lint_t R = Clone(); R->sign() = NEGATIVE; return R; }
      
      private:
        RccPtr<data> d;
        data *operator ->() const { return +d; }
      };

    inline lint_t operator + ( lint_t const &b, lint_t const &q ) { return b.add(q); }
    inline lint_t operator - ( lint_t const &b, lint_t const &q ) { return b.subtract(q); }
    inline lint_t operator * ( lint_t const &b, lint_t const &q ) { return b.multiply(q); }
    inline lint_t operator / ( lint_t const &b, lint_t const &q ) { return b.divide(q); }
    inline lint_t operator >>( lint_t const &b, size_t q)         { return b.rshift(q); } 
    inline lint_t operator <<( lint_t const &b, size_t q)         { return b.lshift(q); } 
    inline int    operator & ( lint_t const &b, int q)            { return b.band_int(q); }
    inline lint_t operator & ( lint_t const &b, lint_t const &q)  { return b.band(q); } 
    //inline ulong_t operator | ( lint_t const &b, ulong_t q)        { return b.bor(lint_t(q)); } 
    inline lint_t operator | ( lint_t const &b, lint_t const &q)  { return b.bor(q); } 
    //inline ulong_t operator ^ ( lint_t const &b, ulong_t q)        { return b.bxor(lint_t(q)); } 
    inline lint_t operator ^ ( lint_t const &b, lint_t const &q)  { return b.bxor(q); } 
    //inline ulong_t operator % ( lint_t const &b, ulong_t q)        { return b.modulo(lint_t(q)); } 
    inline lint_t operator % ( lint_t const &b, lint_t const &q)  { return b.modulo(q); } 
    inline bool operator < ( lint_t const &b, lint_t const &q ) { return b.below(q); }
    inline bool operator >=( lint_t const &b, lint_t const &q ) { return !b.below(q); }
    inline bool operator > ( lint_t const &b, lint_t const &q ) { return b.above(q); }
    inline bool operator <=( lint_t const &b, lint_t const &q ) { return !b.above(q); }
    inline bool operator ==( lint_t const &b, lint_t const &q ) { return b.equal(q); }
    inline bool operator !=( lint_t const &b, lint_t const &q ) { return !b.equal(q); }

    inline CXX_NO_INLINE lint_t lint_t::Sload(void const *buffer, size_t count)
      {
        return Load(buffer,count,true);
      }
    
    inline CXX_NO_INLINE lint_t lint_t::Load(void const *buffer, size_t count, bool S)
      {
        if ( count%sizeof(halflong_t) ) 
          { 
            OCCURED(Unaligned,"size of buffer should be multiple of halflong_t");
            return lint_t();
          }
      
        lint_t R;
        byte_t const *d = (byte_t const*)buffer;
      
        ulong_t CC=1; // carry
        int c = count/sizeof(halflong_t);
        R->Resize(c);
        bool negative = S && !!(d[count-1] & 0x80);
        memset(R->value,0,c);
        for ( int i = 0; i < c; ++i )
          {
            halflong_t C = 0;
            for ( int j =0; j < sizeof(halflong_t); ++j )
              C |= (halflong_t)*d++ << (j*8);
            if ( negative ) { CC = ~C+CC; C = CC; CC = CC >> DIGIT_SHIFT; }
            R->value[i] = C;
          }
        if ( negative ) R->sign() = NEGATIVE;
        return R;
      }
    
    inline CXX_NO_INLINE bool lint_t::Sstore(void *buffer, size_t count) const
      {
        return Store(buffer,count,true);
      }
    
    inline int lint_t::RequiredStorageSize(bool S) const
      {
        int q = cxx_max<int>(4, (BitCount() + (S&&(Sign()==NEGATIVE?1:0))) );
        return ( q + (sizeof(halflong_t)-1) ) & ~(sizeof(halflong_t)-1);
      }
    
    inline CXX_NO_INLINE  bool lint_t::Store(void *buffer, size_t count, bool S) const
      {
        if ( count%sizeof(halflong_t) ) 
          { 
            OCCURED(Unaligned,"size of buffer should be multiple of halflong_t");
            return false;
          }
          
        if ( BitCount() + (S&&(Sign()==NEGATIVE?1:0)) > count*8 )
          {
            OCCURED(NoEnoughSpace,"buffer to small");
            return false;
          }
          
        STRICT_REQUIRE(count >= Count()*sizeof(halflong_t));
      
        byte_t *d = (byte_t *)buffer;
        ulong_t CC=1; // carry if negative
        int c = Count();

        memset(buffer,0,count);
        bool negative = S && (Sign() == NEGATIVE);
      
        for ( int i = 0; i < c; ++i )
          {
            halflong_t C = at(i);
            if ( negative ) { CC = ~C+CC; C = CC; CC = CC >> DIGIT_SHIFT; }
            for ( int j = 0; j < sizeof(halflong_t); ++j )
              *d++ = (C>>(j*8))&0x0ff;
          }
          
        if ( negative ) 
          for ( int i = c*sizeof(halflong_t); i < count; ++i ) 
            { CC+=0x0ff; ((byte_t*)buffer)[i] = CC; CC >>= 8; }
        
        return true;
      }

    inline CXX_NO_INLINE long lint_t::_cmp(lint_t const &q) const
      {
        if ( d == q.d ) return 0;
      
        long ac  = (long)Count();
        long bc  = (long)q.Count();
        halflong_t const *a = (*this)->value+(ac-1); 
        halflong_t const *b = q->value+(bc-1); 
        long Q = ac-bc;
      
        if ( Q ) 
          {
            halflong_t const * &qq = ( Q < 0 )?b:a;
            long QQ = abs(Q);
            while ( QQ-- ) if ( *qq-- ) return Q; 
            Q = cxx_min(ac,bc);
            STRICT_REQUIRE((*this)->value+(Q-1) == a);
            STRICT_REQUIRE(q->value+(Q-1) == b);
          }
        else 
          Q = ac;
      
        for ( int QQ = Q; QQ--;  )
          if ( ( Q = (long)*a-- - (long)*b-- ) ) 
            return Q;

        return 0;
      }

    inline bool lint_t::less(lint_t const &q) const
      {
        return _cmp(q) < 0;
      }

    inline bool lint_t::greater(lint_t const &q) const
      {
        return _cmp(q) > 0;
      }

    inline bool lint_t::xequal(lint_t const &q) const
      {
        return _cmp(q) == 0;
      }

    inline bool CXX_NO_INLINE lint_t::below(lint_t const &q) const
      {
        if ( Sign() != q.Sign() )
          return Sign() == POSITIVE ? false : true;
        long S = _cmp(q);
        if ( Sign() == NEGATIVE ) S = -S;
        return S < 0;
      }

    inline bool CXX_NO_INLINE lint_t::above(lint_t const &q) const
      {
        if ( Sign() != q.Sign() )
          return Sign() == NEGATIVE ? false : true;
        long S = _cmp(q);
        if ( Sign() == NEGATIVE ) S = -S;
        return S > 0;
      }

    inline bool lint_t::equal(lint_t const &q) const
      {
        if ( Sign() != q.Sign() ) return false;
        long Q = _cmp(q);
        //printf("equal == %ld, %ls, %ls\n",Q,+Digits(),+q.Digits());
        return !Q;
      }

    inline CXX_NO_INLINE lint_t lint_t::add1(lint_t const &q, int sign) const
      {
        lint_t R; 
        R->Resize(0);
        ulong_t Q = 0;
        for ( int i = 0, j = cxx_max(q.Count(),Count()); i < j; ++i )
          {
            Q = (ulong_t)Digit(i) + (ulong_t)q.Digit(i) + Q;
            R->Push((halflong_t)Q);
            Q >>= DIGIT_SHIFT;
          };
      
        if ( Q )
          R->Push(Q);
        R->sign() = sign;
        return R;
      }
  
    inline CXX_NO_INLINE lint_t &lint_t::_inc(halflong_t delta)
      {
        ulong_t Q = (ulong_t)at(0)+delta;
        at(0) = (halflong_t)Q;
        Q >>= DIGIT_SHIFT;
        if ( Q )
          for ( int i = 1, iE = d->Count(); i < iE && Q; ++i )
            {
              Q += (ulong_t)at(i);
              at(i) = (halflong_t)Q;
              Q >>= DIGIT_SHIFT;
            }
        if ( Q )
          d->Push(Q);
        return *this;
      }

    inline CXX_NO_INLINE lint_t lint_t::add(lint_t const &q) const
      {
        if ( Sign() != q.Sign() )
          if ( q.Sign() < 0 ) 
            return subtract1(q);
          else
            return q.subtract1(*this);
      
        lint_t R = add1(q,Sign());
        return R;
      }

    inline CXX_NO_INLINE lint_t lint_t::subtract1(lint_t const &q) const
      {
        lint_t R;
        R->Resize(0);
        ulong_t Q = 0;
        for ( int i = 0, j = cxx_max(q.Count(),Count()); i < j; ++i )
          {
            Q = (ulong_t)Digit(i) - (ulong_t)q.Digit(i) - Q;
            R->Push((halflong_t)Q);
            Q >>= DIGIT_SHIFT;
            Q &= 1;
          };
        if ( Q )
          {
            R->sign() = NEGATIVE;
            for ( int i = 0, j = R->Count(); i < j; ++i )
              R[i] = ~R[i];
            R._inc(1);
          }
        return R;
      }

    inline CXX_NO_INLINE lint_t &lint_t::_subtract(lint_t const &q)
      {
        ulong_t Q = 0;
      #if 0
        for ( int i = 0, j = Count(); i < j; ++i )
          {
            Q = (ulong_t)at(i) - (ulong_t)q.Digit(i) - Q;
            at(i) = (halflong_t)Q;
            Q >>= DIGIT_SHIFT;
            Q &= 1;
          };
      #else
        int J = Count();
        int qJ = cxx_min<int>(q.Count(),J);
        halflong_t *i = d->value;
        for ( halflong_t *j = q->value, *iE = i+qJ; i != iE; ++i, ++j )
          {
            Q = (ulong_t)*i - (ulong_t)*j - Q;
            *i = (halflong_t)Q;
            Q >>= (sizeof(Q)*8-1);
          };
        //if ( Q && qJ != J ) 
          for ( halflong_t *iE = i+J-qJ; Q && i != iE; ++i )
            {
              Q = (ulong_t)*i - Q;
              *i = (halflong_t)Q;
              Q >>= (sizeof(Q)*8-1);
            }
      #endif
        STRICT_REQUIRE(!Q);
        return *this;
      }
        
    inline CXX_NO_INLINE lint_t lint_t::subtract(lint_t const &q) const
      {
        if ( Sign() != q.Sign() )
          if ( q.Sign() < 0 ) 
            return add1(q,POSITIVE);
          else
            return q.add1(*this,NEGATIVE);
      
        if ( Sign() == NEGATIVE )
          return q.subtract1(*this);
      
        return subtract1(q);
      }

    inline CXX_NO_INLINE lint_t lint_t::expmod(lint_t const &e,lint_t const &mod) const
      {
        lint_t E = e.Clone();
        lint_t R = 1;
        lint_t t = this->modulo(mod);
        while ( !!E )
          {
            if ( E.band_int(1) )
              {
                R = R.multiply(t).modulo(mod);
              }
            E._rshift(1);
            t = t.multiply(t).modulo(mod);
          }
        return R;
      }

    inline CXX_NO_INLINE lint_t lint_t::invmod(lint_t const &mod) const
      {
        lint_t b = mod;
        lint_t j = 1;
        lint_t i = 0;
        lint_t c = *this;

        while ( !!c )
          {
            lint_t y;
            lint_t x = b.divrem(c,&y);
            b = c;
            c = y;
            lint_t q = i-(j*x);
            i = j;
            j = q;
          }

        if ( i < 0 ) i = i + mod;
        return i;
      }

    inline CXX_NO_INLINE lint_t lint_t::modulo(lint_t const &m) const
      {
        size_t bits = BitCount();
        size_t modbits = m.BitCount();
        if ( modbits > bits ) return *this;
      
        lint_t R = 0;
      
        R._hishiftQ(*this,modbits,&bits);
      
        for(;;) 
          {
            if ( !R.less(m) )
              {
                R._subtract(m);
                STRICT_REQUIRE(R.less(m));
              }
            if ( !bits ) break;
            R._hishift1(*this,&bits);
          }
        
        R->Resize(m.Count());
        return R;
      }
    
    inline CXX_NO_INLINE lint_t &lint_t::_rshift(size_t count)
      {
        STRICT_REQUIRE( Count() > 0 );

        int S = count%BITSTEP;
        ulong_t Q = 0;
        at(0) = at(0) >> S;

        if (S) for ( int i = 1, iS = Count(); i < iS; ++i )
          {
            halflong_t Q = at(i);
            at(i-1) |= Q << (BITSTEP-S);
            at(i) = Q >> S;
          }
        
        int dif = count/BITSTEP;
        if ( dif ) 
          {
            if ( dif < Count() )
              {
                int iS = Count()-dif;
                memmove(d->value,d->value+dif,iS*sizeof(halflong_t));
                memset(d->value+iS,0,dif*sizeof(halflong_t));
                d->Resize(iS);
              }
            else
              d->Clear();
          }
        
        d->Strip();
        return *this;
      }
        
    inline CXX_NO_INLINE lint_t &lint_t::_lshift(size_t count)
      {
        STRICT_REQUIRE( Count() > 0 );

        int S = count%BITSTEP;
        ulong_t Q = 0;

        if (S) for ( int i = 0, iS = Count(); i < iS; ++i )
          {
            Q |= ((ulong_t)at(i)<<S);
            at(i) = (halflong_t)Q;
            Q = (Q>>DIGIT_SHIFT);
          }
        
        if (Q) d->Push(Q&DIGIT_MASK);

        int dif = count/BITSTEP;
        if ( dif ) 
          {
            int iS = Count();
            d->Resize(iS+dif);
            memmove(d->value+dif,d->value,iS*sizeof(halflong_t));
            memset(d->value,0,dif*sizeof(halflong_t));
          }
        
        d->Strip();
        return *this;
      }

    inline lint_t &lint_t::_lshift1()
      {
        halflong_t Q = 0;

        for ( halflong_t *i = d->value, *iE = d->value+Count(); i != iE; ++i )
          {
            halflong_t S = *i;
            *i = ( S << 1 ) | Q;
            Q = S >> (DIGIT_SHIFT-1);
          }
        
        if (Q) d->Push(Q);
        return *this;
      }

    inline CXX_NO_INLINE lint_t &lint_t::_hishiftQ(lint_t const &lo,size_t count,size_t *bits)
      {
        d->Clear();
        d->Resize((count+BITSTEP-1)/BITSTEP);
      
        count = cxx_min(count,*bits);
        *bits -= count;

        for ( int i = 0, iE = count, bi=*bits; i < iE; ++i ) 
          d->set_bit(i,lo->bit(i+bi));

        return *this;
      }

    inline CXX_NO_INLINE lint_t &lint_t::_hishift1(lint_t const &lo,size_t *bits)
      {
        halflong_t Q = 0;

        if ( *bits )
          {
            --*bits;
            Q = lo->bit(*bits);
          }

        for ( halflong_t *i = d->value, *iE = d->value+Count(); i != iE; ++i )
          {
            halflong_t S = *i;
            *i = ( S << 1 ) | Q;
            Q = S >> (DIGIT_SHIFT-1);
          }
        
        if (Q) d->Push(Q);
        return *this;
      }

    inline CXX_NO_INLINE lint_t lint_t::multiply(lint_t const &q) const
      {
        lint_t R; R->Resize(q.Count()+Count()+1);
      
        for ( int i = 0, Qdigno = q.Count(), Tdigno = Count(); i < Qdigno; ++i )
          {
            ulong_t Q = 0;
            ulong_t C = 0;
            for ( int j = 0; j < Tdigno; ++j )
              {
                Q = (ulong_t)q.at(i) * (ulong_t)at(j) + Q;
                C = (ulong_t)R.at(j+i) + (Q & DIGIT_MASK) + C;
                R.at(j+i) = (halflong_t)C;
                Q >>= DIGIT_SHIFT;
                C >>= DIGIT_SHIFT;
              }
            do 
              {
                C = (ulong_t)R.at(Tdigno+i) + (Q & DIGIT_MASK) + C;
                R.at(Tdigno+i) = (halflong_t)C;
                Q >>= DIGIT_SHIFT;
                C >>= DIGIT_SHIFT;
              }
            while ( i < Qdigno && C ); 
          }
     
        R->Strip();
      
        if ( Sign() != q.Sign() )
          R->sign() = NEGATIVE;
      
        return R;
      }

    inline CXX_NO_INLINE lint_t &lint_t::_multiply(halflong_t q)
      {
        if ( d->Last() ) d->Push(0);
      
        ulong_t Q = 0;
        ulong_t C = 0;

        for ( int i = 0, j = d->Count(); i < j; ++i )
          {
            Q = (ulong_t)at(i) * (ulong_t)q + Q;
            C = (Q&DIGIT_MASK) + C;
            at(i) = (halflong_t)C;
            Q >>= DIGIT_SHIFT;
            C >>= DIGIT_SHIFT;
          }
      
        d->Strip();
        return *this;
      }

    inline CXX_NO_INLINE lint_t lint_t::divrem(lint_t const &q,lint_t *rem) const
      {
        lint_t Q = 0;
      
        size_t bits = BitCount();
        size_t divbits = q.BitCount();
      
        if ( divbits > bits ) 
          {
            if ( rem ) *rem = *this;
            return Q;
          }
      
        if ( divbits == 0 )
          {
            OCCURED(ZerroDivide,"");
            return lint_t();
          }
          
        lint_t R = 0;
        Q._hishiftQ(*this,divbits,&bits);
      
        for(;;) 
          {
            if ( !Q.less(q) )
              { 
                Q._subtract(q); R._inc(); 
                STRICT_REQUIRE(Q.less(q));
              }
            if ( !bits ) break;
            R._lshift1();
            Q._hishift1(*this,&bits);
          }

        if ( rem ) *rem = Q;
        return R;
      }

    inline lint_t lint_t::divide(lint_t const &q) const
      {
        return divrem(q,0);
      }

    inline CXX_NO_INLINE void lint_t::setup(Strarg t,int radix)
      {
        lint_t R = 0;
        char const *p = +t;
        if ( p )
          {
            if ( *p == '-' ) { R->sign() = NEGATIVE; ++p; }
            else if ( *p == '+' ) ++p;
          
            for ( ;*p; ++p )
              {
                if ( radix == 10 )
                  {
                    if ( !ChrIsDigit(*p) ) 
                      { OCCURED(InvalidDecimalNumber,t); return; }
                    R._multiply(10)._inc(*p-'0');
                  }
                else if ( radix == 16 )
                  {
                    if ( !ChrIsXdigit(*p) || !ChrIsXdigit(p[1]) ) 
                      { OCCURED(InvalidHexadecimalNumber,t); return; }
                    R._lshift(8)._bor(UnhexChars(p));
                    ++p;
                  }
                else if ( radix == 2 )
                  {
                    if ( *p != '0' && *p != '1' ) 
                      { OCCURED(InvalidBinaryNumber,t); return; }
                    R._lshift1()._bor(*p-'0');
                  }
                else
                  return;
              }
          }
        d = R.d;
      }

    /// write decimal presenation with sign
    inline CXX_NO_INLINE String lint_t::Digits_10() const
      {
        Array<char> chars;
        chars.Reserve(d->Count()*10+1);
      
        lint_t R = *this;
        if ( !!R )
          {
            lint_t ten = 10;
            while ( !!R )
              {
                lint_t rem;
                R = R.divrem(ten,&rem);
                chars.Push('0'+rem[0]);
              }
            if ( Sign() < 0 ) chars.Push('-');
            chars.Reverse();
          }
        else
          chars.Push('0');

        return String(chars,__take_data__);
      }

    /// write hexdecimal presentation without sign!
    inline CXX_NO_INLINE String lint_t::Digits_16() const
      {
        Array<char> chars;
        chars.Reserve(d->Count()*sizeof(halflong_t)*2);
      
        lint_t R = this->Clone();
        if ( !!R )
          {
            while ( !!R )
              {
                HexedChar hc = HexByte(R.Digit(0)&0x0ff); 
                chars.Push(hc.c[1]);
                chars.Push(hc.c[0]);
                R._rshift(8);
              }
            chars.Reverse();
          }
        else
          chars.Push('0');

        return String(chars,__take_data__);
      }

    /// write binary presentation without sign!
    inline CXX_NO_INLINE String lint_t::Digits_2() const
      {
        Array<char> chars;
        int bc = BitCount();
        chars.Reserve(bc);

        if (bc )
          for ( int i = 0; i < bc; ++i )
            chars.Push(Bit(i)?'1':'0');
        else
          chars.Push('0');
        chars.Reverse();

        return String(chars,__take_data__);
      }

    inline CXX_NO_INLINE String lint_t::Digits(int radix) const
      {
        if ( radix == 10 )
          return Digits_10();
        if ( radix == 16 )
          return Digits_16();
        if ( radix == 2 )
          return Digits_2();
        return String();
      }
    
   inline CXX_NO_INLINE void format(TypeQuote<lint_t> q,XprintAgent &j,char fmt,unsigned perc)
      {
        String chars;
        if ( fmt == 'b' )
          chars = q->Digits_2();
        else if ( fmt == 'x' || fmt == 'X' )
          chars = q->Digits_16();
        else 
          chars = q->Digits_10();
        j->SprintAjust(+chars,chars.Length());
      }
    
    inline CXX_NO_INLINE lint_t lint_t::GCD(lint_t b)
      {
        lint_t a = *this;
        while ( !!b )
          {
            lint_t t = a % b;
            a = b;
            b = t;
          }
        return a;
      }

    inline CXX_NO_INLINE bool lint_t::FermaPrimeTest(int q) const
      {
        STRICT_REQUIRE( q > 0 && q < PRIME_MAX_COUNT );

        lint_t p = *this;
        lint_t p_1 = p-1;

        for ( int i =0; i < q; ++i )
          {
            if ( lint_t(first_prime(i)).expmod(p_1,p) != 1 )
              return false;
          }
        return true;
      }
    
    inline CXX_NO_INLINE bool lint_t::IsPrime(int q) const
      {
        return FermaPrimeTest(q);
      }
    
    inline CXX_NO_INLINE lint_t lint_t::Prime(int bits, int q, int maxcount)
      {
        STRICT_REQUIRE( bits > 8 );
        STRICT_REQUIRE( q > 0 && q < 500 );

        int n = cxx_min(64,bits-3);

        while ( maxcount-- )
          {
            lint_t r = 2;
            for ( int i=0; i < bits-3; ++i )
              r._lshift1()._inc(random_bits(1));
            r._lshift1()._inc(1);  
            for ( int i = 0; i < n; ++i )
              {
                if ( r.IsPrime(q) )
                  return r;
                else
                  r._inc(2);
              }
           }
        return lint_t(0);
      }

    inline CXX_NO_INLINE  void lint_t::GenerateRsaKeyPair(
      lint_t /*out*/ *rsa_pub, 
      lint_t /*out*/ *rsa_priv, 
      lint_t /*out*/ *rsa_mod,
      unsigned bits)
      {
        REQUIRE(bits>=64);
      
        unsigned pBits = random(bits/5,bits/2);
        unsigned qBits = (bits+1)-pBits;
        STRICT_REQUIRE(pBits < bits/2);
        STRICT_REQUIRE(pBits+qBits == bits+1);
      
        lint_t p, q, n, phi;
      
        unsigned skip_primes = random(PRIME_RSAPUBLIC_MIN,(PRIME_RSAPUBLIC_MAX-PRIME_RSAPUBLIC_MIN)/2);

      l_regen:
        GenerateRsaPQ(&p,pBits,&q,qBits,&n);
      
        phi = (p-1)*(q-1);
        *rsa_pub   = phi.FirstMutalPrime(skip_primes);
        if ( !*rsa_pub ) { n = 0; goto l_regen; }
        *rsa_priv  = rsa_pub->invmod(phi);
        *rsa_mod   = n;
      }

    inline CXX_NO_INLINE lint_t lint_t::FirstMutalPrime(int skip_primes) const
      {
        REQUIRE(skip_primes > 0 && skip_primes < PRIME_MAX_COUNT/2 );
      
        for ( int i = skip_primes; i < PRIME_MAX_COUNT; ++i )
          {
            ulong_t prime = first_prime(i);
            if ( !!modulo(prime) ) return lint_t(prime);
          }
        
        return 0;      
      }

    inline CXX_NO_INLINE  void lint_t::GenerateRsaPQ(
      lint_t /*out*/ *rsa_P, 
      unsigned pBits,
      lint_t /*out*/ *rsa_Q, 
      unsigned qBits,
      lint_t /*out*/ *rsa_N)
      {
        unsigned bits = qBits+pBits-1;
        REQUIRE(bits>=64);
      
        lint_t L1 = 1;
        lint_t n = 0;
        lint_t p,q;

        lint_t nMax = (L1<<bits) - 1;
        lint_t nMin = (L1<<(bits-1));
        STRICT_REQUIRE(nMin < nMax);

      l_regen:
        while ( n >= nMax || n <= nMin )
          {
            p = lint_t::Prime(pBits);
            q = lint_t::Prime(qBits);
            if ( !p || !q ) continue;
            n = p * q;
          }
      
        *rsa_P = p;
        *rsa_Q = q;  
        *rsa_N = n;

      }

  } // namespace

#endif // nano_cxx_lint_HXX

