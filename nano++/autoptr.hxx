
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "memory.hxx"

namespace nano
{
	struct NewTag { char _; }; 
	static NewTag const __new__ = {0};

	struct CxxHeap
	{
		template <class T> static void Dispose(T* p) { p->~T(); Free(p); }
		static void Free(void* p) { ::operator delete(p); }
		static void* Malloc(size_t s) { return ::operator new(s); }
	};

	template < class T, class heap = CxxHeap >
	struct AutoPtr
	{
		mutable T* ptr_;

		T* forget_() const
		{
			T* tmp = ptr_;
			ptr_ = 0;
			return tmp;
		}

		void reset_(T* p)
		{
			if (ptr_ == p) return;
			if (ptr_) { heap::Dispose(ptr_); }
			ptr_ = p;
		}

		explicit AutoPtr(T* p = 0) : ptr_(p) {}
		AutoPtr(struct NewTag) : ptr_(0)
		{
			T* q = heap::Malloc(sizeof(T));
			CXX_TRY { new(q) T(); } CXX_CATCH_ALL { heap::Free(q); CXX_RETHROW; }
			ptr_ = q;
		}

		AutoPtr(const AutoPtr& p) { ptr_ = p.forget_(); }
		~AutoPtr() { reset_(0); }

		T* operator->()       const { return ptr_; }
		T* operator+(int i)   const { return ptr_ + i; }
		T& operator*()        const { return *ptr_; }
		T& operator[](int i)  const { return ptr_[i]; }
		bool operator!()      const { return !ptr_; }
		operator bool()       const { return !!ptr_; }
		T*& operator +()      const { return ptr_; }

		const AutoPtr& operator = (const AutoPtr& p)
		{
			reset_(p.forget_());
			return *this;
		}

	private:
		const AutoPtr& operator = (T*);
	};

	template < class T, class P > inline void unrefe(AutoPtr<T, P>& p) { p.reset_(0); }
	template < class T, class P > inline T* forget(AutoPtr<T, P>& p) { return p.forget_(); }
	template < class T, class P > inline void reset(AutoPtr<T, P>& p, T* pp) { p.reset_(pp); }

	template <class T> inline AutoPtr<T> auto_ptr(T* p) { return AutoPtr<T>(p); }

	struct CxxArrayHeap
	{
		template <class T> static void Dispose(T* p) { delete[] p; }
	};

	template <class T>
	struct AutoArrPtr : AutoPtr<T, CxxArrayHeap>
	{
		AutoArrPtr(T* p = 0) : AutoPtr<T, CxxArrayHeap> (p) {}
	};

	template <class T> inline AutoArrPtr<T> array_ptr(T* p) { return AutoArrPtr<T>(p); }

	struct MallocHeap
	{
		template <class T> static void Dispose(T* p) { p->~T(); Free(p); }
		static void Free(void* p) { mem_free(p); }
		static void* Malloc(size_t s) { return mem_chk_malloc(s); }
	};

	template <class T>
	struct MallocPtr : AutoPtr<T, MallocHeap>
	{
		explicit MallocPtr(T* p = 0) : AutoPtr<T, MallocHeap> (p) {}
		MallocPtr(NewTag) : AutoPtr<T, MallocHeap>(__new__) {}
	};

	template <class T> inline MallocPtr<T> malloc_ptr(T* p) { return MallocPtr<T>(p); }
}
