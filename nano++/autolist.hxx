
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"

namespace nano
{
	template < class Z > struct auto_list_Of
	{
		mutable Z const* next;
		auto_list_Of(Z const* p) : next(p) {}
		Z const* Glue(int& len) const
		{
			len = 1;
			Z const* N = 0;
			Z const* head = (Z const*)this;
			Z const* Q;
			while (0 != (Q = head->next))
			{
				++len;
				head->next = N;
				N = head;
				head = Q;
			}
			head->next = N;
			return head;
		}
	};

	struct auto_list_L;

	template < class T, class Q > struct map_Of : auto_list_Of < map_Of<T, Q> >
	{
		typename ConstRef<T>::Type t;
		typename ConstRef<Q>::Type q;
		inline map_Of<T, Q> operator()(T const& t, Q const& q) const { return map_Of<T, Q>(t, q, this); }
	protected:
		friend struct auto_list_L;
		inline map_Of(T const& t, Q const& q, map_Of<T, Q> const* next = 0)
			: auto_list_Of < map_Of<T, Q> >(next), t(t), q(q) {}
	};

	template < class T > struct list_Of_T;

	template < class T > struct list_Of : auto_list_Of < list_Of<T> >
	{
		typename ConstRef<T>::Type t;
		inline list_Of<T> operator , (T const& t) const { return list_Of<T>(t, this); }
		inline list_Of<T> operator %(T const& t) const { return list_Of<T>(t, this); }
	protected:
		friend struct auto_list_L;
		inline list_Of(T const& t, list_Of<T> const* next = 0) 
			: auto_list_Of < list_Of<T> >(next), t(t) {}
	};

	struct auto_list_L
	{
		byte_t none;

		template < class T > inline
		list_Of<T> operator , (T const& t) const
		{ return list_Of<T>(t);}

		template < class T > inline
		list_Of<T> operator %(T const& t) const
		{ return list_Of<T>(t);}

		template < class T > inline
		list_Of<T*> operator , (T t[])  const
		{ return list_Of<T*>(t);}

		template < class T > inline
		list_Of<T*> operator %(T t[])  const { return list_Of<T*>(t);}

		template < class T, class Q > inline
		map_Of<T, Q> operator()(T const& t, Q const& q) const
		{ return map_Of<T, Q>(t, q); }

		template < class T, class Q > inline
		map_Of<T*, Q> operator()(T t[], Q const& q) const
		{ return map_Of<T*, Q>(t, q); }

		template < class T, class Q > inline
		map_Of<T, Q*> operator()(T const& t, Q q[]) const
		{ return map_Of<T, Q*>(t, q); }

		template < class T, class Q > inline
		map_Of<T*, Q*> operator()(T t[], Q q[]) const
		{ return map_Of<T*, Q*>(t, q); }
	};

	static const auto_list_L _L = {0};

} // namespace

