
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once

#include "deftypes.hxx"
#include "atomic.hxx"
#include "algo.hxx"

namespace nano
{

	struct Clog_
	{
		int fd_holder;
		int fd;
		int lock;

		void Close()
		{
			if (fd_holder >= 0)
				close(fd_holder);
			fd_holder = -1;
		}

		void Open(pchar_t logfile, bool truncate = true)
		{
			Close();
			if (0 < (fd_holder = open(logfile, 0)))
				fd = fd_holder;
		}

		inline bool AbleToLog()
		{
			return fd >= 0;
		}

		inline void Disable()
		{
			fd = -1;
		}

		inline void Enable()
		{
			fd = fd_holder;
			if (fd < 0) UseStderr();
		}

		CXX_NO_INLINE void UseStderr()
		{
			Close();
#if SYSTEM_IS_WINDOWS
			HANDLE errh = GetStdHandle(STD_ERROR_HANDLE);
			if (!errh || errh == INVALID_HANDLE_VALUE)
				fd = -1;
			else
#endif
				if (stderr)
					fd = fileno(stderr);
				else
					fd = -1;
		}

		CXX_NO_INLINE bool Out(pchar_t text)
		{
			if (fd < 0 || !text) return false;

			__xchg_lockon__(&lock)
			{
				full_write(fd, text, strlen(text));
				write(fd, "\n", 1);
			}

			return true;
		}

		CXX_NO_INLINE bool OutsNo(pchar_t* text, int no)
		{
			if (fd < 0 || !text) return false;

			__xchg_lockon__(&lock)
			for (int i = 0; i < no; ++i)
			{
				if (text[i])
					full_write(fd, text[i], strlen(text[i]));
				write(fd, "\n", 1);
			}

			return true;
		}

		CXX_NO_INLINE bool OutNos(pchar_t* text, int no)
		{
			if (fd < 0 || !text) return false;

			__xchg_lockon__(&lock)
			{
				for (int i = 0; i < no; ++i)
					if (text[i])
						full_write(fd, text[i], strlen(text[i]));
				write(fd, "\n", 1);
			}

			return true;
		}
	};

	struct Clog_t
	{
		int _;
		Clog_* operator->() const
		{
			static Clog_ clog = { -1, -1, 0 };
			return &clog;
		}
	};

	struct Cstd_t
	{
		FILE* _;
		inline FILE* operator+() const { return _; }
		inline Cstd_t const* operator->() const { return this; }

		void CXX_NO_INLINE Println(pchar_t text) const
		{
			fputs(text, _);
			fputc('\n', _);
			fflush(_);
		}

		void Print(pchar_t text) const
		{
			fputs(text, _);
			fflush(_);
		}
	};

	static const Cstd_t Cout = { stdout };
	static const Cstd_t Cerr = { stderr };
	static const Clog_t Clog = { 0 };

	inline bool operator |(Clog_t o, pchar_t q) { return o->Out(+q); }
	inline void operator |(Cstd_t o, pchar_t q) { o->Println(+q); }
	inline void operator||(Cstd_t o, pchar_t q) { o->Print(+q); }

	inline bool __cxx_clog_HXX_avoid_unused_error__() { return Cout._ && Cerr._ && Clog._; }

}
