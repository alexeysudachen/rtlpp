
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"
#include "algo.hxx"
#include "crypto/md.hxx"

#if SYSTEM_IS_WINDOWS
#include <wincrypt.h>
#if CXX_COMPILER_IS_MSVC_COMPATIBLE
#pragma comment(lib,"advapi32.lib")
#endif
#endif

namespace nano
{
	inline CXX_NO_INLINE void soft_random_bytes(byte_t* bits, int count)
	{
		static u64_t rnd_ct[4] = {0};
		static byte_t rnd_bits[16] = {0};
		static int rnd_bcont = 0;
		static int initialized = 0;
		static int lock = 0;

		__xchg_lockon__(&lock)
		{
			if (!initialized)
			{
				rnd_ct[0] = (u64_t)time(0);
				rnd_ct[1] = 0;
				rnd_ct[2] = 0;
				rnd_ct[3] = longptr_t(&bits); /* yes, address of stack parameter */
				initialized = 1;
			}

			while (count)
			{
				if (!rnd_bcont)
				{
					rnd_ct[1] = clock();
					rnd_ct[2] = (rnd_ct[2] + ((u64_t)count ^ longptr_t(bits))) >> 1;
					md5_digest(rnd_ct, sizeof(rnd_ct)).CopyTo(rnd_bits);
					++rnd_ct[3];
					rnd_bcont = sizeof(rnd_bits);
				}
				*bits++ = rnd_bits[--rnd_bcont];
				--count;
			}
		}
	}

#if !defined _DEV_RANDOM
#define _DEV_RANDOM "/dev/urandom"
#endif

	inline CXX_NO_INLINE void system_random(byte_t* bits, int count)
	{
#if !SYSTEM_IS_WINDOWS
		int fd = open(_DEV_RANDOM, O_RDONLY);
		if (fd >= 0)
		{
			for (int i = 0; i < count;)
			{
				int rd = read(fd, bits + i, count);
				if (rd < 0)
					BUGTRAP(_DEV_RANDOM " does not have required data: failed to read");
				i += rd;
				count -= rd;
			}
			close(fd);
			return;
		}
#else
		typedef BOOL (__stdcall * tCryptAcquireContext)(HCRYPTPROV*, LPCTSTR, LPCTSTR, DWORD, DWORD);
		typedef BOOL (__stdcall * tCryptGenRandom)(HCRYPTPROV, DWORD, BYTE*);
		static tCryptAcquireContext fCryptAcquireContext = 0;
		static tCryptGenRandom fCryptGenRandom = 0;
		static HCRYPTPROV cp = 0;
		if (!fCryptAcquireContext)
		{
			HMODULE hm = LoadLibrary("advapi32.dll");
			fCryptAcquireContext = (tCryptAcquireContext)GetProcAddress(hm, "CryptAcquireContextA");
			fCryptGenRandom = (tCryptGenRandom)GetProcAddress(hm, "CryptGenRandom");
		}
		if (!cp && !fCryptAcquireContext(&cp, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
			goto simulate;
		if (!fCryptGenRandom(cp, count, (unsigned char*)bits))
			goto simulate;
		if (count >= 4 && *(unsigned*)bits == 0)
			goto simulate;
		return;
#endif
	simulate:
#if defined _STRICT
		BUGTRAP(_DEV_RANDOM " is not accessable");
#else
		soft_random_bytes(bits, count);
#endif
	}

	inline CXX_NO_INLINE u64_t soft_random_bits(int no)
	{
		u64_t r = 0;
		STRICT_REQUIRE(no > 0 && no <= 8);

		soft_random_bytes((byte_t*)&r, 8);
		return r & (~u64_t(0) >> (64 - no));
	}

	inline u32_t soft_random(u32_t min, u32_t max)
	{
		STRICT_REQUIRE(max > min);
		u32_t r = u32_t((soft_random_bits(32) * (max - min)) >> 32) + min;
		STRICT_REQUIRE(r >= min && r < max);
		return r;
	}

	struct LinearPRG
	{
		u32_t state;
		LinearPRG(u32_t seed = (u32_t)time(0)) : state(seed) {}
	};

	inline u32_t next_random(LinearPRG* prg)
	{
		prg->state = prg->state * 1664525U + 1013904223U;
		return prg->state;
	}

	inline float float_random(LinearPRG* prg)
	{
		u32_t f = 0x3f800000U | (0x007fffffU & next_random(prg));
		return *(float*)&f - 1.f;
	}

	inline u32_t next_random(LinearPRG* prg, u32_t min, u32_t max)
	{
		u32_t rnd = next_random(prg);
		return u32_t(u64_t(rnd) * (max - min) / ~u32_t(0)) + min;
	}
	inline u32_t next_random(LinearPRG* prg, u32_t max)
	{   return next_random(prg, 0, max); }

	inline int next_random(LinearPRG* prg, int min, int max)
	{
		u32_t rnd = next_random(prg);
		return int(i64_t(rnd) * (max - min) / ~u32_t(0) + min);
	}
	inline int next_random(LinearPRG* prg, int max)
	{   return next_random(prg, 0, max); }

}
