
#include "deftypes.hxx"
#include "array.hxx"

namespace nano
{
	struct Large
	{
		struct LargeData
		{
			int sign;
		};

		typedef ExtendedArray<byte_t,LargeData> Container;
		Container bytes;

		int Sign() const { return bytes->sign; }

		Large(int val);
		Large(i64_t val);
		Large(double val);
		Large(Strarg text, int base=10);

		Large Add(Large const& a) const; 
		Large Mul(Large const& a) const; 
		Large Sub(Large const& a) const; 
		Large Div(Large const& a) const; 
		Large Mod(Large const& a) const; 
		Large Exp(Large const& a) const; 
		Large Expmod(Large const& exp, Large const& mod) const; 
		Large Sqrt() const;	
		Large Neg() const;
		Pair<Large,Large> Divmod(Large const& a) const; 
		Large Lshift(Large const& a, int n) const; 
		Large Rshift(Large const& a, int n) const; 

		Large BitXor(Large const& a) const; 
		Large BitAnd(Large const& a) const; 
		Large BitOr(Large const& a) const; 
		Large BitNot() const;

		bool IsZero() const;
		bool IsOne() const;
		bool IsPrime() const;

		Large operator-() const { return Neg(); }
		//Large operator~() const { return BitNot(); }	
		operator () { return IsZero(); }
	};

	inline Large operator +(Large const& a, Large const& b) { return a.Add(b); }
	inline Large operator -(Large const& a, Large const& b) { return a.Sub(b); }
	inline Large operator *(Large const& a, Large const& b) { return a.Mul(b); }
	inline Large operator /(Large const& a, Large const& b) { return a.Div(b); }
	inline Large operator %(Large const& a, Large const& b) { return a.Mod(b); }
	inline Large operator >>(Large const& a, int n) { return a.Rshift(n); }
	inline Large operator <<(Large const& a, int n) { return a.Lshift(n); }
	inline Large operator ^(Large const& a, Large const& b) { return a.BitXor(b); }
	inline Large operator |(Large const& a, Large const& b) { return a.BitOr(b); }
	inline Large operator &(Large const& a, Large const& b) { return a.BitAnd(b); }

	inline CXX_NO_INLINE
	void large_add(Array<byte_t> const& a, Array<byte_t> const& b, Array<byte_t> &result)
	{
	}

	inline CXX_NO_INLINE
	void large_sub(Array<byte_t> const& a, Array<byte_t> const& b, Array<byte_t> &result)
	{
	}

	inline CXX_NO_INLINE
	void large_mul(Array<byte_t> const& a, Array<byte_t> const& b, Array<byte_t> &result)
	{
	}

	inline CXX_NO_INLINE
	void large_lshift(Array<byte_t> &value, int n)
	{
	}

	inline CXX_NO_INLINE
	void large_rshift(Array<byte_t> &value, int n)
	{
	}

	inline CXX_NO_INLINE
	Large gcd(Large const& a, Large const& b)
	{
	}

	inline CXX_NO_INLINE
	Large invert(Large const& a, Large const& mod)
	{
	}

	inline CXX_NO_INLINE
	Large make_prime(int bits)
	{
	}
}

