
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "ios.hxx"

namespace nano
{

	template < class T > struct Marshal;

	template < class T > bool marshal(T const& val, IOs* st)
	{
		return Marshal<T>::Out(val, st);
	}

	template < class T > bool unmarshal(T& val, IOs* st)
	{
		return Marshal<T>::In(val, st);
	}

	template < class T > T unmarshal(IOs* st)
	{
		T val;
		Marshal<T>::In(val, st);
		return val;
	}

	inline bool unmarshal_S(char S, IOs* st)
	{
		if (st->Read8() != S)
		{
			if (XptNfo->Good()) OCCURED(DataCorrupted, "invalid type prefix");
			return false;
		}
		return true;
	}

#define BASETYPE_MARSHAL(T,L,S) \
	template <> struct Marshal<T> \
	{\
		inline bool Out( T const &val, IOs *st ) \
		{ return st->Write8(S) && st->_CXX_COMPOSE(Write,L)(val); } \
		inline bool In( T &val, IOs *st ) \
		{ \
			if ( unmarshal_S(S,st) ) \
				val = st->_CXX_COMPOSE(Read,L)(); \
			return XptNfo->Occured(); \
		} \
	};

	BASETYPE_MARSHAL(bool, 8, '1')
	BASETYPE_MARSHAL(char, 8, '1')
	BASETYPE_MARSHAL(byte_t, 8, '1')
	BASETYPE_MARSHAL(short, 16, '2')
	BASETYPE_MARSHAL(ushort_t, 16, '2')
	BASETYPE_MARSHAL(int, 32, '4')
	BASETYPE_MARSHAL(uint_t, 32, '4')
	BASETYPE_MARSHAL(i64_t, 64, '8')
	BASETYPE_MARSHAL(u64_t, 64, '8')
	BASETYPE_MARSHAL(long, 64, '8')
	BASETYPE_MARSHAL(ulong_t, 64, '8')
	BASETYPE_MARSHAL(float, Float, 'f')
	BASETYPE_MARSHAL(double, Float, 'F')

	enum { UNMARSHAL_ARRAY_LIMIT_SIZE = 128 * 1024 };

	inline int unmarshal_count_of(char S, int sz, IOs* st)
	{
		if (unmarshal_S(S, st))
		{
			int count = st->Read32();
			if (!XptNfo->Occured())
				if (count < 0)
					OCCURED(DataCorrupted, "negative array elemnts count");
				else if (count * sz < UNMARSHAL_ARRAY_LIMIT_SIZE)
					return count;
				else
					OCCURED(NoEnoughSpace, "out of limited space");
		}
		return -1;
	}

	template < class T > inline bool marshal_trivial_array(ConstArray<T> const& val, char S, IOs* st)
	{
		return 1
		       && st->Write(S)
		       && st->Write32(val.Count())
		       && st->Write(+val, val.Count())
		       ;
	}

	template < class T > bool unmarshal_trivial_array(T& val, char S, IOs* st)
	{
		Array<typename T::ContType> r;
		int count = unmarshal_count_of(S, sizeof(typename T::ContType), st);
		if (count >= 0)
		{
			r.Resize(count);
			if (st->Read(+r, count))
			{
				val = T(r, __take_data__);
				return true;
			}
		}
		return false;
	}

	template < class T > struct Marshal_OneByteArray
	{
		inline bool Out(T const& val, IOs* st)
		{ marshal_trivial_array(val, 'S', st); }
		inline bool In(T& val, IOs* st)
		{ unmarshal_trivial_array(val, 'S', st); }
	};

	template < class T > struct Marshal_Array
	{
		inline bool Out(T const& val, IOs* st)
		{
			if (!st->Write('A') || !st->Write32(val.Count())) return false;
			for (int i = 0; i < val.Count(); ++i)
				if (!marshal(val[i], st)) return false;
			return true;
		}

		inline bool In(T& val, IOs* st)
		{
			Array<typename T::ContType> r;
			int count = unmarshal_count_of('A', sizeof(typename T::ContType), st);
			if (count < 0) return false;

			r.Resize(count);
			for (int i = 0; i < count; ++i)
				if (!unmarshal(r[i], st)) return false;

			val = T(r, __take_data__);
			return true;
		}
	};

	template < class T > struct Marshal < ConstArray<T> > : Marshal_Array< ConstArray<T> > {};
	template < class T > struct Marshal < Array<T> > : Marshal_Array< Array<T> > {};

	template <> struct Marshal < ConstArray<byte_t> > : Marshal_OneByteArray< ConstArray<byte_t> > {};
	template <> struct Marshal < ConstArray<char> > : Marshal_OneByteArray< ConstArray<char> > {};
	template <> struct Marshal < Array<byte_t> > : Marshal_OneByteArray< Array<byte_t> > {};
	template <> struct Marshal < Array<char> > : Marshal_OneByteArray< Array<char> > {};
	template <> struct Marshal < String > : Marshal_OneByteArray< String > {};
	template <> struct Marshal < Chars > : Marshal_OneByteArray< Chars > {};

	inline bool marshal_strarg(Strarg a, IOs* st)
	{
		return 1
		       && st->Write('S')
		       && st->Write32(a.Length())
		       && st->Write(+a, a.Length())
	}

	template <> struct Marshal<Strarg_>
	{
		inline bool Out(Strarg_ const& val, IOs* st)
		{ marshal_strarg(val, st); }
	};
}

