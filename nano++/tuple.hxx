
/*

(C)2010-2011, Alexéy Sudáchen, alexey@sudachen.name

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.

*/

#pragma once
#include "deftypes.hxx"

namespace nano
{

	template < class t1, class t2, class t3 >
	struct tuple3_t
	{
		t1 _1;
		t2 _2;
		t3 _3;
		tuple3_t(t1 const& a, t2 const& b, t3 const& c)
			: _1(a), _2(b), _3(c) {}
		tuple3_t() : _1(t1()), _2(t2()), _3(t3()) {}
	};

	template < class t1, class t2, class t3 >
	tuple3_t<t1, t2, t3> tuple3(t1 const& a, t2 const& b, t3 const& c)
	{ return tuple3_t<t1, t2, t3>(a, b, c); }

	template < class t1, class t2, class t3, class t4 >
	struct tuple4_t
	{
		t1 _1;
		t2 _2;
		t3 _3;
		t4 _4;
		tuple4_t(t1 const& a, t2 const& b, t3 const& c, t4 const& d)
			: _1(a), _2(b), _3(c), _4(d) {}
		tuple4_t() : _1(t1()), _2(t2()), _3(t3()), _4(t4())  {}
	};

	template < class t1, class t2, class t3, class t4 >
	tuple4_t<t1, t2, t3, t4> tuple4(t1 const& a, t2 const& b, t3 const& c, t4 const& d)
	{ return tuple4_t<t1, t2, t3, t4>(a, b, c, d); }
}

